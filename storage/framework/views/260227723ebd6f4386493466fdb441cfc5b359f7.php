<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Contact Us</h1>-->
<!--<h2>How Can We Help You</h2>-->
<!--<h3>Find An Agent Or An Office</h3>-->
        <div class="contact">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title">CONTACT US</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO LOCATE THE NEAREST WISEBERRY OFFICE.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="office-search">LET&apos;S GO</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


<!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0 individual-rental individual-agent-one individual-office notice-repair-section contact">
            <div class="container container1">

                
                <div class="row">

                    <div class="col-lg-9 col-md-8 col-xs-12">
                      <?php if(\Session::has('success')): ?>
                          <div class="alert alert-success" style="text-align: center;">
                              <?php echo \Session::get('success'); ?>

                          </div>
                      <?php else: ?>
                        <div class="details">

                            <h1 class="title"><?php echo e($data->heading); ?></h1>
                            <?php echo $data->desc; ?>


                    <div class="row">
                      
                          <div class="col-md-12">
                              
                              <form action="sendmail_contact" id="capt_form" method="POST">
                              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                      <div class="row">
                        
                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" name="fname" placeholder="First Name*">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="text" class="form-control" name="lname" placeholder="Last Name*">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="tel" class="form-control" name="contact" placeholder="Contact Number*">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email Address*">
                          </div>
                        </div>


                        <div class="col-md-12">
                          <div class="form-group">
                              <textarea class="form-control" name="enquiry" placeholder="Enter Your Enquiry Here*"></textarea>
                          </div>
                        </div>
                        
                        <div class="col-md-12">
                            
                            <label>
                              <input type="checkbox" name="check">
                              <span>I have read and agree to the Privacy Statement. *</span>
                            </label>

                        </div>
                        
                        <div class="col-md-12 recapthcha-section">
                          <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                        </div>

                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-primary see-more font-weight-bold">LET &apos; S GO!</button>
                        </div>

                      </div>
                   
                    
                  </form>
                  </div>
                  
                  </div>
                  </div>

                  <?php endif; ?>

                    </div>
                    <div class="col-lg-3 col-md-4 car">
                        
                        <div class="side-bar">

                            <h1 class="title">OFFICE DETAILS</h1>
                            <?php echo $data->address; ?>

                            
                            <ul class="button-list">
                                <li>
                                    <a href="open-home">SALE OPEN HOMES</a>
                                </li>
                                 <li>
                                    <a href="rent-open-home">RENTAL OPEN HOMES</a>
                                </li>
                                 <li>
                                    <a href="appraisal">REQUEST A SALE APPRAISAL</a>
                                </li>
                                 <li>
                                    <a href="rental-appraisal">REQUEST A RENTAL APPRAISAL</a>
                                </li>
                                 <li>
                                    <a href="office-search">WISEBERRY OFFICES</a>
                                </li>
                            </ul>

                            

                        </div>

                    </div>
                </div>
            </div>
        </section>
        </div>



      

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/contact_us.blade.php ENDPATH**/ ?>