<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style type="text/css">
	.hidetab{
		display: none;
	}
	.hidetab_1{
		display: none;
	}
	.hidetab_2{
		display: none;
	}
	.hidetab_3{
		display: none;
	}
	.hidetab_4{
		display: none;
	}
    #subscribe_ind {
        background-color: #830b2c !important;
        color: #ffffff;
        border: none;
        padding: 7px 35px !important;
    }
</style>

<div class="office-home">
    <div class="clearfix"></div>
        <?php if(isset($result['display_image']) && $result['display_image'] !== null){ ?>
            <section class="header-image home-18 d-flex align-items-center individual-office" id="slider"  style="background-image: url('<?php echo e($result['display_image']); ?>');">
                <div class="container">
                </div>
            </section>
        <?php }else{ ?>
            <section class="header-image home-18 d-flex align-items-center individual-office" id="slider"  style="background-image: url('../images/offices-banner.jpg');">
                <div class="container">
                </div>
            </section>
        <?php } ?>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>II WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="../appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

        <!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0 individual-rental individual-agent-one individual-office">
            <div class="container container1">

                <div class="row">
                    <div class="sec-title" style="width: 100%;">
                    <h2 style="text-align: center;"><?php echo e($result['page_title'] ?? ''); ?></h2>
                </div>
                </div>

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-xs-12">

                        <div class="details">

                            <h1 class="title"><?php echo e($result['sub_title'] ?? ''); ?></h1>
                            <p><?php echo $result['content'] ?? ''; ?></p>

                            <?php if(isset($result['awards']) && $result['awards'] !== null){ ?>
                                <h1 class="title" style="margin-top: 2em;">AWARDS</h1>
                                <?php echo $result['awards'] ?? ''; ?>

                            <?php } ?>
                            
                            <?php if(isset($result['opening_hours']) && !empty($result['opening_hours'])){ ?>
                                <h1 class="title">OPENING HOURS</h1>
                                <div class="row">
                                    <?php 
                                        foreach ($result['opening_hours'] as $key => $value) {
                                            if($value['open'] != 0){ 
                                    ?>
                                        <div class="col-md-3">
                                            <div class="hours">
                                                <h5><?php echo e($value['day'] ?? ''); ?></h5>
                                                <p><?php echo e($value['from'] ?? ''); ?> - <?php echo e($value['to'] ?? ''); ?></p>
                                            </div>
                                        </div>
                                    <?php }} ?>
                                
                                </div>
                            <?php } ?>

                        </div>

                    </div>
                    <div class="col-lg-4 col-md-4 car">
                        
                        <div class="side-bar">

                            <h1 class="title">OFFICE DETAILS</h1>
                            <input type="hidden" id="ind_org_idd" value="<?php echo e($result['id'] ?? ''); ?>">
                            <h2 class="subtitle"><?php echo e($result['name'] ?? ''); ?></h2>
                            <p><?php echo e($result['address']['full_address'] ?? ''); ?></p>
                            <ul class="mail">
                                <li>
                                	<?php foreach ($result['phones'] as $key => $value) { ?>
                                    	<a href="tel:<?php echo e($value['value']); ?>"><img src="../images/phone.png"><?php echo e($value['value']); ?></a>
                                	<?php } ?>
                                </li>
                                <li>
                                    <?php foreach ($result['emails'] as $keys => $values) { ?>
                                    	<a href="mailto:<?php echo e($values['value']); ?>"><img src="../images/mail.png"><?php echo e($values['value']); ?></a>
                                	<?php } ?>
                                </li>
                            </ul>
                            
                            <ul class="button-list">
                                <?php if(isset($result['btn_auctions']) && $result['btn_auctions'] != 0){ ?>
                                    <li>
                                    	<a href="../auction" type="button" class="btn btn-primary" >AUCTIONS
                                        </a>
                                        
                                    </li>
                                <?php } if(isset($result['btn_sales_open_home']) && $result['btn_sales_open_home'] != 0){ ?>
                                    <li>
                                        <a href="../open-home" type="button" class="btn btn-primary" >OPEN HOMES
                                        </a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="../appraisal">REQUEST A SALE APPRAISAL</a>
                                </li>
                            </ul>

                            
                                <label>SUBSCRIBE NOW</label>
                                <input type="email" name="" id="subscribe_ind_id" class="form-control" placeholder="Enter an email address">
                                <button id="subscribe_ind" data-toggle="modal" data-target="#myModal_s">Subscribe</button>
                            
                            <ul class="social">
                                <li>
                                    <a href="<?php echo e($result['facebook'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,97.5C784,97.5,982.5,296,982.5,540c0,244-198.5,442.5-442.5,442.5S97.5,784,97.5,540
    C97.5,296,296,97.5,540,97.5z M540,9C246.7,9,9,246.7,9,540c0,293.2,237.8,531,531,531s531-237.8,531-531C1071,246.7,833.3,9,540,9z
     M451.5,451.5H363V540h88.5v265.5h132.8V540h80.5l8-88.5h-88.5v-36.9c0-21.2,4.2-29.5,24.7-29.5h63.8V274.5H566.4
    c-79.6,0-114.9,35-114.9,102.1L451.5,451.5L451.5,451.5z"/>
</svg><!--<img src="../images/facebook.png"> <i class="fa fa-facebook" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['instagram'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.6c244.5,0,443.4,198.9,443.4,443.4c0,244.5-198.9,443.4-443.4,443.4S96.6,784.5,96.6,540
    C96.6,295.5,295.5,96.6,540,96.6z M540,7.9C246.2,7.9,7.9,246.2,7.9,540c0,293.8,238.2,532.1,532.1,532.1s532.1-238.2,532.1-532.1
    C1072.1,246.2,833.8,7.9,540,7.9z M540,321.9c71,0,79.5,0.3,107.6,1.6c72.1,3.3,105.7,37.5,109,109c1.3,28.1,1.6,36.4,1.6,107.5
    c0,71-0.3,79.5-1.6,107.5c-3.3,71.5-36.9,105.8-109,109.1c-28.1,1.2-36.5,1.5-107.6,1.5c-71,0-79.4-0.3-107.5-1.6
    c-72.3-3.3-105.7-37.6-109-109.1c-1.3-28-1.6-36.5-1.6-107.5c0-71,0.3-79.5,1.6-107.5c3.2-71.6,36.9-105.8,109-109.1
    C460.6,322.2,469,321.9,540,321.9z M540,274c-72.2,0-81.2,0.3-109.7,1.6c-96.6,4.4-150.3,58-154.7,154.7
    c-1.3,28.4-1.6,37.5-1.6,109.7c0,72.3,0.4,81.3,1.6,109.7c4.4,96.6,58.1,150.3,154.7,154.7c28.5,1.3,37.5,1.6,109.7,1.6
    c72.3,0,81.3-0.3,109.7-1.6c96.5-4.4,150.4-58,154.8-154.7c1.3-28.4,1.6-37.5,1.6-109.7c0-72.2-0.3-81.3-1.6-109.7
    c-4.3-96.5-58.1-150.3-154.8-154.7C621.3,274.3,612.3,274,540,274z M540,403.4c-75.4,0-136.6,61.1-136.6,136.6
    S464.6,676.6,540,676.6S676.7,615.5,676.7,540C676.7,464.6,615.4,403.4,540,403.4z M540,628.7c-49,0-88.7-39.7-88.7-88.7
    c0-49,39.7-88.7,88.7-88.7c48.9,0,88.8,39.7,88.8,88.7C628.8,589,588.9,628.7,540,628.7z M682,366.1c-17.7,0-32,14.3-32,31.9
    c0,17.6,14.3,31.9,32,31.9c17.6,0,32-14.3,32-31.9C714,380.4,699.7,366.1,682,366.1z"/>
</svg><!--<img src="../images/insta.png"> <i class="fab fa-instagram"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['linkedin'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,93.1c246.4,0,446.9,200.5,446.9,446.9c0,246.4-200.5,446.9-446.9,446.9C293.6,986.9,93.1,786.4,93.1,540
    C93.1,293.6,293.6,93.1,540,93.1z M540,3.8C243.9,3.8,3.8,243.9,3.8,540s240.1,536.2,536.2,536.2s536.2-240.1,536.2-536.2
    S836.1,3.8,540,3.8z M450.6,361.3c0,24.9-19.9,45-44.7,45c-24.7,0-44.7-20.1-44.7-45c0-24.9,20-45,44.7-45
    C430.7,316.2,450.6,336.4,450.6,361.3z M450.6,450.6h-89.4v268.1h89.4V450.6z M584.7,450.6h-89.4v268.1h89.4V590.9
    c0-76.9,89.5-84,89.5,0v127.9h89.3V568.6c0-146.7-139.7-141.4-178.7-69.2L584.7,450.6L584.7,450.6z"/>
</svg><!--<img src="../images/linkedin.png"> <i class="fa fa-linkedin" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['youtube'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M376.5,273h32.1l22,82.4l20.4-82.4h32.3l-37,122.5V479h-31.9v-83.6L376.5,273z M523.2,325.8
    c-24.9,0-41.5,16.5-41.5,40.8v74.5c0,26.8,14,40.8,41.5,40.8c22.7,0,40.6-15.2,40.6-40.8v-74.5C563.9,342.8,546.2,325.8,523.2,325.8
    z M535,439.9c0,8.3-4.2,14.4-11.8,14.4c-7.8,0-12.3-6.4-12.3-14.4v-70.7c0-8.3,3.8-14.5,11.8-14.5c8.6,0,12.3,6,12.3,14.5V439.9z
     M640.2,327.1v115.4c-3.4,4.3-11.1,11.4-16.6,11.4c-6,0-7.5-4.1-7.5-10.2V327.1h-28.2v127c0,15,4.6,27.2,19.7,27.2
    c8.6,0,20.4-4.5,32.7-19v16.8h28.2v-152H640.2z M689.2,635.4c-10,0-12,7-12,17v14.7H701v-14.7C701,642.6,699,635.4,689.2,635.4z
     M584.6,636.3l-5.6,4.4v90.2l6.4,5.1c4.4,2.2,10.8,2.4,13.8-1.5c1.5-2,2.3-5.4,2.3-10v-74.8c0-4.9-0.9-8.6-2.8-11.1
    C595.5,634.4,589.4,633.9,584.6,636.3z M692,520.7c-57.9-4-246.2-3.9-304.1,0c-62.6,4.3-69.9,42.2-70.4,141.7
    c0.5,99.3,7.8,137.4,70.4,141.7c57.8,3.9,246.2,4,304.1,0c62.6-4.3,70-42.1,70.5-141.7C762,563,754.7,525,692,520.7z M417.8,758.5
    h-30.3V590.8h-31.4v-28.5h93v28.5h-31.4V758.5z M525.7,758.5h-26.9v-16c-5,5.9-10.1,10.4-15.5,13.5c-14.5,8.3-34.4,8.1-34.4-21.2
    v-121h26.9v111c0,5.8,1.4,9.7,7.1,9.7c5.3,0,12.6-6.7,15.8-10.9V613.7h26.9L525.7,758.5L525.7,758.5z M629.3,728.4
    c0,18-6.7,31.9-24.6,31.9c-9.9,0-18.1-3.6-25.6-13v11.1h-27.2V562.3h27.2v63.2c6.1-7.4,14.3-13.5,24-13.5
    c19.7,0,26.2,16.6,26.2,36.3V728.4z M728.7,689.5h-51.5v27.3c0,10.9,1,20.2,11.8,20.2c11.3,0,12-7.7,12-20.2v-10h27.7v10.9
    c0,27.9-12,44.8-40.3,44.8c-25.7,0-38.8-18.7-38.8-44.8v-65c0-25.2,16.6-42.6,40.9-42.6c25.8,0,38.3,16.4,38.3,42.6L728.7,689.5
    L728.7,689.5z M540,95c245.4,0,445,199.6,445,445c0,245.4-199.6,445-445,445S95,785.4,95,540C95,294.6,294.6,95,540,95z M540,6
    C245.1,6,6,245.1,6,540s239.1,534,534,534s534-239.1,534-534S834.9,6,540,6z"/>
</svg><!--<img src="../images/you-tube.png"> <i class="fa fa-youtube" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['twitter'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.7c244.4,0,443.3,198.9,443.3,443.3c0,244.4-198.9,443.3-443.3,443.3S96.7,784.4,96.7,540
    C96.7,295.5,295.6,96.7,540,96.7z M540,8C246.2,8,8,246.2,8,540c0,293.8,238.2,532,532,532c293.8,0,532-238.2,532-532
    C1072,246.2,833.8,8,540,8z M828.2,397.1c-19.6,8.7-40.6,14.5-62.7,17.2c22.6-13.5,39.8-34.9,48-60.4
    c-21.1,12.5-44.5,21.6-69.3,26.5c-19.9-21.2-48.3-34.5-79.7-34.5c-70.5,0-122.3,65.7-106.3,134c-90.6-4.6-171.1-48-225-114
    c-28.6,49-14.8,113.2,33.8,145.7c-17.8-0.6-34.6-5.5-49.4-13.7c-1.2,50.5,35.1,97.8,87.5,108.4c-15.3,4.2-32.2,5.2-49.3,1.9
    c13.9,43.3,54.2,74.8,101.9,75.8c-46,36-103.7,52.1-161.7,45.2c48.3,31,105.6,49,167.3,49c202.7,0,317.1-171.1,310.2-324.7
    C795,438.3,813.5,419.1,828.2,397.1z"/>
</svg><!--<img src="../images/twitter.png"> <i class="fa fa-twitter" aria-hidden="true"></i> --></a>
                                </li>
                            </ul>

                            <h1 class="title" style="margin-top: 1em;">WISEBERRY POSTS</h1>


<section role="complementary" class="simple white-back quotes no-fouc">
  <blockquote>
    <div class="post">
        <div class="avatar">
            <img src="../images/post.jpg">
        </div>
        <div class="content">
            <div class="row">
            <div class="col-md-6">
                <p class="date name">J.SMITH</p>
            </div>
            <div class="col-md-6">
                <p class="date text-right">SAT 12 FEB</p>
            </div>
        </div>
            <p>The latest issue of Wise Move is out now!</p>
        <p>Visit: <a href="#">wiseberry.com.au/wisemove</a></p>
        
        </div>
    </div>
</blockquote>
  <blockquote>
    <div class="post">
        <div class="avatar">
            <img src="../images/post.jpg">
        </div>
        <div class="content">
            <div class="row">
            <div class="col-md-6">
                <p class="date name">J.SMITH</p>
            </div>
            <div class="col-md-6">
                <p class="date text-right">SAT 12 FEB</p>
            </div>
        </div>
            <p>The latest issue of Wise Move is out now!</p>
        <p>Visit: <a href="#">wiseberry.com.au/wisemove</a></p>
        
        </div>
    </div>
</blockquote>
</section>



                            
                            
                            
                        </div>

                    </div>
                </div>
            </div>
        </section>


        <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 client-testimonial individual-office">
            <div>
                <div class="sec-title">
                    <h2 class="pt1">CLIENT TESTIMONIALS</h2>
                </div>



    <section class="testimonial text-center">
        <div class="container">

            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
             
                <div class="carousel-inner" role="listbox">
                    <?php   if(!empty($result_test)){ 
                            $i=0;
                            foreach ($result_test as $keyt => $valuet){ 

                            	if($i==0){
                            		$act="active";
                            	}else{
                            		$act="";
                            	}
                    ?>
                    <div class="carousel-item <?php echo e($act); ?>">
                        <div class="testimonial4_slide">
                            <h5><?php echo e($valuet['heading']); ?></h5>
                            <p><?php echo e($valuet['value']); ?></p>
                            <h4><?php echo e($valuet['created_by']); ?></h4>
                        </div>
                    </div>
                    <?php $i++;  } } ?>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </section>


            </div>
        </section>
        <!-- END SECTION -->


        <!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0 individual-rental individual-office-team">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-5 text-center">
                        <div class="sec-title">
                            <h2>MEET THE TEAM</h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <form class="example" method="POST" action="../agent-search-results">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" name="search_type" id="search_type" value="">
                            <input type="hidden" name="pagename" id="pagename" value="office_agent">
                          <input type="text" id="tags" name="agent_name" placeholder="Search by agent name, contact number">
                          <input type="hidden" name="agent" id="tags_val" >
                          <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">

                        <div class="details">


                                <div class="row team-all">
                                	<?php
				                        $m=0;
				                        foreach ($result_agent as $keya => $valuea){

				                        	if($m <= 3){
			                            		$cls_4="showtab_4";
			                            	}else{
			                            		$cls_4="hidetab_4";
			                            	}
				                            
				                    ?>
                                    <div class="col-lg-3 col-md-3 team-pro <?php echo e($cls_4); ?>">
                                        <div class="team-wrap">
                                            <div class="team-img img-box">
                                                <?php if(isset($valuea['contact']['profile_picture']) && $valuea['contact']['profile_picture'] !== null){ ?>
                                                    <img src="<?php echo e($valuea['contact']['profile_picture']); ?>" alt="" />
                                                <?php } else { ?>
                                                    <img src="../images/offices-team1.jpg" alt="" />
                                                <?php } ?>
                                            </div>
                                            <div class="team-content-persons">
                                                <div class="team-info">
                                                    <h3><?php echo e($valuea['contact']['first_name']  ?? ''); ?> <?php echo e($valuea['contact']['last_name']  ?? ''); ?></h3>
                                                    <p><?php echo e($valuea['position']  ?? ''); ?></p>
                                                    <p><?php echo e($valuea['organisation']['name']  ?? ''); ?></p>
                                                    <p><?php echo e($valuea['contact']['phones'][0]['value']  ?? ''); ?></p>
                                                    <div class="team-socials">
                                                        <ul>
                                                            <li>
				                                                <a href="mailto:<?php echo e($valuea['contact']['emails'][0]['value']  ?? ''); ?>" title="mail"><img src="../images/agent-icon1.png"></a>
				                                                <a href="../downloadcard/<?php echo e($valuea['contact']['first_name']); ?>_<?php echo e($valuea['contact']['phones'][0]['value']  ?? ''); ?>" title="twitter"><img src="../images/agent-icon2.png"></a>
				                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $m++;  } ?>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-md-12 text-center mt2">
                                        <a class="read-more seemore_agent">SEE MORE</a>
                                    </div>
                                </div> -->

                        </div>

                    </div>
                    
                </div>
            </div>
        </section>

        <!-- INDIVIDUAL SECTION END -->



<section class="individual-agent-three mt0">
          <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                                    <div class="banner-search-wrap">
                                        <ul class="nav nav-tabs rld-banner-tab text-center">
                                            <?php if(!empty($result_sale)){ ?> 
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#tabs_1">FOR SALE</a>
                                                </li>
                                            <?php } if(!empty($result_sold)){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs_2">SOLD</a>
                                                </li>
                                            <?php } if(!empty($result_rent)){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs_3">RENT</a>
                                                </li>
                                            <?php } if(!empty($result_lease)){ ?>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabs_4">LEASED</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tabs_1">
                                                <!-- START Buy Result -->

        <section class="recently portfolio bg-white-1 home18 buy-result">
            <?php if(!empty($result_sale)){ ?>
            <div>
                <div class="row">
                    
                    <?php    
                            $i=0;
                            foreach ($result_sale as $key => $value){ 
                            	if($i <= 2){
                            		$cls="showtab";
                            	}else{
                            		$cls="hidetab";
                            	}
                    ?>
                    <div class="col-md-4 <?php echo e($cls); ?>">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="individual-result/<?php echo e($value['id']); ?>" class="homes-img img-box hover-effect">
                                                <?php if(isset($value['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($value['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($value['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($value['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom'|| $values['name'] == 'Parking'){
                                                    if($values['name'] == 'Bedroom'){
                                                        $img="../images/bed.png";
                                                    }
                                                    if($values['name'] == 'Bathroom'){
                                                        $img="../images/bath.png";
                                                    }

                                                    if($values['name'] == 'Parking'){
                                                        $img="../images/car.png";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($values['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;   } ?>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more seemore_sale" style="margin-top: 2em; display: inline-block;">SEE MORE</a>
                    </div>
                </div>

            </div>
        	<?php  } ?>

        </section>
    </div>
    <div class="tab-pane fade" id="tabs_2">
        <section class="recently portfolio bg-white-1 home18 buy-result">
            <?php if(!empty($result_sold)){  ?>
            <div>
                <div class="row">
                    
                    <?php   
                            $j=0;
                            foreach ($result_sold as $keys => $values){ 

                            	if($j <= 2){
                            		$cls_1="showtab_1";
                            	}else{
                            		$cls_1="hidetab_1";
                            	}
                    ?>
                    <div class="col-md-4 <?php echo e($cls_1); ?>">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#" class="homes-img img-box hover-effect">
                                                <?php if(isset($values['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($values['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($values['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($values['property']['features'] as $keyy => $valuey){
                                                if($valuey['name'] == 'Bedroom' || $valuey['name'] == 'Bathroom'|| $valuey['name'] == 'Parking'){
                                                    if($valuey['name'] == 'Bedroom'){
                                                        $img="../images/bed.png";
                                                    }
                                                    if($valuey['name'] == 'Bathroom'){
                                                        $img="../images/bath.png";
                                                    }

                                                    if($valuey['name'] == 'Parking'){
                                                        $img="../images/car.png";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($valuey['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $j++;  } ?>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more seemore_sold" style="margin-top: 2em; display: inline-block;">SEE MORE</a>
                    </div>
                </div>

            </div>
        	<?php } ?>

        </section>
    </div>
    <div class="tab-pane fade" id="tabs_3">
        <section class="recently portfolio bg-white-1 home18 buy-result">
        	<?php if(!empty($result_rent)){  ?>
            <div>
                <div class="row">
                    <?php   
                            $k=0;
                            foreach ($result_rent as $keys => $values){ 

                            	if($k <= 2){
                            		$cls_2="showtab_2";
                            	}else{
                            		$cls_2="hidetab_2";
                            	}
                    ?>
                    <div class="col-md-4 <?php echo e($cls_2); ?>">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#" class="homes-img img-box hover-effect">
                                                <?php if(isset($values['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($values['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($values['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($values['property']['features'] as $keyy => $valuey){
                                                if($valuey['name'] == 'Bedroom' || $valuey['name'] == 'Bathroom'|| $valuey['name'] == 'Parking'){
                                                    if($valuey['name'] == 'Bedroom'){
                                                        $img="../images/bed.png";
                                                    }
                                                    if($valuey['name'] == 'Bathroom'){
                                                        $img="../images/bath.png";
                                                    }

                                                    if($valuey['name'] == 'Parking'){
                                                        $img="../images/car.png";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($valuey['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $k++;  } ?>

                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more seemore_rent" style="margin-top: 2em; display: inline-block;">SEE MORE</a>
                    </div>
                </div>

            </div>
            <?php } ?>

        </section>
    </div>
    <div class="tab-pane fade" id="tabs_4">
        <section class="recently portfolio bg-white-1 home18 buy-result">
            <?php if(!empty($result_lease)){  ?>

            <div>
                <div class="row">
                    <?php   
                            $l=0;
                            foreach ($result_lease as $keys => $values){ 

                            	if($l <= 2){
                            		$cls_3="showtab_3";
                            	}else{
                            		$cls_3="hidetab_3";
                            	}
                    ?>
                    <div class="col-md-4 <?php echo e($cls_3); ?>">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#" class="homes-img img-box hover-effect">
                                                <?php if(isset($values['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($values['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($values['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($values['property']['features'] as $keyy => $valuey){
                                                if($valuey['name'] == 'Bedroom' || $valuey['name'] == 'Bathroom'|| $valuey['name'] == 'Parking'){
                                                    if($valuey['name'] == 'Bedroom'){
                                                        $img="../images/bed.png";
                                                    }
                                                    if($valuey['name'] == 'Bathroom'){
                                                        $img="../images/bath.png";
                                                    }

                                                    if($valuey['name'] == 'Parking'){
                                                        $img="../images/car.png";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($valuey['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $l++;  } ?>

                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more seemore_lease" style="margin-top: 2em; display: inline-block;">SEE MORE</a>
                    </div>
                </div>

            </div>
            <?php } ?>

        </section>

        <!-- END Buy Result -->
                                            </div>
                                        </div>
                                    </div>
             </div>
          </div>
          </div>
        </section>


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner individual-office">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>JOIN US</h2>
                            <h3>WANT TO WORK WITH US?</h3>
                            <p>Are you seeking a challenging and rewarding career?</p>
                            <p>We’re always on the lookout for great people to work with us.
We see our agents as being part of a large family and we
expect them to treat their clients in the same way; as if they
were part of their family.</p>
                            <a href="../join-us">LEARN MORE</a>
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="../images/owner.jpg">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


        <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 individual-office">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="../individual_blog/<?php echo e($value->id); ?>" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="<?php echo e($img_path); ?><?php echo e($value->featured_image); ?>" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>Buyers</h6>
                                        <a href="../individual_blog/<?php echo e($value->id); ?>"><h3><?php echo e($value->name); ?></h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="../blogs" class="read-more">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
</div>

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
	$(".seemore_sale").click(function(){
		$(".hidetab").show();
	});
	$(".seemore_sold").click(function(){
		$(".hidetab_1").show();
	});
	$(".seemore_rent").click(function(){
		$(".hidetab_2").show();
	});
	$(".seemore_lease").click(function(){
		$(".hidetab_3").show();
	});
	$(".seemore_agent").click(function(){
		$(".hidetab_4").show();
	});
</script><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/office_home.blade.php ENDPATH**/ ?>