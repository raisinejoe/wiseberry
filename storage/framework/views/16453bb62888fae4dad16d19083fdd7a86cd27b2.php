<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    
    <div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Private Rental Inspections</h2>-->
        <!-- STAR HEADER IMAGE -->
        <div class="rent-open-home">
            <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-landing">
                                 
                                 <h1 style="margin-left: 15px;">OPEN FOR INSPECTIONS</h1>
                                 <div class="inner">
                                    <form method="POST" action="rent-open-home-results">
                                    
                                    <div class="row">
                                        <div class="col-md-2 pr-0">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="hidden" name="pagename" id="pagename" value="rent">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="txt" id="datepic" name="datepic" placeholder="Date" class="form-control date" autocomplete="off">
                                            <input type="txt" id="datepic2" placeholder="Date"  style=" border-right: 1px solid;visibility: hidden;position: absolute;" autocomplete="off">
                                        </div>
                                        <div class="col-md-2 pr-0 pl-0">
                                            <select class="form-control" name="page_option_open" id="page_option_open">
                                                <option <?php if ($option == '1') echo ' selected="selected"'; ?> value="1">Residential Sales</option>
                                                <option <?php if ($option == '2') echo ' selected="selected"'; ?> value="2">Rural Sales</option>
                                                <option <?php if ($option == '3') echo ' selected="selected"'; ?> value="3">Commercial Sales</option>
                                                <option <?php if ($option == '4') echo ' selected="selected"'; ?> value="4" >Residential Rental</option>
                                                <option <?php if ($option == '5') echo ' selected="selected"'; ?> value="5">Rural Rental </option>
                                                <option <?php if ($option == '6') echo ' selected="selected"'; ?> value="6">Commercial Rental</option>
                                                <option <?php if ($option == '7') echo ' selected="selected"'; ?> value="7">Holiday Rental</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5 pl-0">
                                            <input type="txt" id="tags" name="suburb_name" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <select class="form-control"  id="buy_options" name="option">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option value="1">1 Bed</option>
                                                                    <option value="2">2 Bed</option>
                                                                    <option value="3">3 Bed</option>
                                                                    <option value="4">4 Bed</option>
                                                                    <option value="5">5 Bed</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option value="1">1 Bath</option>
                                                                    <option value="2">2 Bath</option>
                                                                    <option value="3">3 Bath</option>
                                                                    <option value="4">4 Bath</option>
                                                                    <option value="5">5 Bath</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option value="1">1 Car</option>
                                                                    <option value="2">2 Car</option>
                                                                    <option value="3">3 Car</option>
                                                                    <option value="4">4 Car</option>
                                                                    <option value="5">5 Car</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Weekly Min $</option>
                                                                    <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1000">$1,000</option>
                          <option value="1250">$1,250</option>
                          <option value="150">$1,500</option>
                          <option value="2000">$2,000</option>
                          <option value="3000">$3,000</option>
                          <option value="4000">$4,000</option>
                          <option value="5000">$5,000</option>
                          <option value="10000">$10,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Weekly Max $</option>
                                                                    <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1000">$1,000</option>
                          <option value="1250">$1,250</option>
                          <option value="150">$1,500</option>
                          <option value="2000">$2,000</option>
                          <option value="3000">$3,000</option>
                          <option value="4000">$4,000</option>
                          <option value="5000">$5,000</option>
                          <option value="10000">$10,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <!-- <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div> -->
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa1 fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>LET US HELP YOU FIND THE RIGHT RENTAL PROPERTY.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="rent">LET’S GO</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


<section class="buy-landing-page">


    <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy m-0 ptb6">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>OPEN FOR INSPECTIONS</h2>
                            <p>To assist you in making the right decision and avoiding issues down the track, it is important to attend inspections. This will allow you to inspect the property yourself and ask the agent questions you may have.</p>

<p>Open Homes are held at various times throughout the week, where you are able to inspect and check out not only the house, but also the neighbourhood.</p>
                            <a href="blogs">FIND OUT MORE</a>
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="images/open.jpg">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy">
         <div class="container">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p2->image); ?>">
                        </div>
                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p2->heading); ?></h2>
                            <?php echo $p2->desc; ?>

                        </div>
                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

    
    <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18" style="padding: 8rem 0 8rem;">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/<?php echo e($value->id); ?>" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="<?php echo e($img_path); ?><?php echo e($value->featured_image); ?>" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>RENT</h6>
                                        <a href="individual_blog/<?php echo e($value->id); ?>"><h3><?php echo e($value->name); ?></h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                     <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="#" class="read-more">SEE MORE</a>
                        </div>
                    </div> 

                </div>
            </div>
        </section>
        <!-- END SECTION BLOG -->
        </div>
        

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- END SECTION WHY WISEBERRY -->

       
       <!-- START SUBSCRIBE NOW -->

       
<?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/rent_open_home.blade.php ENDPATH**/ ?>