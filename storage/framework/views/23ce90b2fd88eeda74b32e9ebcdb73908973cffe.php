<body class="th-18">
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- START SECTION HEADINGS -->
        <!-- Header Container
        ================================================== -->
        <header id="header-container" class="header head-tr">
            <!-- Header -->
            <div id="header" class="head-tr bottom">
                <div class="container">
                    <!-- Left Side Content -->
                    <div class="left-side">
                        <div class="row">
                            
                            <!-- Logo -->
                        <div class="col-lg-3 logo-section">
                            <div id="logo">
                            <a href="homepage"><img src="<?php echo e(asset('images/logo.png')); ?>" data-sticky-logo="images/logo-dark-orange.svg" alt=""></a>
                        </div>
                        </div>
                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                        <?php 
                            // echo "<pre>"; print_r(getmenu()); echo "</pre>"; die; 
                            $menu = getmenu();
                            $menus=$menu->menus;
                            // echo "<pre>"; print_r($menus); echo "</pre>"; die; 
                        ?>
                        <div class="col-lg-9">
                            <!-- Main Navigation -->
                        <nav id="navigation" class="style-1 head-tr">
                            <ul id="responsive">
                               <?php foreach($menus as $key => $value){ ?>
                                   <li>
                                      <?php if(isset($value->link)){ ?>
                                        <a href="<?php echo e($value->link); ?>"><?php echo e($value->title); ?></a>
                                      <?php }else{ ?>
                                        <a href="#"><?php echo e($value->title); ?></a>
                                      <?php } ?>

                                       <?php if(isset($value->submenu)){ ?>
                                       <ul>
                                       <?php
                                          foreach($value->submenu as $keys => $values){ 
                                            if($values->title != 'Blogs' && $values->title != 'Our Offices' && $values->title != 'Our Agents'&& $values->title != 'Contact Us'){
                                       ?>
                                            <li><a href="<?php echo e($values->link); ?>"><?php echo e($values->title); ?></a></li>
                                       <?php }} ?>
                                    </ul>
                                    <?php } ?>  
                                   </li>
                                <?php } ?>
                               <!--<li><a href="#">Rent</a></li>-->
                               <!--<li><a href="#">Sell</a></li>-->
                               <!--<li><a href="#">Manage</a></li>-->
                               <!--<li><a href="#">Our Agents</a></li>-->
                               <!--<li><a href="#">Our Offices</a></li>-->
                               <!--<li><a href="#">Blog</a></li>-->
                               <!--<li><a href="#">About Us</a></li>-->
                               <!--<li><a href="#">Contact Us</a></li>-->
                                    
                            </ul>
                        </nav>
                        <!-- Main Navigation / End -->
                        </div>

                        </div>
                    </div>
                    <!-- Left Side Content / End -->

                  



                  

                </div>
            </div>
            <!-- Header / End -->

        </header><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/layout/menu.blade.php ENDPATH**/ ?>