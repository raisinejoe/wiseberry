<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Real Estate Blogs</h1>-->
<!--<h3>Blog Sub Title?-->
<!--(Example: How To Increase Your Home Value)-->
<!--</h3>-->
        <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center blogs back-100"  style="background-image: url(<?php echo e(asset('images/25-Header-V2.png')); ?>);background-repeat: no-repeat !important;">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title">WISEBERRY BLOGS</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="rental-appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

<section class="individual-agent-three blogs individual bg-white">
          <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <div class="banner-search-wrap">
                    <div class="row">
                        <div class="col-md-8">
                            <!-- <ul class="nav nav-tabs rld-banner-tab">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs_1">SELLERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs_2">BUYERS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs_3">LANDLORDS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs_4">TENANTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs_5">LIFESTYLE</a>
                        </li>
                    </ul> -->
                        </div>

                        <div class="col-md-4">
                          <form class="example" action="blog-search" method="POST">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="text" id="blogsearch" placeholder="What are you looking for?" name="search">
                            <input type="hidden" id="blogid" placeholder="What are you looking for?" name="search_id">
                            <button type="submit"><i class="fa fa-search"></i></button>
                          </form>
                        </div>

                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tabs_1">

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="crumbs">
    <ul>
        <li><a href="../blogs"><?php echo e($data[0]->cat_name); ?></a></li>
        <li><a href="#2"><?php echo e($data[0]->name); ?></a></li>
    </ul>
</div>
<!-- partial -->
                                </div>
                            </div>


                            <div class="row">
                    <div class="sec-title" style="width: 100%;">
                    <h2 style="text-align: center;"><?php echo e($data[0]->name); ?></h2>
                </div>
                </div>

                        <!-- START SECTION WHY WISEBERRY -->


                             <section class="why-section owner individual-office blogs individual bg-white individual-blogs">
                                 <div class="container-fluid container-fluid1">
                                     <div class="row">
                                         

                                            <?php echo $data[0]->content; ?>


                                        
                                        </div>
                                    </div>
             </div>
          </div>
          </div>
        </section>



        <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 individual-office individual-blogs" style="padding: 1rem 0 7rem;">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>YOU MAY ALSO LIKE THESE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php foreach ($data_may as $keys => $values)  { ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/<?php echo e($values->blog_id); ?>" class="news-img-link img-box hover-effect img-box hover-effect">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="../images/tips1.jpg" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6><?php echo e($values->name); ?></h6>
                                        <!-- <a href="#!"><h3>Current Areas That Are Booming</h3></a>
                                        <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate
            your kitchen, you want it to be something special, a place
            that will make you delighted to come home</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <?php  } ?>
                    </div>

                    <!--<div class="row">-->
                    <!--    <div class="col-md-12 text-center">-->
                    <!--        <a href="#" class="read-more">SEE MORE</a>-->
                    <!--    </div>-->
                    <!--</div>-->

                </div>
            </div>
        </section>
        

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/ind_blog.blade.php ENDPATH**/ ?>