<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!--<h1>Manage With Us</h1>-->
<!--<h2>Lease Your Property With Us</h2>-->
<!--<h3>Find An Agent Or An Office</h3>-->
  <div class="clearfix"></div>
  <div class="manage-with-us">
      <section class="header-image home-18 d-flex align-items-center inner-banner" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title"><span class="play">Managing</span> WITH WISEBERRY</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-7 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH EQUITY MY PROPERTY HAS.</h1>
                    </div>
                    <div class="col-md-3 pr-0 pl-0">
                        <a href="#!" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">FIND OUT NOW</a>
                        <!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">FIND OUT NOW</a></div> -->
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process mb0">
         <div class="container container1">
               
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p1->heading); ?></h2>
                            <?php echo $p1->desc; ?>

                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process pt-0 mb0">
         <div class="container container1">
             <div class="row">


                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p2->image); ?>">
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <!-- <h2>WHY MANAGE WITH US?</h2> -->
                            <?php echo $p2->desc; ?>

                            
<a href="<?php echo e($p2->button_link); ?>">REQUEST APPRAISAL</a>
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">VIEW FOR LEASE PROPERTIES</a></div> -->
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process ptb5 pb2 mb0  manage-with-us count-section">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center">
                            <h2>THE WISEBERRY EXPERIENCE</h2>
                            <?php echo $p3->desc; ?>

                        </div>
                    </div>

                    
                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>1</span>-->
                    <!--                <h6>PRE-RENTING</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>2</span>-->
                    <!--        <h6>FINDING A TENANT</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>3</span>-->
                    <!--        <h6>SHOWING THE PROPERTY</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>4</span>-->
                    <!--        <h6>TENANT SELECTION</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>5</span>-->
                    <!--        <h6>TENANT APPROVAL</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>6</span>-->
                    <!--        <h6>LEASE SIGNING</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>7</span>-->
                    <!--        <h6>MANAGEMENT</h6>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-md-3">-->
                    <!--    <div class="count text-center">-->
                    <!--        <span>8</span>-->
                    <!--        <h6>CELEBRATE</h6>-->
                    <!--    </div>-->
                    <!--</div>-->
                    
                    <div class="col-md-3">
                            <span id="icon-block-1" class="title"> 
                              <div class="count text-center">
                                        <span>1</span>
                                        <h6>PRE-RENTING</h6>
                                        <p></p>
                                    </div>
                            </span>

                            <div id="icon-block-1-bottom" class="content">
                                	<?php echo $p3->desc_t1; ?>

                        	</div>
                    </div>

                        <div class="col-md-3">
                            <span id="icon-block-2" class="title">
                                    <div class="count text-center">
                            <span>2</span>
                            <h6>FINDING A TENANT</h6>
                        </div>
                            </span>

                            <div id="icon-block-2-bottom" class="content">
                                 <?php echo $p3->desc_t2; ?>

                            </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-3" class="title">
                          <div class="count text-center">
                            <span>3</span>
                            <h6>SHOWING THE PROPERTY</h6>
                        </div>
                        </span>

                        <div id="icon-block-3-bottom" class="content">
                            <?php echo $p3->desc_t3; ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-4" class="title">
                          <div class="count text-center">
                            <span>4</span>
                            <h6>TENANT SELECTION</h6>
                        </div>
                        </span>

                        <div id="icon-block-4-bottom" class="content">
                            <?php echo $p3->desc_t4; ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-5" class="title">
                          <div class="count text-center">
                            <span>5</span>
                            <h6>TENANT APPROVAL</h6>
                        </div>
                        </span>

                        <div id="icon-block-5-bottom" class="content">
                            <?php echo $p3->desc_t5; ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-6" class="title">
                          <div class="count text-center">
                            <span>6</span>
                            <h6>LEASE SIGNING</h6>
                        </div>
                        </span>

                        <div id="icon-block-6-bottom" class="content">
                            <?php echo $p3->desc_t6; ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-7" class="title">
                          <div class="count text-center">
                            <span>7</span>
                            <h6>MANAGEMENT</h6>
                        </div>
                        </span>

                        <div id="icon-block-7-bottom" class="content">
                            <?php echo $p3->desc_t7; ?>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <span id="icon-block-8" class="title">
                          <div class="count text-center">
                            <span>8</span>
                            <h6>CELEBRATE</h6>
                        </div>
                        </span>

                        <div id="icon-block-8-bottom" class="content">
                            <?php echo $p3->desc_t8; ?>

                        </div>
                    </div>
                    
                    
                    
                    
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->
<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process mb2">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p4->heading); ?></h2>
                            <?php echo $p4->desc; ?>

                            
<a href="#!" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ENQUIRE NOW</a>
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><a href="#!">ENQUIRE NOW</a></button>-->
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">FIND OUT NOW</a></div> -->
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">VIEW FOR LEASE PROPERTIES</a></div> -->
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p4->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 pt-0" style="padding: 6rem 0 8rem;">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/<?php echo e($value->id); ?>" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="<?php echo e($img_path); ?><?php echo e($value->featured_image); ?>" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>Buyers</h6>
                                        <a href="individual_blog/<?php echo e($value->id); ?>"><h3><?php echo e($value->name); ?></h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
  </div>
  <script src='https://code.jquery.com/jquery-2.2.0.js'></script>
  <script>
      //icon-blocks show-hide
$(document).ready(function() {
  $('#icon-block-1-bottom').hide();

  $('#icon-block-1').mouseover(function() {

    $('#icon-block-1-bottom').show();
  });
  $('#icon-block-1-bottom').mouseover(function() {

    $('#icon-block-1-bottom').show();
  });

  //Hide div on mouseover
  $('#icon-block-1').mouseout(function() {

    if ($('#icon-block-1').is(':hover')) {

      $('#icon-block-1-bottom').show();
    } else {
      $('#icon-block-1-bottom').hide();
    }

  });

  $('#icon-block-1-bottom').mouseout(function() {

    $('#icon-block-1-bottom').hide();
  });

});

$(document).ready(function() {

  $('#icon-block-2-bottom').hide();

  $('#icon-block-2').mouseover(function() {

    $('#icon-block-2-bottom').show();
  });
  $('#icon-block-2-bottom').mouseover(function() {

    $('#icon-block-2-bottom').show();
  });

  $('#icon-block-2').mouseout(function() {

    if ($('#icon-block-2').is(':hover')) {

      $('#icon-block-2-bottom').show();
    } else {
      $('#icon-block-2-bottom').hide();
    }

  });

  $('#icon-block-2-bottom').mouseout(function() {

    $('#icon-block-2-bottom').hide();
  });

});

$(document).ready(function() {

  $('#icon-block-3-bottom').hide();

  $('#icon-block-3').mouseover(function() {

    $('#icon-block-3-bottom').show();
  });
  $('#icon-block-3-bottom').mouseover(function() {

    $('#icon-block-3-bottom').show();
  });

  $('#icon-block-3').mouseout(function() {

    if ($('#icon-block-3').is(':hover')) {

      $('#icon-block-3-bottom').show();
    } else {
      $('#icon-block-3-bottom').hide();
    }

  });

  $('#icon-block-3-bottom').mouseout(function() {

    $('#icon-block-3-bottom').hide();
  });

});
$(document).ready(function() {

  $('#icon-block-4-bottom').hide();

  $('#icon-block-4').mouseover(function() {

    $('#icon-block-4-bottom').show();
  });
  $('#icon-block-4-bottom').mouseover(function() {

    $('#icon-block-4-bottom').show();
  });

  $('#icon-block-4').mouseout(function() {

    if ($('#icon-block-4').is(':hover')) {

      $('#icon-block-4-bottom').show();
    } else {
      $('#icon-block-4-bottom').hide();
    }

  });

  $('#icon-block-4-bottom').mouseout(function() {

    $('#icon-block-4-bottom').hide();
  });

});
$(document).ready(function() {

  $('#icon-block-5-bottom').hide();

  $('#icon-block-5').mouseover(function() {

    $('#icon-block-5-bottom').show();
  });
  $('#icon-block-5-bottom').mouseover(function() {

    $('#icon-block-5-bottom').show();
  });

  $('#icon-block-5').mouseout(function() {

    if ($('#icon-block-5').is(':hover')) {

      $('#icon-block-5-bottom').show();
    } else {
      $('#icon-block-5-bottom').hide();
    }

  });

  $('#icon-block-5-bottom').mouseout(function() {

    $('#icon-block-5-bottom').hide();
  });

});
$(document).ready(function() {

  $('#icon-block-6-bottom').hide();

  $('#icon-block-6').mouseover(function() {

    $('#icon-block-6-bottom').show();
  });
  $('#icon-block-6-bottom').mouseover(function() {

    $('#icon-block-6-bottom').show();
  });

  $('#icon-block-6').mouseout(function() {

    if ($('#icon-block-6').is(':hover')) {

      $('#icon-block-6-bottom').show();
    } else {
      $('#icon-block-6-bottom').hide();
    }

  });

  $('#icon-block-6-bottom').mouseout(function() {

    $('#icon-block-6-bottom').hide();
  });

});
$(document).ready(function() {

  $('#icon-block-7-bottom').hide();

  $('#icon-block-7').mouseover(function() {

    $('#icon-block-7-bottom').show();
  });
  $('#icon-block-7-bottom').mouseover(function() {

    $('#icon-block-7-bottom').show();
  });

  $('#icon-block-7').mouseout(function() {

    if ($('#icon-block-7').is(':hover')) {

      $('#icon-block-7-bottom').show();
    } else {
      $('#icon-block-7-bottom').hide();
    }

  });

  $('#icon-block-7-bottom').mouseout(function() {

    $('#icon-block-7-bottom').hide();
  });

});
$(document).ready(function() {

  $('#icon-block-8-bottom').hide();

  $('#icon-block-8').mouseover(function() {

    $('#icon-block-8-bottom').show();
  });
  $('#icon-block-8-bottom').mouseover(function() {

    $('#icon-block-8-bottom').show();
  });

  $('#icon-block-8').mouseout(function() {

    if ($('#icon-block-8').is(':hover')) {

      $('#icon-block-8-bottom').show();
    } else {
      $('#icon-block-8-bottom').hide();
    }

  });

  $('#icon-block-8-bottom').mouseout(function() {

    $('#icon-block-8-bottom').hide();
  });

});
  </script>
        



<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/manage_with_us.blade.php ENDPATH**/ ?>