<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Agent Name-->
<!--(Example: Darin Butcher)-->
<!--</h1>-->
<!--<h2>Sub Heading - Agent Role, Office Identifier-->
<!--(Example: Principal, Wiseberry Heritage)-->
<!--</h2>-->
<!--<h3>Your Local Real Estate Agent</h3>-->
        <div class="individual-agent">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner individual" id="slider" style="background: url('../images/21-Header-V2.png');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title">WISEBERRY PICTON</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="../rental-appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0 individual-rental individual-agent-one">
            <div class="container">
                  
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-xs-12">

                        <div class="details">

                            <h1 class="title pb1"><?php echo e($result['contact']['first_name']  ?? ''); ?> <?php echo e($result['contact']['last_name']  ?? ''); ?></h1>

                            <div class="avatar">

                              
                              <?php if(isset($result['contact']['profile_picture']) && $result['contact']['profile_picture'] !== null){ ?>
                                    <img src="<?php echo e($result['contact']['profile_picture']); ?>" alt="" />
                                <?php } else { ?>
                                    <img src="../images/darin.jpg">
                                <?php } ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-4 car">
                        
                        <div class="side-bar pt5">
                            
                            
                            <h1 class="title">AGENT DETAILS</h1>
                            <h3 class="subtitle"><?php echo e($result['position']  ?? ''); ?></h3>

                            <ul class="mail">
                                <li>
                                    <a href="#"><!--<img src="../images/phone.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.2c245.2,0,444.8,199.5,444.8,444.8S785.2,984.8,540,984.8S95.2,785.2,95.2,540S294.8,95.2,540,95.2z
     M540,6.3C245.3,6.3,6.3,245.3,6.3,540s239,533.7,533.7,533.7s533.7-239,533.7-533.7S834.7,6.3,540,6.3z M740.1,776.2l-78.3-151.1
    L616,647.6c-49.8,24.2-151.2-173.9-102.5-200l46.3-22.8l-77.7-151.6l-46.8,23.1c-160.2,83.5,94.2,577.8,258,502.9L740.1,776.2z"/>
</svg><?php echo e($result['contact']['phones'][0]['value']  ?? ''); ?></a>
                                </li>
                                <li>
                                    <a href="mailto:<?php echo e($result['contact']['emails'][0]['value']  ?? ''); ?>"><!--<img src="../images/mail.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg><?php echo e($result['contact']['emails'][0]['value']  ?? ''); ?></a>
                                </li>
                                <li>
                                    <a href="../downloadcard/<?php echo e($result['contact']['first_name']); ?>_<?php echo e($result['contact']['phones'][0]['value']  ?? ''); ?>_<?php echo e($result['contact']['emails'][0]['value']  ?? ''); ?>"><!--<img src="../images/v.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#830B2C;}
	.st1{font-family:'Arial-BoldMT';}
	.st2{font-size:655.684px;}
</style>
<g>
	<path class="st0" d="M540,101c242,0,439,196.9,439,439S782,979,540,979S101,782,101,540S298,101,540,101z M540,13.3
		C249.1,13.3,13.3,249.1,13.3,540s235.9,526.7,526.7,526.7s526.7-235.9,526.7-526.7S830.9,13.3,540,13.3z"/>
	<text transform="matrix(1 0 0 1 335.5405 807.1714)" class="st0 st1 st2">V</text>
</g>
</svg> Download V Card</a>
                                </li>
                            </ul>

                            <ul class="social">
                                <li>
                                    <a href="<?php echo e($result['contact']['facebook'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,97.5C784,97.5,982.5,296,982.5,540c0,244-198.5,442.5-442.5,442.5S97.5,784,97.5,540
    C97.5,296,296,97.5,540,97.5z M540,9C246.7,9,9,246.7,9,540c0,293.2,237.8,531,531,531s531-237.8,531-531C1071,246.7,833.3,9,540,9z
     M451.5,451.5H363V540h88.5v265.5h132.8V540h80.5l8-88.5h-88.5v-36.9c0-21.2,4.2-29.5,24.7-29.5h63.8V274.5H566.4
    c-79.6,0-114.9,35-114.9,102.1L451.5,451.5L451.5,451.5z"/>
</svg><!--<img src="../images/facebook.png"> <i class="fa fa-facebook" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['contact']['instagram'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.6c244.5,0,443.4,198.9,443.4,443.4c0,244.5-198.9,443.4-443.4,443.4S96.6,784.5,96.6,540
    C96.6,295.5,295.5,96.6,540,96.6z M540,7.9C246.2,7.9,7.9,246.2,7.9,540c0,293.8,238.2,532.1,532.1,532.1s532.1-238.2,532.1-532.1
    C1072.1,246.2,833.8,7.9,540,7.9z M540,321.9c71,0,79.5,0.3,107.6,1.6c72.1,3.3,105.7,37.5,109,109c1.3,28.1,1.6,36.4,1.6,107.5
    c0,71-0.3,79.5-1.6,107.5c-3.3,71.5-36.9,105.8-109,109.1c-28.1,1.2-36.5,1.5-107.6,1.5c-71,0-79.4-0.3-107.5-1.6
    c-72.3-3.3-105.7-37.6-109-109.1c-1.3-28-1.6-36.5-1.6-107.5c0-71,0.3-79.5,1.6-107.5c3.2-71.6,36.9-105.8,109-109.1
    C460.6,322.2,469,321.9,540,321.9z M540,274c-72.2,0-81.2,0.3-109.7,1.6c-96.6,4.4-150.3,58-154.7,154.7
    c-1.3,28.4-1.6,37.5-1.6,109.7c0,72.3,0.4,81.3,1.6,109.7c4.4,96.6,58.1,150.3,154.7,154.7c28.5,1.3,37.5,1.6,109.7,1.6
    c72.3,0,81.3-0.3,109.7-1.6c96.5-4.4,150.4-58,154.8-154.7c1.3-28.4,1.6-37.5,1.6-109.7c0-72.2-0.3-81.3-1.6-109.7
    c-4.3-96.5-58.1-150.3-154.8-154.7C621.3,274.3,612.3,274,540,274z M540,403.4c-75.4,0-136.6,61.1-136.6,136.6
    S464.6,676.6,540,676.6S676.7,615.5,676.7,540C676.7,464.6,615.4,403.4,540,403.4z M540,628.7c-49,0-88.7-39.7-88.7-88.7
    c0-49,39.7-88.7,88.7-88.7c48.9,0,88.8,39.7,88.8,88.7C628.8,589,588.9,628.7,540,628.7z M682,366.1c-17.7,0-32,14.3-32,31.9
    c0,17.6,14.3,31.9,32,31.9c17.6,0,32-14.3,32-31.9C714,380.4,699.7,366.1,682,366.1z"/>
</svg><!--<img src="../images/insta.png"> <i class="fab fa-instagram"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['contact']['linkedin'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,93.1c246.4,0,446.9,200.5,446.9,446.9c0,246.4-200.5,446.9-446.9,446.9C293.6,986.9,93.1,786.4,93.1,540
    C93.1,293.6,293.6,93.1,540,93.1z M540,3.8C243.9,3.8,3.8,243.9,3.8,540s240.1,536.2,536.2,536.2s536.2-240.1,536.2-536.2
    S836.1,3.8,540,3.8z M450.6,361.3c0,24.9-19.9,45-44.7,45c-24.7,0-44.7-20.1-44.7-45c0-24.9,20-45,44.7-45
    C430.7,316.2,450.6,336.4,450.6,361.3z M450.6,450.6h-89.4v268.1h89.4V450.6z M584.7,450.6h-89.4v268.1h89.4V590.9
    c0-76.9,89.5-84,89.5,0v127.9h89.3V568.6c0-146.7-139.7-141.4-178.7-69.2L584.7,450.6L584.7,450.6z"/>
</svg><!--<img src="../images/linkedin.png"> <i class="fa fa-linkedin" aria-hidden="true"></i> --></a>
                                </li>
                            </ul>
                            
                            <ul class="button-list">
                                <li>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaltwo">CONTACT DARIN</button>
                                    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal_s">CONTACT DARIN</button>-->
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalone">VIDEO</button>
                                    <!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">MAP</a></div> -->
                                </li>
                            </ul>
                            <h1 class="title">OFFICE DETAILS</h1>
                            <input type="hidden" id="ind_org_idd" value="<?php echo e($result['organisation']['id'] ?? ''); ?>">
                            <h2 class="subtitle"><?php echo e($result['organisation']['name']  ?? ''); ?></h2>
                            <p><?php echo e($result['organisation']['address']['full_address']  ?? ''); ?></p>
                            <ul class="mail">
                                <li>
                                    <a href="#"><!--<img src="../images/phone.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.2c245.2,0,444.8,199.5,444.8,444.8S785.2,984.8,540,984.8S95.2,785.2,95.2,540S294.8,95.2,540,95.2z
     M540,6.3C245.3,6.3,6.3,245.3,6.3,540s239,533.7,533.7,533.7s533.7-239,533.7-533.7S834.7,6.3,540,6.3z M740.1,776.2l-78.3-151.1
    L616,647.6c-49.8,24.2-151.2-173.9-102.5-200l46.3-22.8l-77.7-151.6l-46.8,23.1c-160.2,83.5,94.2,577.8,258,502.9L740.1,776.2z"/>
</svg><?php echo e($result['organisation']['phones'][0]['value']  ?? ''); ?></a>
                                </li>
                                <li>
                                    <a href="#"><!--<img src="../images/mail.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg><?php echo e($result['organisation']['emails'][0]['value']  ?? ''); ?></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- INDIVIDUAL SECTION END -->

        <section class="individual-agent-two">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <p><?php echo e($result['bio']  ?? ''); ?></p>

                            

                            <h1 class="title" style="margin-top: 2em;margin-bottom: 1em;">AWARDS</h1>
                            <ul class="list">
                                <li>Awarded Best Real Estate on the Central Coast by Local Business Awards 2019</li>
                                <li>Finalist for REB Property Management and Sales Office of the Year 2019</li>
                                <li>Finalist in the Small Business Champions Awards 2018, 2019 & 2020</li>
                                <li>Finalist in REB top 50 sales offices 2018 - Only Central Coast representative</li>
                                <li>Top 4 in Australia at the AREA Awards for Excellence in Customer Service 2018</li>
                                <li>Wiseberry Sales Team of the Year 2019</li>
                            </ul>
              </div>
            </div>
          </div>
        </section>

        <section class="individual-agent-three">
          <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                                    <div class="banner-search-wrap">
                                        <ul class="nav nav-tabs rld-banner-tab text-center">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#tabs_1">FOR SALE</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#tabs_2">SOLD</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="tabs_1">
                                                <!-- START Buy Result -->

        <section class="recently portfolio bg-white-1 home18 buy-result">
            
            <div>


                <div class="row">
                    <?php   if(!empty($result_sale)){ 
                            $i=0;
                            foreach ($result_sale as $key => $value){ 
                    ?>
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="../individual-result/<?php echo e($value['id']); ?>" class="homes-img img-box hover-effect">
                                                <?php if(isset($value['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($value['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="../individual-result/<?php echo e($value['id']); ?>"><?php echo e($value['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($value['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom'|| $values['name'] == 'Parking'){
                                                    if($values['name'] == 'Bedroom'){
                                                        $img="../images/bed.svg";
                                                    }
                                                    if($values['name'] == 'Bathroom'){
                                                        $img="../images/bath.svg";
                                                    }

                                                    if($values['name'] == 'Parking'){
                                                        $img="../images/car.svg";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($values['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform: uppercase;"><?php echo e($value['status']['name']  ?? ''); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  } } ?>

                    

                    

                </div>

                <!-- <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more" style="margin-top: 2em;display: inline-block;">SEE MORE</a>
                    </div>
                </div> -->

            </div>

        </section>

        <!-- END Buy Result -->
                                            </div>
                                            <div class="tab-pane fade" id="tabs_2">
                                                <section class="recently portfolio bg-white-1 home18 buy-result">
            
            <div>


                <div class="row">
                    
                    <?php   if(!empty($result_sold)){ 
                            $i=0;
                            foreach ($result_sold as $keys => $values){ 
                    ?>
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="../individual-result/<?php echo e($value['id']); ?>" class="homes-img img-box hover-effect">
                                                <?php if(isset($values['assets'][0]['url'])){ ?>
                                                    <img src="<?php echo e($values['assets'][0]['url']); ?>" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="../images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="../individual-result/<?php echo e($value['id']); ?>"><?php echo e($values['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($values['property']['features'] as $keyy => $valuey){
                                                if($valuey['name'] == 'Bedroom' || $valuey['name'] == 'Bathroom'|| $valuey['name'] == 'Parking'){
                                                    if($valuey['name'] == 'Bedroom'){
                                                        $img="../images/bed.svg";
                                                    }
                                                    if($valuey['name'] == 'Bathroom'){
                                                        $img="../images/bath.svg";
                                                    }

                                                    if($valuey['name'] == 'Parking'){
                                                        $img="../images/car.svg";
                                                    }
                                                 ?>
                                                <a>
                                                    <span><?php echo e($valuey['qty']  ?? '-'); ?></span>
                                                    <img src="<?php echo e($img); ?>">
                                                </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform: uppercase;"><?php echo e($values['status']['name']  ?? ''); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  } } ?>

                    

                </div>

                <!-- <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more" style="margin-top: 2em;display: inline-block;">SEE MORE</a>
                    </div>
                </div> -->

            </div>

        </section>
                                            </div>
                                        </div>
                                    </div>
             </div>
          </div>
          </div>
        </section>



<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 client-testimonial" style="padding: 4rem 0 8em;">
            <div>
                <div class="sec-title pt4">
                    <h2>CLIENT TESTIMONIALS</h2>
                </div>



    <section class="testimonial text-center">
        <div class="container">

            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
             
                <div class="carousel-inner" role="listbox">
                    <?php   if(!empty($result_test)){ 
                            $i=0;
                            foreach ($result_test as $keyt => $valuet){ 

                                if($i==0){
                                    $act="active";
                                }else{
                                    $act=""; 
                                }
                    ?>
                    <div class="carousel-item <?php echo e($act); ?>">
                        <div class="testimonial4_slide">
                            <h5><?php echo e($valuet['heading']); ?></h5>
                            <p><?php echo e($valuet['value']); ?></p>
                            <h4><?php echo e($valuet['created_by']); ?></h4>
                        </div>
                    </div>
                    <?php $i++;  } } ?>
                    
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </section>


            </div>
        </section>






<!-- The Modal -->
  <div class="modal fade home-rent register-intrest" id="myModaltwo">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">WE WOULD LOVE TO HEAR FROM YOU</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <p>Simply fill out the form on this page and we will contact you.</p>

            <form action="#!" id="capt_form">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email Address*">
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Comments"></textarea>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  
                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
        </div>
  
  
  
  
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/ind_agent.blade.php ENDPATH**/ ?>