<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->
<!--<h2>Wiseberry Terms and Conditions</h2>-->
        <!-- STAR HEADER IMAGE -->
        <div class="terms">
            <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


  
     <section class="privacy-section p-80">
         
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     
                     <h1><?php echo e($data->heading); ?></h1>
                     <?php echo $data->desc; ?>

                     <!--<h2>Wiseberry 'Terms and Conditions of Use' current as at March 2017</h2>-->
                     


                 </div>
             </div>
         </div>

     </section>
        </div>

     
<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/terms.blade.php ENDPATH**/ ?>