<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <div class="clearfix"></div>
        <!-- Header Container / End -->
<!--<h1>Premium Australian Real Estate</h1>-->
<!--<h3>Find An Agent Or An Office</h3>-->
        <!-- STAR HEADER IMAGE --> 
        <div class="home">
            <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            
                            <div class="banner-search-wrap home">
                                 
                                 <h2>AT WISEBERRY, WE <span class="play">Move</span> PEOPLE</h2>
                                 <div class="inner">
                                     <div class="row">
                                         <div class="col-md-12">
                                             <h2>I AM LOOKING TO</h2>
                                         </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href="search">BUY</a>
                                        </div>
                                        <div class="col-md-3">
                                            <!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">RENT</a></div> -->
                                            <a href="rent">RENT</a>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="sell-with-us">SELL</a>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="manage-with-us">MANAGE</a>
                                        </div>
                                    </div>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

        <!-- START SECTION PROPERTIES FOR SALE -->
        <section class="recently portfolio bg-white-1 home18 pb4">
               

                <!-- <div class="alert alert-success alert-block" role="alert">
                  <button class="close" data-dismiss="alert"></button>
                  <?php echo e(Session::get("success")); ?>

                </div> -->
            <div class="properties-section">
                <div class="container-fluid">
                <div class="sec-title">
                    <h2>FEATURED PROPERTIES</h2>
                </div>

                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p3->image_1); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p3->add_1); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p3->bed_1); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->bath_1); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->car_1); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p3->date_1); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p3->image_2); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p3->add_2); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p3->bed_2); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->bath_2); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->car_2); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p3->date_2); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p3->image_3); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p3->add_3); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p3->bed_3); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->bath_3); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p3->car_3); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p3->date_3); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    

                </div>

            </div>

            <div class="properties-section">
                <div class="container-fluid">
                <div class="sec-title">
                    <h2 class="mt1">FEATURED RENTAL PROPERTIES</h2>
                </div>
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p4->image_1); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p4->add_1); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p4->bed_1); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->bath_1); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->car_1); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p4->date_1); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p4->image_2); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p4->add_2); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p4->bed_2); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->bath_2); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->car_2); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p4->date_2); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="<?php echo e($img_path); ?><?php echo e($p4->image_3); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!"><?php echo e($p4->add_3); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span><?php echo e($p4->bed_3); ?></span>
                                                <img src="images/bed.svg">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->bath_3); ?></span>
                                                <img src="images/bath.svg">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span><?php echo e($p4->car_3); ?></span>
                                                <img src="images/car.svg">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span><?php echo e($p4->date_3); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                </div>
            </div>
            </div>
            
        </section>
        <!-- END SECTION PROPERTIES FOR SALE -->




<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section">
         <div class="container">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                      <img  src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>" class="img-responsive" alt="">
                        <!--<div class="image-animation-zoom">-->
                           
                        <!--<a  class="img-box hover-effect">-->
                        <!--    <img  src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>" class="img-responsive" alt="">-->
                           
                        <!--</a>-->
                        <!--</div>-->

                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p1->heading); ?></h2>
                            <?php echo $p1->desc; ?>

                            <a href="<?php echo e($p1->button_link); ?>">FIND OUT MORE</a>
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


        <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18">
            <div class="container-fluid">
                <div class="sec-title pb1">
                    <h2>PROPERTY TIPS</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tips1.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>INDOOR STYLE</h6>
                                    <a href="#!"><h3>2020 Top Kitchen Trends</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tips2.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>HOW TO RENOVATE</h6>
                                    <a href="#!"><h3>Bathroom Renovation Tips</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Bathroom renovation is a big task, and can be especially daunting when working with a space that’s awkwardly shaped, has quirky pipelines or is lacking in natural lighting.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tips3.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>OUTDOOR GAME</h6>
                                    <a href="#!"><h3>Swimming Pools Are Back</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Data from realestate.com.au shows swimming pools as the number one feature Australian buyers are searching for when on the property hunt. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="#" class="read-more">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- END SECTION BLOG -->


        <!-- START SECTION WHY WISEBERRY -->

     <section class="why-section owner home mb8">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt1">
                            <h2><?php echo e($p2->heading); ?></h2>
                            <h3><?php echo e($p2->sub_heading); ?></h3>
                            <!-- <p>If you ask any one of our offices what sets Wiseberry apart from other real estate groups, you will receive an unanimous answer; our culture.</p>
                            <p>All aspects of Wiseberry reflect our underlying culture of conducting business in a caring, trustworthy, transparent, ethical and honest and professional manner.</p> -->
                            <?php echo $p2->desc; ?>

                            <a href="<?php echo e($p2->button_link); ?>">LEARN MORE</a>
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                         <div class="">
                            <img src="<?php echo e($img_path); ?><?php echo e($p2->image); ?>" class="img-responsive" alt="">
                        </div> 
                        <!--<div class="image-animation-zoom">-->
                           
                        <!--<a  class="img-box hover-effect">-->
                        <!--    <img src="<?php echo e($img_path); ?><?php echo e($p2->image); ?>" class="img-responsive" alt="">-->
                           
                        <!--</a>-->
                        <!--</div>-->
                    </div>

                 
                   
             </div>
         </div>
     </section>
        </div>

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- END SECTION WHY WISEBERRY -->

       
       <!-- START SUBSCRIBE NOW -->

       
<?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/home.blade.php ENDPATH**/ ?>