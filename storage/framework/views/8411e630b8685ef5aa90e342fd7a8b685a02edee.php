<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Properties For Rent In “Suburb” (Example: Properties For Rent In Castle Hill NSW 2154)</h2>-->
<!--<h3>Suburb (Example: Castle Hill NSW 2154)</h3>
<h1>Properties For Rent</h1>
<h2>Properties For Rent In “Suburb” (Example: Properties For Rent In Castle Hill NSW 2154)</h2
<h3>Suburb (Example: Castle Hill NSW 2154)</h3>-->

        <div class="rent-result">
           <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-result">
                                 
                                 <h1 class="text-center">REFINE SEARCH</h1>
                                 <div class="inner">
                                    <form method="POST" id="search_form" action="rent-results">
                                    
                                    <div class="row">
                                        <div class="col-md-3 pr-0">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="hidden" name="pageno" id="pageno" value="2">
                                            <input type="hidden" name="pagename" id="pagename" value="rent">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <select class="form-control" id="page_option">
                                                <option value="1">Buy</option>
                                                <option value="2" selected>Rent</option>
                                                <option value="3">Holiday Rental</option>
                                                <option value="4">Commercial Rental</option>
                                                <option value="5">Sold</option>
                                                <option value="6">Leased</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input type="txt" id="rent_tags" value="<?php echo e($request->suburb_name); ?>" name="" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" value="<?php echo e($request->suburb); ?>" id="renttags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">
                                               
                                                <div class="collapse" id="paypal" role="tablist" aria-expanded="false">
                                                    <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <input type="hidden" name="prop_hid" id="prop_hid" value="<?php echo e($request->option); ?>">
                                                                <select class="form-control" id="buy_options" name="option" style="border-right: none;">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option <?php if ($request->bed == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bed == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bed == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bed == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bed == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option <?php if ($request->bath == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bath == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bath == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bath == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bath == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option <?php if ($request->parking == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->parking == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->parking == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->parking == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->parking == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Weekly Min $</option>
                                                                      <option <?php if ($request->price_from == '25') echo ' selected="selected"'; ?> value="25">$25</option>
                                                                      <option <?php if ($request->price_from == '50') echo ' selected="selected"'; ?> value="50">$50</option>
                                                                      <option <?php if ($request->price_from == '75') echo ' selected="selected"'; ?> value="75">$75</option>
                                                                      <option <?php if ($request->price_from == '100') echo ' selected="selected"'; ?> value="100">$100</option>
                                                                      <option <?php if ($request->price_from == '125') echo ' selected="selected"'; ?> value="125">$125</option>
                                                                      <option <?php if ($request->price_from == '150') echo ' selected="selected"'; ?> value="150">$150</option>
                                                                      <option <?php if ($request->price_from == '200') echo ' selected="selected"'; ?> value="200">$200</option>
                                                                      <option <?php if ($request->price_from == '250') echo ' selected="selected"'; ?> value="250">$250</option>
                                                                      <option <?php if ($request->price_from == '300') echo ' selected="selected"'; ?> value="300">$300</option>
                                                                      <option <?php if ($request->price_from == '350') echo ' selected="selected"'; ?> value="350">$350</option>
                                                                      <option <?php if ($request->price_from == '400') echo ' selected="selected"'; ?> value="400">$400</option>
                                                                      <option <?php if ($request->price_from == '450') echo ' selected="selected"'; ?> value="450">$450</option>
                                                                      <option <?php if ($request->price_from == '500') echo ' selected="selected"'; ?> value="500">$500</option>
                                                                      <option <?php if ($request->price_from == '600') echo ' selected="selected"'; ?> value="600">$600</option>
                                                                      <option <?php if ($request->price_from == '700') echo ' selected="selected"'; ?> value="700">$700</option>
                                                                      <option <?php if ($request->price_from == '800') echo ' selected="selected"'; ?> value="800">$800</option>
                                                                      <option <?php if ($request->price_from == '900') echo ' selected="selected"'; ?> value="900">$900</option>
                                                                      <option <?php if ($request->price_from == '1000') echo ' selected="selected"'; ?> value="1000">$1,000</option>
                                                                      <option <?php if ($request->price_from == '1250') echo ' selected="selected"'; ?> value="1250">$1,250</option>
                                                                      <option <?php if ($request->price_from == '1500') echo ' selected="selected"'; ?> value="1500">$1,50</option>

                                                                      <option <?php if ($request->price_from == '2000') echo ' selected="selected"'; ?> value="2000">$2,000</option>
                                                                      <option <?php if ($request->price_from == '3000') echo ' selected="selected"'; ?> value="3000">$3,000</option>
                                                                      <option <?php if ($request->price_from == '4000') echo ' selected="selected"'; ?> value="4000">$4,000</option>
                                                                      <option <?php if ($request->price_from == '5000') echo ' selected="selected"'; ?> value="5000">$5,000</option>
                                                                      <option <?php if ($request->price_from == '10000') echo ' selected="selected"'; ?> value="10000">$10,000</option>
                                                                      
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Weekly Max $</option>
                                                                    <option <?php if ($request->price_to == '25') echo ' selected="selected"'; ?> value="25">$25</option>
                                                                      <option <?php if ($request->price_to == '50') echo ' selected="selected"'; ?> value="50">$50</option>
                                                                      <option <?php if ($request->price_to == '75') echo ' selected="selected"'; ?> value="75">$75</option>
                                                                      <option <?php if ($request->price_to == '100') echo ' selected="selected"'; ?> value="100">$100</option>
                                                                      <option <?php if ($request->price_to == '125') echo ' selected="selected"'; ?> value="125">$125</option>
                                                                      <option <?php if ($request->price_to == '150') echo ' selected="selected"'; ?> value="150">$150</option>
                                                                      <option <?php if ($request->price_to == '200') echo ' selected="selected"'; ?> value="200">$200</option>
                                                                      <option <?php if ($request->price_to == '250') echo ' selected="selected"'; ?> value="250">$250</option>
                                                                      <option <?php if ($request->price_to == '300') echo ' selected="selected"'; ?> value="300">$300</option>
                                                                      <option <?php if ($request->price_to == '350') echo ' selected="selected"'; ?> value="350">$350</option>
                                                                      <option <?php if ($request->price_to == '400') echo ' selected="selected"'; ?> value="400">$400</option>
                                                                      <option <?php if ($request->price_to == '450') echo ' selected="selected"'; ?> value="450">$450</option>
                                                                      <option <?php if ($request->price_to == '500') echo ' selected="selected"'; ?> value="500">$500</option>
                                                                      <option <?php if ($request->price_to == '600') echo ' selected="selected"'; ?> value="600">$600</option>
                                                                      <option <?php if ($request->price_to == '700') echo ' selected="selected"'; ?> value="700">$700</option>
                                                                      <option <?php if ($request->price_to == '800') echo ' selected="selected"'; ?> value="800">$800</option>
                                                                      <option <?php if ($request->price_from == '900') echo ' selected="selected"'; ?> value="900">$900</option>
                                                                      <option <?php if ($request->price_to == '1000') echo ' selected="selected"'; ?> value="1000">$1,000</option>
                                                                      <option <?php if ($request->price_to == '1250') echo ' selected="selected"'; ?> value="1250">$1,250</option>
                                                                      <option <?php if ($request->price_to == '1500') echo ' selected="selected"'; ?> value="1500">$1,500</option>
                                                                      <option <?php if ($request->price_to == '2000') echo ' selected="selected"'; ?> value="2000">$2,000</option>
                                                                      <option <?php if ($request->price_to == '3000') echo ' selected="selected"'; ?> value="3000">$3,000</option>
                                                                      <option <?php if ($request->price_to == '4000') echo ' selected="selected"'; ?> value="4000">$4,000</option>
                                                                      <option <?php if ($request->price_to == '5000') echo ' selected="selected"'; ?> value="5000">$5,000</option>
                                                                      <option <?php if ($request->price_to == '10000') echo ' selected="selected"'; ?> value="10000">$10,000</option>
                                                                      
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                         
                                        </div>
                                    </div>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

    

        <!-- START Buy Result -->
        <?php if(!empty($result)){ ?>
        <section class="recently portfolio bg-white-1 home18 buy-result">
            
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="sec-title pb1">
                    <h1><?php echo e($data->heading); ?></h1>
                </div>
                    </div>
                </div>

                <div class="row" id="append_results">
                    <?php
                        $i=0;
                        foreach ($result as $key => $value){
                    ?>
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="individual-rent-results/<?php echo e($value['id']); ?>" class="homes-img img-box hover-effect">
                                                
                                                <?php 
                                                    $img="images/properties1.jpg";
                                                    foreach ($value['assets'] as $key => $val) {
                                                        if($val['extension'] != 'mp4' && $val['floorplan'] != 1){
                                                            $img=$val['url'];
                                                            break;
                                                        } 
                                                    } 

                                                ?>
                                                <img src="<?php echo e($img); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="individual-rent-results/<?php echo e($value['id']); ?>"><?php echo e($value['property']['address']['full_address']); ?></a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php
                                                  if (isset($value['property']['features'][1]) && $value['property']['features'][1]['name'] == "Bedroom"  && $value['property']['features'][1]['qty'] != "0")
                                                  {
                                              ?>
                                              <a>
                                              <span><?php echo e($value['property']['features'][1]['qty']  ?? ''); ?></span>
                                              <img src="images/bed.svg">
                                              </a>
                                              <?php   
                                                      unset($value['property']['features'][1]);
                                                  }
                                              ?>
                                            <?php foreach ($value['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom'){
                                                    $img="images/bed.svg";
                                                }
                                                if($values['name'] == 'Bathroom'){
                                                    $img="images/bath.svg";
                                                }

                                                if($values['name'] == 'Parking'){
                                                    $img="images/car.svg";
                                                }

                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                                                  if($values['qty'] != 0){
                                             ?>
                                            <a>
                                                <span><?php echo e($values['qty']  ?? '-'); ?></span>
                                                <img src="<?php echo e($img); ?>">
                                            </a>
                                            <?php } } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform: uppercase;"><?php echo e($value['price_view'] ?? ''); ?></span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; }  ?>

                    

                </div>
                <?php 
                    if(!empty($result)){ 
                        if($i == 12 && $next != null){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center mt2">
                        <a href="#!" class="see-more" id="see-more">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } }
                ?>
                

            </div>

        </section> 
        </div>

        <?php } else{ ?>

        <section class="no-result">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Thanks for searching. We do not currently have any properties for rent in your area.</p>
                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>

        <!-- END Buy Result -->

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/rent-results.blade.php ENDPATH**/ ?>