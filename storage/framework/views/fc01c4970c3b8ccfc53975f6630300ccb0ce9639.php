
<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php if(isset($data->meta_desc)){ ?>
    <meta name="title" content="<?php echo e($data->meta_title); ?>">
    <meta name="description" content="<?php echo strip_tags($data->meta_desc); ?>">
    <meta name="keywords" content="<?php echo e($data->meta_keywords); ?>">
<?php } ?>
    <meta name="author" content="">
    <title>Wiseberry</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="<?php echo e(asset('css/jquery-ui.css')); ?>">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="<?php echo e(asset('css/fontawesome-all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/fontawesome-5-all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
    <!--<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>-->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css'>
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" href="<?php echo e(asset('revolution/css/settings.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('revolution/css/layers.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('revolution/css/navigation.css')); ?>">
    <!-- ARCHIVES CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('css/animate.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/magnific-popup.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/lightcase.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/menu.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/nice-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/slick.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/styles.css')); ?>">
    <link rel="stylesheet" id="color" href="<?php echo e(asset('css/colors/dark-orange.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/default.css')); ?>">
    <script src='https://www.google.com/recaptcha/api.js' async defer ></script>
    <link rel="apple-touch-icon" href="">
    <link rel="icon" href="images/favicon.ico" type="image/ico" sizes="16x16">
    <style type="text/css">
        section.recently.portfolio ul.slick-dots {
    display: none !important;
}
@font-face {
    font-family: 'webfontregular' !important;
    src: url('<?php echo e(asset('../css/laylist_script-webfont-webfont.woff2')); ?>') format('woff2'),
         url('<?php echo e(asset('../css/playlist_script-webfont-webfont.woff')); ?>') format('woff');
    font-weight: normal;
    font-style: normal;

}
/*.ui-menu .ui-menu-item-wrapper {
    background: inherit !important;
    width: 100% !important;
}*/
    </style>
</head><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/layout/header.blade.php ENDPATH**/ ?>