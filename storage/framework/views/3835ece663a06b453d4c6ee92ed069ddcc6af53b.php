<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

        <!-- STAR HEADER IMAGE -->
        <div class="auction">
            <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                       <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-landing buy-action">
                                 
                                 <h1 style="margin-left: 15px;">AUCTIONS</h1>
                                 <div class="inner">
                                 <form method="POST" action="auction-result">
                                 <!--<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">-->
                                    <div class="row">
                                        <div class="col-md-2 pr-0">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="txt" id="datepic" name="datepic" placeholder="Date" class="form-control date" autocomplete="off">
                                            <input type="txt" id="datepic2" placeholder="Date" class="form-control date" style="position: absolute; border-right: 1px solid;visibility:hidden" autocomplete="off">

                                        </div>
                                        <div class="col-md-2 pr-0 pl-0">
                                            <select class="form-control" name="page_option_open" id="page_option_opens">
                                                <option value="1" selected>Residential Sales</option>
                                                <option value="2">Rural Sales</option>
                                                <option value="3">Commercial Sales</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5 pl-0">
                                            <input type="txt" id="tags" name="suburb_name" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <select class="form-control" id="buy_options" name="option">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option value="1">1 Bed</option>
                                                                    <option value="2">2 Bed</option>
                                                                    <option value="3">3 Bed</option>
                                                                    <option value="4">4 Bed</option>
                                                                    <option value="5">5 Bed</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option value="1">1 Bath</option>
                                                                    <option value="2">2 Bath</option>
                                                                    <option value="3">3 Bath</option>
                                                                    <option value="4">4 Bath</option>
                                                                    <option value="5">5 Bath</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option value="1">1 Car</option>
                                                                    <option value="2">2 Car</option>
                                                                    <option value="3">3 Car</option>
                                                                    <option value="4">4 Car</option>
                                                                    <option value="5">5 Car</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Min Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Max Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <!-- <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                                        <input id="check-b" type="checkbox" name="check">
                                                                        <label for="check-b">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div> -->
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MONEY I CAN BORROW.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a style="color: #fff;cursor: pointer;" data-toggle="modal"  data-target="#myModal_calculator">HOME LOANS CALCULATOR</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


<section class="home18 buy-landing-page action">



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="sec-title">
                    <h2>BUYING AT AUCTION </h2>
                </div>
        </div>
    </div>
</div>

    

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy mb1">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p1->sub_head); ?></h2>
                            <?php echo $p1->desc; ?>

                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->


 

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy mb3">
         <div class="container">
             <div class="row">
                 
                 
                    
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p2->image); ?>">
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center p2">
                            <h2><?php echo e($p2->sub_head); ?></h2>
                            <?php echo $p2->desc; ?>

                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy mb-0 pb-0">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center  pt5 p3">
                            <h2><?php echo e($p3->sub_head); ?></h2>
                            <?php echo $p3->desc; ?>

                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p3->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->

    
    <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 pb-0">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice1.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>INVESTORS</h6>
                                    <a href="#!"><h3>Current Areas That Are Booming</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice2.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>BUYERS</h6>
                                    <a href="#!"><h3>Private Inspections</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Bathroom renovation is a big task, and can be especially daunting when working with a space that’s awkwardly shaped, has quirky pipelines or is lacking in natural lighting.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice3.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>FINANCE</h6>
                                    <a href="#!"><h3>Borrowing Money Tips</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Data from realestate.com.au shows swimming pools as the number one feature Australian buyers are searching for when on the property hunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="#" class="read-more">SEE MORE</a>
                        </div>
                    </div> -->

                </div>
            </div>
        </section>
        <!-- END SECTION BLOG -->

       



 


</section>
        </div>

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/auction.blade.php ENDPATH**/ ?>