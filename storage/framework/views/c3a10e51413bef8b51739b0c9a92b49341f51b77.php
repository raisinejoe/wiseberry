<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Open Your Own Real Estate Business</h1>-->
<!--<h2>Join Us</h2>-->
<!--<h3>Real Estate Franchise Opportunity</h3>-->
        <div class="own-business">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner own-business back-100" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">OPEN YOUR OWN BUSINESS</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO BECOME A WISEBERRY FRANCHISEE.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a id="enquiry" class="btn btn-primary">ENQUIRE NOW</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent sell">
         <div class="container container1">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p1->heading); ?></h2>
                            <?php echo $p1->desc; ?>

                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom">
                           <!-- Image Box -->
                        
                            <img src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->



<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy stand sell mb-0" style="margin-top: 3em;">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center title">
                            <h2>WHAT WE STAND FOR</h2>
                            <?php echo $p2->desc; ?>

                        </div>
                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon1.png"> -->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <g>
        <path style="fill:#830B2C;" d="M722.1,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C819.8,718.8,775.9,762.6,722.1,762.6z M639.4,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H639.4z"/>
        <path style="fill:#830B2C;" d="M364.8,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C462.5,718.8,418.7,762.6,364.8,762.6z M282.2,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H282.2z"/>
        <path style="fill:#830B2C;" d="M455.2,672.3c-3.2,0-6.1-2.1-7.1-5.3l-83.3-286.6L281.6,667c-1.1,3.9-5.2,6.1-9.1,5s-6.1-5.2-5-9.1
            l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3c3.3,0,6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1C456.5,672.2,455.8,672.3,455.2,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C372.2,669,368.9,672.3,364.8,672.3z"/>
        <path style="fill:#830B2C;" d="M812.4,672.3c-3.2,0-6.1-2.1-7.1-5.3L722,380.4L638.8,667c-1.1,3.9-5.2,6.1-9.1,5
            c-3.9-1.1-6.1-5.2-5-9.1l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3s6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1
            C813.8,672.2,813.1,672.3,812.4,672.3z"/>
        <path style="fill:#830B2C;" d="M722.1,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C729.4,669,726.1,672.3,722.1,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C396.3,347.2,382.2,361.3,364.8,361.3z M364.8,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C381.6,320.6,374.1,313.1,364.8,313.1z"/>
        <path style="fill:#830B2C;" d="M722.1,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C753.5,347.2,739.4,361.3,722.1,361.3z M722.1,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C738.8,320.6,731.3,313.1,722.1,313.1z"/>
        <path style="fill:#830B2C;" d="M543.4,344.9c-21.9,0-39.7-17.8-39.7-39.7c0-21.9,17.8-39.7,39.7-39.7s39.7,17.8,39.7,39.7
            C583.1,327.1,565.3,344.9,543.4,344.9z M543.4,280.3c-13.7,0-24.9,11.2-24.9,24.9s11.2,24.9,24.9,24.9s24.9-11.2,24.9-24.9
            S557.2,280.3,543.4,280.3z"/>
        <path style="fill:#830B2C;" d="M388.9,337.2c-3.4,0-6.5-2.4-7.2-5.9c-0.8-4,1.8-7.9,5.8-8.7L509.7,298c4-0.8,7.9,1.8,8.7,5.8
            s-1.8,7.9-5.8,8.7l-122.2,24.6C389.9,337.2,389.4,337.2,388.9,337.2z"/>
        <path style="fill:#830B2C;" d="M697.9,337.2c-0.5,0-1,0-1.5-0.1l-122.2-24.6c-4-0.8-6.6-4.7-5.8-8.7s4.7-6.6,8.7-5.8l122.2,24.6
            c4,0.8,6.6,4.7,5.8,8.7C704.4,334.8,701.4,337.2,697.9,337.2z"/>
        <path style="fill:#830B2C;" d="M543.4,805.1c-4.1,0-7.4-3.3-7.4-7.4V337.5c0-4.1,3.3-7.4,7.4-7.4c4.1,0,7.4,3.3,7.4,7.4v460.2
            C550.8,801.8,547.5,805.1,543.4,805.1z"/>
    </g>
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="542.5" cy="541.5" r="523.8"/> -->
</g>
</svg>
                            <h2><?php echo e($p2->sub_head1); ?></h2>
                            <?php echo $p2->sub_desc1; ?>

                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon2.png"> -->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="530.1"/> -->
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#830B2C;" d="M534.4,505.8v-71.3c0-49.2-39.9-89.1-89.1-89.1
        s-89.1,39.9-89.1,89.1v35.6h-17.8v-35.6c0-59,47.9-106.9,106.9-106.9s106.9,47.9,106.9,106.9v71.3h178.2v249.5H409.6V505.8H534.4z
         M712.6,523.6H427.4v213.9h285.2V523.6z"/>
</g>
</svg>
                            <h2><?php echo e($p2->sub_head2); ?></h2>
                            <?php echo $p2->sub_desc2; ?>

                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon3.svg"> -->
<!-- Generator: Adobe Illustrator 24.3.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="534.1"/> -->
    <path style="fill:#830B2C;" d="M679,667.1h-23c0-35.7,1.4-40.9-20.3-45.9c-33.1-7.6-64.4-14.9-75.8-39.8
        c-4.1-9.1-6.9-24.1,3.6-43.9C587,493.1,592.9,455,579.6,433c-15.5-25.7-62.3-25.8-78,0.4c-13.3,22.3-7.3,60.2,16.4,103.9
        c10.7,19.7,8,34.8,3.9,43.9c-11.3,25.1-42.3,32.2-75.1,39.8c-22.6,5.2-21.2,10-21.2,46.1h-22.9v-14.3c0-29,2.3-45.7,36.6-53.6
        c38.7-9,77-17,58.6-50.9c-54.5-100.5-15.6-157.5,43-157.5c57.4,0,97.4,54.9,43,157.5c-17.8,33.7,19,41.7,58.6,50.9
        c34.3,7.9,36.6,24.7,36.6,53.7L679,667.1z M789.7,616.1c-29.7-6.9-57.3-12.9-43.9-38.2c40.8-77,10.8-118.1-32.2-118.1
        c-29.1,0-51.8,18.8-51.8,53.5c0,29.2,13.2,50.1,20.8,67.4h24.1c-3.9-17.3-33.8-59-17.6-86c9.5-15.9,39.2-16,48.7-0.3
        c8.8,14.5,4.2,41.7-12.3,72.8c-8.9,16.9-6.4,30-2.8,38c7,15.3,22.5,22.7,40,27.8c34.7,10.2,31.5,1,31.5,34.1h23v-10.7
        C817.2,634.7,815.5,622.1,789.7,616.1z M264.5,667.1h23c0-33.1-3.2-23.9,31.5-34.1c17.5-5.1,33-12.5,40-27.8c3.7-8,6.1-21.1-2.8-38
        c-16.5-31.1-21.1-58.3-12.3-72.8c9.4-15.7,39.2-15.7,48.7,0.3c16.1,27.1-13.7,68.8-17.6,86h24.1c7.6-17.3,20.8-38.1,20.8-67.4
        c0-34.8-22.6-53.6-51.8-53.6c-43.1,0-73,41.2-32.2,118.1c13.4,25.3-14.3,31.3-43.9,38.2c-25.8,5.9-27.5,18.5-27.5,40.3
        C264.5,656.5,264.5,667.1,264.5,667.1z"/>
</g>
</svg>
                            <h2><?php echo e($p2->sub_head3); ?></h2>
                            <?php echo $p2->sub_desc3; ?>

                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon4.png"> -->
<!-- Generator: Adobe Illustrator 24.3.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="533.6"/> -->
    <polygon style="fill:#830B2C;" points="493.1,685.2 364,561.3 404,520.1 492.4,604.4 693.8,398.3 734.6,438.7  "/>
</g>
</svg>
                            <h2><?php echo e($p2->sub_head4); ?></h2>
                            <?php echo $p2->sub_desc4; ?>

                        </div>

                    </div>
                   
             </div>
         </div>
     </section>

 
<!-- END SECTION WHY WISEBERRY -->

<!-- START THE WISEBERRY BUSINESS MODEL -->


<section class="popular-places home18 own-business-model">
            <div class="container-fluid p-0">
                <div class="sec-title">
                    <h2>THE WISEBERRY BUSINESS MODEL</h2>
                    <?php echo $p3->desc; ?>

                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 p-0">
                        <div class="image-hover-text-container">
                          <div class="image-hover-image">
                            <h1><?php echo e($p3->sub_head1); ?></h1>
                            <img src="images/model1.jpg">
                          </div>
                          <div class="image-hover-text">
                            <div class="image-hover-text-bubble">
                              <span class="image-hover-text-title"><?php echo e($p3->sub_head1); ?></span>
                              <?php echo $p3->sub_desc1; ?>

                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-0">
                        <div class="image-hover-text-container">
                          <div class="image-hover-image">
                            <h1><?php echo e($p3->sub_head2); ?></h1>
                            <img src="images/model2.jpg">
                          </div>
                          <div class="image-hover-text">
                            <div class="image-hover-text-bubble">
                              <span class="image-hover-text-title"><?php echo e($p3->sub_head2); ?></span>
                              <?php echo $p3->sub_desc2; ?>

                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-0">
                        <div class="image-hover-text-container">
                          <div class="image-hover-image">
                            <h1><?php echo e($p3->sub_head3); ?></h1>
                            <img src="images/model3.jpg">
                          </div>
                          <div class="image-hover-text">
                            <div class="image-hover-text-bubble">
                              <span class="image-hover-text-title"><?php echo e($p3->sub_head3); ?></span>
                              <?php echo $p3->sub_desc3; ?>

                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-0">
                        <div class="image-hover-text-container">
                          <div class="image-hover-image">
                            <h1><?php echo e($p3->sub_head4); ?></h1>
                            <img src="images/model4.jpg">
                          </div>
                          <div class="image-hover-text">
                            <div class="image-hover-text-bubble">
                              <span class="image-hover-text-title"><?php echo e($p3->sub_head4); ?></span>
                              <?php echo $p3->sub_desc4; ?>

                            </div>
                          </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

<!-- END THE WISEBERRY BUSINESS MODEL -->


<!-- START OUR BENEFITS -->

<section class="our-benifits">

    <div class="row">
        <div class="col-md-12">
            
            <div class="title">
                    <h2>OUR BENEFITS</h2>
                </div>

        </div>
    </div>
    <div class="list">
    <div class="flip-flop">
        <div class="fc-adv">
            <div class="image-animation-zoom">
                           <!-- Image Box -->
                        
                            <img src="images/technology.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
            <div class="fc-adv-txt-r">
                <h3>TECHNOLOGY</h3>
                <p>At Wiseberry, we partner ourselves with the latest technology
and pride ourselves in being innovative in our field. Our
ability to tailor-make platforms that meet our agents&apos; needs
keeps us one step ahead of the competition.</p>
            </div>
        </div>
    </div>
    <div class="flip-flop">
        <div class="fc-adv">
            <div class="fc-adv-txt-l">
                <h3>CULTURE</h3>
                <p>Above all, Wiseberry is a franchise grout that values culture and
ethics before anything else. Our values shape ourculture and lie at
the heart of who we are and what we do. We are committed to
providing the highest standards of business conduct not only in our
relationships with our clients, but also with each other.</p>
            </div>
            <div class="image-animation-zoom">
                           <!-- Image Box -->
                        
                            <img src="images/culture.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
        </div>
    </div>
    <div class="flip-flop">
        <div class="fc-adv">
            <div class="image-animation-zoom">
                           <!-- Image Box -->
                        
                            <img src="images/training.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
            <div class="fc-adv-txt-r">
                <h3>TRAINING</h3>
                <p>The recruitment and training processes at Wiseberry are one of the
most rigorous in the industry. Our focus on never-ending
development means that training is an ongoing part of our work. We
ensure that our agents continuously grow not in only skill, but also
personally, by equipping them with the skills and mindset needed to
succeed in a competitive industry.</p>
            </div>
        </div>
    </div>
    <div class="flip-flop">
        <div class="fc-adv">
            <div class="fc-adv-txt-l">
                <h3>MARKETING</h3>
                <p>Our corporate branding and colour palette gives our offices a leading
edge in their marketing, and helps them stand out from the crowd as
a premium choice to potential vendors, purchasers and the public.</p>
            </div>
            <div class="image-animation-zoom">
                           <!-- Image Box -->
                        
                            <img src="images/marketing.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
        </div>
    </div>
</div>
</section>


<!-- END OUR BENEFITS -->

<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 client-testimonial" style="padding: 8rem 0 8em;">
            <div>
                <div class="sec-title">
                    <h2>HEAR IT FROM OUR FRANCHISEES</h2>
                </div>



    <section class="testimonial text-center">
        <div class="container">

            <div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
             
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">
                            <!-- <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" /> -->
                            <h5>PROFESSIONAL AND HASSLE FREE</h5>
                            <p>Peter was very easy to work with and kept us informed on a regular basis throughout the sale of our house.
We were happy with the way Peter responded to any concerns or questions we had during the process.
The sale was made much easier by having an agent who was so easy to work with</p>
                            <h4>J.Smith - Buyer</h4>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <!-- <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" /> -->
                            <h5>PROFESSIONAL AND HASSLE FREE</h5>
                            <p>Peter was very easy to work with and kept us informed on a regular basis throughout the sale of our house.
We were happy with the way Peter responded to any concerns or questions we had during the process.
The sale was made much easier by having an agent who was so easy to work with</p>
                            <h4>J.Smith - Buyer</h4>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <!-- <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive" /> -->
                            <h5>PROFESSIONAL AND HASSLE FREE</h5>
                            <p>Peter was very easy to work with and kept us informed on a regular basis throughout the sale of our house.
We were happy with the way Peter responded to any concerns or questions we had during the process.
The sale was made much easier by having an agent who was so easy to work with</p>
                            <h4>J.Smith - Buyer</h4>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </section>


            </div>
        </section>
        <!-- END SECTION -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent bg-gray ceo">
         <div class="container container1">
             <div class="row">


                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom p3">
                           <!-- Image Box -->
                        
                            <img src="<?php echo e($img_path); ?><?php echo e($p4->image); ?>" class="img-responsive" alt="">
                            <!-- Badge -->
                        
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center p3">
                            <h2><?php echo e($p4->heading); ?></h2>
                              <?php echo $p4->desc; ?>

                            <a href="https://thomaslehoang.com/">FIND OUT MORE</a>
                            <ul class="ceo">
                                <li><a href="#"><img src="images/facebook.png"><!-- <i class="fa fa-facebook" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/insta.png"><!-- <i class="fa fa-instagram" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/linkedin.png"><!-- <i class="fa fa-linkedin" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/you-tube.png"><!-- <i class="fa fa-youtube" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/twitter.png"><!-- <i class="fa fa-twitter" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/6.png"><!-- <i class="fa fa-twitter" aria-hidden="true"></i> --></a></li>
                                <li><a href="#"><img src="images/7.png"><!-- <i class="fa fa-twitter" aria-hidden="true"></i> --></a></li>
                            </ul>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

<section class="notice-repair-section p-80" id="enq">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h1>REQUEST MORE INFORMATION</h1>
            <p>This is your time, this is your moment. Take the leap to controlling your own destiny.</p>
            <p>To get started, simply complete the enquiry form and we will send through a Franchisee Information Pack.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail_own_business" id="capt_form" method="POST">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>


                  <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  <div class="col-md-12">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary see-more">LET&apos;S GO!</button>
                  </div>

                </div>
             
              
            </form>
                    </div>
                </div>

            </div>
        </section>


        <div class="modal fade home-rent register-intrest" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">REGISTER YOUR INTEREST</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <p>Simply fill out the form on this page and we will have someone contact you!</p>

            <form action="#!">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Property Type*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Desired Region(s) or Suburb(s)*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Bed</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Bath</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Car</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Wiseberry Office*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Weekly Min Price</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Weekly Max Price</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Comments"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>
                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" required name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
        </div>

        


<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
    $("#enquiry").click(function() {
        $('html, body').animate({
            scrollTop: $("#enq").offset().top
        }, 2000);
    });
</script><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/own_business.blade.php ENDPATH**/ ?>