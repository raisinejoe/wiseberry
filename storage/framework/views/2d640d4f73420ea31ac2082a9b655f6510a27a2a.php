<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style type="text/css">
    ul.ui-autocomplete.ui-menu {
  z-index: 1000;
}
</style>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h3>Find An Agent Or An Office</h3>-->
<div class="rent-with-us">
    <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home">
                                 
                                 <h1 style="margin-left: 15px;"><span class="play">Rent</span> WITH WISEBERRY</h1>
                                 <div class="inner">
                                    <form method="POST" action="rent-results">
                                    
                                    <div class="row">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <input type="hidden" name="search_type" id="search_type" value="">
                                    <input type="hidden" name="pagename" id="pagename" value="rent">
                                        <div class="col-md-3 pr-0">
                                            <select class="form-control" id="page_option">
                                                <option value="1">Buy</option>
                                                <option value="2" selected>Rent</option>
                                                <option value="3">Holiday Rental</option>
                                                <option value="4">Commercial Rental</option>
                                                <option value="5">Sold</option>
                                                <option value="6">Leased</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input type="txt" id="tags" name="suburb_name" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3 pl-0">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <select class="form-control" id="buy_options" name="option">
                                                                    <option value="">Any Property Type*</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Studio</option>
                                                                    <option value="1">1 Bed or More</option>
                                                                    <option value="2">2 Bed or More</option>
                                                                    <option value="3">3 Bed or More</option>
                                                                    <option value="4">4 Bed or More</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option value="1">1 Bath or More</option>
                                                                    <option value="2">2 Bath or More</option>
                                                                    <option value="3">3 Bath or More</option>
                                                                    <option value="4">4 Bath or More</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option value="1">1 Car or More</option>
                                                                    <option value="2">2 Car or More</option>
                                                                    <option value="3">3 Car or More</option>
                                                                    <option value="4">4 Car or More</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Weekly Min Price</option>
                                                                    <option value="25">$25</option>
                                                                      <option value="50">$50</option>
                                                                      <option value="75">$75</option>
                                                                      <option value="100">$100</option>
                                                                      <option value="125">$125</option>
                                                                      <option value="150">$150</option>
                                                                      <option value="200">$200</option>
                                                                      <option value="250">$250</option>
                                                                      <option value="300">$300</option>
                                                                      <option value="350">$350</option>
                                                                      <option value="400">$400</option>
                                                                      <option value="450">$450</option>
                                                                      <option value="500">$500</option>
                                                                      <option value="600">$600</option>
                                                                      <option value="700">$700</option>
                                                                      <option value="800">$800</option>
                                                                      <option value="900">$900</option>
                                                                      <option value="1000">$1,000</option>
                                                                      <option value="1250">$1,250</option>
                                                                      <option value="150">$1,500</option>
                                                                      <option value="2000">$2,000</option>
                                                                      <option value="3000">$3,000</option>
                                                                      <option value="4000">$4,000</option>
                                                                      <option value="5000">$5,000</option>
                                                                      <option value="10000">$10,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Weekly Max Price</option>
                                                                    <option value="25">$25</option>
                                                                      <option value="50">$50</option>
                                                                      <option value="75">$75</option>
                                                                      <option value="100">$100</option>
                                                                      <option value="125">$125</option>
                                                                      <option value="150">$150</option>
                                                                      <option value="200">$200</option>
                                                                      <option value="250">$250</option>
                                                                      <option value="300">$300</option>
                                                                      <option value="350">$350</option>
                                                                      <option value="400">$400</option>
                                                                      <option value="450">$450</option>
                                                                      <option value="500">$500</option>
                                                                      <option value="600">$600</option>
                                                                      <option value="700">$700</option>
                                                                      <option value="800">$800</option>
                                                                      <option value="900">$900</option>
                                                                      <option value="1000">$1,000</option>
                                                                      <option value="1250">$1,250</option>
                                                                      <option value="150">$1,500</option>
                                                                      <option value="2000">$2,000</option>
                                                                      <option value="3000">$3,000</option>
                                                                      <option value="4000">$4,000</option>
                                                                      <option value="5000">$5,000</option>
                                                                      <option value="10000">$10,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div> 
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa1 fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">

            <div class="container">
                 <?php if(\Session::has('success')): ?>
                    <div class="alert alert-success" style="text-align: center;">
                        <?php echo \Session::get('success'); ?>

                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>LET US HELP YOU FIND THE RIGHT RENTAL PROPERTY.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <!-- <a href="#!">LET’S GO</a> -->
                        <!--<div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">LET’S GO</a></div>-->
                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalone">LET’S GO</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent pb6">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt2">
                            <h2><?php echo e($p1->heading); ?></h2>
                            <?php echo $p1->desc; ?>

                            <!-- <a href="#!">FIND OUT MORE</a> -->
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->



<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy stand">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center title">
                            <h2><?php echo e($p2->heading); ?></h2>
                            <!-- <h3>CAN I BOOK A PRIVATE INSPECTION?</h3> -->
                            <?php echo $p2->desc; ?>

                        </div>
                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!--<img src="images/about-icon1.png">-->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <g>
        <path style="fill:#830B2C;" d="M722.1,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C819.8,718.8,775.9,762.6,722.1,762.6z M639.4,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H639.4z"/>
        <path style="fill:#830B2C;" d="M364.8,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C462.5,718.8,418.7,762.6,364.8,762.6z M282.2,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H282.2z"/>
        <path style="fill:#830B2C;" d="M455.2,672.3c-3.2,0-6.1-2.1-7.1-5.3l-83.3-286.6L281.6,667c-1.1,3.9-5.2,6.1-9.1,5s-6.1-5.2-5-9.1
            l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3c3.3,0,6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1C456.5,672.2,455.8,672.3,455.2,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C372.2,669,368.9,672.3,364.8,672.3z"/>
        <path style="fill:#830B2C;" d="M812.4,672.3c-3.2,0-6.1-2.1-7.1-5.3L722,380.4L638.8,667c-1.1,3.9-5.2,6.1-9.1,5
            c-3.9-1.1-6.1-5.2-5-9.1l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3s6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1
            C813.8,672.2,813.1,672.3,812.4,672.3z"/>
        <path style="fill:#830B2C;" d="M722.1,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C729.4,669,726.1,672.3,722.1,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C396.3,347.2,382.2,361.3,364.8,361.3z M364.8,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C381.6,320.6,374.1,313.1,364.8,313.1z"/>
        <path style="fill:#830B2C;" d="M722.1,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C753.5,347.2,739.4,361.3,722.1,361.3z M722.1,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C738.8,320.6,731.3,313.1,722.1,313.1z"/>
        <path style="fill:#830B2C;" d="M543.4,344.9c-21.9,0-39.7-17.8-39.7-39.7c0-21.9,17.8-39.7,39.7-39.7s39.7,17.8,39.7,39.7
            C583.1,327.1,565.3,344.9,543.4,344.9z M543.4,280.3c-13.7,0-24.9,11.2-24.9,24.9s11.2,24.9,24.9,24.9s24.9-11.2,24.9-24.9
            S557.2,280.3,543.4,280.3z"/>
        <path style="fill:#830B2C;" d="M388.9,337.2c-3.4,0-6.5-2.4-7.2-5.9c-0.8-4,1.8-7.9,5.8-8.7L509.7,298c4-0.8,7.9,1.8,8.7,5.8
            s-1.8,7.9-5.8,8.7l-122.2,24.6C389.9,337.2,389.4,337.2,388.9,337.2z"/>
        <path style="fill:#830B2C;" d="M697.9,337.2c-0.5,0-1,0-1.5-0.1l-122.2-24.6c-4-0.8-6.6-4.7-5.8-8.7s4.7-6.6,8.7-5.8l122.2,24.6
            c4,0.8,6.6,4.7,5.8,8.7C704.4,334.8,701.4,337.2,697.9,337.2z"/>
        <path style="fill:#830B2C;" d="M543.4,805.1c-4.1,0-7.4-3.3-7.4-7.4V337.5c0-4.1,3.3-7.4,7.4-7.4c4.1,0,7.4,3.3,7.4,7.4v460.2
            C550.8,801.8,547.5,805.1,543.4,805.1z"/>
    </g>
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="542.5" cy="541.5" r="523.8"/> -->
</g>
</svg>
                            <h2>HONESTY & INTEGRITY</h2>
                            <p>At Wiseberry, this is the foundation we search for in our people. We seek to be transparent with our clients and we need people who tell you what you need to hear, rather than what you want to hear</p>
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon2.png"> -->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="530.1"/> -->
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#830B2C;" d="M534.4,505.8v-71.3c0-49.2-39.9-89.1-89.1-89.1
        s-89.1,39.9-89.1,89.1v35.6h-17.8v-35.6c0-59,47.9-106.9,106.9-106.9s106.9,47.9,106.9,106.9v71.3h178.2v249.5H409.6V505.8H534.4z
         M712.6,523.6H427.4v213.9h285.2V523.6z"/>
</g>
</svg>
                            <h2>TRUSTWORTHY</h2>
                            <p>A Wiseberry agent is, at all times, reliable and worthy of your
confidence. Trust is an indispensable part of a successful
relationship.</p>
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon3.svg"> -->
<!-- Generator: Adobe Illustrator 24.3.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="534.1"/> -->
    <path style="fill:#830B2C;" d="M679,667.1h-23c0-35.7,1.4-40.9-20.3-45.9c-33.1-7.6-64.4-14.9-75.8-39.8
        c-4.1-9.1-6.9-24.1,3.6-43.9C587,493.1,592.9,455,579.6,433c-15.5-25.7-62.3-25.8-78,0.4c-13.3,22.3-7.3,60.2,16.4,103.9
        c10.7,19.7,8,34.8,3.9,43.9c-11.3,25.1-42.3,32.2-75.1,39.8c-22.6,5.2-21.2,10-21.2,46.1h-22.9v-14.3c0-29,2.3-45.7,36.6-53.6
        c38.7-9,77-17,58.6-50.9c-54.5-100.5-15.6-157.5,43-157.5c57.4,0,97.4,54.9,43,157.5c-17.8,33.7,19,41.7,58.6,50.9
        c34.3,7.9,36.6,24.7,36.6,53.7L679,667.1z M789.7,616.1c-29.7-6.9-57.3-12.9-43.9-38.2c40.8-77,10.8-118.1-32.2-118.1
        c-29.1,0-51.8,18.8-51.8,53.5c0,29.2,13.2,50.1,20.8,67.4h24.1c-3.9-17.3-33.8-59-17.6-86c9.5-15.9,39.2-16,48.7-0.3
        c8.8,14.5,4.2,41.7-12.3,72.8c-8.9,16.9-6.4,30-2.8,38c7,15.3,22.5,22.7,40,27.8c34.7,10.2,31.5,1,31.5,34.1h23v-10.7
        C817.2,634.7,815.5,622.1,789.7,616.1z M264.5,667.1h23c0-33.1-3.2-23.9,31.5-34.1c17.5-5.1,33-12.5,40-27.8c3.7-8,6.1-21.1-2.8-38
        c-16.5-31.1-21.1-58.3-12.3-72.8c9.4-15.7,39.2-15.7,48.7,0.3c16.1,27.1-13.7,68.8-17.6,86h24.1c7.6-17.3,20.8-38.1,20.8-67.4
        c0-34.8-22.6-53.6-51.8-53.6c-43.1,0-73,41.2-32.2,118.1c13.4,25.3-14.3,31.3-43.9,38.2c-25.8,5.9-27.5,18.5-27.5,40.3
        C264.5,656.5,264.5,667.1,264.5,667.1z"/>
</g>
</svg>
                            <h2>PEOPLE PERSON</h2>
                            <p>We want our agents to care, to treat the sale of your home as
if it was their parents', and to take the time to evaluate your
needs.</p>
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!-- <img src="images/about-icon4.svg"> -->
<!-- Generator: Adobe Illustrator 24.3.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="533.6"/> -->
    <polygon style="fill:#830B2C;" points="493.1,685.2 364,561.3 404,520.1 492.4,604.4 693.8,398.3 734.6,438.7  "/>
</g>
</svg>
                            <h2>ACCOUNTABILITY</h2>
                            <p>We hand pick people who understand the responsibility that
comes with the selling of your home. Wiseberry encourages
you to discuss with us any concerns you may have with our
agents so that we may continue to meet your needs and the
high standards we set for ourselves.</p>
                        </div>

                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 rent pb6">
            <div class="container-fluid">
                <div class="sec-title pb2">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/<?php echo e($value->id); ?>" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="<?php echo e($img_path); ?><?php echo e($value->featured_image); ?>" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>RENT</h6>
                                        <a href="individual_blog/<?php echo e($value->id); ?>"><h3><?php echo e($value->name); ?></h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center mt2">
                            <a href="blogs" class="read-more mt0">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        
        <!-- The Modal -->
  <div class="modal fade home-rent register-intrest" id="myModalone">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">REGISTER YOUR INTEREST</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body ui-front">
            <p>Simply fill out the form on this page and we will have someone contact you!</p>

            <form action="sendmail_rent_with_us" id="capt_form" method="POST">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="fname" placeholder="First Name*">
                      <input type="hidden" class="form-control" value="rent-with-us" name="pagename" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" required class="form-control" name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" required class="form-control" name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="property" id="buy_options_dup">
                          <option value="1">House</option>
                          <option value="2">Townhouse/Duplex</option>
                          <option value="3">Apartment & Unit</option>
                          <option value="4">Vacant Land</option>
                          <option value="5">Acreage/Rural</option>
                          <option value="6">Commercial</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" id="subtags"  placeholder="Desired Region(s) or Suburb(s)*">
                      <input type="hidden" class="form-control" id="subtags_val" name="region" placeholder="Desired Region(s) or Suburb(s)*">
                    </div>
                  </div>

                  <div class="col-md-6" style="padding:0 30px;">
                    <div class="row">
                        <div class="col-md-4 first">
                        <div class="form-group">
                      <select class="form-control" name="bed">
                          <option value="">Bed</option>
                          <option value="1 Bed or More">1 Bed or More</option>
                          <option value="2 Bed or More">2 Bed or More</option>
                          <option value="3 Bed or More">3 Bed or More</option>
                          <option value="4 Bed or More">4 Bed or More</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4 second">
                        <div class="form-group">
                      <select class="form-control" name="bath">
                            <option value="">Bath</option>
                          <option value="1 Bath or More">1 Bath or More</option>
                          <option value="2 Bath or More">2 Bath or More</option>
                          <option value="3 Bath or More">3 Bath or More</option>
                          <option value="4 Bath or More">4 Bath or More</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4 third">
                        <div class="form-group">
                      <select class="form-control" name="car">
                        <option value="">Car</option>
                          <option value="1 Car or More">1 Car or More</option>
                          <option value="2 Car or More">2 Car or More</option>
                          <option value="3 Car or More">3 Car or More</option>
                          <option value="4 Car or More">4 Car or More</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                            <option value="All">Wiseberry Office *</option>
                            <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                            <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                            <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                            <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                            <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                            <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                            <option value="Wiseberry Dural">Wiseberry Dural</option>
                            <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                            <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                            <option value="Wiseberry Forster">Wiseberry Forster</option>
                            <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                            <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                            <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                            <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                            <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                            <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                            <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                            <option value="Wiseberry Picton ">Wiseberry Picton </option>
                            <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                            <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                            <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                            <option value="Wiseberry Taree">Wiseberry Taree</option>
                            <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                            <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6" style="padding:0 15px;">
                    <div class="row">
                        <div class="col-md-6 first">
                        <div class="form-group">
                      <select class="form-control" name="min_price">
                          <option value="">Weekly Min Price</option>
                          <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1,000">$1,000</option>
                          <option value="1,250">$1,250</option>
                          <option value="1,50">$1,50</option>
                          <option value="2,000">$2,000</option>
                          <option value="3,000">$3,000</option>
                          <option value="4,000">$4,000</option>
                          <option value="5,000">$5,000</option>
                          <option value="10,000">$10,000</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-6 second">
                        <div class="form-group">
                      <select class="form-control" name="max_price">
                          <option value="">Weekly Max Price</option>
                          <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1,000">$1,000</option>
                          <option value="1,250">$1,250</option>
                          <option value="1,50">$1,50</option>
                          <option value="2,000">$2,000</option>
                          <option value="3,000">$3,000</option>
                          <option value="4,000">$4,000</option>
                          <option value="5,000">$5,000</option>
                          <option value="10,000">$10,000</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  
                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
  
</div>
        
        
        
        
        

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/rent_with_us.blade.php ENDPATH**/ ?>