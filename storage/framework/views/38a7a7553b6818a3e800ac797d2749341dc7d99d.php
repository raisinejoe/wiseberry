<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style>
    #subscribe_ind{
        background-color: #830b2c !important;
    color: #ffffff;
    border: none;
    padding: 7px 35px !important;
    }
        .modal-backdrop 
        background-color: initial;
    }

    .modal.fade.home-rent.map.floorplan .modal-dialog .modal-body {
	    height: 60em !important;
	    overflow: hidden;
	}
    /*.social-btn-sp #social-links {*/
    /*    margin: 0 auto;*/
    /*    max-width: 500px;*/
    /*}*/
    /*.social-btn-sp #social-links ul li {*/
    /*    display: inline-block;*/
    /*}          */
    /*.social-btn-sp #social-links ul li a {*/
    /*    padding: 15px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 30px;*/
    /*}*/
    /*table #social-links{*/
    /*    display: inline-table;*/
    /*}*/
    /*table #social-links ul li{*/
    /*    display: inline;*/
    /*}*/
    /*table #social-links ul li a{*/
    /*    padding: 5px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 15px;*/
    /*    background: #e3e3ea;*/
    /*}*/
    /*.fab {*/
    /*    font-family: "Font Awesome 5 Brands" !important;*/
    /*    font-size: 23px !important;*/
    /*    color: #0d6efd !important;*/
    /*}*/

    /*.share-button .btn {*/
    /*    background-color: #830b2c;*/
    /*    color: #ffffff;*/
    /*    border-radius: 0;*/
    /*    width: 130px;*/
    /*}*/
    
</style>
<div class="clearfix"></div>
        
       <div class="individual-result">
           <section class="individual-rental-slider header-image home-18 d-flex align-items-center" style="background: none;">
            <input type="hidden" id="tags" name="">
           <!--<h2>Properties For Sale In �Suburb� (Example: Properties For Sale In Castle Hill NSW 2154)</h2>-->
            <!-- partial:index.partial.html -->
<div id="wrap" class="my-5">
    <div class="row">
        <div class="col-12">

            <!-- Carousel -->
            <div id="carousel" class="carousel slide gallery" data-ride="carousel">
                <div class="carousel-inner">
                    <?php 
                        $z=0;
                        foreach ($result['assets'] as $keya => $valuea){ 
                            if($z==0){
                                $act="active";
                            }else{
                                $act=""; 
                            }

                            if($valuea['floorplan'] == 0){
                    ?>    
                    <div class="carousel-item <?php echo e($act); ?>" data-slide-number="<?php echo e($z); ?>" data-toggle="lightbox" data-gallery="gallery" data-remote="<?php echo e($valuea['url']); ?>">
                        <img src="<?php echo e($valuea['url']); ?>" class="d-block w-100" alt="...">
                    </div>
                    <?php $z++; } } ?>
                </div>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <a class="carousel-fullscreen" href="#carousel" role="button">
                    <span class="carousel-fullscreen-icon" aria-hidden="true"></span>
                    <span class="sr-only">Fullscreen</span>
                </a>
                <!-- <a class="carousel-pause play" href="#carousel" role="button">
                    <span class="carousel-pause-icon" aria-hidden="true"></span>
                    <span class="sr-only">Pause</span>
                </a> -->
            </div>

            <div id="carousel-thumbs" class="carousel slide" data-ride="carousel"  data-interval="5000">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-slide-number="0">
                        <div class="row mx-0">
                            <?php 
                                $z=0;
                                foreach ($result['assets'] as $keya => $valuea){
                                    if($z < 8){ 
                                    if($z==0){
                                        $act="selected";
                                    }else{
                                        $act=""; 
                                    }
                                    if($valuea['floorplan'] == 0){
                            ?>  
                                        <div id="carousel-selector-<?php echo e($z); ?>" class="thumb col-3 px-1 py-2 <?php echo e($act); ?>" data-target="#carousel" data-slide-to="<?php echo e($z); ?>">
                                            <img src="<?php echo e($valuea['url']); ?>" class="img-fluid" alt="...">
                                        </div>
                            <?php $z++; unset($result['assets'][$keya]); } } } ?>
                        </div>
                    </div>
                    <div class="carousel-item" data-slide-number="1">
                        <div class="row mx-0">
                            <?php 
                                $zz=$z+1;
                                foreach ($result['assets'] as $keya => $valuea){ 
                                    if($zz==1){
                                        $act="selected";
                                    }else{
                                        $act=""; 
                                    }
                                    if($valuea['floorplan'] == 0){
                            ?>  
                                        <div id="carousel-selector-<?php echo e($zz); ?>" class="thumb col-3 px-1 py-2 <?php echo e($act); ?>" data-target="#carousel" data-slide-to="<?php echo e($zz); ?>">
                                            <img src="<?php echo e($valuea['url']); ?>" class="img-fluid" alt="...">
                                        </div>
                            <?php $zz++; } } ?>
                        </div>
                    </div>
                    <!-- <div class="carousel-item " data-slide-number="1">
                        <div class="row mx-0">
                            <div id="carousel-selector-4" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="4">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-5" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="5">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-6" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="6">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-7" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="7">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-8" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="8">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-9" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="9">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-10" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="10">
                                <img src="<?php echo e($img_path); ?>slider3.jpg" class="img-fluid" alt="...">
                            </div>
       
                            <div id="carousel-selector-11" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="11">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item" data-slide-number="2">
                        <div class="row mx-0">
                            <div id="carousel-selector-8" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="8">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-9" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="9">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-10" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="10">
                                <img src="<?php echo e($img_path); ?>slider3.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-11" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="11">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-0" class="thumb col-3 px-1 py-2 selected" data-target="#carousel" data-slide-to="0">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-1" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="1">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-2" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="2">
                                <img src="<?php echo e($img_path); ?>list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-3" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="3">
                                <img src="<?php echo e($img_path); ?>slider2.jpg" class="img-fluid" alt="...">
                            </div>
                        </div>
                    </div> -->
                </div>
                <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- partial -->
       </section>


        <!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0  individual-rental individual-agent-one individual-office list">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="sec-title pt1">
                        	<?php 
                        		if(isset($result['marketing_text'])){ 
                        			foreach ($result['marketing_text'] as $key_mark => $value_mark) {
                        				if($value_mark['is_heading'] == 1){
            				?>
                            	<h2><?php echo e($value_mark['text']); ?></h2>
                        	<?php } } } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-xs-12">

                        <div class="details">
                            <input type="hidden" name="latitude" value="<?php echo e($result['property']['address']['latitude']); ?>" id="latitude">
                            <input type="hidden" name="longitude" value="<?php echo e($result['property']['address']['longitude']); ?>" id="longitude">
                            <h1 class="title"><?php echo e($result['property']['address']['full_address']); ?></h1>
                            <ul class="subtitle">
                                <?php
                                $array=$result['property']['features'];
                                ?>    
                                <?php foreach ($result['property']['features'] as $keys => $values){
                                        $img="";
                                        if($values['name'] == 'Bedroom'){
                                            $img="bed.svg";
                                        }
                                        if($values['name'] == 'Bathroom'){
                                            $img="bath.svg";
                                        }

                                        if($values['name'] == 'Parking'){
                                            $img="car.svg";
                                        }

                                        if($values['name'] == 'Garages'){
                                            $img="list-propertiy-icon3.svg";
                                        }
                                        if($values['name'] == 'Conditioning'){
                                            $img="list-propertiy-icon5.svg";
                                        }
                                        if($values['name'] == 'Toilet'){
                                            $img="list-propertiy-icon7.svg";
                                        }
                                        if($values['name'] == 'Fireplace'){
                                            $img="list-propertiy-icon8.svg";
                                        }
                                        if($values['name'] == 'Tennis Court'){
                                            $img="list-propertiy-icon9.svg";
                                        }
                                        if($values['name'] == 'Gym'){
                                            $img="list-propertiy-icon10.svg";
                                        }
                                        if($values['name'] == 'Outdoor Entertaining'){
                                            $img="list-propertiy-icon11.svg";
                                        }
                                        if($values['name'] == 'Living Areas'){
                                            $img="list-propertiy-icon12.svg";
                                        }
                                        if($values['name'] == 'Heating'){
                                            $img="list-propertiy-icon14.svg";
                                        }
                                        if($values['name'] == 'Alarm System'){
                                            $img="list-propertiy-icon13.svg";
                                        }
                                        if($values['name'] == 'Shed'){
                                            $img="list-propertiy-icon15.svg";
                                        }
                                        if($values['name'] == 'Pool'){
                                            $img="list-propertiy-icon16.svg";
                                        }
                                        if($values['name'] == 'Study'){
                                            $img="list-propertiy-icon17.svg";
                                        }
                                        if($values['name'] == 'Land Size'){
                                            $img="list-propertiy-icon4.svg";
                                        }
                                        if($values['name'] == 'Living Room'){
                                            $img="livingroom.svg";
                                        }
                                     ?>
                                <?php if($values['name'] == 'Bedroom' && $values['qty'] != 0){ ?>
                                    <li style="list-style-type: none;order:1"><h1><?php echo e($values['qty']); ?></h1> <span><img src="<?php echo e($img_path); ?>bed.svg" alt=""></span></li>
                                <?php }else if($values['name'] == 'Bathroom' && $values['qty'] != 0){ ?>
                                    <li style="list-style-type: none;order:2"><h1><?php echo e($values['qty']); ?></h1> <span><img src="<?php echo e($img_path); ?>bath.svg" alt=""></span></li>
                                <?php }else if($values['name'] == 'Parking' && $values['qty'] != 0){ ?>
                                    <li style="list-style-type: none;order: 3"><h1><?php echo e($values['qty']); ?></h1> <span><img src="<?php echo e($img_path); ?>car.svg" alt=""></span></li>
                                <?php }else if($values['name'] == 'Land Size'){ ?>
                                    <li style="list-style-type: none;order: 4"><h1><?php echo e($values['feature_menu_input']); ?> m2</h1> <span><img src="<?php echo e($img_path); ?>list-propertiy-icon4.svg" alt=""></span></li>
                                <?php } } ?>
                                <!-- 
                                <li><h1><?php echo e($result['property']['features'][1]['qty']  ?? '-'); ?></h1> <span><img src="<?php echo e($img_path); ?>list-propertiy-icon2.svg" alt=""></span></li>
                                <li><h1><?php echo e($result['property']['features'][2]['qty']  ?? '-'); ?></h1> <span><img src="<?php echo e($img_path); ?>list-propertiy-icon3.svg" alt=""></span></li>
                                <li><img src="<?php echo e($img_path); ?>list-propertiy-icon4.svg" alt="" /> <h1><?php echo e($result['assets'][0]['size']  ?? '-'); ?>m<sup>2</sup></h1></li> -->
                            </ul>
                            <p><?php echo e($result['price_view']  ?? ''); ?></p>
                            <?php 
                        		if(isset($result['marketing_text'])){ 
                        			foreach ($result['marketing_text'] as $key_mark => $value_mark) {
                        				if($value_mark['is_description'] == 1){
            				?>
                            	<p><?php echo nl2br($value_mark['text']); ?></p>
                        	<?php } } } ?>
                            <!-- <p>Features include:</p>
                            <ul class="list">
                                <li>Brick and tile home</li>
                                <li>4 bedrooms with built ins</li>
                                <li>DLUG with drive thru access</li>
                                <li>Reverse cycle air conditioning</li>
                                <li>Fully fenced 838sqm block</li>
                                <li>Short stroll to school and park</li>
                            </ul> -->

                            <div class="properties-features">
                                <div class="row">

                                    <div class="col-md-12">
                                        <h1 class="title" style="    padding: 0 0 10px;">PROPERTY FEATURES</h1>
                                    </div>

                                    <?php
                                        if (isset($array[1]['name']) && $array[1]['name'] == "Bedroom" && $array[1]['qty'] != 0)
                                        {
                                    ?>
                                    <div class="col-md-3">
                                        <p><?php echo e($array[1]['qty']); ?> <span><img style="top: -2px;" src="<?php echo e($img_path); ?>bed.svg" alt="" /></span><?php echo e($array[1]['name']); ?></p>
                                    </div>
                                    <?php   
                                            unset($array[1]);
                                        }
                                    ?>

                                    <?php foreach ($array as $keys => $values){
                                        $img="";
                                        if($values['name'] == 'Bedroom'){
                                            $img="bed.svg";
                                        }
                                        if($values['name'] == 'Bathroom'){
                                            $img="bath.svg";
                                        }

                                        if($values['name'] == 'Parking'){
                                            $img="car.svg";
                                        }

                                        if($values['name'] == 'Garages'){
                                            $img="list-propertiy-icon3.svg";
                                        }
                                        if($values['name'] == 'A/C'){
                                            $img="list-propertiy-icon5.svg";
                                        }
                                        if($values['name'] == 'Toilet'){
                                            $img="list-propertiy-icon7.svg";
                                        }
                                        if($values['name'] == 'Fireplace'){
                                            $img="list-propertiy-icon8.svg";
                                        }
                                        if($values['name'] == 'Tennis Court'){
                                            $img="list-propertiy-icon9.svg";
                                        }
                                        if($values['name'] == 'Gym'){
                                            $img="list-propertiy-icon10.svg";
                                        }
                                        if($values['name'] == 'Outdoor Entertaining'){
                                            $img="list-propertiy-icon11.svg";
                                        }
                                        if($values['name'] == 'Living Areas'){
                                            $img="list-propertiy-icon12.svg";
                                        }
                                        if($values['name'] == 'Heating'){
                                            $img="list-propertiy-icon14.svg";
                                        }
                                        if($values['name'] == 'Alarm System'){
                                            $img="list-propertiy-icon13.svg";
                                        }
                                        if($values['name'] == 'Shed'){
                                            $img="list-propertiy-icon15.svg";
                                        }
                                        if($values['name'] == 'Pool'){
                                            $img="list-propertiy-icon16.svg";
                                        }
                                        if($values['name'] == 'Study'){
                                            $img="list-propertiy-icon17.svg";
                                        }
                                        if($values['name'] == 'Land Size'){ 
                                            $img="list-propertiy-icon4.svg";
                                        }
                                        if($values['name'] == 'Living Room'){
                                            $img="livingroom.svg";
                                        }
                                     ?>
                                     <?php 
                                        if($img != "" && $values['qty'] != 0){ 
                                            if($values['name'] == 'Land Size'){
                                     ?>
                                        <div class="col-md-3">
                                            <p><span><img style="top: -2px;" src="<?php echo e($img_path); ?>list-propertiy-icon4.svg" alt="" /></span><?php echo e($values['feature_menu_input']); ?> m2 &nbsp;  <?php echo e($values['name']); ?></p>
                                        </div>
                                    <?php }else{ ?>

                                        <div class="col-md-3">
                                            <p><span><img style="top: -2px;" src="<?php echo e($img_path); ?><?php echo e($img); ?>" alt="" /></span><?php echo e($values['qty']); ?> &nbsp; <?php echo e($values['name']); ?></p>
                                        </div>
                                     <?php }}} ?>
                                </div>
                            </div>

                                <div class="row team-all">
                                    <?php if(!empty($result['agents'] && $result['agents'] != null)){ foreach ($result['agents'] as $key => $value){ ?>
                                    <div class="col-md-5 team-pro">
                                        <div class="team-wrap">
                                            <a href="../individual-agent/<?php echo e($value['id']); ?>">
                                            <div class="team-img">
                                                <img src="<?php echo e($value['profile_picture']); ?>" alt="" />
                                            </div>
                                            </a>
                                            <div class="team-content-persons">
                                                <div class="team-info">
                                                    <a href="../individual-agent/<?php echo e($value['id']); ?>">
                                                        <h3><?php echo e($value['first_name'] ?? ''); ?> <?php echo e($value['last_name'] ?? ''); ?></h3>
                                                    </a>
                                                    <p><?php echo e($value['position'] ?? ''); ?></p>
                                                    <p><?php echo e($value['organisation']['name'] ?? ''); ?></p>
                                                    <p><a href="tel:<?php echo e($value['phones'][0]['value'] ?? ''); ?>"><?php echo e($value['phones'][0]['value'] ?? ''); ?></a></p>
                                                    <div class="team-socials">
                                                        <ul>
                                                            <li>
                                                                <a href="mailto:<?php echo e($value['emails'][0]['value'] ?? ''); ?>" title="mail"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg><!-- <img src="<?php echo e($img_path); ?>mail.png"><i class="fa fa-envelope" aria-hidden="true"></i> --></a>
                                                                <a href="../downloadcard/<?php echo e($value['first_name']  ?? '-'); ?>_<?php echo e($value['phones'][0]['value']  ?? '-'); ?>" title="twitter"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
    .st1{font-family:'Arial-BoldMT';}
    .st2{font-size:655.684px;}
</style>
<g>
    <path class="st0" d="M540,101c242,0,439,196.9,439,439S782,979,540,979S101,782,101,540S298,101,540,101z M540,13.3
        C249.1,13.3,13.3,249.1,13.3,540s235.9,526.7,526.7,526.7s526.7-235.9,526.7-526.7S830.9,13.3,540,13.3z"/>
    <text transform="matrix(1 0 0 1 335.5405 807.1714)" class="st0 st1 st2">V</text>
</g>
</svg><!-- <img src="<?php echo e($img_path); ?>v.png"><i class="fa fa-vimeo" aria-hidden="true"></i> --></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } } ?>


                                    <div class="col-md-2 team-pro"></div>
                                    
                                </div>

                        </div>

                    </div>
                    <div class="col-lg-3 col-md-4 car">
                        
                        <div class="side-bar">
                            
                            <ul class="button-list">
                                <li>
                                    <a href="#!" id="share-option" class="firts"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.1c244.8,0,443.9,199.1,443.9,443.9S784.8,983.9,540,983.9S96.1,784.8,96.1,540S295.2,96.1,540,96.1z
     M540,7.3C245.8,7.3,7.3,245.8,7.3,540s238.5,532.7,532.7,532.7s532.7-238.5,532.7-532.7S834.2,7.3,540,7.3z M273.7,762
    c67.2-292.4,310.7-345.3,310.7-345.3V318l222,196.4l-222,198.2V614C584.4,614,416.6,608.9,273.7,762z"/>
</svg><!-- <i class="fa fa-share" aria-hidden="true"></i> --> Share Property</a>
                            <div class="social-btn-sp" style="display: none;">
                                <?php echo $shareButtons; ?>

                            </div>
                                </li>
                                <?php 
                                  $f=1;  foreach ($result['assets'] as $keyf => $valuef){ 
                                        if($valuef['floorplan'] == "1" && $f==1){
                                  ?>
                                    <li>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalone">FLOORPLAN</button>
                                    </li>

                                  <?php $f++; } } ?>  
                                <?php if(isset($result['property']['address']['latitude']) && $result['property']['address']['latitude'] != null){ ?>
                                    <li>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">MAP</button>
                                    </li>
                                <?php } ?>
                                <input type="hidden" id="ind_org_idd" value="<?php echo e($result['organisation']['id'] ?? ''); ?>">
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal_s">ENQUIRY NOW</a>
                                </li>
                            </ul>
                            <?php 
                                if(isset($result['open_homes'][0])){ 

                                    $exp=explode(" ", $result['open_homes'][0]);

                                    if(isset($exp[0])){
                                       $timestamp =  strtotime($exp[0]);
                                       $day = date('D d M', $timestamp);
                                    }
                                    if(isset($exp[1])){
                                       // $timestamp =  strtotime($exp[0]);
                                       $time = date('H:i', strtotime($result['open_homes'][0]));
                                    }

                            ?>
                                <h1 class="title">NEXT OPEN HOME</h1>
                                <ul class="time">

                                    <li style="text-transform: uppercase;">
                                        <span><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
                                        <style type="text/css">
                                            .st0{fill:#830B2C;}
                                        </style>
                                        <path class="st0" d="M888.3,888.3H714.1V714.1h174.1V888.3z M627.1,452.9H452.9v174.1h174.1L627.1,452.9L627.1,452.9z M888.3,452.9
                                            H714.1v174.1h174.1V452.9z M365.9,714.1H191.7v174.1h174.1V714.1z M627.1,714.1H452.9v174.1h174.1L627.1,714.1L627.1,714.1z
                                             M365.9,452.9H191.7v174.1h174.1V452.9z M1062.4,104.7v957.7H17.6V104.7h130.6v43.5c0,48,39.1,87.1,87.1,87.1s87.1-39.1,87.1-87.1
                                            v-43.5h435.3v43.5c0,48,39,87.1,87.1,87.1c48,0,87.1-39.1,87.1-87.1v-43.5C931.8,104.7,1062.4,104.7,1062.4,104.7z M975.3,365.9
                                            H104.7v609.5h870.7L975.3,365.9L975.3,365.9z M888.3,61.1c0-24-19.5-43.5-43.5-43.5s-43.5,19.5-43.5,43.5v87.1
                                            c0,24,19.5,43.5,43.5,43.5s43.5-19.5,43.5-43.5V61.1z M278.8,148.2c0,24-19.5,43.5-43.5,43.5s-43.5-19.5-43.5-43.5V61.1
                                            c0-24,19.5-43.5,43.5-43.5s43.5,19.5,43.5,43.5V148.2z"/>
                                        </svg> <?php echo e($day); ?></span>
                                    </li>
                                    <li>
                                        <span><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
                                        <style type="text/css">
                                            .st0{fill:#830B2C;}
                                        </style>
                                        <path class="st0" d="M540,98.7c243.3,0,441.2,197.9,441.2,441.2S783.3,981.2,540,981.2S98.8,783.3,98.8,540S296.7,98.7,540,98.7z
                                             M540,10.5C247.6,10.5,10.5,247.6,10.5,540s237.1,529.5,529.5,529.5s529.5-237.1,529.5-529.5S832.4,10.5,540,10.5z M584.1,540V275.2
                                            h-88.2v353h308.9V540H584.1z"/>
                                        </svg> <?php echo e($time); ?></span>
                                    </li>
                                </ul>


                            <?php } ?>

                            <?php 
                                if($result['status']['id'] == 4 && $result['auction_date'] != null){ 

                                    $exp=explode(" ", $result['auction_date']);

                                    if(isset($exp[0])){
                                       $timestamp =  strtotime($exp[0]);
                                       $day = date('D d M', $timestamp);
                                    }
                                    

                            ?>
                                <h1 class="title">AUCTION DATE</h1>
                                <ul class="time">

                                    <li style="text-transform: uppercase;">
                                        <span><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
                                        <style type="text/css">
                                            .st0{fill:#830B2C;}
                                        </style>
                                        <path class="st0" d="M888.3,888.3H714.1V714.1h174.1V888.3z M627.1,452.9H452.9v174.1h174.1L627.1,452.9L627.1,452.9z M888.3,452.9
                                            H714.1v174.1h174.1V452.9z M365.9,714.1H191.7v174.1h174.1V714.1z M627.1,714.1H452.9v174.1h174.1L627.1,714.1L627.1,714.1z
                                             M365.9,452.9H191.7v174.1h174.1V452.9z M1062.4,104.7v957.7H17.6V104.7h130.6v43.5c0,48,39.1,87.1,87.1,87.1s87.1-39.1,87.1-87.1
                                            v-43.5h435.3v43.5c0,48,39,87.1,87.1,87.1c48,0,87.1-39.1,87.1-87.1v-43.5C931.8,104.7,1062.4,104.7,1062.4,104.7z M975.3,365.9
                                            H104.7v609.5h870.7L975.3,365.9L975.3,365.9z M888.3,61.1c0-24-19.5-43.5-43.5-43.5s-43.5,19.5-43.5,43.5v87.1
                                            c0,24,19.5,43.5,43.5,43.5s43.5-19.5,43.5-43.5V61.1z M278.8,148.2c0,24-19.5,43.5-43.5,43.5s-43.5-19.5-43.5-43.5V61.1
                                            c0-24,19.5-43.5,43.5-43.5s43.5,19.5,43.5,43.5V148.2z"/>
                                        </svg> <?php echo e($day); ?></span>
                                    </li>
                                    
                                </ul>


                            <?php } ?>


                            <h1 class="title">OFFICE DETAILS</h1>

                            <h2 class="subtitle"><?php echo e($result['organisation']['name']  ?? '-'); ?></h2>
                            <?php if(isset($result['organisation']['address']['full_address'])){ ?>
                            <?php $__currentLoopData = explode(',', $result['organisation']['address']['full_address']); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <p><?php echo e($info  ?? '-'); ?></p>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php } ?>
                            <!-- <p><?php echo e($result['agents'][0]['organisation']['address']['full_address']  ?? '-'); ?></p> -->
                            <!-- <p>GOROKAN NSW 2156</p> -->
                            <ul class="mail">
                                <li>
                                    <a href="../downloadcard/<?php echo e($result['organisation']['name']  ?? '-'); ?>_<?php echo e($result['organisation']['phones'][0]['value']  ?? ''); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.2c245.2,0,444.8,199.5,444.8,444.8S785.2,984.8,540,984.8S95.2,785.2,95.2,540S294.8,95.2,540,95.2z
     M540,6.3C245.3,6.3,6.3,245.3,6.3,540s239,533.7,533.7,533.7s533.7-239,533.7-533.7S834.7,6.3,540,6.3z M740.1,776.2l-78.3-151.1
    L616,647.6c-49.8,24.2-151.2-173.9-102.5-200l46.3-22.8l-77.7-151.6l-46.8,23.1c-160.2,83.5,94.2,577.8,258,502.9L740.1,776.2z"/>
</svg><!--<img src="<?php echo e($img_path); ?>phone.png"><i class="fa fa-phone" aria-hidden="true"></i> --> <?php echo e($result['organisation']['phones'][0]['value']  ?? '-'); ?></a>
                                </li>
                                <li>
                                    <a href="mailto:<?php echo e($result['organisation']['emails'][0]['value']  ?? ''); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg><!--<img src="<?php echo e($img_path); ?>mail.png"><i class="fa fa-envelope" aria-hidden="true"></i> --> <?php echo e($result['organisation']['emails'][0]['value']  ?? '-'); ?></a>
                                </li>
                            </ul>
                            
                                <label>SUBSCRIBE NOW</label>
                                <input type="email" name="" id="subscribe_ind_id" class="form-control" placeholder="Enter an email address">
                                <button id="subscribe_ind" data-toggle="modal" data-target="#myModal_s">Subscribe</button>
                            
                            <ul class="social">
                                <li>
                                    <a href="<?php echo e($result['organisation']['facebook'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,97.5C784,97.5,982.5,296,982.5,540c0,244-198.5,442.5-442.5,442.5S97.5,784,97.5,540
    C97.5,296,296,97.5,540,97.5z M540,9C246.7,9,9,246.7,9,540c0,293.2,237.8,531,531,531s531-237.8,531-531C1071,246.7,833.3,9,540,9z
     M451.5,451.5H363V540h88.5v265.5h132.8V540h80.5l8-88.5h-88.5v-36.9c0-21.2,4.2-29.5,24.7-29.5h63.8V274.5H566.4
    c-79.6,0-114.9,35-114.9,102.1L451.5,451.5L451.5,451.5z"/>
</svg><!-- <img src="<?php echo e($img_path); ?>facebook.png"><i class="fa fa-facebook" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['organisation']['instagram'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.6c244.5,0,443.4,198.9,443.4,443.4c0,244.5-198.9,443.4-443.4,443.4S96.6,784.5,96.6,540
    C96.6,295.5,295.5,96.6,540,96.6z M540,7.9C246.2,7.9,7.9,246.2,7.9,540c0,293.8,238.2,532.1,532.1,532.1s532.1-238.2,532.1-532.1
    C1072.1,246.2,833.8,7.9,540,7.9z M540,321.9c71,0,79.5,0.3,107.6,1.6c72.1,3.3,105.7,37.5,109,109c1.3,28.1,1.6,36.4,1.6,107.5
    c0,71-0.3,79.5-1.6,107.5c-3.3,71.5-36.9,105.8-109,109.1c-28.1,1.2-36.5,1.5-107.6,1.5c-71,0-79.4-0.3-107.5-1.6
    c-72.3-3.3-105.7-37.6-109-109.1c-1.3-28-1.6-36.5-1.6-107.5c0-71,0.3-79.5,1.6-107.5c3.2-71.6,36.9-105.8,109-109.1
    C460.6,322.2,469,321.9,540,321.9z M540,274c-72.2,0-81.2,0.3-109.7,1.6c-96.6,4.4-150.3,58-154.7,154.7
    c-1.3,28.4-1.6,37.5-1.6,109.7c0,72.3,0.4,81.3,1.6,109.7c4.4,96.6,58.1,150.3,154.7,154.7c28.5,1.3,37.5,1.6,109.7,1.6
    c72.3,0,81.3-0.3,109.7-1.6c96.5-4.4,150.4-58,154.8-154.7c1.3-28.4,1.6-37.5,1.6-109.7c0-72.2-0.3-81.3-1.6-109.7
    c-4.3-96.5-58.1-150.3-154.8-154.7C621.3,274.3,612.3,274,540,274z M540,403.4c-75.4,0-136.6,61.1-136.6,136.6
    S464.6,676.6,540,676.6S676.7,615.5,676.7,540C676.7,464.6,615.4,403.4,540,403.4z M540,628.7c-49,0-88.7-39.7-88.7-88.7
    c0-49,39.7-88.7,88.7-88.7c48.9,0,88.8,39.7,88.8,88.7C628.8,589,588.9,628.7,540,628.7z M682,366.1c-17.7,0-32,14.3-32,31.9
    c0,17.6,14.3,31.9,32,31.9c17.6,0,32-14.3,32-31.9C714,380.4,699.7,366.1,682,366.1z"/>
</svg><!--<img src="<?php echo e($img_path); ?>insta.png"><i class="fab fa-instagram"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['organisation']['linkedin'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,93.1c246.4,0,446.9,200.5,446.9,446.9c0,246.4-200.5,446.9-446.9,446.9C293.6,986.9,93.1,786.4,93.1,540
    C93.1,293.6,293.6,93.1,540,93.1z M540,3.8C243.9,3.8,3.8,243.9,3.8,540s240.1,536.2,536.2,536.2s536.2-240.1,536.2-536.2
    S836.1,3.8,540,3.8z M450.6,361.3c0,24.9-19.9,45-44.7,45c-24.7,0-44.7-20.1-44.7-45c0-24.9,20-45,44.7-45
    C430.7,316.2,450.6,336.4,450.6,361.3z M450.6,450.6h-89.4v268.1h89.4V450.6z M584.7,450.6h-89.4v268.1h89.4V590.9
    c0-76.9,89.5-84,89.5,0v127.9h89.3V568.6c0-146.7-139.7-141.4-178.7-69.2L584.7,450.6L584.7,450.6z"/>
</svg><!-- <img src="<?php echo e($img_path); ?>linkedin.png"><i class="fa fa-linkedin" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['organisation']['youtube'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M376.5,273h32.1l22,82.4l20.4-82.4h32.3l-37,122.5V479h-31.9v-83.6L376.5,273z M523.2,325.8
    c-24.9,0-41.5,16.5-41.5,40.8v74.5c0,26.8,14,40.8,41.5,40.8c22.7,0,40.6-15.2,40.6-40.8v-74.5C563.9,342.8,546.2,325.8,523.2,325.8
    z M535,439.9c0,8.3-4.2,14.4-11.8,14.4c-7.8,0-12.3-6.4-12.3-14.4v-70.7c0-8.3,3.8-14.5,11.8-14.5c8.6,0,12.3,6,12.3,14.5V439.9z
     M640.2,327.1v115.4c-3.4,4.3-11.1,11.4-16.6,11.4c-6,0-7.5-4.1-7.5-10.2V327.1h-28.2v127c0,15,4.6,27.2,19.7,27.2
    c8.6,0,20.4-4.5,32.7-19v16.8h28.2v-152H640.2z M689.2,635.4c-10,0-12,7-12,17v14.7H701v-14.7C701,642.6,699,635.4,689.2,635.4z
     M584.6,636.3l-5.6,4.4v90.2l6.4,5.1c4.4,2.2,10.8,2.4,13.8-1.5c1.5-2,2.3-5.4,2.3-10v-74.8c0-4.9-0.9-8.6-2.8-11.1
    C595.5,634.4,589.4,633.9,584.6,636.3z M692,520.7c-57.9-4-246.2-3.9-304.1,0c-62.6,4.3-69.9,42.2-70.4,141.7
    c0.5,99.3,7.8,137.4,70.4,141.7c57.8,3.9,246.2,4,304.1,0c62.6-4.3,70-42.1,70.5-141.7C762,563,754.7,525,692,520.7z M417.8,758.5
    h-30.3V590.8h-31.4v-28.5h93v28.5h-31.4V758.5z M525.7,758.5h-26.9v-16c-5,5.9-10.1,10.4-15.5,13.5c-14.5,8.3-34.4,8.1-34.4-21.2
    v-121h26.9v111c0,5.8,1.4,9.7,7.1,9.7c5.3,0,12.6-6.7,15.8-10.9V613.7h26.9L525.7,758.5L525.7,758.5z M629.3,728.4
    c0,18-6.7,31.9-24.6,31.9c-9.9,0-18.1-3.6-25.6-13v11.1h-27.2V562.3h27.2v63.2c6.1-7.4,14.3-13.5,24-13.5
    c19.7,0,26.2,16.6,26.2,36.3V728.4z M728.7,689.5h-51.5v27.3c0,10.9,1,20.2,11.8,20.2c11.3,0,12-7.7,12-20.2v-10h27.7v10.9
    c0,27.9-12,44.8-40.3,44.8c-25.7,0-38.8-18.7-38.8-44.8v-65c0-25.2,16.6-42.6,40.9-42.6c25.8,0,38.3,16.4,38.3,42.6L728.7,689.5
    L728.7,689.5z M540,95c245.4,0,445,199.6,445,445c0,245.4-199.6,445-445,445S95,785.4,95,540C95,294.6,294.6,95,540,95z M540,6
    C245.1,6,6,245.1,6,540s239.1,534,534,534s534-239.1,534-534S834.9,6,540,6z"/>
</svg><!-- <img src="<?php echo e($img_path); ?>you-tube.png"><i class="fa fa-youtube" aria-hidden="true"></i> --></a>
                                </li>
                                <li>
                                    <a href="<?php echo e($result['organisation']['twitter'] ?? '#'); ?>"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,96.7c244.4,0,443.3,198.9,443.3,443.3c0,244.4-198.9,443.3-443.3,443.3S96.7,784.4,96.7,540
    C96.7,295.5,295.6,96.7,540,96.7z M540,8C246.2,8,8,246.2,8,540c0,293.8,238.2,532,532,532c293.8,0,532-238.2,532-532
    C1072,246.2,833.8,8,540,8z M828.2,397.1c-19.6,8.7-40.6,14.5-62.7,17.2c22.6-13.5,39.8-34.9,48-60.4
    c-21.1,12.5-44.5,21.6-69.3,26.5c-19.9-21.2-48.3-34.5-79.7-34.5c-70.5,0-122.3,65.7-106.3,134c-90.6-4.6-171.1-48-225-114
    c-28.6,49-14.8,113.2,33.8,145.7c-17.8-0.6-34.6-5.5-49.4-13.7c-1.2,50.5,35.1,97.8,87.5,108.4c-15.3,4.2-32.2,5.2-49.3,1.9
    c13.9,43.3,54.2,74.8,101.9,75.8c-46,36-103.7,52.1-161.7,45.2c48.3,31,105.6,49,167.3,49c202.7,0,317.1-171.1,310.2-324.7
    C795,438.3,813.5,419.1,828.2,397.1z"/>
</svg><!-- <img src="<?php echo e($img_path); ?>twitter.png"><i class="fa fa-twitter" aria-hidden="true"></i> --></a>
                                </li>
                            </ul>
                            <?php if($result['seller_posts'] != null){ ?>
                                <h1 class="title" style="margin-top: 1em;">SELLER POSTS</h1>
                            <?php } ?>


                            <section role="complementary" class="simple white-back quotes no-fouc">
                              <?php if($result['seller_posts'] != null){ foreach ($result['seller_posts'] as $keyse => $valuese) { ?>
                                  <blockquote>
                                        <div class="post">
                                            <div class="avatar">
                                                <img src="<?php echo e($valuese['images'][0]['url'] ?? ''); ?>">
                                            </div>
                                            <div class="content">
                                                <div class="row">
                                                <div class="col-md-6">
                                                    <p class="date name"><?php echo e($valuese['created_by']['first_name'] ?? ''); ?> <?php echo e($valuese['created_by']['last_name'] ?? ''); ?></p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="date">SAT 12 FEB</p>
                                                </div>
                                            </div>
                                                <p><?php echo e($valuese['text'] ?? ''); ?></p>
                                            
                                            </div>
                                        </div>
                                    </blockquote>

                                <?php } } ?>
                              
                            </section>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- INDIVIDUAL SECTION END -->

        <!-- START SECTION PROPERTIES FOR SALE -->

        <!-- START SECTION PROPERTIES FOR SALE -->
        <?php if(!empty($results)){ ?>
        <section class="recently portfolio bg-white-1 home18 individual-rental-one pb3">

            <div class="properties-section">
                <div class="container-fluid">
                <div class="sec-title">
                    <h2>PROPERTIES YOU MAY ALSO LIKE</h2>
                </div>
                <div>


                <div class="row">
                    
                    <?php
                        $i=0;
                        if($apiname == 'others'){
                        foreach ($results as $key => $value){
                            if($i <= 2 && $value['id'] != $result['id']){
                    ?>
                    
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <a href="<?php echo e($value['id']); ?>" class="homes-img img-box hover-effect">
                                                <?php 
                                                    $img="../images/properties1.jpg";
                                                    foreach ($value['assets'] as $key => $val) {
                                                        if($val['floorplan'] != 1){
                                                            $img=$val['url'];
                                                            break;
                                                        } 
                                                    } 

                                                ?>
                                                <img src="<?php echo e($img); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <div class="homes-content">
                                        <h3><a href="<?php echo e($value['id']); ?>"><?php echo e($value['property']['address']['full_address']); ?></a></h3>
                                        <div class="text-center properties-icon">
                                            <?php foreach ($value['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom'){
                                                    $img="../images/bed.svg";
                                                }
                                                if($values['name'] == 'Bathroom'){
                                                    $img="../images/bath.svg";
                                                }

                                                if($values['name'] == 'Parking'){
                                                    $img="../images/car.svg";
                                                }

                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                                                    if($values['qty'] != 0){
                                             ?>
                                            <a>
                                                <span><?php echo e($values['qty']  ?? '-'); ?></span>
                                                <img src="<?php echo e($img); ?>">
                                            </a>
                                            <?php } } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform: uppercase;"><?php echo e($value['price_view'] ?? ''); ?></span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; }  } } else { ?>


                    <?php
                        $i=0;
                        
                        foreach ($results as $key => $value){
                            if($i <= 2 && $value['listings'][0]['id'] != $result['id']){
                    ?>
                    
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <a href="<?php echo e($value['listings'][0]['id']); ?>" class="homes-img img-box hover-effect">
                                                <?php 
                                                    $img="../images/properties1.jpg";
                                                    foreach ($value['listings'][0]['assets'] as $key => $val) {
                                                        if($val['floorplan'] != 1){
                                                            $img=$val['url'];
                                                            break;
                                                        } 
                                                    } 

                                                ?>
                                                <img src="<?php echo e($img); ?>" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <div class="homes-content">
                                        <h3><a href="<?php echo e($value['listings'][0]['id']); ?>"><?php echo e($value['listings'][0]['property']['address']['full_address']); ?></a></h3>
                                        <div class="text-center properties-icon">
                                            <?php foreach ($value['listings'][0]['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom'){
                                                    $img="../images/bed.svg";
                                                }
                                                if($values['name'] == 'Bathroom'){
                                                    $img="../images/bath.svg";
                                                }

                                                if($values['name'] == 'Parking'){
                                                    $img="../images/car.svg";
                                                }

                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                                                    if($values['qty'] != 0){
                                             ?>
                                            <a>
                                                <span><?php echo e($values['listings'][0]['qty']  ?? ''); ?></span>
                                                <img src="<?php echo e($img); ?>">
                                            </a>
                                            <?php } } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform: uppercase;"><?php echo e($value['listings'][0]['price_view'] ?? ''); ?></span>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; }  } } ?>

                    

                    

                </div>

               <!--  <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more" style="margin-top: 2em; display: inline-block;">SEE MORE</a>
                    </div>
                </div> -->

            </div>
            </div>

          
            
        </section>
        <?php } ?>
       
     <section class="why-section buy mb6">
            
         <div class="container">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?>list-left-finance.jpg">
                        </div>
                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>WISEBERRY FINANCE</h2>
                            <h3>HOW MUCH MONEY CAN I BORROW?</h3>
                            <p>At Wiseberry, we understand that helping our clients find the
right home is a part of us being different in the way we care.</p>
<p>In our endeavour to make the experience complete and to
get you to owning your dream home, we have partnered
ourselves with brokers who have access to a wide range of
loans.</p>
                            <a href="../finance-solutions">FIND OUT MORE</a>
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>
       </div>


     <div class="modal fade home-rent map" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="map"  width:100%;"></div>
        </div>
        
        
        
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal fade home-rent map floorplan" id="myModalone">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <?php 
            foreach ($result['assets'] as $keyf => $valuef){ 
                if($valuef['floorplan'] == "1"){
          ?>
            <img src="<?php echo e($valuef['url']); ?>">

          <?php } } ?>  

        </div>
        
        
      </div>
    </div>
  </div>

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA54bmvkid26Rlh78iXNLiZJyzFk_4nnaQ"></script>
<script>

  function initialize() {
    var latitude=parseFloat($("#latitude").val());
    var longitude=parseFloat($("#longitude").val());
    var uluru = {lat: latitude, lng: longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: uluru
      // You can set more attributes value here like map marker icon etc
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });
  }
  google.maps.event.addDomListener(window, 'load', initialize);


  $("#share-option").click(function(){
      $(".social-btn-sp").toggle();
  });
  // $(document).ready(function (e) {
      $(document).on('click','.fa-print',function(){
            window.print();
      });
      $(document).on('click','.fa-share',function(){
            var image = $(".img-fluid").attr("src");
            window.location.href = "https://www.facebook.com/sharer/sharer.php?u="+image;
      });

      
</script><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/buy_ind_list.blade.php ENDPATH**/ ?>