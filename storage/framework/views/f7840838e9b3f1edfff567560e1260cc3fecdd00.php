<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style type="text/css">
    .hideblog{
        display: none;
    }
</style>
<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Real Estate Blogs</h1>-->
<!--<h2>Real Estate Tips & Advice</h2>-->
<!--<h3>Prepare Yourself For Your Real Estate Journey</h3>-->
        <div class="blogs-pages">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center blogs" id="slider" style="background-image: url('<?php echo e($img_path); ?><?php echo e($datab->banner); ?>');background-repeat: no-repeat !important;">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title">WISEBERRY BLOGS</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="rental-appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>
    <section class="individual-agent-three blogs bg-white">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">
                    <div class="banner-search-wrap">
                      <?php if(isset($_SERVER['HTTP_REFERER'])) {  ?>
                      <input type="hidden" name="prev_url" id="prev_url" value="<?=basename($_SERVER['HTTP_REFERER']);?>">  
                      <?php } ?>
                      <div class="row">
                        <div class="col-md-8">
                            <ul class="nav nav-tabs rld-banner-tab">
                              <?php $j=1;  foreach ($data as $key => $value) { 
                                
                                if($j==1){ $act="active"; } else { $act=""; }
                                
                              ?>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo e($act); ?>" id="tabss<?php echo e($j); ?>" data-toggle="tab" href="#tabs_<?php echo e($j); ?>"><?php echo e($value->name); ?></a>
                                </li>
                              <?php $j++; } ?>
                            </ul>
                        </div>
                        <div class="col-md-4">
                          <form class="example" action="blog-search" method="POST">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="text" id="blogsearch" placeholder="What are you looking for?" name="search">
                            <input type="hidden" id="blogid" placeholder="What are you looking for?" name="search_id">
                            <button type="submit"><i class="fa fa-search"></i></button>
                          </form>
                        </div>
                      </div>
                    <div class="tab-content">
                        <?php $i=1; foreach ($data as $key => $value) { 

                          if($i==1){ $acti="show active"; } else { $acti=""; }
                          if(isset($blogdata[$value['id']])){
                              $blogs=$blogdata[$value['id']];
                              ?>
                              <div class="tab-pane fade <?php echo e($acti); ?>"  id="tabs_<?php echo e($i); ?>">
                                
                                <section class="blog-section bg-white-2 home18 individual-office">
                                    <div class="container-fluid">
                                        <div class="news-wrap">
                                            <div class="row">
                                                <?php foreach ($blogs as $keys => $values)  { ?>
                                                <div class="col-xl-4 col-md-6 col-xs-12">
                                                    <div class="news-item text-center">
                                                        <a href="individual_blog/<?php echo e($values['blog_id']); ?>" class="news-img-link img-box hover-effect img-box hover-effect">
                                                            <div class="news-item-img">
                                                                <img class="img-responsive" src="https://szwebprofile.com/PHP/wiseberry_admin/public/images/<?php echo e($values['image']); ?>" alt="blog image">
                                                            </div>
                                                        </a>
                                                        <div class="news-item-text">
                                                            <h6><?php echo e($values['cat_name']); ?></h6>
                                                            <a href="#!"><h3><?php echo e($values['name']); ?></h3></a>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php  } ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                              
                              </div>
                        <?php $i++; }  } ?>
                      </div>
                    </div>
             </div>
          </div>
          </div>
        </section>


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner individual-office blogs bg-gray mb3 pt3">
         <div class="container container1">
             <div class="row">
                 

                 <div class="col-md-8">
                        <div class="avatar img-box">
                            <img alt="image" src="images/house-values.jpg">
                        </div>
                    </div>

                 <div class="col-md-4 who-1">
                        <div class="text-left">
                            <h2>HOW TO INCREASE YOUR HOUSE VALUE</h2>
                            <!--<h3>WANT TO WORK WITH US?</h3>-->
                            <p>Bathroom renovation is a big task, and can be especially
daunting when working with a space that&apos;s awkwardly shaped,
has quirky pipelines or is lacking in natural lighting.
</p>
                            <p>Bathroom renovation is a big task, and can be especially
daunting when working with a space that&apos;s awkwardly shaped,
has quirky pipelines or is lacking in natural lighting.</p>
                            <a href="#!" class="font-weight-bold">CONTINUE READING</a>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


        <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 individual-office">
            <div class="container-fluid">
                <div class="news-wrap">
                    <div class="row">
                        <?php 
                        $a=1;
                        foreach ($data as $key => $value) { 

                          if(isset($blogdata[$value['id']])){
                              $blogs=$blogdata[$value['id']];

                              

                        foreach ($blogs as $keys => $values)  { 
                            if($a >= 4){
                                $id="hideblog";
                              }else{
                                $id="";
                              }
                        ?>
                            <div class="col-xl-4 col-md-6 col-xs-12 <?php echo e($id); ?>">
                                <div class="news-item text-center">
                                    <a href="individual_blog/<?php echo e($values['blog_id']); ?>" class="news-img-link img-box hover-effect img-box hover-effect">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="https://szwebprofile.com/PHP/wiseberry_admin/public/images/<?php echo e($values['image']); ?>" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6><?php echo e($values['cat_name']); ?></h6>
                                        <a href="#!"><h3><?php echo e($values['name']); ?></h3></a>
                                    </div>
                                </div>
                            </div>
                        <?php $a++; } }} ?>
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a class="read-more" id="seemoreblog">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        </div>

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
    $("#seemoreblog").click(function(){
        $(".hideblog").show();
    });

    $( window ).on("load", function() {
        var id=$("#prev_url").val();
        if(id == 'sell-with-us' || id == 'sell-with-us?option=1' || id == 'sell-with-us?option=2' || id == 'recently-sold'){
            $("#tabss4").click();
        }else if(id == 'manage-with-us'){
            $("#tabss5").click();
        }
    });
</script><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/blogs.blade.php ENDPATH**/ ?>