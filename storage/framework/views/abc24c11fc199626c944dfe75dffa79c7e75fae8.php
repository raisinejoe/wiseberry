<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<div class="rent-apply">
    <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">APPLYING FOR A RENTAL PROPERTY</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>LET US HELP YOU FIND THE RIGHT RENTAL PROPERTY.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalone" >LET’S GO</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process mb0">
         <div class="container container1">
            
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2><?php echo e($p1->heading); ?></h2>
                            <?php echo $p1->desc; ?>

<!--<div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="<?php echo e($p1->button_link); ?>">VIEW FOR LEASE PROPERTIES</a></div>-->
<a href="rent" class="btn btn-primary">VIEW FOR LEASE PROPERTIES</a>
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="<?php echo e($img_path); ?><?php echo e($p1->image); ?>">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process mb6">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center">
                            <h2>THINGS TO INCLUDE IN A RENTAL APPLICATION</h2>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>1</span>
                            <h6><?php echo e($p2->s1); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>2</span>
                            <h6><?php echo e($p2->s2); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>3</span>
                            <h6><?php echo e($p2->s3); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>4</span>
                            <h6><?php echo e($p2->s4); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>5</span>
                            <h6><?php echo e($p2->s5); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>6</span>
                            <h6><?php echo e($p2->s6); ?></h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                    </div>
                   
             </div>
         </div>
     </section>
     
      <!-- The Modal -->
  <div class="modal fade home-rent register-intrest" id="myModalone">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">REGISTER YOUR INTEREST</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body  ui-front">
            <p>Simply fill out the form on this page and we will have someone contact you!</p>

            <form action="sendmail_rent_with_us" id="capt_form" method="POST">
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="fname" placeholder="First Name*">
                      <input type="hidden" class="form-control" value="rent-apply" name="pagename" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" required class="form-control" name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" required class="form-control" name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="property">
                          <option value="House">House</option>
                          <option value="Townhouse/Duplex">Townhouse/Duplex</option>
                          <option value="Apartment & Unit">Apartment & Unit</option>
                          <option value="Vacant Land">Vacant Land</option>
                          <option value="Acreage/Rural">Acreage/Rural</option>
                          <option value="Commercial">Commercial</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" id="oldtags"  placeholder="Desired Region(s) or Suburb(s)*">
                      <input type="hidden" class="form-control" id="oldtags_val" name="region" placeholder="Desired Region(s) or Suburb(s)*">
                    </div>
                  </div>

                  <div class="col-md-6 rent-apply" style="padding:0 30px;">
                    <div class="row">
                        <div class="col-md-4 first">
                        <div class="form-group">
                      <select class="form-control" name="bed">
                          <option value="">Bed</option>
                          <option value="1 Bed or More">1 Bed or More</option>
                          <option value="2 Bed or More">2 Bed or More</option>
                          <option value="3 Bed or More">3 Bed or More</option>
                          <option value="4 Bed or More">4 Bed or More</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4 second">
                        <div class="form-group">
                      <select class="form-control" name="bath">
                          <option value="">Bath</option>
                          <option value="1 Bath or More">1 Bath or More</option>
                          <option value="2 Bath or More">2 Bath or More</option>
                          <option value="3 Bath or More">3 Bath or More</option>
                          <option value="4 Bath or More">4 Bath or More</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4 third">
                        <div class="form-group">
                      <select class="form-control" name="car">
                          <option value="">Car</option>
                          <option value="1 Car or More">1 Car or More</option>
                          <option value="2 Car or More">2 Car or More</option>
                          <option value="3 Car or More">3 Car or More</option>
                          <option value="4 Car or More">4 Car or More</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                            <option value="All">Wiseberry Office *</option>
                            <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                            <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                            <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                            <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                            <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                            <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                            <option value="Wiseberry Dural">Wiseberry Dural</option>
                            <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                            <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                            <option value="Wiseberry Forster">Wiseberry Forster</option>
                            <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                            <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                            <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                            <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                            <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                            <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                            <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                            <option value="Wiseberry Picton ">Wiseberry Picton </option>
                            <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                            <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                            <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                            <option value="Wiseberry Taree">Wiseberry Taree</option>
                            <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                            <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6" style="padding:0 15px;">
                    <div class="row">
                        <div class="col-md-6 first">
                        <div class="form-group">
                      <select class="form-control" name="min_price">
                          <option value="">Weekly Min Price</option>
                          <option value="$25">$25</option>
                          <option value="$50">$50</option>
                          <option value="$75">$75</option>
                          <option value="$100">$100</option>
                          <option value="$125">$125</option>
                          <option value="$150">$150</option>
                          <option value="$200">$200</option>
                          <option value="$250">$250</option>
                          <option value="$300">$300</option>
                          <option value="$350">$350</option>
                          <option value="$400">$400</option>
                          <option value="$450">$450</option>
                          <option value="$500">$500</option>
                          <option value="$600">$600</option>
                          <option value="$700">$700</option>
                          <option value="$800">$800</option>
                          <option value="$900">$900</option>
                          <option value="$1,000">$1,000</option>
                          <option value="$1,250">$1,250</option>
                          <option value="$1,50">$1,50</option>
                          <option value="$2,000">$2,000</option>
                          <option value="$3,000">$3,000</option>
                          <option value="$4,000">$4,000</option>
                          <option value="$5,000">$5,000</option>
                          <option value="$10,000">$10,000</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-6 second">
                        <div class="form-group">
                      <select class="form-control" name="max_price">
                          <option value="">Weekly Max Price</option>
                          <option value="$25">$25</option>
                          <option value="$50">$50</option>
                          <option value="$75">$75</option>
                          <option value="$100">$100</option>
                          <option value="$125">$125</option>
                          <option value="$150">$150</option>
                          <option value="$200">$200</option>
                          <option value="$250">$250</option>
                          <option value="$300">$300</option>
                          <option value="$350">$350</option>
                          <option value="$400">$400</option>
                          <option value="$450">$450</option>
                          <option value="$500">$500</option>
                          <option value="$600">$600</option>
                          <option value="$700">$700</option>
                          <option value="$800">$800</option>
                          <option value="$900">$900</option>
                          <option value="$1,000">$1,000</option>
                          <option value="$1,250">$1,250</option>
                          <option value="$1,50">$1,50</option>
                          <option value="$2,000">$2,000</option>
                          <option value="$3,000">$3,000</option>
                          <option value="$4,000">$4,000</option>
                          <option value="$5,000">$5,000</option>
                          <option value="$10,000">$10,000</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>

                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
  
</div>
        
     
    

<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/rent_apply.blade.php ENDPATH**/ ?>