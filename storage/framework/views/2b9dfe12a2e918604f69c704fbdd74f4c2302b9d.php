<?php echo $__env->make('layout.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layout.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Property Management Services</h2>-->
        <div class="apprisal">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('<?php echo e($img_path); ?><?php echo e($data->banner); ?>');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">GET YOUR PROPERTY APPRAISAL</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>HAVE YOU THOUGHT ABOUT REFINANCING?</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="finance-solutions">FIND OUT MORE</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <section class="notice-repair-section p-80 pb1">

            <div class="container">
              
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2><?php echo e($data->heading); ?></h2>
                            <?php echo $data->desc; ?>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail_appraisal" method="POST">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required id="tags" placeholder="Postcode*">
                      <input type="hidden" class="form-control" id="tags_val" name="pin" placeholder="Postcode*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                            <option value="">Wiseberry Office *</option>
                            <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                            <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                            <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                            <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                            <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                            <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                            <option value="Wiseberry Dural">Wiseberry Dural</option>
                            <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                            <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                            <option value="Wiseberry Forster">Wiseberry Forster</option>
                            <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                            <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                            <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                            <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                            <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                            <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                            <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                            <option value="Wiseberry Picton ">Wiseberry Picton </option>
                            <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                            <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                            <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                            <option value="Wiseberry Taree">Wiseberry Taree</option>
                            <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                            <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>

                  <div class="col-md-12">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary see-more">LET&apos;S GO!</button>
                  </div>

                </div>
             
              
            </form>
                    </div>
                </div>

            </div>
            
            

        </section>




        <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent properties-apprisal">
         <div class="container">
             <div class="row">


                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="images/apprisal-left.jpg">
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt6">
                            <h3 class="h3">VIRTUAL APPRAISAL</h3>
                            <p>Request an appraisal today, whether you're considering
selling, need information for refinancing purposes, or if you're
just interested to know what your property is worth in the
current market.</p>
                            <!-- <a href="#!">REQUEST YOUR APPRAISAL</a> -->
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process sell properties-apprisal">
         <div class="container">
             <div class="row">
                 

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>1</span>
                            <h6>VIRTUAL</h6>
                            <p>Virtually, everything is possible.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>2</span>
                            <h6>FAST</h6>
                            <p>We can get back to you within 24 hours.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>3</span>
                            <h6>CONVENIENT</h6>
                            <p>We will work with you to do a tour of your home without disturbing you.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>4</span>
                            <h6>FREE</h6>
                            <p>Our appraisals are cost free and in no
way oblige you to sell your property with
Wiseberry</p>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>
        </div>


<?php echo $__env->make('layout.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
  $('form').on('submit', function(e) {
    if(grecaptcha.getResponse() == "") {
      e.preventDefault();
      alert("Recaptcha Validation Failed!!!!");
    } else {
      // alert("Thank you");
    }
  });
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script><?php /**PATH /home3/szwebj4i/public_html/PHP/wiseberry/resources/views/appraisal.blade.php ENDPATH**/ ?>