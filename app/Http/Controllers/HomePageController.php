<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Models\Pages;
use Redirect;


class HomePageController extends Controller
{
    
    public function index()
    {
        
        $pages = new Pages;
		$datas = $pages->getdata(2);
        $data = json_decode($datas[0]->pagedata);
        
        $p1=$data->why;
        $p2=$data->ofc;
        $p3=$data->fp;
        $p4=$data->fr;
        $img_path=env('IMG_PATH');
        
        // echo "<pre>"; print_r($data); echo "</pre>"; die;
        return view('home',compact('p1','p2','img_path','data','p3','p4'));
    }

    public function openhome(Request $request)
    {
        if(isset($request->option)){
            $option=$request->option;
        }else{
            $option="1";
        }
        $pages = new Pages;
		$datas = $pages->getdata(6);
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;
        $p2=$data->p2;
        $img_path=env('IMG_PATH');

        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=7");
        return view('open-home',compact('p1','p2','img_path','data','option','blogdata'));
        
    }

    public function openhomes_result(Request $request)
    {

        $date=$request->datepic;
        $exp=explode("-", $date);

        if(isset($exp[0]) && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }
        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }

        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        // $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/open-home?page=1&limit=10&search_type=".$request->page_option_open."&property_type=".$request->option."&suburb=".$param."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."";

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/open-home?limit=12&search_type=".$request->page_option_open."&suburb=".$param."&agent=".$param_agent."&organisation=".$param_office."&listing=".$param_listing."&rental=&proeprty_type=".$request->option."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."";

        session()->put('apiurl', $api_url);
        // print_r($api_url);die;
        $pages = new Pages;
        $datas = $pages->getdata(16);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $res=$this->api($api_url);
        $result=$res['data'];
        $next=$res['next_page_url'];

        // echo "<pre>"; print_r($result);echo "</pre>";die;
        $shareButtons = \Share::page(
            env('BASE_URL').'file_upload/files/img.png',
            'Wiseberry',
        )->whatsapp();
        
        return view('open-home-result',compact('shareButtons','img_path','data','result','request','next'));
    }

    public function openhomes_result_api(Request $request)
    {

  //       $date=$request->datepic;
  //       $exp=explode("-", $date);

		
		
		// // echo date('Y-m-d', strtotime($date));die;

  //       if(isset($exp[0])){
  //           $date = str_replace('/', '-', $exp[0]);
  //           $from_date=date("Y-m-d",strtotime($date));
  //       }else{
  //           $from_date="";
  //       }
  //       if(isset($exp[1])){
  //           $date1 = str_replace('/', '-', $exp[1]);
  //           $to_date=date("Y-m-d",strtotime($date1));
  //       }else{
  //           $to_date="";
  //       }


        $date=$request->datepic;
        $exp=explode("-", $date);

        if(isset($exp[0])  && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }
        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }
        
        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        // $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/open-home?limit=10&page=".$request->pageno."&property_type=".$request->option."&suburb=".$request->suburb."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."";

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/open-home?page=".$request->pageno."&limit=12&search_type=".$request->page_option_open."&suburb=".$param."&agent=".$param_agent."&organisation=".$param_office."&listing=".$param_listing."&rental=&proeprty_type=".$request->option."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."";

        // print_r($api_url);die;

        $res=$this->api($api_url);
        $result=$res['data'];

        echo json_encode($result);

    }

    public function api($data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
        
    }

}