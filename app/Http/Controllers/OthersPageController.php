<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Models\Pages;
use App\Models\Blogcat;
use Illuminate\Support\Facades\Mail;
use App\Mail\Gmail;
use Redirect;


class OthersPageController extends Controller
{
    
    public function index()
    {
        $pages = new Pages;
		$datas = $pages->getdata(13);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('privacy',compact('img_path','data'));
    }


    public function terms()
    {
        $pages = new Pages;
        $datas = $pages->getdata(14);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('terms',compact('img_path','data'));
    }
    
    public function finance_solutions()
    {
        $pages = new Pages;
		$datas = $pages->getdata(10);
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        $img_path=env('IMG_PATH');

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/menu?name=enquiry_finance_type";
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }
        return view('finance_solutions',compact('p1','p2','p3','img_path','data','result'));
    }
    public function off_market()
    {
        $pages = new Pages;
		$datas = $pages->getdata(8);
        $data = json_decode($datas[0]->pagedata);
        
        $img_path=env('IMG_PATH');
        return view('off_market',compact('data','img_path'));
    }
    public function wise_move()
    {
        $pages = new Pages;
		$datas = $pages->getdata(9);
        $data = json_decode($datas[0]->pagedata);
        
        $img_path=env('IMG_PATH');
        return view('wise_move',compact('data','img_path'));
    }

    public function sell_with_us(Request $request)
    {
        if(isset($request->option)){
            $option=$request->option;
        }else{
            $option="1";
        }
        $pages = new Pages;
		$datas = $pages->getdata(25);
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        $img_path=env('IMG_PATH');

        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=18");

        return view('sell_with_us',compact('p1','p2','p3','img_path','data','option','blogdata'));
    }

    public function appraisal()
    {
        $pages = new Pages;
		$datas = $pages->getdata(26);
        $data = json_decode($datas[0]->pagedata);
        
        $img_path=env('IMG_PATH');
        return view('appraisal',compact('img_path','data'));
    }
	public function recently_sold()
    {
        $pages = new Pages;
		$datas = $pages->getdata(27);
        $data = json_decode($datas[0]->pagedata);
        
        $img_path=env('IMG_PATH');

        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=18");
        return view('recently_sold',compact('img_path','data','blogdata'));
    }
    public function recently_sold_results(Request $request)
    {
        if($request->suburb == ''){
            $param=$request->suburb_name;
        }else{
            $param=$request->suburb;
        }

        if($request->search_type == 'listing'){

          return Redirect::to(env('BASE_URL')."public/individual-result/".$request->suburb);
        }else if($request->search_type == 'agent'){
          return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          return Redirect::to(env('BASE_URL')."public/office-home/".$request->suburb);
        }else{
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/sold?page=1&limit=12&property_type=".$request->option."&suburb=".$param."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
        }
        // print_r($api_url);die;
        $pages = new Pages;
		$datas = $pages->getdata(28);
        $data = json_decode($datas[0]->pagedata);
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
            $next="";
        }
        
        $img_path=env('IMG_PATH');
        return view('recently_sold_results',compact('img_path','data','result','request','next'));
    }
    public function recently_sold_results_api(Request $request)
    {
       
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/sold?limit=12&page=".$request->pageno."&property_type=".$request->option."&suburb=".$request->suburb."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
        
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }
        
        echo json_encode($result);
    }
    public function manage_with_us()
    {
        $pages = new Pages;
		$datas = $pages->getdata(29);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        $p4=$data->p4;

        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=19");

        return view('manage_with_us',compact('p1','p2','p3','p4','img_path','data','blogdata'));
    }
    public function rental_appraisal()
    {
        $pages = new Pages;
		$datas = $pages->getdata(30);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $p1=$data->p1;
        return view('rental_appraisal',compact('p1','img_path','data'));
    }

    public function recently_leased()
    {
        $pages = new Pages;
		$datas = $pages->getdata(31);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('recently_leased',compact('img_path','data'));
    }

    public function recently_leased_results(Request $request)
    {
        if($request->suburb == ''){
            $param=$request->suburb_name;
        }else{
            $param=$request->suburb;
        }
        if($request->search_type == 'listing'){

          return Redirect::to(env('BASE_URL')."public/individual-result/".$request->suburb);
        }else if($request->search_type == 'agent'){
          return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          return Redirect::to(env('BASE_URL')."wiseberry/public/office-home/".$request->suburb);
        }else{
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/rental/leased?page=1&limit=10&property_type=".$request->option."&suburb=".$param."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&rent_from=".$request->price_from."&rent_to=".$request->price_to."";
        }
        // print_r($api_url);die;
        $pages = new Pages;
		$datas = $pages->getdata(32);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
            $next="";
        }
        return view('recently_leased_results',compact('img_path','data','result','request','next'));
    }

    public function recently_leased_results_api(Request $request)
    {
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/rental/leased?limit=10&page=".$request->pageno."&property_type=".$request->option."&suburb=".$request->suburb."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&rent_from=".$request->price_from."&rent_to=".$request->price_to."";

        

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }
        echo json_encode($result);
    }
    public function agent_search_results(Request $request)
    {
        // print_r("s");die;
        if($request->suburb != ''){
            $param=$request->suburb;
        }else{
            $param=$request->suburb_name;
        }

        if($request->search_type == 'agent'){
            return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          // return Redirect::to("https://szwebprofile.com/PHP/wiseberry/public/office-home/".$request->suburb);
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/member?organisation=".$request->suburb;
        }else if($request->search_type == 'suburb'){
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/member?suburb=".$param;
        }else{
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/member?limit=12&page=1&direction=asc&sort_by=name&search_kw=".$param;
        }

        // print_r($api_url);die;
        $pages = new Pages;
		$datas = $pages->getdata(33);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
        }

        return view('agent_search_results',compact('img_path','data','result','request','next'));
    }

    public function agent_search_results_api(Request $request)
    {
        // print_r("s");die;
        if($request->agent != ''){
            $param=$request->agent;
        }else{
            $param=$request->agent_name;
        }
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/member?limit=12&page=".$request->pageno."&search_kw=".$param;
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];


        }else{
            $result=array();
        }

        echo json_encode($result);
    }
    public function office_search(Request $request)
    {
        // print_r($request->suburb);die;
        
        if($request->search_type == 'agent'){
          return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          return Redirect::to(env('BASE_URL')."public/office-home/".$request->suburb);
        }else if($request->search_type == 'suburb'){
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/organisation?suburb=".$request->suburb;
        }else{
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/organisation?page=1&limit=12&search_kw=".$request->suburb_name;
        }

        // print_r($api_url);die;
        $pages = new Pages;
		$datas = $pages->getdata(34);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
        }

        // print_r($result);die;

        return view('office_search',compact('img_path','data','result','request','next'));
    }

    public function office_search_api(Request $request)
    {

        if($request->suburb != ''){
            $param=$request->suburb;
        }else{
            $param="";
        }
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/organisation?page=".$request->pageno."&limit=12&search_kw=".$param;

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        echo json_encode($result);
    }

    public function office_home(Request $request)
    {
        // print_r($request->id);



        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/organisation/".$request->id;

        $api_url_test="https://v2.wiseberryonline.com.au/api/v1/public/testimonial?organisation=".$request->id;

        $api_url_agent="https://v2.wiseberryonline.com.au/api/v1/public/member?organisation=".$request->id;

        $api_url_sale="https://v2.wiseberryonline.com.au/api/v1/public/listing/buy?organisation=".$request->id;

        $api_url_sold="https://v2.wiseberryonline.com.au/api/v1/public/listing/sold?organisation=".$request->id;
        $api_url_rent="https://v2.wiseberryonline.com.au/api/v1/public/rental/rent?organisation=".$request->id;

        $api_url_lease="https://v2.wiseberryonline.com.au/api/v1/public/rental/leased?organisation=".$request->id;




        $pages = new Pages;
        $datas = $pages->getdata(34);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        $res_test=$this->api($api_url_test);
        if(isset($res_test['data'])){
            $result_test=$res_test['data'];
        }else{
            $result_test=array();
        }

        $res_agent=$this->api($api_url_agent);
        if(isset($res_agent['data'])){
            $result_agent=$res_agent['data'];
        }else{
            $result_agent=array();
        }

        $res_sale=$this->api($api_url_sale);
        if(isset($res_sale['data'])){
            $result_sale=$res_sale['data'];
        }else{
            $result_sale=array();
        }

        $res_sold=$this->api($api_url_sold);
        if(isset($res_sold['data'])){
            $result_sold=$res_sold['data'];
        }else{
            $result_sold=array();
        }

        $res_rent=$this->api($api_url_rent);
        if(isset($res_rent['data'])){
            $result_rent=$res_rent['data'];
        }else{
            $result_rent=array();
        }

        $res_lease=$this->api($api_url_lease);
        if(isset($res_lease['data'])){
            $result_lease=$res_lease['data'];
        }else{
            $result_lease=array();
        }

        $blogdata = DB::select("SELECT id,name,featured_image,content FROM  blogs order by id desc");
        // echo "<pre>"; print_r($result); echo "<pre>";die;

        return view('office_home',compact('img_path','data','result','result_test','result_agent','result_sale','result_sold','result_rent','result_lease','blogdata'));
    }
    public function about_us()
    {
        $pages = new Pages;
		$datas = $pages->getdata(35);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        $p4=$data->p4;
        $p5=$data->p5;
        return view('about_us',compact('p1','p2','p3','p4','p5','img_path','data'));
    }

    public function contact_us()
    {
        $pages = new Pages;
		$datas = $pages->getdata(36);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('contact_us',compact('img_path','data'));
    }
    public function join_us()
    {
        $pages = new Pages;
		$datas = $pages->getdata(37);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('join_us',compact('img_path','data'));
    }
    public function own_business()
    {
        $pages = new Pages;
		$datas = $pages->getdata(38);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        $p4=$data->p4;
        return view('own_business',compact('p1','p2','p3','p4','img_path','data'));
    }
    
    public function ind_agent(Request $request)
    {
        // print_r($request->data);die;

        // $exp=explode("_", $request->data);

        $id=$request->data;
        // $contact_id=$exp[1];
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/member/".$id;
        
        // $api_url_sale="https://v2.wiseberryonline.com.au/api/v1/public/listing/buy?limit=30&contact=".$contact_id;
        
        // $api_url_sold="https://v2.wiseberryonline.com.au/api/v1/public/listing/sold?limit=10&page=1&contact=".$contact_id;
        

        $pages = new Pages;
		$datas = $pages->getdata(40);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        $contact_id=$result['contact']['id'];

        $api_url_sale="https://v2.wiseberryonline.com.au/api/v1/public/listing/buy?limit=30&contact=".$contact_id;

        $res_sale=$this->api($api_url_sale);
        if(isset($res_sale['data'])){
            $result_sale=$res_sale['data'];
        }else{
            $result_sale=array();
        }

        $api_url_sold="https://v2.wiseberryonline.com.au/api/v1/public/listing/sold?limit=10&page=1&contact=".$contact_id;

        $res_sold=$this->api($api_url_sold);
        if(isset($res_sold['data'])){
            $result_sold=$res_sold['data'];
        }else{
            $result_sold=array();
        }

        $api_url_test="https://v2.wiseberryonline.com.au/api/v1/public/testimonial?contact=".$contact_id;

        $res_test=$this->api($api_url_test);
        if(isset($res_test['data'])){
            $result_test=$res_test['data'];
        }else{
            $result_test=array();
        }

        // echo "<pre>"; print_r($result);echo "</pre>";die;
        return view('ind_agent',compact('img_path','data','result','result_sale','result_sold','result_test'));
    }
    public function notice_to_repair()
    {
        $pages = new Pages;
		$datas = $pages->getdata(23);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('notice_to_repair',compact('img_path','data'));
    }
    public function notice_to_vacate()
    {
        $pages = new Pages;
		$datas = $pages->getdata(24);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('notice_to_vacate',compact('img_path','data'));
    }
    public function individual_listing()
    {
        $pages = new Pages;
		$datas = $pages->getdata(41);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('individual_listing',compact('img_path','data'));
    }
    public function blogs()
    {
        $blogcat = new Blogcat;
        $data = $blogcat->getdata();

        $cards = DB::select("SELECT b.id as blog_id,b.name,c.name as cat_name,c.id as category_id,b.content,b.featured_image FROM blog_map_categories a inner join blogs b on a.blog_id=b.id inner join blog_categories c on a.category_id=c.id");

        $blogdata=array();
        $i=0;
        foreach ($cards as $key => $value) {
            $blogdata[$value->category_id][$i]['name']=$value->name;
            $blogdata[$value->category_id][$i]['cat_name']=$value->cat_name;
            $blogdata[$value->category_id][$i]['blog_id']=$value->blog_id;
            $blogdata[$value->category_id][$i]['content']=$value->content;
            $blogdata[$value->category_id][$i]['image']=$value->featured_image;
            $i++;
        }
        $pages = new Pages;
        $datas = $pages->getdata(42);
        $datab = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        return view('blogs',compact('img_path','data','datab','blogdata'));
    }


    public function blogs_api(Request $request)
    {
        if($request->search_kw != ''){
            $param=$request->search_kw;
        }else{
            $param="";
        }
        $cards = DB::select("SELECT b.id as blog_id,b.name,c.name as cat_name,c.id as category_id,b.content,b.featured_image FROM blog_map_categories a inner join blogs b on a.blog_id=b.id inner join blog_categories c on a.category_id=c.id where b.name like '%$param%'");

        $blogdata=array();
        $i=0;
        foreach ($cards as $key => $value) {
            $blogdata[$i]['name']=$value->name;
            $blogdata[$i]['id']=$value->blog_id;
            $i++;
        }

        echo json_encode($blogdata);
    }

    public function blogs_search(Request $request)
    {
        
        if(isset($request->search_id) && $request->search_id != ''){
            return redirect('/individual_blog/'.$request->search_id);
        }
        
    }

    public function agent()
    {
        $pages = new Pages;
        $datas = $pages->getdata(39);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $p1=$data->p1;
        $p2=$data->p2;
        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,c.name as cat_name,c.id as category_id,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id inner join blog_categories c on a.category_id=c.id order by b.id desc");
        return view('agent',compact('p1','p2','img_path','data','blogdata'));
    }

    public function individual_blog($id)
    {
        

        $data = DB::select("SELECT b.id as blog_id,b.name,c.name as cat_name,c.id as category_id,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id inner join blog_categories c on a.category_id=c.id where a.blog_id='$id'");

        $data_may = DB::select("SELECT b.id as blog_id,b.name,c.name as cat_name,c.id as category_id,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id inner join blog_categories c on a.category_id=c.id where a.blog_id !='$id' limit 3");

        // print_r($data_may);die;


        $img_path=env('IMG_PATH');
        return view('ind_blog',compact('img_path','data','data_may'));
    }

    

    public function sendmail(Request $request){
        
        // print_r($request->name);
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                        'lname'=>$request->lname,
                        'contact'=>$request->contact,
                        'email'=>$request->email,
                        'pin'=>$request->pin,
                        'locality'=>$request->locality,
                        'office'=>$request->office,
                        'bed'=>$request->bed,
                        'bath'=>$request->bath,
                        'parking'=>$request->parking,
                        'price_from'=>$request->price_from,
                        'price_to'=>$request->price_to,
                        'region'=>$request->region,
                        'comments'=>$request->comments,
                        "body" => "A test mail"
                     );

        // echo "<pre>"; print_r($data); echo "<pre>";die;

        

        $this->api_off_market($data);

        // Mail::send('emails.gmail', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('Wiseberry - Off Market');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });

        // return redirect('/off-market')->with('status', 'Profile updated!');
        return redirect('/off-market')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_subscribe(Request $request){
        
        // print_r($request->email);die;
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,'lname'=>$request->lname,'email'=>$request->email,'user_type'=>$request->user_type,'locality'=>$request->locality,'comments'=>$request->comments,"body" => "A test mail");
        Mail::send('emails.subscribe', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('Wiseberry - Subscribe');
        $message->from('raisinejoe@gmail.com','Test Mail');
        });

        // return redirect('/off-market')->with('status', 'Profile updated!');
        return redirect('/off-market');
    }
    public function sendmail_rent_with_us(Request $request){
        
        // print_r($request->email);die;
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'email'=>$request->email,
                      'contact'=>$request->contact,
                      'property'=>$request->property,
                      'region'=>$request->region,
                      'bed'=>$request->bed,
                      'bath'=>$request->bath,
                      'car'=>$request->car,
                      'office'=>$request->office,
                      'min_price'=>$request->min_price,
                      'max_price'=>$request->max_price,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_post($data);

        // Mail::send('emails.rent-with-us', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('Wiseberry - Rent Subscribe');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect($request->pagename)->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_finance_solutions(Request $request){
        
        // print_r($request->email);die;
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'pin'=>$request->pin,
                      'locality'=>$request->locality,
                      'request_best'=>$request->request_best,
                      "body" => "A test mail"
                  );

        $this->api_finance_solutions($data);

        // Mail::send('emails.finance_solutions', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY FINANCE SOLUTIONS');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });

        // return redirect('/off-market')->with('status', 'Profile updated!');
        return redirect('/finance-solutions')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_notice_to_repair(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'address'=>$request->address,
                      'office'=>$request->office,
                      'authority'=>$request->authority,
                      'pref_address'=>$request->pref_address,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_notice_repair($data);

        // Mail::send('emails.notice_to_repair', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - NOTICE TO REPAIR');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/notice-to-repair')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_notice_to_vacate(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'address'=>$request->address,
                      'office'=>$request->office,
                      'lease'=>$request->lease,
                      'vac_date'=>$request->vac_date,
                      'for_address'=>$request->for_address,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_notice_vacate($data);

        // Mail::send('emails.notice_to_vacate', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - NOTICE TO VACATE');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/notice-to-vacate')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_appraisal(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'pin'=>$request->pin,
                      'office'=>$request->office,
                      "body" => "A test mail"
                  );

        $this->api_appraisal($data);

        // Mail::send('emails.appraisal', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - APPRAISAL');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/appraisal')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }
    public function sendmail_manage_with_us(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'property_type'=>$request->property_type,
                      'region'=>$request->region,
                      'office'=>$request->office,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_manage($data);

        // Mail::send('emails.manage_with_us', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - MANAGE WITH US');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/manage-with-us')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_rental_appraisal(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'address'=>$request->address,
                      'suburb'=>$request->suburb,
                      'property_type'=>$request->property_type,
                      'office'=>$request->office,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );
        $this->api_rental_appraisal($data);

        // Mail::send('emails.rental_appraisal', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - MANAGE WITH US');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/rental-appraisal')->with('success', 'Thank you, your form has been successfully submitted')->with('data', 'success');
    }

    public function sendmail_ind_agent(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'email'=>$request->email,
                      'locality'=>$request->locality,
                      'office'=>$request->office,
                      'type'=>$request->type,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );


        // print_r($data);die;

        $this->api_subscribe($data);

        // Mail::send('emails.ind_agent', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - Agent');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        $pagename=$request->pagename;

        // print_r($pagename);die;

        if($pagename == 'auction-result'){
            $pagename="auction";
        }
        if($pagename == 'rent-open-home-result'){
            $pagename="rent-open-home";
        }
        if($pagename == 'open-homes-result'){
            $pagename="open-homes";
        }
        if($pagename == 'individual-result'){
            $pagename="search";
        }
        if($pagename == 'individual-agent'){
            $pagename="agent";
        }
        if($pagename == 'individual-rent-results'){
            $pagename="rent";
        }
        if($pagename == 'recently-sold-results'){
            $pagename="recently-sold";
        }
        if($pagename == 'recently-leased-results'){
            $pagename="recently-leased";
        }
        if($pagename == 'agent-search-results'){
            $pagename="agent-search";
        }
        if($pagename == 'search-results'){
            $pagename="search";
        }
        if($pagename == 'rent-results'){
            $pagename="rent";
        }
        return redirect($pagename)->with('success', 'Thank you for subscribing')->with('data', 'success');
    }

    public function sendmail_contact(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'enquiry'=>$request->enquiry,
                      "body" => "A test mail"
                  );

        $this->api_contact($data);

        Mail::send('emails.contact_us', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('WISEBERRY - CONTACT US');
        $message->from('raisinejoe@gmail.com','Test Mail');
        });
        return redirect('/contact-us')->with('success', 'Thank you for submitting the form, someone will be in contact with you soon!!!')->with('data', 'success')->withHeaders([
        'Cache-Control' => 'no-cache, no-store, max-age=0, must-revalidate',
        'Pragma' => 'no-cache',
        'Expires' => 'Sun, 02 Jan 1990 00:00:00 GMT',
    ]);
    }

    public function sendmail_own_business(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_own_business($data);

        // Mail::send('emails.own_business', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - OPEN BUSINESS');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/own-business')->with('success', 'Thank you for submitting the form, someone will be in contact with you soon!!!')->with('data', 'success');
    }

    public function sendmail_join(Request $request){
        
        $to_name = 'Joe';
        $to_email = "raisinejoshua@svirtzone.com";
        $data = array('fname'=>$request->fname,
                      'lname'=>$request->lname,
                      'contact'=>$request->contact,
                      'email'=>$request->email,
                      'office'=>$request->office,
                      'comments'=>$request->comments,
                      "body" => "A test mail"
                  );

        $this->api_join_us($data);

        // Mail::send('emails.join_us', $data, function($message) use ($to_name, $to_email) {
        // $message->to($to_email, $to_name)
        // ->subject('WISEBERRY - JOIN US');
        // $message->from('raisinejoe@gmail.com','Test Mail');
        // });
        return redirect('/join-us')->with('success', 'Thank you for submitting the form, someone will be in contact with you soon')->with('data', 'success');
    }

    public function api_post($data){

        
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 3,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "comments": "'.$data['comments'].'",
                        "price_from": '.$data['min_price'].',
                        "price_to": '.$data['max_price'].',
                        "property_type": '.$data['property'].',
                        "suburbs": '.$data['region'].',
                        "organisation": 4,
                        "agreed_to_privacy_statement": 1
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
        
    }


    public function api_off_market($data){

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 1,
                        "sub_type": 1,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "suburb": "'.$data['locality'].'",
                        "bed": "'.$data['bed'].'",
                        "bath": "'.$data['bath'].'",
                        "car": "'.$data['parking'].'",
                        "suburbs": ['.$data['region'].'],
                        "agreed_to_privacy_statement": 1,
                        "comments": "'.$data['comments'].'",
                        "price_from": '.$data['price_from'].',
                        "price_to": '.$data['price_to'].',
                        "organisation": 4
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_notice_repair($data){

        if($data['pref_address'] != ''){
            $date = str_replace('/', '-', $data['pref_address']);
            $date=date('Y-m-d',strtotime($date));
        }else{
            $date="";
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 3,
                        "sub_type": 3,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "rental": "'.$data['address'].'",
                        "agreed_to_privacy_statement": 1,
                        "comments": "'.$data['comments'].'",
                        "preferred_access_date": "'.$date.'",
                        "organisation": 4,
                        "authorised_to_enter": "'.$data['authority'].'"
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_contact($data){

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/contact-us",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'
                    {
                        
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "enquiry": "'.$data['enquiry'].'"
                    }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_join_us($data){

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                            "enquiries": [
                                {
                                    "type": 6,
                                    "source": 9,
                                    "first_name": "'.$data['fname'].'",
                                    "last_name": "'.$data['lname'].'",
                                    "mobile": "'.$data['contact'].'",
                                    "email": "'.$data['email'].'",
                                    "organisation": 4,
                                    "comments": "'.$data['comments'].'",
                                    "agreed_to_privacy_statement": 1

                                }
                             ]
                        }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_own_business($data){

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/contact-us",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'
                    {
                        
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "enquiry": "'.$data['comments'].'"
                    }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_finance_solutions($data){

        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 1,
                        "sub_type": 2,
                        "finance_type": "'.$data['request_best'].'",
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "suburb": "'.$data['locality'].'"
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_notice_vacate($data){

        
        if($data['vac_date'] != ''){
            $date = str_replace('/', '-', $data['vac_date']);
            $date=date('Y-m-d',strtotime($date));
        }else{
            $date="";
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 3,
                        "sub_type": 3,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "comments": "'.$data['comments'].'",
                        "rental": "'.$data['address'].'",
                        "authorised_to_enter": 1,
                        "organisation": 4,
                        "agreed_to_privacy_statement": 1,
                        "preferred_access_date": "'.$date.'"
                        
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_appraisal($data){

       
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 2,
                        "sub_type": 5,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "suburb": "'.$data['pin'].'",
                        "organisation": 4,
                        "agreed_to_privacy_statement": 1
                        
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_subscribe($data){
        // "organisation": "'.$data['office'].'",
       
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/subscribe",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                                "first_name": "'.$data['fname'].'",
                                "last_name": "'.$data['lname'].'",
                                "phone": "1312312312",
                                "email": "'.$data['email'].'",
                                "suburb": "'.$data['locality'].'",
                                "subscribe_contact_type": "'.$data['type'].'",
                                "comments": "'.$data['comments'].'",
                                "organisation": "4",
                            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }


    public function api_manage($data){


        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 4,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "comments": "'.$data['comments'].'",
                        "property_type": "'.$data['property_type'].'",
                        "suburbs": "'.$data['region'].'",
                        "organisation": 4,
                        "agreed_to_privacy_statement": 1
                        
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api_rental_appraisal($data){

        

        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://v2.wiseberryonline.com.au/api/v1/public/enquiry",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>'{
                "enquiries": [
                    {
                        "type": 4,
                        "source": 9,
                        "first_name": "'.$data['fname'].'",
                        "last_name": "'.$data['lname'].'",
                        "mobile": "'.$data['contact'].'",
                        "email": "'.$data['email'].'",
                        "comments": "'.$data['comments'].'",
                        "property_type": "'.$data['property_type'].'",
                        "rental": "'.$data['address'].'",
                        "suburb": "'.$data['suburb'].'",
                        "organisation": 4,
                        "agreed_to_privacy_statement": 1                      
                    }
                ]
            }',
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);

        // print_r($response);die;
        return $array;
        
    }

    public function api($data){
        // print_r($data);die;
        $link = trim($data);
        $link = str_replace ( ' ', '%20', $link);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
    }
    

    



}