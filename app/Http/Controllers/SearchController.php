<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Models\Pages;
use JeroenDesloovere\VCard\VCard;
use Session;
use Redirect;


class SearchController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = new Pages;
        $datas = $pages->getdata(5);
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->wf;
        $p2=$data->wmm;
        $img_path=env('IMG_PATH');
        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=7");
        
        return view('search',compact('p1','p2','img_path','data','blogdata'));
    }

    public function downloadVcard(Request $request)
    {
     
      $vcard = new VCard();
      $param = $request->data;

      $exp=explode("_", $param);

      // print_r($param);die;
      // define variables
         $lastname = $exp[0];
         $firstname = '';
         $additional = '';
         $prefix = '';
         $suffix = '';

      // add personal data
      $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);

      // add work data
      // $vcard->addCompany('Siesqo');
      // $vcard->addJobtitle('Web Developer');
      // $vcard->addRole('Data Protection Officer');
      if(isset($exp[2]) && $exp[2] != ''){
        $vcard->addEmail($exp[2]);
      }
      
      $vcard->addPhoneNumber($exp[1], 'PREF;WORK');
      // $vcard->addPhoneNumber(123456789, 'WORK');
      // $vcard->addAddress(null, null, 'street', 'worktown', null, 'workpostcode', 
      // 'Belgium');
      // $vcard->addLabel('street, worktown, workpostcode Belgium');
      // $vcard->addURL('http://www.jeroendesloovere.be');

      //$vcard->addPhoto(__DIR__ . '/landscape.jpeg');

      // return vcard as a string
      //return $vcard->getOutput();

      // return vcard as a download
      return $vcard->download();
      }


    public function results(Request $request)
    {
        // print_r($request);die;
        if($request->suburb == ''){
            $param=$request->suburb_name;
        }else{
            $param=$request->suburb;
        }
        if($request->search_type == 'listing'){

          return Redirect::to(env('BASE_URL')."public/individual-result/".$request->suburb);
        }else if($request->search_type == 'agent'){
          return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          return Redirect::to(env('BASE_URL')."public/office-home/".$request->suburb);
        }else{
          $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/buy?limit=12&page=1&property_type=".$request->option."&suburb=".$param."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
        }
        session()->put('apiurl', $api_url);

        $pages = new Pages;
        $datas = $pages->getdata(15);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
            $next="";
        }
        // print_r($result);die;
        return view('search-result',compact('img_path','data','result','request','next'));
    }

    public function results_api(Request $request)
    {
        
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/buy?limit=12&page=".$request->pageno."&property_type=".$request->option."&suburb=".$request->suburb."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
        
        $pages = new Pages;
        $datas = $pages->getdata(15);
        $data = json_decode($datas[0]->pagedata);
        $img_path=env('IMG_PATH');
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        echo json_encode($result);
    }

    public function ind_results(Request $request)
    {
        $api_url_1=session()->get('apiurl');
        // print_r($api_url_1);die;

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/listing/".$request->id;
        $pages = new Pages;
        $datas = $pages->getdata(43);
        $img_path=env('IMG_PATH_FRONT');
        $data = json_decode($datas[0]->pagedata);
        $res=$this->api($api_url);
        $result=$res['data'];

        $shareButtons = \Share::page(
            env('BASE_URL').'public/individual-result/'.$request->id,
            'Wiseberry',
        )
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp();

        if(isset($api_url_1) && $api_url_1 != ''){
          $resp=$this->api($api_url_1);
          $results=$resp['data'];
          if(isset($results[0]['listings'])){
            $apiname="openhome";
          }else{
            $apiname="others";
          }
        }else{
          $results=array();
          $apiname="";
        }
        
        // echo "<pre>"; print_r($apiname); echo "</pre>"; die;
        return view('buy_ind_list',compact('shareButtons','data','img_path','result','results','apiname'));
    }
    
    
    public function api($data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
    }

    

}