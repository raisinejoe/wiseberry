<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Models\Pages;
use Session;
use Redirect;


class RentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = new Pages;
		$datas = $pages->getdata(11);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);
        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=16");
        // print_r($data);die;
        return view('rent',compact('data','img_path','blogdata'));
    }
    public function rentopenhome(Request $request)
    {
        if(isset($request->option)){
            $option=$request->option;
        }else{
            $option="4";
        }
        
        $pages = new Pages;
		$datas = $pages->getdata(12);
		
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);
        // $p1=$data->p1;
        $p2=$data->p2;
        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=16");
        // echo "<pre>"; print_r($data); echo "</pre>"; die;
        return view('rent_open_home',compact('p2','data','img_path','option','blogdata'));
    }

    public function rentresults(Request $request)
    {
        if($request->suburb == ''){
            $param=$request->suburb_name;
        }else{
            $param=$request->suburb;
        }
        if($request->search_type == 'rental'){

          return Redirect::to(env('BASE_URL')."public/individual-rent-results/".$request->suburb);
        }else if($request->search_type == 'agent'){
          return Redirect::to(env('BASE_URL')."public/individual-agent/".$request->suburb);
        }else if($request->search_type == 'office'){
          return Redirect::to(env('BASE_URL')."public/office-home/".$request->suburb);
        }else{
            $api_url="https://v2.wiseberryonline.com.au/api/v1/public/rental/rent?limit=12&page=1&property_type=".$request->option."&suburb=".$param."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&rent_from=".$request->price_from."&rent_to=".$request->price_to."";
        }
        session()->put('apiurl', $api_url);
        $pages = new Pages;
		$datas = $pages->getdata(18);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
            $next=$res['next_page_url'];
        }else{
            $result=array();
            $next="";
        }
        return view('rent-results',compact('data','img_path','result','request','next'));
    }

    public function rentresults_api(Request $request)
    {
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/rental/rent?limit=12&page=".$request->pageno."&property_type=".$request->option."&suburb=".$request->suburb."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
        
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }
        echo json_encode($result);
    }
    
    public function rentopenhomeresult(Request $request)
    {

        if(isset($request->page_option_open)){
            $option=$request->page_option_open;
        }else{
            $option="4";
        }

        $date=$request->datepic;
        $exp=explode("-", $date);

        
        if(isset($exp[0]) && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }

        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }

        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/open-home?limit=12&search_type=".$request->page_option_open."&suburb=".$param."&agent=".$param_agent."&organisation=".$param_office."&listing=".$param_listing."&rental=&proeprty_type=".$request->option."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."";

        // print_r($api_url);die;
        session()->put('apiurl', $api_url);
        $pages = new Pages;
		$datas = $pages->getdata(20);
        $img_path=env('IMG_PATH');

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        // print_r($result);die;
        $data = json_decode($datas[0]->pagedata);

        $shareButtons = \Share::page(
            env('BASE_URL').'file_upload/files/img.png',
            'Wiseberry',
        )->whatsapp();

        
        return view('rent-openhome-results',compact('data','img_path','result','request','shareButtons','option'));
    }

    public function rentopenhomeresult_api(Request $request)
    {
        $date=$request->datepic;
        $exp=explode("-", $date);

        
        if(isset($exp[0]) && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }

        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }

        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/open-home?limit=12&search_type=".$request->page_option_open."&suburb=".$param."&agent=".$param_agent."&organisation=".$param_office."&listing=".$param_listing."&rental=&proeprty_type=".$request->option."&price_from=".$request->price_from."&price_to=".$request->price_to."&appointment_starts_from=".$from_date."&appointment_starts_to=".$to_date."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."";
        
        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        echo json_encode($result);

    }
    public function ind_results(Request $request)
    {
        $api_url_1=session()->get('apiurl');
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/rental/".$request->id;
        $pages = new Pages;
		$datas = $pages->getdata(19);
        $img_path=env('IMG_PATH');
        $img_path_front=env('IMG_PATH_FRONT');
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;

        $res=$this->api($api_url);
        $result=$res['data'];

        $shareButtons = \Share::page(
            env('BASE_URL').'public/individual-rent-results/'.$request->id,
            'Wiseberry',
        )
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp();
        if(isset($api_url_1) && $api_url_1 != ''){
        	$resp=$this->api($api_url_1);
        	$results=$resp['data'];
            if(isset($results[0]['listings'])){
                $apiname="openhome";
              }else{
                $apiname="others";
              }
        }else{
        	$results=array();
            $apiname="";
        }
        


        return view('ind_rent_results',compact('shareButtons','data','img_path','p1','result','img_path_front','results','apiname'));
    }
    public function rentwithus()
    {
        $pages = new Pages;
		$datas = $pages->getdata(21);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;
        $p2=$data->p2;
        $blogdata = DB::select("SELECT b.id,b.name,b.featured_image,b.content FROM blog_map_categories a inner join blogs b on a.blog_id=b.id where a.category_id=17");
        return view('rent_with_us',compact('data','img_path','p1','p2','blogdata'));
    }
    public function rentapply()
    {
        $pages = new Pages;
		$datas = $pages->getdata(22);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);
        $p1=$data->p1;
        $p2=$data->p2;
        return view('rent_apply',compact('data','img_path','p1','p2'));
    }


    public function api($data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer ".env('ACCESS_TOKEN')
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
        
    }

}