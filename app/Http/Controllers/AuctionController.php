<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Input;
use DB;
use App\Models\Pages;
use Session;
use Redirect;



class AuctionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages = new Pages;
		$datas = $pages->getdata(7);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);
        
        $p1=$data->p1;
        $p2=$data->p2;
        $p3=$data->p3;
        return view('auction',compact('data','img_path','p1','p2','p3'));
    }

    public function results(Request $request)
    {
        $date=$request->datepic;
        $exp=explode("-", $date);

        if(isset($exp[0])  && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }

        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }

        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/auction?limit=12&search_type=".$request->page_option_open."&organisation=".$param_office."&agent=".$param_agent."&suburb=".$param."&listing=".$param_listing."&auction_date_from=".$from_date."&auction_date_to=".$to_date."&property_type=".$request->option."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";

         session()->put('apiurl', $api_url);
         // print_r($api_url);die;
        $pages = new Pages;
		$datas = $pages->getdata(17);
        $img_path=env('IMG_PATH');
        $data = json_decode($datas[0]->pagedata);

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }

        $shareButtons = \Share::page(
            env('BASE_URL').'file_upload/files/img.pdf',
            'Wiseberry',
        )->whatsapp();
        
        return view('auction-result',compact('shareButtons','data','img_path','result','request'));
    }

    public function results_api(Request $request)
    {
         $date=$request->datepic;
        $exp=explode("-", $date);

        if(isset($exp[0])  && $date != ''){
            $date = str_replace('/', '-', $exp[0]);
            $from_date=date("Y-m-d",strtotime($date));
        }else{
            $from_date="";
        }

        if(isset($exp[1])){
            $date1 = str_replace('/', '-', $exp[1]);
            $to_date=date("Y-m-d",strtotime($date1));
        }else{
            $to_date="";
        }

        $param_listing='';
        $param_agent='';
        $param_office='';
        $param='';

        if($request->search_type == 'listing'){
          $param_listing=$request->suburb;
        }else if($request->search_type == 'agent'){
          $param_agent=$request->suburb;
        }else if($request->search_type == 'office'){
          $param_office=$request->suburb;
        }else{
            if($request->suburb == ''){
                $param=$request->suburb_name;
            }else{
                $param=$request->suburb;
            }
        }

        
        $api_url="https://v2.wiseberryonline.com.au/api/v1/public/auction?limit=12&search_type=".$request->page_option_open."&organisation=".$param_office."&agent=".$param_agent."&suburb=".$param."&listing=".$param_listing."&auction_date_from=".$from_date."&auction_date_to=".$to_date."&property_type=".$request->option."&bed=".$request->bed."&bath=".$request->bath."&parking=".$request->parking."&price_from=".$request->price_from."&price_to=".$request->price_to."";
         

        $res=$this->api($api_url);
        if(isset($res['data'])){
            $result=$res['data'];
        }else{
            $result=array();
        }
        
        echo json_encode($result);
    }

    public function api($data){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY"
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        $results = json_decode($response);
        $array = json_decode(json_encode($results), true);
        return $array;
    }

}