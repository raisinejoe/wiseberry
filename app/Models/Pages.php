<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use DB;

class Pages extends Model {

	protected $table = 'pages';	
	public function getdata($id)
	{
	   
		$pages  = Pages::where('id',$id)->get();
		
		return $pages;
	}
	
	public function menudata()
	{
	   
		$menu  = Pages::where('id',4)->get();
		
		return $menu;
	}
	
}
	

