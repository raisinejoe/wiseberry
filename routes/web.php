<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\AuctionController;
use App\Http\Controllers\OthersPageController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\RentController;
use App\Http\Controllers\SocialShareController;


Route::get('/', function () {
    return view('welcome');
});
Route::resource('homepage', HomePageController::class);
Route::resource('auction', AuctionController::class);
Route::post('auction-result', [AuctionController::class, 'results']);
Route::post('auction-result-api', [AuctionController::class, 'results_api']);
Route::get('open-home', [HomePageController::class, 'openhome']);
Route::get('rent-open-home', [RentController::class, 'rentopenhome']);
Route::post('rent-open-home-results', [RentController::class, 'rentopenhomeresult']);
Route::post('rent-open-home-results-api', [RentController::class, 'rentopenhomeresult_api']);
Route::get('rent-with-us', [RentController::class, 'rentwithus']);
Route::get('rent-apply', [RentController::class, 'rentapply']);
Route::post('open-homes-result', [HomePageController::class, 'openhomes_result']);
Route::post('open-homes-result-api', [HomePageController::class, 'openhomes_result_api']);
Route::resource('privacy-policy', OthersPageController::class);
Route::get('finance-solutions', [OthersPageController::class, 'finance_solutions']);
Route::get('off-market', [OthersPageController::class, 'off_market']);
Route::get('wise-move', [OthersPageController::class, 'wise_move']);
Route::get('terms', [OthersPageController::class,'terms']);
Route::get('sell-with-us', [OthersPageController::class,'sell_with_us']);
Route::get('appraisal', [OthersPageController::class,'appraisal']);
Route::get('recently-sold', [OthersPageController::class,'recently_sold']);
Route::post('recently-sold-results', [OthersPageController::class,'recently_sold_results']);
Route::post('recently-sold-results-api', [OthersPageController::class,'recently_sold_results_api']);
Route::get('manage-with-us', [OthersPageController::class,'manage_with_us']);
Route::get('rental-appraisal', [OthersPageController::class,'rental_appraisal']);
Route::get('recently-leased', [OthersPageController::class,'recently_leased']);
Route::post('recently-leased-results', [OthersPageController::class,'recently_leased_results']);
Route::get('blog-api', [OthersPageController::class,'blogs_api']);
Route::post('blog-search', [OthersPageController::class,'blogs_search']);
Route::post('recently-leased-results-api', [OthersPageController::class,'recently_leased_results_api']);
Route::post('agent-search-results', [OthersPageController::class,'agent_search_results']);
Route::match(array('GET','POST'),'office-search', [OthersPageController::class,'office_search']);
Route::match(array('GET','POST'),'office-search-api', [OthersPageController::class,'office_search_api']);
Route::get('office-home/{id}', [OthersPageController::class,'office_home']);
Route::get('about-us', [OthersPageController::class,'about_us']);
Route::get('contact-us', [OthersPageController::class,'contact_us']);
Route::get('join-us', [OthersPageController::class,'join_us']);
Route::get('own-business', [OthersPageController::class,'own_business']);
Route::get('agent', [OthersPageController::class,'agent']);
Route::get('individual-agent/{data}', [OthersPageController::class,'ind_agent']);
Route::get('notice-to-repair', [OthersPageController::class,'notice_to_repair']);
Route::get('notice-to-vacate', [OthersPageController::class,'notice_to_vacate']);
Route::get('individual-listing', [OthersPageController::class,'individual_listing']);
Route::get('blogs', [OthersPageController::class,'blogs']);
Route::get('individual_blog/{id}', [OthersPageController::class,'individual_blog']);
Route::post('sendmail', [OthersPageController::class,'sendmail']);
Route::post('sendmail_subscribe', [OthersPageController::class,'sendmail_subscribe']);
Route::post('sendmail_rent_with_us', [OthersPageController::class,'sendmail_rent_with_us']);
Route::post('sendmail_finance_solutions', [OthersPageController::class,'sendmail_finance_solutions']);
Route::post('sendmail_notice_to_repair', [OthersPageController::class,'sendmail_notice_to_repair']);
Route::post('sendmail_notice_to_vacate', [OthersPageController::class,'sendmail_notice_to_vacate']);
Route::post('sendmail_appraisal', [OthersPageController::class,'sendmail_appraisal']);
Route::post('sendmail_manage_with_us', [OthersPageController::class,'sendmail_manage_with_us']);
Route::post('sendmail_rental_appraisal', [OthersPageController::class,'sendmail_rental_appraisal']);
Route::post('sendmail_ind_agent', [OthersPageController::class,'sendmail_ind_agent']);
Route::post('sendmail_contact', [OthersPageController::class,'sendmail_contact']);
Route::post('sendmail_join', [OthersPageController::class,'sendmail_join']);
Route::post('sendmail_own_business', [OthersPageController::class,'sendmail_own_business']);
Route::resource('search', SearchController::class);
Route::post('search-results', [SearchController::class,'results']);
Route::post('search-results-api', [SearchController::class,'results_api']);
Route::post('agent-search-results-api', [OthersPageController::class,'agent_search_results_api']);
Route::get('individual-result/{id}', [SearchController::class,'ind_results']);
Route::resource('rent', RentController::class);
Route::post('rent-results', [RentController::class,'rentresults']);
Route::post('rent-results-api', [RentController::class,'rentresults_api']);
Route::get('individual-rent-results/{id}', [RentController::class,'ind_results']);
Route::get('downloadcard/{data}', [SearchController::class,'downloadVcard']);
Route::get('social-share', [SocialShareController::class, 'index']);
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});


