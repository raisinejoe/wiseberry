@include('layout.header')
@include('layout.menu')




<!--<h2>Leasing With Wiseberry</h2>-->
      <div class="clearfix"></div>
      <div class="recently-leased">
          <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('{{$img_path}}{{$data->head_image}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                           
                            <div class="banner-search-wrap home buy-home">
                                 
                                 <h1 style="margin-left: 15px;"><span class="play">Leased</span> PROPERTIES</h1>
                                 <div class="inner">
                                    <form method="POST" action="recently-leased-results">
                                         
                                    <div class="row">
                                        <div class="col-md-3 pr-0">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="pagename" id="pagename" value="leased">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <select class="form-control" id="page_option">
                                                <option value="1">Buy</option>
                                                <option value="2">Rent</option>
                                                <option value="3">Holiday Rental</option>
                                                <option value="4">Commercial Rental</option>
                                                <option value="5">Sold</option>
                                                <option value="6" selected>Leased</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input type="txt" id="tags" name="suburb_name" placeholder="Search by, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <select class="form-control" id="buy_options" name="option">
                                                                    <option>Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option value="1">1 Bed</option>
                                                                    <option value="2">2 Bed</option>
                                                                    <option value="3">3 Bed</option>
                                                                    <option value="4">4 Bed</option>
                                                                    <option value="5">5 Bed</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option value="1">1 Bath</option>
                                                                    <option value="2">2 Bath</option>
                                                                    <option value="3">3 Bath</option>
                                                                    <option value="4">4 Bath</option>
                                                                    <option value="5">5 Bath</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option value="1">1 Car</option>
                                                                    <option value="2">2 Car</option>
                                                                    <option value="3">3 Car</option>
                                                                    <option value="4">4 Car</option>
                                                                    <option value="5">5 Car</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Weekly Min Price</option>
                                                                    <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1000">$1,000</option>
                          <option value="1250">$1,250</option>
                          <option value="150">$1,500</option>
                          <option value="2000">$2,000</option>
                          <option value="3000">$3,000</option>
                          <option value="4000">$4,000</option>
                          <option value="5000">$5,000</option>
                          <option value="10000">$10,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Weekly Max Price</option>
                                                                    <option value="25">$25</option>
                          <option value="50">$50</option>
                          <option value="75">$75</option>
                          <option value="100">$100</option>
                          <option value="125">$125</option>
                          <option value="150">$150</option>
                          <option value="200">$200</option>
                          <option value="250">$250</option>
                          <option value="300">$300</option>
                          <option value="350">$350</option>
                          <option value="400">$400</option>
                          <option value="450">$450</option>
                          <option value="500">$500</option>
                          <option value="600">$600</option>
                          <option value="700">$700</option>
                          <option value="800">$800</option>
                          <option value="900">$900</option>
                          <option value="1000">$1,000</option>
                          <option value="1250">$1,250</option>
                          <option value="150">$1,500</option>
                          <option value="2000">$2,000</option>
                          <option value="3000">$3,000</option>
                          <option value="4000">$4,000</option>
                          <option value="5000">$5,000</option>
                          <option value="10000">$10,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa1 fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="rental-appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$data->heading}}</h2>
                            {!! $data->desc !!}
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$data->image}}">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 rent pb4">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tipadnadvice1.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>TENANTS</h6>
                                    <a href="#!"><h3>First Time Renter Tips</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>The kitchen is the heart of the home. When you decorate
your kitchen, you want it to be something special, a place
that will make you delighted to come home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tips2.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>RENT</h6>
                                    <a href="#!"><h3>Who Is Responsible For Pest Control?</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Bathroom renovation is a big task, and can be especially
daunting when working with a space that&apos;s awkwardly
shaped, has quirky pipelines or is lacking in natural lighting.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link img-box hover-effect">
                                    <div class="news-item-img">
                                        <img class="img-responsive" src="images/tipadnadvice3.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>TENANTS</h6>
                                    <a href="#!"><h3>8 Tips For A Winning Rental Application</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Data from realestate.com.au shows swimming pools as the
number one feature Australian buyers are searching for when
on the property hunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more sell">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
      </div>
        

@include('layout.footer')