@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Sell A Property</h1>-->
<!--<h3>Find An Agent Or An Office</h3>-->
        <div class="sell-with-us">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home">
                                 
                                 <h1 style="margin-left: 15px;"><span class="play">Selling</span> WITH WISEBERRY</h1>
                                 <div class="inner">

                                    <?php

                                        if($option == 1){
                                            $formaction='agent-search-results';

                                        }else{
                                            $formaction='office-search';
                                        }

                                    ?>

                                    <form method="POST" id="sell_with_form" action="{{$formaction}}">
                                        <div class="row">
                                            <div class="col-md-3 pr-0">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="pageno" id="pageno" value="2">
                                                <input type="hidden" name="pagename" id="pagename" value="sell_with_us">
                                                <input type="hidden" name="search_type" id="search_type" value="">
                                                <select class="form-control" name="option" id="sel_type">
                                                    <option <?php if ($option == '1') echo ' selected="selected"'; ?> value="1">Agent</option>
                                                    <option <?php if ($option == '2') echo ' selected="selected"'; ?> value="2">Office</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 pl-0" id="agent_part">
                                                
                                                <input type="txt" id="tags" name="suburb_name"  name="" placeholder="Search by suburb, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                                <input type="hidden"  id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <button type="submit" id="submit_form" name="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <!-- <a href="#!">LET�S GO</a> -->
                        <div><a href="appraisal">REQUEST YOUR APPRAISAL</a></div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent sell">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$p1->heading}}</h2>
                            {!! $p1->desc !!}
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$p1->image}}">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->



<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy stand sell">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center title">
                            <h2>WHAT WE STAND FOR</h2>
                            <!-- <h3>CAN I BOOK A PRIVATE INSPECTION?</h3> -->
                            {!! $p2->desc !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!--<img src="images/sell-icon1.png">-->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <g>
        <path style="fill:#830B2C;" d="M722.1,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C819.8,718.8,775.9,762.6,722.1,762.6z M639.4,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H639.4z"/>
        <path style="fill:#830B2C;" d="M364.8,762.6c-53.9,0-97.7-43.8-97.7-97.7c0-4.1,3.3-7.4,7.4-7.4h180.7c4.1,0,7.4,3.3,7.4,7.4
            C462.5,718.8,418.7,762.6,364.8,762.6z M282.2,672.3c3.7,42.3,39.4,75.6,82.7,75.6s78.9-33.3,82.7-75.6H282.2z"/>
        <path style="fill:#830B2C;" d="M455.2,672.3c-3.2,0-6.1-2.1-7.1-5.3l-83.3-286.6L281.6,667c-1.1,3.9-5.2,6.1-9.1,5s-6.1-5.2-5-9.1
            l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3c3.3,0,6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1C456.5,672.2,455.8,672.3,455.2,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C372.2,669,368.9,672.3,364.8,672.3z"/>
        <path style="fill:#830B2C;" d="M812.4,672.3c-3.2,0-6.1-2.1-7.1-5.3L722,380.4L638.8,667c-1.1,3.9-5.2,6.1-9.1,5
            c-3.9-1.1-6.1-5.2-5-9.1l90.3-311c0.9-3.1,3.8-5.3,7.1-5.3s6.2,2.2,7.1,5.3l90.3,311c1.1,3.9-1.1,8-5,9.1
            C813.8,672.2,813.1,672.3,812.4,672.3z"/>
        <path style="fill:#830B2C;" d="M722.1,672.3c-4.1,0-7.4-3.3-7.4-7.4V354c0-4.1,3.3-7.4,7.4-7.4s7.4,3.3,7.4,7.4v311
            C729.4,669,726.1,672.3,722.1,672.3z"/>
        <path style="fill:#830B2C;" d="M364.8,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C396.3,347.2,382.2,361.3,364.8,361.3z M364.8,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C381.6,320.6,374.1,313.1,364.8,313.1z"/>
        <path style="fill:#830B2C;" d="M722.1,361.3c-17.4,0-31.5-14.1-31.5-31.5s14.1-31.5,31.5-31.5s31.5,14.1,31.5,31.5
            C753.5,347.2,739.4,361.3,722.1,361.3z M722.1,313.1c-9.2,0-16.8,7.5-16.8,16.8c0,9.2,7.5,16.8,16.8,16.8
            c9.2,0,16.8-7.5,16.8-16.8C738.8,320.6,731.3,313.1,722.1,313.1z"/>
        <path style="fill:#830B2C;" d="M543.4,344.9c-21.9,0-39.7-17.8-39.7-39.7c0-21.9,17.8-39.7,39.7-39.7s39.7,17.8,39.7,39.7
            C583.1,327.1,565.3,344.9,543.4,344.9z M543.4,280.3c-13.7,0-24.9,11.2-24.9,24.9s11.2,24.9,24.9,24.9s24.9-11.2,24.9-24.9
            S557.2,280.3,543.4,280.3z"/>
        <path style="fill:#830B2C;" d="M388.9,337.2c-3.4,0-6.5-2.4-7.2-5.9c-0.8-4,1.8-7.9,5.8-8.7L509.7,298c4-0.8,7.9,1.8,8.7,5.8
            s-1.8,7.9-5.8,8.7l-122.2,24.6C389.9,337.2,389.4,337.2,388.9,337.2z"/>
        <path style="fill:#830B2C;" d="M697.9,337.2c-0.5,0-1,0-1.5-0.1l-122.2-24.6c-4-0.8-6.6-4.7-5.8-8.7s4.7-6.6,8.7-5.8l122.2,24.6
            c4,0.8,6.6,4.7,5.8,8.7C704.4,334.8,701.4,337.2,697.9,337.2z"/>
        <path style="fill:#830B2C;" d="M543.4,805.1c-4.1,0-7.4-3.3-7.4-7.4V337.5c0-4.1,3.3-7.4,7.4-7.4c4.1,0,7.4,3.3,7.4,7.4v460.2
            C550.8,801.8,547.5,805.1,543.4,805.1z"/>
    </g>
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="542.5" cy="541.5" r="523.8"/> -->
</g>
</svg>
                            <h2>{{$p2->sub_head1}}</h2>
                            {!! $p2->sub_desc1 !!}
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!--<img src="images/sell-icon2.png">-->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="530.1"/> -->
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#830B2C;" d="M534.4,505.8v-71.3c0-49.2-39.9-89.1-89.1-89.1
        s-89.1,39.9-89.1,89.1v35.6h-17.8v-35.6c0-59,47.9-106.9,106.9-106.9s106.9,47.9,106.9,106.9v71.3h178.2v249.5H409.6V505.8H534.4z
         M712.6,523.6H427.4v213.9h285.2V523.6z"/>
</g>
</svg>
                            <h2>{{$p2->sub_head2}}</h2>
                            {!! $p2->sub_desc2 !!}
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!--<img src="images/sell-icon3.png">-->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="540" cy="540" r="534.1"/> -->
    <path style="fill:#830B2C;" d="M679,667.1h-23c0-35.7,1.4-40.9-20.3-45.9c-33.1-7.6-64.4-14.9-75.8-39.8
        c-4.1-9.1-6.9-24.1,3.6-43.9C587,493.1,592.9,455,579.6,433c-15.5-25.7-62.3-25.8-78,0.4c-13.3,22.3-7.3,60.2,16.4,103.9
        c10.7,19.7,8,34.8,3.9,43.9c-11.3,25.1-42.3,32.2-75.1,39.8c-22.6,5.2-21.2,10-21.2,46.1h-22.9v-14.3c0-29,2.3-45.7,36.6-53.6
        c38.7-9,77-17,58.6-50.9c-54.5-100.5-15.6-157.5,43-157.5c57.4,0,97.4,54.9,43,157.5c-17.8,33.7,19,41.7,58.6,50.9
        c34.3,7.9,36.6,24.7,36.6,53.7L679,667.1z M789.7,616.1c-29.7-6.9-57.3-12.9-43.9-38.2c40.8-77,10.8-118.1-32.2-118.1
        c-29.1,0-51.8,18.8-51.8,53.5c0,29.2,13.2,50.1,20.8,67.4h24.1c-3.9-17.3-33.8-59-17.6-86c9.5-15.9,39.2-16,48.7-0.3
        c8.8,14.5,4.2,41.7-12.3,72.8c-8.9,16.9-6.4,30-2.8,38c7,15.3,22.5,22.7,40,27.8c34.7,10.2,31.5,1,31.5,34.1h23v-10.7
        C817.2,634.7,815.5,622.1,789.7,616.1z M264.5,667.1h23c0-33.1-3.2-23.9,31.5-34.1c17.5-5.1,33-12.5,40-27.8c3.7-8,6.1-21.1-2.8-38
        c-16.5-31.1-21.1-58.3-12.3-72.8c9.4-15.7,39.2-15.7,48.7,0.3c16.1,27.1-13.7,68.8-17.6,86h24.1c7.6-17.3,20.8-38.1,20.8-67.4
        c0-34.8-22.6-53.6-51.8-53.6c-43.1,0-73,41.2-32.2,118.1c13.4,25.3-14.3,31.3-43.9,38.2c-25.8,5.9-27.5,18.5-27.5,40.3
        C264.5,656.5,264.5,667.1,264.5,667.1z"/>
</g>
</svg>
                            <h2>{{$p2->sub_head3}}</h2>
                            {!! $p2->sub_desc3 !!}
                        </div>

                    </div>

                    <div class="col-md-6">
                        
                        <div class="text-center">
                            <!--<img src="images/sell-icon4.png">-->
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 136.276 136.89" style="enable-background:new 0 0 136.276 136.89;" xml:space="preserve">
<g style="opacity:1;">
    <!-- <circle style="fill:none;stroke:#830B2C;stroke-miterlimit:10;" cx="67.5" cy="68.5" r="65"/> -->
    <path style="fill-rule:evenodd;clip-rule:evenodd;fill:#830B2C;" d="M67.449,41.318c-10.378,0-17.283,10.104-7.622,27.926
        c3.258,6.013-3.522,7.434-10.392,9.02c-6.274,1.448-6.507,4.563-6.486,10.012l0.008,2.042h48.98l0.008-1.978
        c0.024-5.488-0.19-8.622-6.486-10.076c-7.019-1.621-13.555-3.042-10.392-9.02C84.701,51.048,77.621,41.318,67.449,41.318
         M67.449,43.36c3.859,0,6.921,1.56,8.626,4.39c2.764,4.596,1.768,11.891-2.811,20.539c-1.178,2.229-1.374,4.243-0.578,5.986
        c1.913,4.183,9.714,5.374,12.315,5.978c4.702,1.09,4.924,2.681,4.902,8.024H44.991c-0.02-5.339,0.184-6.934,4.902-8.024
        c2.362-0.543,10.49-1.876,12.35-6.003c0.79-1.752,0.58-3.763-0.621-5.98c-4.555-8.401-5.602-15.852-2.869-20.441
        C60.468,44.946,63.558,43.36,67.449,43.36"/>
</g>
</svg>
                            <h2>{{$p2->sub_head4}}</h2>
                            {!! $p2->sub_desc4 !!}
                        </div>

                    </div>
                   
             </div>
         </div>
     </section>




     <section class="why-section owner buy rent pb0">
         <div class="container">
             <div class="row">


                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$p3->image}}">
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt3 mt1">
                            <h2>{{$p3->heading}}</h2>
                            {!! $p3->desc !!}
                            <a href="{{$p3->button_link}}">REQUEST YOUR APPRAISAL</a>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process sell">
         <div class="container">
             <div class="row">
                 

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>1</span>
                            <h6>VIRTUAL</h6>
                            <p>Virtually, everything is possible.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>2</span>
                            <h6>FAST</h6>
                            <p>We can get back to you within 24 hours.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>3</span>
                            <h6>CONVENIENT</h6>
                            <p>We will work with you to do a tour of your home without disturbing you.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>4</span>
                            <h6>FREE</h6>
                            <p>Our appraisals are cost free and in no
way oblige you to sell your property with
Wiseberry</p>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->



<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 rent">
            <div class="container-fluid">
                <div class="sec-title pb2">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/{{$value->id}}" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="{{$img_path}}{{$value->featured_image}}" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>RENT</h6>
                                        <a href="individual_blog/{{$value->id}}"><h3>{{$value->name}}</h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more sell">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        </div>

<div class="modal fade home-rent register-intrest" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">REGISTER YOUR INTEREST</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <p>Simply fill out the form on this page and we will have someone contact you!</p>

            <form action="#!">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Property Type*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Desired Region(s) or Suburb(s)*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Bed</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Bath</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Car</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control">
                          <option>Wiseberry Office*</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Weekly Min Price</option>
                      </select>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                      <select class="form-control">
                          <option>Weekly Max Price</option>
                      </select>
                    </div>
                    </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Comments"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET�S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
@include('layout.footer')

<script type="text/javascript">
    // $("#sel_type").change(function(){
    //     if($(this).val() ==1){
    //         // $("#agent_part").show();
    //         // $("#office_part").hide();
    //         $("#sell_with_form").attr("action","agent-search-results");
    //     }else{
    //         // $("#agent_part").hide();
    //         // $("#office_part").show();
    //         $("#sell_with_form").attr("action","office-search");
    //     }
    // });
</script>