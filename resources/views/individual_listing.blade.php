@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
       
       <section class="individual-rental-slider header-image home-18 d-flex align-items-center"  style="background-image: none;">
           
            <!-- partial:index.partial.html -->
<div id="wrap" class="my-5">
    <div class="row">
        <div class="col-12">

            <!-- Carousel -->
            <div id="carousel" class="carousel slide gallery" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-slide-number="0" data-toggle="lightbox" data-gallery="gallery" data-remote="images/list-slider1.jpg">
                        <img src="images/list-slider1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="1" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider2.jpg">
                        <img src="images/slider2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="2" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider3.jpg">
                        <img src="images/slider3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="3" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider1.jpg">
                        <img src="images/slider1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="4" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider2.jpg">
                        <img src="images/slider2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="5" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider3.jpg">
                        <img src="images/slider3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="6" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider1.jpg">
                        <img src="images/slider1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="7" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider2.jpg">
                        <img src="images/slider2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="8" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider3.jpg">
                        <img src="images/slider3.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="9" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider1.jpg">
                        <img src="images/slider1.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="10" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider2.jpg">
                        <img src="images/slider2.jpg" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item" data-slide-number="11" data-toggle="lightbox" data-gallery="gallery" data-remote="images/slider3.jpg">
                        <img src="images/slider3.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <a class="carousel-fullscreen" href="#carousel" role="button">
                    <span class="carousel-fullscreen-icon" aria-hidden="true"></span>
                    <span class="sr-only">Fullscreen</span>
                </a>
                <!-- <a class="carousel-pause play" href="#carousel" role="button">
                    <span class="carousel-pause-icon" aria-hidden="true"></span>
                    <span class="sr-only">Pause</span>
                </a> -->
            </div>

            <div id="carousel-thumbs" class="carousel slide" data-ride="carousel"  data-interval="5000">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-slide-number="0">
                        <div class="row mx-0">
                            <div id="carousel-selector-0" class="thumb col-3 px-1 py-2 selected" data-target="#carousel" data-slide-to="0">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-1" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="1">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-2" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="2">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-3" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="3">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-4" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="4">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-5" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="5">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-6" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="6">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-7" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="7">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item " data-slide-number="1">
                        <div class="row mx-0">
                            <div id="carousel-selector-4" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="4">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-5" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="5">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-6" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="6">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-7" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="7">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-8" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="8">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-9" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="9">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-10" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="10">
                                <img src="images/slider3.jpg" class="img-fluid" alt="...">
                            </div>
       
                            <div id="carousel-selector-11" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="11">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                        </div>
                    </div>
                    </div>
                    <div class="carousel-item" data-slide-number="2">
                        <div class="row mx-0">
                            <div id="carousel-selector-8" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="8">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-9" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="9">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-10" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="10">
                                <img src="images/slider3.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-11" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="11">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-0" class="thumb col-3 px-1 py-2 selected" data-target="#carousel" data-slide-to="0">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-1" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="1">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-2" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="2">
                                <img src="images/list-thumnail-slider1.jpg" class="img-fluid" alt="...">
                            </div>
                            <div id="carousel-selector-3" class="thumb col-3 px-1 py-2" data-target="#carousel" data-slide-to="3">
                                <img src="images/slider2.jpg" class="img-fluid" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- partial -->
       </section>


        <!-- INDIVIDUAL SECTION START -->

        <section class="blog blog-section portfolio single-proper details mb-0  individual-rental individual-agent-one individual-office list">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="sec-title pt1">
                            <h2>THIS PROPERTY EXUDES LUXURY</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-xs-12">

                        <div class="details">

                            <h1 class="title">100 NAME STREET, SUBURB NSW</h1>
                            <ul class="subtitle">
                                <li style="list-style-type: none;"><h1>5</h1> <span><img src="images/list-propertiy-icon1.png" alt=""></span></li>
                                <li><h1>4</h1> <span><img src="images/list-propertiy-icon2.png" alt=""></span></li>
                                <li><h1>3</h1> <span><img src="images/list-propertiy-icon3.png" alt=""></span></li>
                                <li><img src="images/list-propertiy-icon4.png" alt="" /> <h1>3,127m<sup>2</sup></h1></li>
                            </ul>
                            <p>$2,000,000 - $2,200,000</p>
                            <p>Need to store a caravan, boat or trailer? This home can offer you that and plenty more. This perfectly positioned four
bedroom home in the quiet streets of the ever popular suburb of Woongarrah, encompassing easy living with a functional
floorplan and comfortable design, this home is perfect for your first home, next family adventure, retirement option or next
investment move. With tiles throughout this home it creates a welcoming ambience and will have even the pickiest of buyers
fall in love. 
</p>
                            <p>Featuring a formal, informal living and dining areas with an open kitchen worthy of creating family feasts or entertaining
guests all year round. With four generous bedrooms ensuring the whole family is catered for. Ideally located only a short
stroll to the picturesque parklands, sporting fields, ponds and only minutes� drive to local schools, shops, hospital, M1 and
public transport, this inviting home offers an exceptional sanctuary for a growing family</p>
                            <p>Features include:</p>
                            <ul class="list">
                                <li>Brick and tile home</li>
                                <li>4 bedrooms with built ins</li>
                                <li>DLUG with drive thru access</li>
                                <li>Reverse cycle air conditioning</li>
                                <li>Fully fenced 838sqm block</li>
                                <li>Short stroll to school and park</li>
                            </ul>

                            <div class="properties-features">
                                <div class="row">

                                    <div class="col-md-12">
                                        <h1 class="title" style="    padding: 0 0 10px;">PROPERTY FEATURES</h1>
                                    </div>

                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon1.png" alt="" /> Bedrooms</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon2.png" alt="" /> Bathrooms</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon3.png" alt="" /> Garages</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon4.png" alt="" /> 3,127m<sup>2</sup></p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon5.png" alt="" /> Air Conditioning</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon6.png" alt="" />3 Car Spaces</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon7.png" alt="" /> Toilet</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon8.png" alt="" /> Fireplace</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon9.png" alt="" /> Tennis Court</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon10.png" alt="" /> Gym</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon11.png" alt="" /> Outdoor Entertaining</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon12.png" alt="" /> Living Areas</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon13.png" alt="" /> Alarm System</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon14.png" alt="" /> Heating</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon15.png" alt="" /> Shed</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon16.png" alt="" /> Pool</p>
                                    </div>
                                    <div class="col-md-3">
                                        <p><img src="images/list-propertiy-icon17.png" alt="" /> Study</p>
                                    </div>
                                </div>
                            </div>

                                <div class="row team-all">

                                    <div class="col-md-5 team-pro">
                                        <div class="team-wrap">
                                            <div class="team-img">
                                                <img src="images/person.jpg" alt="" />
                                            </div>
                                            <div class="team-content-persons">
                                                <div class="team-info">
                                                    <h3>JOHN SMITH</h3>
                                                    <p>PROPERTY MANAGER</p>
                                                    <p>WISEBERRY HERITAGE</p>
                                                    <p>0400 123 456</p>
                                                    <div class="team-socials">
                                                        <ul>
                                                            <li>
                                                                <a href="#" title="mail"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                                <a href="#" title="twitter"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5 team-pro">
                                        <div class="team-wrap">
                                            <div class="team-img">
                                                <img src="images/member4.jpg" alt="" />
                                            </div>
                                            <div class="team-content-persons">
                                                <div class="team-info">
                                                    <h3>JOHN SMITH</h3>
                                                    <p>PROPERTY MANAGER</p>
                                                    <p>WISEBERRY HERITAGE</p>
                                                    <p>0400 123 456</p>
                                                    <div class="team-socials">
                                                        <ul>
                                                            <li>
                                                                <a href="#" title="mail"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                                                                <a href="#" title="twitter"><i class="fa fa-vimeo" aria-hidden="true"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2 team-pro"></div>
                                    
                                </div>

                        </div>

                    </div>
                    <div class="col-lg-3 col-md-4 car">
                        
                        <div class="side-bar">
                            
                            <ul class="button-list">
                                <li>
                                    <a href="#!" class="firts"><i class="fa fa-share" aria-hidden="true"></i> Share Property</a>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalone">FLOORPLAN</button>
                                </li>
                                <li>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">MAP</button>
                                    <!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">MAP</a></div> -->
                                </li>
                                <li>
                                    <a href="#">APPLY NOW</a>
                                </li>
                            </ul>
                            <h1 class="title">NEXT OPEN HOME</h1>
                            <ul class="time">
                                <li><i class="fa fa-calendar" aria-hidden="true"></i> <span>FRI 17 JAN</span></li>
                                <li><i class="fa fa-clock-o" aria-hidden="true"></i> <span>16:30 - 17:30</span></li>
                            </ul>
                            <h1 class="title">OFFICE DETAILS</h1>
                            <h2 class="subtitle">WISEBERRY PICTON</h2>
                            <p>345 ADDRESS STREET</p>
                            <p>GOROKAN NSW 2156</p>
                            <ul class="mail">
                                <li>
                                    <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> 02 9876 5432</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> name@wiseberry.com.au</a>
                                </li>
                            </ul>
                            <form>
                                <label>SUBSCRIBE NOW</label>
                                <input type="email" name="" class="form-control" placeholder="Enter an email address">
                                <button type="submit">Subscribe</button>
                            </form>
                            <ul class="social">
                                <li>
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                            </ul>

                            <h1 class="title" style="margin-top: 1em;">SELLER POSTS</h1>


<section role="complementary" class="simple white-back quotes no-fouc">
  <blockquote>
    <div class="post">
        <div class="avatar">
            <img src="images/list-post.jpg">
        </div>
        <div class="content">
            <p>The latest issue of Wise Move is out now!</p>
        <p>Visit: <a href="#">wiseberry.com.au/wisemove</a></p>
        <div class="row">
            <div class="col-md-6">
                <p class="date">J.SMITH</p>
            </div>
            <div class="col-md-6">
                <p class="date">SAT 12 FEB</p>
            </div>
        </div>
        </div>
    </div>
</blockquote>
  <blockquote>
    <div class="post">
        <div class="avatar">
            <img src="images/list-post.jpg">
        </div>
        <div class="content">
            <p>The latest issue of Wise Move is out now!</p>
        <p>Visit: <a href="#">wiseberry.com.au/wisemove</a></p>
        <div class="row">
            <div class="col-md-6">
                <p class="date">J.SMITH</p>
            </div>
            <div class="col-md-6">
                <p class="date">SAT 12 FEB</p>
            </div>
        </div>
        </div>
    </div>
</blockquote>
</section>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        <!-- INDIVIDUAL SECTION END -->

        <!-- START SECTION PROPERTIES FOR SALE -->

        <!-- START SECTION PROPERTIES FOR SALE -->
        <section class="recently portfolio bg-white-1 home18 individual-rental-one pb3">

            <div class="properties-section">
                <div class="container-fluid">
                <div class="sec-title">
                    <h2>PROPERTIES YOU MAY ALSO LIKE</h2>
                </div>
                <div>


                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="images/properties1.jpg" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!">SMITH STREET, SUBURB NSW</a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span>5</span>
                                                <img src="images/bed.png">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>4</span>
                                                <img src="images/bath.png">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>3</span>
                                                <img src="images/car.png">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="images/properties2.jpg" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!">SMITH STREET, SUBURB NSW</a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span>5</span>
                                                <img src="images/bed.png">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>4</span>
                                                <img src="images/bath.png">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>3</span>
                                                <img src="images/car.png">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="#!" class="homes-img img-box hover-effect">
                                                
                                                <img src="images/properties3.jpg" alt="home-1" class="img-responsive">
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="#!">SMITH STREET, SUBURB NSW</a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <a>
                                                <span>5</span>
                                                <img src="images/bed.png">
                                                <!-- <i class="fa fa-bed" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>4</span>
                                                <img src="images/bath.png">
                                                <!-- <i class="fa fa-bath" aria-hidden="true"></i> -->
                                            </a>
                                            <a>
                                                <span>3</span>
                                                <img src="images/car.png">
                                                <!-- <i class="fa fa-car" aria-hidden="true"></i> -->
                                            </a>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span>AUCTION 12TH DECEMBER </span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    

                </div>

            </div>
            </div>
 
        </section>
       
     <section class="why-section buy mb6">
         <div class="container">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box hover-effect">
                            <img alt="image" src="images/list-left-finance.jpg">
                        </div>
                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>WISEBERRY FINANCE</h2>
                            <h3>HOW MUCH MONEY CAN I BORROW?</h3>
                            <p>At Wiseberry, we understand that helping our clients find the
right home is a part of us being different in the way we care.</p>
<p>In our endeavour to make the experience complete and to
get you to owning your dream home, we have partnered
ourselves with brokers who have access to a wide range of
loans.</p>
                            <a href="#!">FIND OUT MORE</a>
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>

@include('layout.footer')