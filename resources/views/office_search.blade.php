@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

        <div class="office-search">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-result">
                                 
                                 <h1 class="text-center">FIND YOUR LOCAL OFFICE</h1>
                                 <div class="inner">
                                    <form method="POST" id="search_form"  action="office-search">
                                         
                                    <div class="row">
                                        
                                        <div class="col-md-9 pl-0">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="pageno" id="pageno" value="2">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="hidden" name="pagename" id="pagename" value="office">
                                            <input type="txt" id="tags"  name="suburb_name" placeholder="Search by Suburb or Postcode" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>
                                    <div class="row" id="select_error" style="display: none;">
                                        <p style="color: red;text-align: center;">select an item from the Search drop down menu</p>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

    

        <!-- START Buy Result -->
        <?php if(!empty($result)){  ?>
        <section class="recently portfolio bg-white-1 home18 buy-result office-search" style="padding: 8rem 25px 8rem;">
            
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="sec-title">
                    <h2>WISEBERRY OFFICES</h2>
                </div>
                    </div>
                </div>

                <div class="row" id="append_results">
                	<?php 
                		
            			$i=0;
                        foreach ($result as $key => $value){
                	?>
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            
                                            <a href="office-home/{{$value['id']}}" class="homes-img img-box">
                                                <?php if(isset($value['assets'][0]['url'])){ ?>
                                                    <img src="{{$value['assets'][0]['url']}}" style="max-height: 283px !important" alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="images/offices1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="office-home/{{$value['id']}}">{{$value['name']}}</a></h3>

                                        <h1 class="address">{{$value['address']['full_address'] ?? ''}}</h1>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                        	<?php foreach ($value['phones'] as $keys => $values){ ?>
                                            <a href="tel:{{$values['value']}}"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.2c245.2,0,444.8,199.5,444.8,444.8S785.2,984.8,540,984.8S95.2,785.2,95.2,540S294.8,95.2,540,95.2z
     M540,6.3C245.3,6.3,6.3,245.3,6.3,540s239,533.7,533.7,533.7s533.7-239,533.7-533.7S834.7,6.3,540,6.3z M740.1,776.2l-78.3-151.1
    L616,647.6c-49.8,24.2-151.2-173.9-102.5-200l46.3-22.8l-77.7-151.6l-46.8,23.1c-160.2,83.5,94.2,577.8,258,502.9L740.1,776.2z"/>
</svg>
                                                <!--<img src="images/phone.png">-->{{$values['value']}}
                                            </a>
                                        	<?php } ?>
                                            <?php foreach ($value['emails'] as $keye => $valuee){ ?>
                                            <a href="mailto:{{$valuee['value']}}"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg>
                                                <!--<img src="images/mail.png">-->{{$valuee['value']}}
                                            </a>
                                        	<?php } ?>
                                            
                                        </div>

                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  } ?>
                </div>

                <?php 
                    if(!empty($result)){ 
                        if($i == 12 && $next != null){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center mt4" style="margin-top: 0em !important;">
                        <a href="#!" class="see-more" id="see-more-office">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } }
                ?>

            </div>

        </section>

        <?php } else { ?>

        <section class="no-result">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Thanks for searching. We do not currently have an office under this search.</p>
                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <?php } ?>

@include('layout.footer')

<script type="text/javascript">
    // $("#tags").keyup(function(){
    //     $("#tags_vals").val($(this).val());
    // });

    $('form').on('submit', function() {
         if($("#tags_vals").val() == ""){
            $("#select_error").show();
            return false;
         }else{
            $("#select_error").hide();
            return true;
         }
     });
</script>