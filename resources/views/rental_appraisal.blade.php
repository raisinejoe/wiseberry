@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
<div class="rental-appraisal">
    <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h2 class="banner-title">GET YOUR <span class="play">Rental</span> APPRAISAL</h2>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW WHAT HAS BEEN RECENTLY LEASED.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="recently-leased">FIND OUT NOW</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <section class="notice-repair-section p-80">

            <div class="container">
               
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h1>{{$data->heading}}</h1>
                            {!! $data->desc !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail_rental_appraisal" id="capt_form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required id="prop_address"  placeholder="Property Address*">
                      <input type="hidden" class="form-control" id="prop_address_id" name="address" placeholder="Property Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required id="oldtags"  placeholder="Suburb*">
                      <input type="hidden" class="form-control" id="oldtags_val" name="suburb" placeholder="Desired Region(s) or Suburb(s)*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required id="buy_options" name="property_type">
                          <option value="">Property Type</option>
                          <option value="House">House</option>
                          <option value="Townhouse/Duplex">Townhouse/Duplex</option>
                          <option value="Apartment & Unit">Apartment & Unit</option>
                          <option value="Vacant Land">Vacant Land</option>
                          <option value="Acreage/Rural">Acreage/Rural</option>
                          <option value="Commercial">Commercial</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                          <option value="">Wiseberry Office *</option>
                          <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                          <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                          <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                          <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                          <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                          <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                          <option value="Wiseberry Dural">Wiseberry Dural</option>
                          <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                          <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                          <option value="Wiseberry Forster">Wiseberry Forster</option>
                          <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                          <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                          <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                          <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                          <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                          <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                          <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                          <option value="Wiseberry Picton ">Wiseberry Picton </option>
                          <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                          <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                          <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                          <option value="Wiseberry Taree">Wiseberry Taree</option>
                          <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                          <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>



                  <div class="col-md-12">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  
                  <div class="col-md-12">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary see-more">LET&apos;S GO!</button>
                  </div>

                </div>
             
              
            </form>
                    </div>
                </div>

            </div>
            
            

        </section>


  

       <!-- START SUBSCRIBE NOW -->


        <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent sell rental-apprisal">
         <div class="container">
             <div class="row">

                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$p1->image}}">
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt6 p4">
                            <h2>{{$p1->heading}}</h2>
                            {!! $p1->desc !!}
                            <!-- <a href="#!">FIND OUT MORE</a> -->
                        </div>
                    </div>

             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process sell rental-apprisal">
         <div class="container">
             <div class="row">
                 

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>1</span>
                            <h6>VIRTUAL</h6>
                            <p>Virtually, everything is possible.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>2</span>
                            <h6>FAST</h6>
                            <p>We can get back to you within 24 hours.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>3</span>
                            <h6>CONVENIENT</h6>
                            <p>We will work with you to do a tour of your home without disturbing you.</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>4</span>
                            <h6>FREE</h6>
                            <p>Our appraisals are cost free and in no
way oblige you to sell your property with
Wiseberry.</p>
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>
</div>
        

@include('layout.footer')