@include('layout.header')
@include('layout.menu')




<!--<h2>Search Sold Properties</h2>-->
  <div class="clearfix"></div>
        <div class="recently-sold">
            <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('{{$img_path}}{{$data->head_image}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home">
                                 
                                 <h1 style="margin-left: 15px;"><span class="play">Sold</span> Properties</h1>
                                 <div class="inner">
                                    <form method="POST" action="recently-sold-results">
                                         
                                    <div class="row">
                                        <div class="col-md-3 pr-0">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <select class="form-control" id="page_option">
                                                <option value="1">Buy</option>
                                                <option value="2">Rent</option>
                                                <option value="3">Holiday Rental</option>
                                                <option value="4">Commercial Rental</option>
                                                <option value="5" selected>Sold</option>
                                                <option value="6">Leased</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input type="txt" id="tags" name="suburb_name" placeholder="Search by, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <select class="form-control" id="buy_options" name="option">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option value="1">1 Bed</option>
                                                                    <option value="2">2 Bed</option>
                                                                    <option value="3">3 Bed</option>
                                                                    <option value="4">4 Bed</option>
                                                                    <option value="5">5 Bed</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option value="1">1 Bath</option>
                                                                    <option value="2">2 Bath</option>
                                                                    <option value="3">3 Bath</option>
                                                                    <option value="4">4 Bath</option>
                                                                    <option value="5">5 Bath</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option value="1">1 Car</option>
                                                                    <option value="2">2 Car</option>
                                                                    <option value="3">3 Car</option>
                                                                    <option value="4">4 Car</option>
                                                                    <option value="5">5 Car</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Min Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Max Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
       <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa1 fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>

 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy rent">
         <div class="container">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$data->heading}}</h2>
                            {!! $data->desc !!}
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$data->image}}">
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18 rent" style="padding: 5rem 0 8em;">
            <div class="container-fluid">
                <div class="sec-title pb2">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/{{$value->id}}" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="{{$img_path}}{{$value->featured_image}}" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>RENT</h6>
                                        <a href="individual_blog/{{$value->id}}"><h3>{{$value->name}}</h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more sell">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        </div>

@include('layout.footer')