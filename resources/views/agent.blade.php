@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Australian Premium Real Estate Agents</h2>-->
         <!-- STAR HEADER IMAGE -->
        <div class="agent agent1">
            <section class="header-image home-18 d-flex align-items-center" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home">
                                 
                                 <h1>FIND A LOCAL <span class="play">Agent</span></h1>
                                 <div class="inner">
                                    <form method="POST" action="agent-search-results">
                                         
                                        <div class="row">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="hidden" name="pagename" id="pagename" value="agent">
                                            <div class="col-md-9 pl-0">
                                                <input type="txt" name="suburb_name" id="tags" placeholder="Search by Office, Suburb, Postcode or Agent Name" class="form-control" style="border-right: 1px solid;">
                                                <input type="hidden" name="suburb" id="tags_val" placeholder="Search by Office, Suburb, Postcode or Agent Name" class="form-control" style="border-right: 1px solid;">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="submit" id="submit_form" name="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-7 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-3 pr-0 pl-0">
                        <!-- <a href="#!">FIND OUT NOW</a> -->
                        <div><a href="appraisal">REQUEST YOUR APPRAISAL</a></div>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process">
         <div class="container container1">
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center pt2">
                            <h2>{{$p1->heading}}</h2>
                            {!! $p1->desc !!}
                            
<a href="{{$p1->button_link}}">FIND OUT MORE</a>
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">VIEW FOR LEASE PROPERTIES</a></div> -->
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom">
                           <!-- Image Box -->
                        <!--<a  class="img-box hover-effect">-->
                            <img src="{{$img_path}}{{$p1->image}}" class="img-responsive" alt="">
                            <!-- Badge -->
                        <!--</a>-->
                        </div>
                    </div>

                 
                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process pt-0">
         <div class="container container1">
             <div class="row">


                <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom">
                           <!-- Image Box -->
                        <!--<a  class="img-box hover-effect">-->
                            <img src="{{$img_path}}{{$p2->image}}" class="img-responsive" alt="">
                            <!-- Badge -->
                        <!--</a>-->
                        </div>
                    </div>
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$p2->heading}}</h2>
                            {!! $p2->desc !!}
                            
<a href="{{$p2->button_link}}">FIND OUT MORE</a>
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">VIEW FOR LEASE PROPERTIES</a></div> -->
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy process pt-0 custom">
         <div class="container container1">
             <div class="row">


                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2 class="pt3">OUR AGENTS</h2>
                            <p>We see ourselves as a real estate group like no other. Our approach to
recruitment and training sets us apart as being one of the most rigorous in the
industry. We take great care in selecting and training our agents before they
commence their real estate career. Our agents also participate in ongoing
training and career development.</p>
                            <p>We ensure our agents do not just grow in skills, but also grow personally. The
reason for our success is simple; our highly trained agents are able to offer
professional caring services and negotiate the highest possible price for your
property.</p>
                            
<!-- <a href="#!">FIND OUT MORE</a> -->
<!-- <div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModal"><a href="#">VIEW FOR LEASE PROPERTIES</a></div> -->
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom">
                           <!-- Image Box -->
                        <!--<a  class="img-box hover-effect">-->
                            <img src="images/owner.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        <!--</a>-->
                        </div>
                    </div>

                   
             </div>
         </div>
     </section>




<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy" style="display: none;">
         <div class="container container1">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="image-animation-zoom">
                           <!-- Image Box -->
                        <!--<a  class="img-box hover-effect">-->
                            <img src="images/wise.jpg" class="img-responsive" alt="">
                            <!-- Badge -->
                        <!--</a>-->
                        </div>
                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2 class="pt2">WISE MOVE MAGAZINE</h2>
                            <h3>VOLUME 4 ISSUE 42</h3>
                            <p>With our local expertise, combined with the latest technology,
we are able to sell your property for the best price in the
fastest possible time.</p>
                            <p>Looking for a property? Our Wise Move magazine
featuresfresh, new properties each week, keeping you up to
date with what&apos;s on offer.</p>
                            <a href="wise-move">VIEW WISE MOVE</a>
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


<!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <?php $i=1; foreach ($blogdata as $key => $value)  { if($i <= 3){ ?>
                            <div class="col-xl-4 col-md-6 col-xs-12">
                                <div class="news-item text-center">
                                    <a href="individual_blog/{{$value->id}}" class="news-img-link img-box">
                                        <div class="news-item-img">
                                            <img class="img-responsive" src="{{$img_path}}{{$value->featured_image}}" alt="blog image">
                                        </div>
                                    </a>
                                    <div class="news-item-text">
                                        <h6>{{$value->cat_name}}</h6>
                                        <a href="individual_blog/{{$value->id}}"><h3>{{$value->name}}</h3></a>
                                        <!-- <div class="news-item-descr big-news">
                                            <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        </div>

        


@include('layout.footer')