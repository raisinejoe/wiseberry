@include('layout.header')
@include('layout.menu')



<style>
    /*.social-btn-sp #social-links {*/
    /*    margin: 0 auto;*/
    /*    max-width: 500px;*/
    /*}*/
    /*.social-btn-sp #social-links ul li {*/
    /*    display: inline-block;*/
    /*}          */
    /*.social-btn-sp #social-links ul li a {*/
    /*    padding: 15px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 30px;*/
    /*}*/
    /*table #social-links{*/
    /*    display: inline-table;*/
    /*}*/
    /*table #social-links ul li{*/
    /*    display: inline;*/
    /*}*/
    /*table #social-links ul li a{*/
    /*    padding: 5px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 15px;*/
    /*    background: #e3e3ea;*/
    /*}*/
    /*.fab {*/
    /*    font-family: "Font Awesome 5 Brands" !important;*/
    /*    font-size: 23px !important;*/
    /*    color: #0d6efd !important;*/
    /*}*/

   
    
</style>
<div class="clearfix"></div>
        <!-- Header Container / End -->
<!--<h2>Open Homes In Suburb (Example: Castle Hill NSW 2154)</h2>-->
<!--<h3>Suburb (Example: Castle Hill NSW 2154)</h3>-->
        <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-landing home-result">
                                 
                                 <h1 class="text-center">REFINE SEARCH</h1>
                                 <div class="inner">
                                     <form method="POST" id="search_form" action="open-homes-result">
                                     
                                    <div class="row">
                                        <div class="col-md-2 pr-0">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="pageno" id="pageno" value="2">
                                            <input type="hidden" name="pagename" id="pagename" value="openhome">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="txt" value="{{$request->datepic}}" id="datepic" placeholder="Date" class="form-control date" name="datepic"  autocomplete="off">
                                            <input type="txt" id="datepic2" placeholder="Date"  style=" border-right: 1px solid;visibility: hidden;position: absolute;"  autocomplete="off">
                                        </div>
                                        <div class="col-md-2 pr-0 pl-0">
                                            <select class="form-control" name="page_option_open" id="page_option_open">
                                                <option value="1" selected>Residential Sales</option>
                                                <option value="2">Rural Sales</option>
                                                <option value="3">Commercial Sales</option>
                                                <option value="4">Residential Rental</option>
                                                <option value="5">Rural Rental </option>
                                                <option value="6">Commercial Rental</option>
                                                <option value="7">Holiday Rental</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5 pl-0">
                                            <input type="txt" id="tags" value="{{$request->suburb_name}}" name="" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" value="{{$request->suburb}}" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <input type="hidden" name="prop_hid" id="prop_hid" value="{{$request->option}}">
                                                                <select class="form-control" id="buy_options" name="option" style="border-right: none;">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option <?php if ($request->bed == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bed == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bed == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bed == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bed == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option <?php if ($request->bath == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bath == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bath == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bath == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bath == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option <?php if ($request->parking == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->parking == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->parking == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->parking == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->parking == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Min Price</option>
                                                                    <option <?php if ($request->price_from == '50000') echo ' selected="selected"'; ?> value="50000">$50,000</option>
                                                                    <option <?php if ($request->price_from == '75000') echo ' selected="selected"'; ?> value="75000">$75,000</option>
                                                                    <option <?php if ($request->price_from == '100000') echo ' selected="selected"'; ?> value="100000">$100,000</option>
                                                                    <option <?php if ($request->price_from == '125000') echo ' selected="selected"'; ?> value="125000">$125,000</option>
                                                                    <option <?php if ($request->price_from == '150000') echo ' selected="selected"'; ?> value="150000">$150,000</option>
                                                                    <option <?php if ($request->price_from == '175000') echo ' selected="selected"'; ?> value="175000">$175,000</option>
                                                                    <option <?php if ($request->price_from == '200000') echo ' selected="selected"'; ?> value="200000">$200,000</option>
                                                                    <option <?php if ($request->price_from == '225000') echo ' selected="selected"'; ?> value="225000">$225,000</option>
                                                                    <option <?php if ($request->price_from == '250000') echo ' selected="selected"'; ?> value="250000">$250,000</option>
                                                                    <option <?php if ($request->price_from == '275000') echo ' selected="selected"'; ?> value="275000">$275,000</option>
                                                                    <option <?php if ($request->price_from == '300000') echo ' selected="selected"'; ?> value="300000">$300,000</option>
                                                                    <option <?php if ($request->price_from == '325000') echo ' selected="selected"'; ?> value="325000">$325,000</option>
                                                                    <option <?php if ($request->price_from == '350000') echo ' selected="selected"'; ?> value="350000">$350,000</option>
                                                                    <option <?php if ($request->price_from == '375000') echo ' selected="selected"'; ?> value="375000">$375,000</option>
                                                                    <option  <?php if ($request->price_from == '400000') echo ' selected="selected"'; ?> value="400000">$400,000</option>
                                                                    <option <?php if ($request->price_from == '425000') echo ' selected="selected"'; ?> value="425000">$425,000</option>
                                                                    <option <?php if ($request->price_from == '450000') echo ' selected="selected"'; ?> value="450000">$450,000</option>
                                                                    <option <?php if ($request->price_from == '475000') echo ' selected="selected"'; ?> value="475000">$475,000</option>
                                                                    <option <?php if ($request->price_from == '500000') echo ' selected="selected"'; ?> value="500000">$500,000</option>
                                                                    <option <?php if ($request->price_from == '550000') echo ' selected="selected"'; ?> value="550000">$550,000</option>
                                                                    <option <?php if ($request->price_from == '600000') echo ' selected="selected"'; ?> value="600000">$600,000</option>
                                                                    <option <?php if ($request->price_from == '650000') echo ' selected="selected"'; ?> value="650000">$650,000</option>
                                                                    <option <?php if ($request->price_from == '700000') echo ' selected="selected"'; ?> value="700000">$700,000</option>
                                                                    <option <?php if ($request->price_from == '750000') echo ' selected="selected"'; ?> value="750000">$750,000</option>
                                                                    <option <?php if ($request->price_from == '800000') echo ' selected="selected"'; ?> value="800000">$800,000</option>
                                                                    <option <?php if ($request->price_from == '850000') echo ' selected="selected"'; ?> value="850000">$850,000</option>
                                                                    <option <?php if ($request->price_from == '900000') echo ' selected="selected"'; ?> value="900000">$900,000</option>
                                                                    <option <?php if ($request->price_from == '950000') echo ' selected="selected"'; ?> value="950000">$950,000</option>
                                                                    <option <?php if ($request->price_from == '1000000') echo ' selected="selected"'; ?> value="1000000">$1,000,000</option>
                                                                    <option <?php if ($request->price_from == '1100000') echo ' selected="selected"'; ?> value="1100000">$1,100,000</option>
                                                                    <option <?php if ($request->price_from == '1200000') echo ' selected="selected"'; ?> value="1200000">$1,200,000</option>
                                                                    <option <?php if ($request->price_from == '1300000') echo ' selected="selected"'; ?> value="1300000">$1,300,000</option>
                                                                    <option <?php if ($request->price_from == '1400000') echo ' selected="selected"'; ?> value="1400000">$1,400,000</option>
                                                                    <option <?php if ($request->price_from == '1500000') echo ' selected="selected"'; ?> value="1500000">$1,500,000</option>
                                                                    <option <?php if ($request->price_from == '1600000') echo ' selected="selected"'; ?> value="1600000">$1,600,000</option>
                                                                    <option <?php if ($request->price_from == '1700000') echo ' selected="selected"'; ?> value="1700000">$1,700,000</option>
                                                                    <option <?php if ($request->price_from == '1800000') echo ' selected="selected"'; ?> value="1800000">$1,800,000</option>
                                                                    <option <?php if ($request->price_from == '1900000') echo ' selected="selected"'; ?> value="1900000">$1,900,000</option>
                                                                    <option <?php if ($request->price_from == '2000000') echo ' selected="selected"'; ?> value="2000000">$2,000,000</option>
                                                                    <option <?php if ($request->price_from == '2250000') echo ' selected="selected"'; ?> value="2250000">$2,250,000</option>
                                                                    <option <?php if ($request->price_from == '2500000') echo ' selected="selected"'; ?> value="2500000">$2,500,000</option>
                                                                    <option <?php if ($request->price_from == '2750000') echo ' selected="selected"'; ?> value="2750000">$2,750,000</option>
                                                                    <option <?php if ($request->price_from == '3000000') echo ' selected="selected"'; ?> value="3000000">$3,000,000</option>
                                                                    <option <?php if ($request->price_from == '3500000') echo ' selected="selected"'; ?> value="3500000">$3,500,000</option>
                                                                    <option <?php if ($request->price_from == '4000000') echo ' selected="selected"'; ?> value="4000000">$4,000,000</option>
                                                                    <option <?php if ($request->price_from == '4500000') echo ' selected="selected"'; ?> value="4500000">$4,500,000</option>
                                                                    <option <?php if ($request->price_from == '5000000') echo ' selected="selected"'; ?> value="5000000">$5,000,000</option>
                                                                    <option <?php if ($request->price_from == '6000000') echo ' selected="selected"'; ?> value="6000000">$6,000,000</option>
                                                                    <option <?php if ($request->price_from == '7000000') echo ' selected="selected"'; ?> value="7000000">$7,000,000</option>
                                                                    <option <?php if ($request->price_from == '8000000') echo ' selected="selected"'; ?> value="8000000">$8,000,000</option>
                                                                    <option <?php if ($request->price_from == '9000000') echo ' selected="selected"'; ?> value="9000000">$9,000,000</option>
                                                                    <option <?php if ($request->price_from == '10000000') echo ' selected="selected"'; ?> value="10000000">$10,000,000</option>
                                                                    <option <?php if ($request->price_from == '12000000') echo ' selected="selected"'; ?> value="12000000">$12,000,000</option>
                                                                    <option <?php if ($request->price_from == '15000000') echo ' selected="selected"'; ?> value="15000000">$15,000,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Max Price</option>
                                                                    <option <?php if ($request->price_to == '50000') echo ' selected="selected"'; ?> value="50000">$50,000</option>
                                                                    <option <?php if ($request->price_to == '75000') echo ' selected="selected"'; ?> value="75000">$75,000</option>
                                                                    <option <?php if ($request->price_to == '100000') echo ' selected="selected"'; ?> value="100000">$100,000</option>
                                                                    <option <?php if ($request->price_to == '125000') echo ' selected="selected"'; ?> value="125000">$125,000</option>
                                                                    <option <?php if ($request->price_to == '150000') echo ' selected="selected"'; ?> value="150000">$150,000</option>
                                                                    <option <?php if ($request->price_to == '175000') echo ' selected="selected"'; ?> value="175000">$175,000</option>
                                                                    <option <?php if ($request->price_to == '200000') echo ' selected="selected"'; ?> value="200000">$200,000</option>
                                                                    <option <?php if ($request->price_to == '225000') echo ' selected="selected"'; ?> value="225000">$225,000</option>
                                                                    <option <?php if ($request->price_to == '250000') echo ' selected="selected"'; ?> value="250000">$250,000</option>
                                                                    <option <?php if ($request->price_to == '275000') echo ' selected="selected"'; ?> value="275000">$275,000</option>
                                                                    <option <?php if ($request->price_to == '300000') echo ' selected="selected"'; ?> value="300000">$300,000</option>
                                                                    <option <?php if ($request->price_to == '325000') echo ' selected="selected"'; ?> value="325000">$325,000</option>
                                                                    <option <?php if ($request->price_to == '350000') echo ' selected="selected"'; ?> value="350000">$350,000</option>
                                                                    <option <?php if ($request->price_to == '375000') echo ' selected="selected"'; ?> value="375000">$375,000</option>
                                                                    <option  <?php if ($request->price_to == '400000') echo ' selected="selected"'; ?> value="400000">$400,000</option>
                                                                    <option <?php if ($request->price_to == '425000') echo ' selected="selected"'; ?> value="425000">$425,000</option>
                                                                    <option <?php if ($request->price_to == '450000') echo ' selected="selected"'; ?> value="450000">$450,000</option>
                                                                    <option <?php if ($request->price_to == '475000') echo ' selected="selected"'; ?> value="475000">$475,000</option>
                                                                    <option <?php if ($request->price_to == '500000') echo ' selected="selected"'; ?> value="500000">$500,000</option>
                                                                    <option <?php if ($request->price_to == '550000') echo ' selected="selected"'; ?> value="550000">$550,000</option>
                                                                    <option <?php if ($request->price_to == '600000') echo ' selected="selected"'; ?> value="600000">$600,000</option>
                                                                    <option <?php if ($request->price_to == '650000') echo ' selected="selected"'; ?> value="650000">$650,000</option>
                                                                    <option <?php if ($request->price_to == '700000') echo ' selected="selected"'; ?> value="700000">$700,000</option>
                                                                    <option <?php if ($request->price_to == '750000') echo ' selected="selected"'; ?> value="750000">$750,000</option>
                                                                    <option <?php if ($request->price_to == '800000') echo ' selected="selected"'; ?> value="800000">$800,000</option>
                                                                    <option <?php if ($request->price_to == '850000') echo ' selected="selected"'; ?> value="850000">$850,000</option>
                                                                    <option <?php if ($request->price_to == '900000') echo ' selected="selected"'; ?> value="900000">$900,000</option>
                                                                    <option <?php if ($request->price_to == '950000') echo ' selected="selected"'; ?> value="950000">$950,000</option>
                                                                    <option <?php if ($request->price_to == '1000000') echo ' selected="selected"'; ?> value="1000000">$1,000,000</option>
                                                                    <option <?php if ($request->price_to == '1100000') echo ' selected="selected"'; ?> value="1100000">$1,100,000</option>
                                                                    <option <?php if ($request->price_to == '1200000') echo ' selected="selected"'; ?> value="1200000">$1,200,000</option>
                                                                    <option <?php if ($request->price_to == '1300000') echo ' selected="selected"'; ?> value="1300000">$1,300,000</option>
                                                                    <option <?php if ($request->price_to == '1400000') echo ' selected="selected"'; ?> value="1400000">$1,400,000</option>
                                                                    <option <?php if ($request->price_to == '1500000') echo ' selected="selected"'; ?> value="1500000">$1,500,000</option>
                                                                    <option <?php if ($request->price_to == '1600000') echo ' selected="selected"'; ?> value="1600000">$1,600,000</option>
                                                                    <option <?php if ($request->price_to == '1700000') echo ' selected="selected"'; ?> value="1700000">$1,700,000</option>
                                                                    <option <?php if ($request->price_to == '1800000') echo ' selected="selected"'; ?> value="1800000">$1,800,000</option>
                                                                    <option <?php if ($request->price_to == '1900000') echo ' selected="selected"'; ?> value="1900000">$1,900,000</option>
                                                                    <option <?php if ($request->price_to == '2000000') echo ' selected="selected"'; ?> value="2000000">$2,000,000</option>
                                                                    <option <?php if ($request->price_to == '2250000') echo ' selected="selected"'; ?> value="2250000">$2,250,000</option>
                                                                    <option <?php if ($request->price_to == '2500000') echo ' selected="selected"'; ?> value="2500000">$2,500,000</option>
                                                                    <option <?php if ($request->price_to == '2750000') echo ' selected="selected"'; ?> value="2750000">$2,750,000</option>
                                                                    <option <?php if ($request->price_to == '3000000') echo ' selected="selected"'; ?> value="3000000">$3,000,000</option>
                                                                    <option <?php if ($request->price_to == '3500000') echo ' selected="selected"'; ?> value="3500000">$3,500,000</option>
                                                                    <option <?php if ($request->price_to == '4000000') echo ' selected="selected"'; ?> value="4000000">$4,000,000</option>
                                                                    <option <?php if ($request->price_to == '4500000') echo ' selected="selected"'; ?> value="4500000">$4,500,000</option>
                                                                    <option <?php if ($request->price_to == '5000000') echo ' selected="selected"'; ?> value="5000000">$5,000,000</option>
                                                                    <option <?php if ($request->price_to == '6000000') echo ' selected="selected"'; ?> value="6000000">$6,000,000</option>
                                                                    <option <?php if ($request->price_to == '7000000') echo ' selected="selected"'; ?> value="7000000">$7,000,000</option>
                                                                    <option <?php if ($request->price_to == '8000000') echo ' selected="selected"'; ?> value="8000000">$8,000,000</option>
                                                                    <option <?php if ($request->price_to == '9000000') echo ' selected="selected"'; ?> value="9000000">$9,000,000</option>
                                                                    <option <?php if ($request->price_to == '10000000') echo ' selected="selected"'; ?> value="10000000">$10,000,000</option>
                                                                    <option <?php if ($request->price_to == '12000000') echo ' selected="selected"'; ?> value="12000000">$12,000,000</option>
                                                                    <option <?php if ($request->price_to == '15000000') echo ' selected="selected"'; ?> value="15000000">$15,000,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <!-- <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                                        <input id="check-b" type="checkbox" name="check">
                                                                        <label for="check-b">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div> -->
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa1 fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

       

        <?php if(!empty($result)){ ?>
        <section class="home18 home-result">
            <div class="container-fluid">
                <div class="row openhome-title">
                    <div class="col-md-8">
                        <div class="sec-title text-right">
                            <h1>{{$data->heading}}</h1>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="share-button">
                            <button class="btn" id="download">Download</button>
                            <button class="btn" id="share-option">Share</button>
                            <div class="social-btn-sp" style="display: none;">
                                <div id="social-links">
                                    <ul>
                                        <li><a target="_blank" href="https://wa.me/?text=https://szwebprofile.com/PHP/wiseberry/file_upload/files/Open Homes.pdf" class="social-button " id="" title="" rel=""><span class="fab fa-whatsapp"></span></a></li>
                                        <li><a  class="social-button " data-target="#myModal_share" data-toggle="modal" id="" title="" rel=""><span><i class="fa fa-envelope" aria-hidden="true"></i></span>
</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                    
    
                    <div class="table-responsive">
                        <div class="row m-0 tick-plus">
                            <div class="col-md-6 tick">
                                <img id="selectall" src="images/tick.png">
                            </div>
                            <div class="col-md-6 plus">
                                <img src="images/plus.png">
                            </div>
                        </div>
                    <table class="table table-bordered" id="resp_table">
                      
                      <tbody id="append_results">
                        <?php
                         $i=0; 
                         foreach ($result as $key => $value){ 
                                    $day='';
                                    $time='';
                                if(isset($value['starts'])){
                                    $exp=explode(" ", $value['starts']);

                                    if(isset($exp[0])){
                                       $timestamp =  strtotime($exp[0]);
                                       $day = date('D d M', $timestamp);
                                    }
                                    if(isset($exp[1])){
                                       $time = date('H:i', strtotime($value['starts']));
                                    }
                                }
                         ?> 
                        <tr data-html2canvas-ignore="true">
                          <td class="text-center">
                            <div class="form-group">
                              <input type="checkbox" class="down_check" value="false" id="html{{$i}}">
                              <label for="html{{$i}}"></label>
                            </div>
                          </td>
                          <td>
                            <div class="Property">
                                <a href="individual-result/{{$value['listings'][0]['id']}}" class="homes-img img-box hover-effect">
                                    <?php 
                                        $img="images/properties1.jpg";
                                        foreach ($value['listings'][0]['assets'] as $key => $val) {
                                            if($val['floorplan'] != 1){
                                                $img=$val['url'];
                                                break;
                                            } 
                                        } 

                                    ?>
                                    <!-- <img src="{{$img}}"  alt="home-1" class="img-responsive"> -->
                                    <img src="{{$img}}?timestamp=${<?=date("Y-m-d h:i:sa");?>}" crossOrigin="anonymous" alt="home-1" class="img-responsive">
                                    <!-- <img src="https://v2-wiseberryonline-com-au.s3.ap-southeast-2.amazonaws.com/listing-assets/41045/tQ6KdF1HXwVXQhqQHbWj9O6taY7Jc45u.jpg?timestamp=${new Date()}" crossOrigin="anonymous" alt="home-1" class="img-responsive"> -->
                                </a>
                             
                          </div>
                          </td>
                          <td>
                             <a href="individual-result/{{$value['listings'][0]['id']}}">
                                  @foreach(explode(',', $value['listings'][0]['property']['address']['full_address']) as $info)
                                    <h1 class="name">{{$info ?? ""}}</h1>
                                  @endforeach  
                              </a>
                          </td>
                          <td>
                              <div class="text-center properties-icon">
                                  <?php foreach ($value['listings'][0]['property']['features'] as $keys => $values){
                                        if($values['name'] == 'Bedroom'){
                                            $img="images/bed.png";
                                        }
                                        if($values['name'] == 'Bathroom'){
                                            $img="images/bath.png";
                                        }

                                        if($values['name'] == 'Parking'){
                                            $img="images/car.png";
                                        }

                                        if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                                     ?>
                                    <a>
                                        <span>{{$values['qty']  ?? '-' }}</span>
                                        <img src="{{$img}}">
                                    </a>
                                    <?php } } ?>
                                                            
                              </div>
                          </td>
                          <td>
                              <p class="rupee">{{$value['listings'][0]['price_view'] ?? ""}}</p>
                          </td>
                          <td>
                              <p class="date" style="text-transform: uppercase;">{{$day}}</p>
                          </td>
                          <td>
                              <p class="time">{{$time}}</p>
                          </td>
                          <td class="text-center">
                              <a href="https://calendar.google.com/calendar/u/0/r/eventedit?text=Wiseberry%20meet&location={{$value['listings'][0]['property']['address']['full_address']}}"><i class="fa fa-calendar"></i></a>
                          </td>
                        </tr>
                        <?php  
                          $i++; } 

                        ?>
                      </tbody>
                    </table>
                </div>


                    </div>
                </div>
                <?php 
                    if(!empty($result)){ 
                        if($i == 12 && $next != null){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center mt4">
                        <a href="#!" class="see-more" id="see-more-open">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } }
                ?>
                
            </div>
  
            </div>
        </section>

        
        <?php } else{ ?>

        <section class="no-result">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Thanks for searching. Thanks for searching. We do not currently have any Open Homes in your area.</p>
                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        


@include('layout.footer')
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>


        <script type="text/javascript">
          $("#download").click(function(){
            var checkedbox = false;
                $(".down_check").each(function(){
                    // alert($(this).val());
                    if($(this).val() == 'true'){
                        checkedbox=true;

                    } 
                });

                if(checkedbox){

                    html2canvas($('#resp_table'), {
                        useCORS: true,
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL();
                            console.log(data);
                            var docDefinition = {
                                content: [{
                                    image: data,
                                    width: 500
                                }]
                            };
                            

                            pdfMake.createPdf(docDefinition).getBuffer(function(buffer) {
                              var blob = new Blob([buffer]);

                              var reader = new FileReader();
                              // this function is triggered once a call to readAsDataURL returns
                              reader.onload = function(event) {
                                var fd = new FormData();
                                fd.append('fname', 'test.pdf');
                                fd.append('data', event.target.result);
                                $.ajax({
                                  type: 'POST',
                                  url: 'https://szwebprofile.com/PHP/wiseberry/file_upload/upload.php?action=openhome', // Change to PHP filename
                                  data: fd,
                                  processData: false,
                                  contentType: false
                                }).done(function(data) {
                                  // print the output from the upload.php script
                                  console.log(data);
                                });
                              };
                              // trigger the read from the reader...
                              reader.readAsDataURL(blob);
                            });

                            pdfMake.createPdf(docDefinition).download("Wiseberry Open Homes.pdf");
                        }
                    });
                }else{
                    alert("Please Select Atleast One..")
                }
            });

          
            $('.down_check').on('click', function(e) {
                var elem = this;
                setTimeout(function(){ 

                   if($(elem).attr('value') == 'true'){
                       $(elem).parent().parent().parent().removeAttr("data-html2canvas-ignore");
                   }else{
                       $(elem).parent().parent().parent().attr('data-html2canvas-ignore',true);
                   }
                   // alert($(elem).attr('value'));
                 }, 100);
               
            });

          $(document).on("click","#selectall",function() {
                $('.down_check').prop('checked',true);
                $('.down_check').val('true');
                $('#selectall').prop('id',"unselectall");
                $("tr").removeAttr("data-html2canvas-ignore");
          });

          $(document).on("click","#unselectall",function() {
                $('.down_check').prop('checked',false);
                $('.down_check').val('false');
                $('#unselectall').prop('id',"selectall");
                $("tr").attr('data-html2canvas-ignore',true);
          });

          $("#share-option").click(function(){
              $(".social-btn-sp").toggle();

              var checkedbox = false;
                $(".down_check").each(function(){
                    // alert($(this).val());
                    if($(this).val() == 'true'){
                        checkedbox=true;

                    } 
                });

                if(checkedbox){

                    html2canvas($('#resp_table'), {
                        useCORS: true,
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL();
                            console.log(data);
                            var docDefinition = {
                                content: [{
                                    image: data,
                                    width: 500
                                }]
                            };
                            // $.ajax({
                            //     method: "POST",
                            //     url: "https://szwebprofile.com/PHP/wiseberry/file_upload/upload.php?action=openhome",
                            //     data: {data: docDefinition},
                            // }).done(function(data){
                            //     console.log(data);
                            // });

                            pdfMake.createPdf(docDefinition).getBuffer(function(buffer) {
                              var blob = new Blob([buffer]);

                              var reader = new FileReader();
                              // this function is triggered once a call to readAsDataURL returns
                              reader.onload = function(event) {
                                var fd = new FormData();
                                fd.append('fname', 'test.pdf');
                                fd.append('data', event.target.result);
                                $.ajax({
                                  type: 'POST',
                                  url: 'https://szwebprofile.com/PHP/wiseberry/file_upload/upload.php?action=openhome', // Change to PHP filename
                                  data: fd,
                                  processData: false,
                                  contentType: false
                                }).done(function(data) {
                                  // print the output from the upload.php script
                                  console.log(data);
                                });
                              };
                              // trigger the read from the reader...
                              reader.readAsDataURL(blob);
                            });

                            
                        }
                    });
                }else{
                    alert("Please Select Atleast One..")
                }
          });

          
        </script>