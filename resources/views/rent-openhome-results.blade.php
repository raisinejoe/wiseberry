@include('layout.header')
@include('layout.menu')
<style>
    /*.social-btn-sp #social-links {*/
    /*    margin: 0 auto;*/
    /*    max-width: 500px;*/
    /*}*/
    /*.social-btn-sp #social-links ul li {*/
    /*    display: inline-block;*/
    /*}          */
    /*.social-btn-sp #social-links ul li a {*/
    /*    padding: 15px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 30px;*/
    /*}*/
    /*table #social-links{*/
    /*    display: inline-table;*/
    /*}*/
    /*table #social-links ul li{*/
    /*    display: inline;*/
    /*}*/
    /*table #social-links ul li a{*/
    /*    padding: 5px;*/
    /*    border: 1px solid #ccc;*/
    /*    margin: 1px;*/
    /*    font-size: 15px;*/
    /*    background: #e3e3ea;*/
    /*}*/
    /*.fab {*/
    /*    font-family: "Font Awesome 5 Brands" !important;*/
    /*    font-size: 23px !important;*/
    /*    color: #0d6efd !important;*/
    /*}*/

    /*.share-button .btn {*/
    /*    background-color: #830b2c;*/
    /*    color: #ffffff;*/
    /*    border-radius: 0;*/
    /*    width: 130px;*/
    /*}*/
    
</style>
<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Rental Open For Inspections in Suburb (Example: Castle Hill NSW 2154)</h2>-->
<!--<h3>Suburb (Example: Castle Hill NSW 2154)</h3>-->
        <!-- STAR HEADER IMAGE -->
        <div class="rent-open-home-results">
            <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-landing home-result">
                                 
                                 <h1 class="text-center">REFINE SEARCH</h1>
                                 <div class="inner">
                                    <form method="POST" id="search_form" action="rent-open-home-results">
                                    
                                    <div class="row">
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      <input type="hidden" name="pageno" id="pageno" value="2">
                                      <input type="hidden" name="search_type" id="search_type" value="">
                                        <input type="hidden" name="pagename" id="pagename" value="rentopenhome">
                                        <div class="col-md-2 pr-0">
                                            <input type="txt" id="datepic" value="{{$request->datepic}}" placeholder="Date" class="form-control date" autocomplete="off">
                                            <input type="txt" id="datepic2" placeholder="Date"  style=" border-right: 1px solid;visibility: hidden;position: absolute;" autocomplete="off">
                                        </div>
                                        <div class="col-md-2 pr-0 pl-0">
                                            <select class="form-control" name="page_option_open" id="page_option_open">
                                                <option <?php if ($option == '1') echo ' selected="selected"'; ?> value="1">Residential Sales</option>
                                                <option <?php if ($option == '2') echo ' selected="selected"'; ?> value="2">Rural Sales</option>
                                                <option <?php if ($option == '3') echo ' selected="selected"'; ?> value="3">Commercial Sales</option>
                                                <option <?php if ($option == '4') echo ' selected="selected"'; ?> value="4" >Residential Rental</option>
                                                <option <?php if ($option == '5') echo ' selected="selected"'; ?> value="5">Rural Rental </option>
                                                <option <?php if ($option == '6') echo ' selected="selected"'; ?> value="6">Commercial Rental</option>
                                                <option <?php if ($option == '7') echo ' selected="selected"'; ?> value="7">Holiday Rental</option>
                                            </select>
                                        </div>
                                        <div class="col-md-5 pl-0">
                                            <input type="txt" id="tags" value="{{$request->suburb_name}}" name="" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" value="{{$request->suburb}}" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">

                                    <div class="collapse" id="collapseDiv">
                                        <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <input type="hidden" name="prop_hid" id="prop_hid" value="{{$request->option}}">
                                                                <select class="form-control" id="buy_options" name="option" style="border-right: none;">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option <?php if ($request->bed == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bed == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bed == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bed == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bed == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option <?php if ($request->bath == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bath == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bath == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bath == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bath == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option <?php if ($request->parking == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->parking == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->parking == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->parking == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->parking == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Weekly Min $</option>
                                                                    <option <?php if ($request->price_from == '25') echo ' selected="selected"'; ?> value="25">$25</option>
                                                                      <option <?php if ($request->price_from == '50') echo ' selected="selected"'; ?> value="50">$50</option>
                                                                      <option <?php if ($request->price_from == '75') echo ' selected="selected"'; ?> value="75">$75</option>
                                                                      <option <?php if ($request->price_from == '100') echo ' selected="selected"'; ?> value="100">$100</option>
                                                                      <option <?php if ($request->price_from == '125') echo ' selected="selected"'; ?> value="125">$125</option>
                                                                      <option <?php if ($request->price_from == '150') echo ' selected="selected"'; ?> value="150">$150</option>
                                                                      <option <?php if ($request->price_from == '200') echo ' selected="selected"'; ?> value="200">$200</option>
                                                                      <option <?php if ($request->price_from == '250') echo ' selected="selected"'; ?> value="250">$250</option>
                                                                      <option <?php if ($request->price_from == '300') echo ' selected="selected"'; ?> value="300">$300</option>
                                                                      <option <?php if ($request->price_from == '350') echo ' selected="selected"'; ?> value="350">$350</option>
                                                                      <option <?php if ($request->price_from == '400') echo ' selected="selected"'; ?> value="400">$400</option>
                                                                      <option <?php if ($request->price_from == '450') echo ' selected="selected"'; ?> value="450">$450</option>
                                                                      <option <?php if ($request->price_from == '500') echo ' selected="selected"'; ?> value="500">$500</option>
                                                                      <option <?php if ($request->price_from == '600') echo ' selected="selected"'; ?> value="600">$600</option>
                                                                      <option <?php if ($request->price_from == '700') echo ' selected="selected"'; ?> value="700">$700</option>
                                                                      <option <?php if ($request->price_from == '800') echo ' selected="selected"'; ?> value="800">$800</option>
                                                                      <option <?php if ($request->price_from == '900') echo ' selected="selected"'; ?> value="900">$900</option>
                                                                      <option <?php if ($request->price_from == '1000') echo ' selected="selected"'; ?> value="1000">$1,000</option>
                                                                      <option <?php if ($request->price_from == '1250') echo ' selected="selected"'; ?> value="1250">$1,250</option>
                                                                      <option <?php if ($request->price_from == '1500') echo ' selected="selected"'; ?> value="1500">$1,50</option>

                                                                      <option <?php if ($request->price_from == '2000') echo ' selected="selected"'; ?> value="2000">$2,000</option>
                                                                      <option <?php if ($request->price_from == '3000') echo ' selected="selected"'; ?> value="3000">$3,000</option>
                                                                      <option <?php if ($request->price_from == '4000') echo ' selected="selected"'; ?> value="4000">$4,000</option>
                                                                      <option <?php if ($request->price_from == '5000') echo ' selected="selected"'; ?> value="5000">$5,000</option>
                                                                      <option <?php if ($request->price_from == '10000') echo ' selected="selected"'; ?> value="10000">$10,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Weekly Max $</option>
                                                                    <option <?php if ($request->price_to == '25') echo ' selected="selected"'; ?> value="25">$25</option>
                                                                      <option <?php if ($request->price_to == '50') echo ' selected="selected"'; ?> value="50">$50</option>
                                                                      <option <?php if ($request->price_to == '75') echo ' selected="selected"'; ?> value="75">$75</option>
                                                                      <option <?php if ($request->price_to == '100') echo ' selected="selected"'; ?> value="100">$100</option>
                                                                      <option <?php if ($request->price_to == '125') echo ' selected="selected"'; ?> value="125">$125</option>
                                                                      <option <?php if ($request->price_to == '150') echo ' selected="selected"'; ?> value="150">$150</option>
                                                                      <option <?php if ($request->price_to == '200') echo ' selected="selected"'; ?> value="200">$200</option>
                                                                      <option <?php if ($request->price_to == '250') echo ' selected="selected"'; ?> value="250">$250</option>
                                                                      <option <?php if ($request->price_to == '300') echo ' selected="selected"'; ?> value="300">$300</option>
                                                                      <option <?php if ($request->price_to == '350') echo ' selected="selected"'; ?> value="350">$350</option>
                                                                      <option <?php if ($request->price_to == '400') echo ' selected="selected"'; ?> value="400">$400</option>
                                                                      <option <?php if ($request->price_to == '450') echo ' selected="selected"'; ?> value="450">$450</option>
                                                                      <option <?php if ($request->price_to == '500') echo ' selected="selected"'; ?> value="500">$500</option>
                                                                      <option <?php if ($request->price_to == '600') echo ' selected="selected"'; ?> value="600">$600</option>
                                                                      <option <?php if ($request->price_to == '700') echo ' selected="selected"'; ?> value="700">$700</option>
                                                                      <option <?php if ($request->price_to == '800') echo ' selected="selected"'; ?> value="800">$800</option>
                                                                      <option <?php if ($request->price_from == '900') echo ' selected="selected"'; ?> value="900">$900</option>
                                                                      <option <?php if ($request->price_to == '1000') echo ' selected="selected"'; ?> value="1000">$1,000</option>
                                                                      <option <?php if ($request->price_to == '1250') echo ' selected="selected"'; ?> value="1250">$1,250</option>
                                                                      <option <?php if ($request->price_to == '1500') echo ' selected="selected"'; ?> value="1500">$1,500</option>
                                                                      <option <?php if ($request->price_to == '2000') echo ' selected="selected"'; ?> value="2000">$2,000</option>
                                                                      <option <?php if ($request->price_to == '3000') echo ' selected="selected"'; ?> value="3000">$3,000</option>
                                                                      <option <?php if ($request->price_to == '4000') echo ' selected="selected"'; ?> value="4000">$4,000</option>
                                                                      <option <?php if ($request->price_to == '5000') echo ' selected="selected"'; ?> value="5000">$5,000</option>
                                                                      <option <?php if ($request->price_to == '10000') echo ' selected="selected"'; ?> value="10000">$10,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <!-- <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div> -->
                                                            </div>

                                                        </div>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
      <button type="button" id="refine-button" data-toggle="collapse" data-target="#collapseDiv"><span>Refine Search</span> <i class="fa fa-angle-down"></i></button>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

       

        <?php if(!empty($result)){ ?>
        <section class="home18 home-result">
            <div class="container-fluid">
                <div class="row rent-open-home-results-title">
                    <div class="col-md-8">
                        <div class="sec-title text-right">
                            <h1>{{$data->heading}}</h1>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="share-button">
                            <button class="btn" id="download">Download</button>
                            <button class="btn" id="share-option">Share</button>
                            <div class="social-btn-sp" style="display: none;">
                                <div id="social-links">
                                    <ul>
                                        <li><a target="_blank" href="https://wa.me/?text=https://szwebprofile.com/PHP/wiseberry/file_upload/files/Rental Open Homes.pdf" class="social-button " id="" title="" rel=""><span class="fab fa-whatsapp"></span></a></li>
                                        <li><a  class="social-button " data-target="#myModal_share" data-toggle="modal" id="" title="" rel=""><span><i class="fa fa-envelope" aria-hidden="true"></i></span>
</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
    <div class="table-responsive">
        <div class="row m-0 tick-plus">
            <div class="col-md-6 tick">
                <img id="selectall" src="images/tick.png">
            </div>
            <div class="col-md-6 plus">
                <img src="images/plus.png">
            </div>
        </div>
    <table class="table table-bordered" id="resp_table">
      
      <tbody id="append_results">
        <?php $i=0; foreach ($result as $key => $value){ 
          $day='';
          $time='';
          if(isset($value['starts'])){
              $exp=explode(" ", $value['starts']);

              if(isset($exp[0])){
                 $timestamp =  strtotime($exp[0]);
                 $day = date('D d M', $timestamp);
              }
              if(isset($exp[1])){
                 $time = date('H:i', strtotime($value['starts']));
              }
          }
          ?>
        <tr  data-html2canvas-ignore="true">
          <td>
            <div class="form-group">
              <input type="checkbox" class="down_check" value="false" id="html{{$i}}">
              <label for="html{{$i}}"></label>
            </div>
          </td>
          <td>
            <div class="Property">
                  <a href="individual-result/{{$value['rentals'][0]['id']}}" class="homes-img img-box hover-effect">
                      <?php 
                          $img="images/properties1.jpg";
                          foreach ($value['rentals'][0]['assets'] as $key => $val) {
                              if($val['floorplan'] != 1){
                                  $img=$val['url'];
                                  break;
                              } 
                          } 

                      ?>
                      <!-- <img src="{{$img}}" alt="home-1" class="img-responsive"> -->
                      <img src="{{$img}}?timestamp=${new Date()}" crossOrigin="anonymous" alt="home-1" class="img-responsive">
                  </a>
               
            </div>
          </td>
          <td>
            <?php if(isset($value['rentals'][0]['property']['address']['full_address'])){ ?>
            <a href="individual-rent-results/{{$value['rentals'][0]['id']}}">
              @foreach(explode(',', $value['rentals'][0]['property']['address']['full_address']) as $info)
                <h1 class="name">{{$info ?? ""}}</h1>
              @endforeach 
            </a>
            <?php } ?>
          </td>
          <td>
              <div class="text-center properties-icon">
                  <?php
                      if (isset($value['rentals'][0]['property']['features'][1]['name']) && $value['rentals'][0]['property']['features'][1]['name'] == "Bedroom" && $value['rentals'][0]['property']['features'][1]['qty'] != 0)
                      {
                  ?>
                    <a>
                        <span>{{$value['rentals'][0]['property']['features'][1]['qty']  ?? '-' }}</span>
                        <img src="images/bed.png">
                    </a>

                  <?php unset($value['rentals'][0]['property']['features'][1]); } ?>


                  <?php 
                  $array=$value['rentals'][0]['property']['features'];

                  foreach ($value['rentals'][0]['property']['features'] as $keys => $values){
                        if($values['name'] == 'Bedroom'){
                            $img="images/bed.png";
                        }
                        if($values['name'] == 'Bathroom'){
                            $img="images/bath.png";
                        }

                        if($values['name'] == 'Parking'){
                            $img="images/car.png";
                        }

                        if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                     ?>
                    <a>
                        <span>{{$values['qty']  ?? '-' }}</span>
                        <img src="{{$img}}">
                    </a>
                    <?php } } ?>
                  
              </div>
          </td>
          <td>
              <p class="rupee">{{$value['rentals'][0]['price_view']}}</p>
          </td>
          <td>
              <p class="date" style="text-transform: uppercase;">{{$day ?? ''}}</p>
          </td>
          <td>
              <p class="time">{{$time ?? ''}}</p>
          </td>
          <td class="text-center">
              <a href="https://calendar.google.com/calendar/u/0/r/eventedit?text=Wiseberry%20meet&location={{$value['rentals'][0]['property']['address']['full_address']}}"><i class="fa fa-calendar"></i></a>
          </td>
        </tr>
      <?php $i++; } ?>
        
      </tbody>
    </table>
  </div>


                    </div>
                </div>
                <?php 
                    if(!empty($result)){ 
                        if($i == 10){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center mt4">
                        <a href="#!" class="see-more"  id="see-more-rentopen">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } }
                ?>
                
  </div>
  
            </div>
        </section>
        <?php } else{ ?>

        <section class="no-result">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>Thanks for searching. We do not currently have any properties for Rent in your area.</p>
                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <?php } ?>

@include('layout.footer')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<script type="text/javascript">
          $("#download").click(function(){
            var checkedbox = false;
                $(".down_check").each(function(){
                    // alert($(this).val());
                    if($(this).val() == 'true'){
                        checkedbox=true;

                    } 
                });

                if(checkedbox){

                    html2canvas($('#resp_table'), {
                        useCORS: true,
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL();
                            console.log(data);
                            var docDefinition = {
                                content: [{
                                    image: data,
                                    width: 500
                                }]
                            };
                            

                            pdfMake.createPdf(docDefinition).getBuffer(function(buffer) {
                              var blob = new Blob([buffer]);

                              var reader = new FileReader();
                              // this function is triggered once a call to readAsDataURL returns
                              reader.onload = function(event) {
                                var fd = new FormData();
                                fd.append('fname', 'test.pdf');
                                fd.append('data', event.target.result);
                                $.ajax({
                                  type: 'POST',
                                  url: 'https://szwebprofile.com/PHP/wiseberry/file_upload/upload.php?action=rentopenhome', // Change to PHP filename
                                  data: fd,
                                  processData: false,
                                  contentType: false
                                }).done(function(data) {
                                  // print the output from the upload.php script
                                  console.log(data);
                                });
                              };
                              // trigger the read from the reader...
                              reader.readAsDataURL(blob);
                            });

                            pdfMake.createPdf(docDefinition).download("Wiseberry Rental Open Homes.pdf");
                        }
                    });
                }else{
                    alert("Please Select Atleast One..")
                }
            });

          
            $('.down_check').on('click', function(e) {
                var elem = this;
                setTimeout(function(){ 

                   if($(elem).attr('value') == 'true'){
                       $(elem).parent().parent().parent().removeAttr("data-html2canvas-ignore");
                   }else{
                       $(elem).parent().parent().parent().attr('data-html2canvas-ignore',true);
                   }
                   // alert($(elem).attr('value'));
                 }, 100);
            });

          $(document).on("click","#selectall",function() {
                $('.down_check').prop('checked',true);
                $('.down_check').val('true');
                $('#selectall').prop('id',"unselectall");
                $("tr").removeAttr("data-html2canvas-ignore");
          });

          $(document).on("click","#unselectall",function() {
                $('.down_check').prop('checked',false);
                $('.down_check').val('false');
                $('#unselectall').prop('id',"selectall");
                $("tr").attr('data-html2canvas-ignore',true);
          });
          $("#share-option").click(function(){
              $(".social-btn-sp").toggle();

              var checkedbox = false;
                $(".down_check").each(function(){
                    // alert($(this).val());
                    if($(this).val() == 'true'){
                        checkedbox=true;

                    } 
                });

                if(checkedbox){

                    html2canvas($('#resp_table'), {
                        useCORS: true,
                        onrendered: function (canvas) {
                            var data = canvas.toDataURL();
                            console.log(data);
                            var docDefinition = {
                                content: [{
                                    image: data,
                                    width: 500
                                }]
                            };
                            

                            pdfMake.createPdf(docDefinition).getBuffer(function(buffer) {
                              var blob = new Blob([buffer]);

                              var reader = new FileReader();
                              // this function is triggered once a call to readAsDataURL returns
                              reader.onload = function(event) {
                                var fd = new FormData();
                                fd.append('fname', 'test.pdf');
                                fd.append('data', event.target.result);
                                $.ajax({
                                  type: 'POST',
                                  url: 'https://szwebprofile.com/PHP/wiseberry/file_upload/upload.php?action=rentopenhome', // Change to PHP filename
                                  data: fd,
                                  processData: false,
                                  contentType: false
                                }).done(function(data) {
                                  // print the output from the upload.php script
                                  console.log(data);
                                });
                              };
                              // trigger the read from the reader...
                              reader.readAsDataURL(blob);
                            });

                            
                        }
                    });
                }else{
                    alert("Please Select Atleast One..");
                }
          });
        </script>