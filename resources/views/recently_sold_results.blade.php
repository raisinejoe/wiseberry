@include('layout.header')
@include('layout.menu')

 <div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h1>Sold Properties In “Suburb” (Example: Sold Properties In Castle Hill NSW 2154)</h1>-->
<!--<h2>Suburb (Example: Castle Hill) </h2>
<h1>Sold Properties In “Suburb” (Example: Sold Properties In Castle Hill NSW 2154)</h1>
<h2>Suburb (Example: Castle Hill)  </h2>
-->

        <div class="recently-sold-results">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('images/sold-result-banner.jpg');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-result">
                                 
                                 <h1 class="text-center">REFINE SEARCH</h1>
                                 <div class="inner">
                                    <form method="POST" id="search_form" action="recently-sold-results">
                                         
                                    <div class="row">
                                        <div class="col-md-3 pr-0">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="pageno" id="pageno" value="2">
                                            <input type="hidden" name="pagename" id="pagename" value="recently_sold">
                                            <input type="hidden" name="search_type" id="search_type" value="">
                                            <select class="form-control" id="page_option">
                                                <option value="1">Buy</option>
                                                <option value="2">Rent</option>
                                                <option value="3">Holiday Rental</option>
                                                <option value="4">Commercial Rental</option>
                                                <option value="5" selected>Sold</option>
                                                <option value="6">Leased</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input type="txt" id="tags" value="{{$request->suburb_name}}" name="" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" value="{{$request->suburb}}" id="tags_val" name="suburb" placeholder="Search by region, suburb, address, postcode or agent" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                    <div class="tr-single-box">
                                        <div class="tr-single-body">
                                           
                                            <!-- Paypal Option -->
                                            <div class="payment-card">
                                               
                                                <div class="collapse" id="paypal" role="tablist" aria-expanded="false">
                                                    <div class="payment-card-body">
                                                        <div class="row">

                                                            <div class="col-md-3 pr-0">
                                                                <input type="hidden" name="prop_hid" id="prop_hid" value="{{$request->option}}">
                                                                <select class="form-control" id="buy_options" name="option" style="border-right: none;">
                                                                    <option value="">Any Property Type</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-6 pl-0" style="padding-right: 7px;">
                                                                <div class="details">
                                                                <select class="form-control" name="bed">
                                                                    <option value="">Any Bed</option>
                                                                    <option <?php if ($request->bed == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bed == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bed == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bed == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bed == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="bath">
                                                                    <option value="">Any Bath</option>
                                                                    <option <?php if ($request->bath == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->bath == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->bath == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->bath == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->bath == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="parking">
                                                                    <option value="">Any Parking</option>
                                                                    <option <?php if ($request->parking == '1') echo ' selected="selected"'; ?> value="1">1</option>
                                                                    <option <?php if ($request->parking == '2') echo ' selected="selected"'; ?> value="2">2</option>
                                                                    <option <?php if ($request->parking == '3') echo ' selected="selected"'; ?> value="3">3</option>
                                                                    <option <?php if ($request->parking == '4') echo ' selected="selected"'; ?> value="4">4</option>
                                                                    <option <?php if ($request->parking == '5') echo ' selected="selected"'; ?> value="5">5</option>
                                                                </select>
                                                                <select class="form-control" name="price_from">
                                                                    <option value="">Min Price</option>
                                                                    <option <?php if ($request->price_from == '50000') echo ' selected="selected"'; ?> value="50000">$50,000</option>
                                                                    <option <?php if ($request->price_from == '75000') echo ' selected="selected"'; ?> value="75000">$75,000</option>
                                                                    <option <?php if ($request->price_from == '100000') echo ' selected="selected"'; ?> value="100000">$100,000</option>
                                                                    <option <?php if ($request->price_from == '125000') echo ' selected="selected"'; ?> value="125000">$125,000</option>
                                                                    <option <?php if ($request->price_from == '150000') echo ' selected="selected"'; ?> value="150000">$150,000</option>
                                                                    <option <?php if ($request->price_from == '175000') echo ' selected="selected"'; ?> value="175000">$175,000</option>
                                                                    <option <?php if ($request->price_from == '200000') echo ' selected="selected"'; ?> value="200000">$200,000</option>
                                                                    <option <?php if ($request->price_from == '225000') echo ' selected="selected"'; ?> value="225000">$225,000</option>
                                                                    <option <?php if ($request->price_from == '250000') echo ' selected="selected"'; ?> value="250000">$250,000</option>
                                                                    <option <?php if ($request->price_from == '275000') echo ' selected="selected"'; ?> value="275000">$275,000</option>
                                                                    <option <?php if ($request->price_from == '300000') echo ' selected="selected"'; ?> value="300000">$300,000</option>
                                                                    <option <?php if ($request->price_from == '325000') echo ' selected="selected"'; ?> value="325000">$325,000</option>
                                                                    <option <?php if ($request->price_from == '350000') echo ' selected="selected"'; ?> value="350000">$350,000</option>
                                                                    <option <?php if ($request->price_from == '375000') echo ' selected="selected"'; ?> value="375000">$375,000</option>
                                                                    <option  <?php if ($request->price_from == '400000') echo ' selected="selected"'; ?> value="400000">$400,000</option>
                                                                    <option <?php if ($request->price_from == '425000') echo ' selected="selected"'; ?> value="425000">$425,000</option>
                                                                    <option <?php if ($request->price_from == '450000') echo ' selected="selected"'; ?> value="450000">$450,000</option>
                                                                    <option <?php if ($request->price_from == '475000') echo ' selected="selected"'; ?> value="475000">$475,000</option>
                                                                    <option <?php if ($request->price_from == '500000') echo ' selected="selected"'; ?> value="500000">$500,000</option>
                                                                    <option <?php if ($request->price_from == '550000') echo ' selected="selected"'; ?> value="550000">$550,000</option>
                                                                    <option <?php if ($request->price_from == '600000') echo ' selected="selected"'; ?> value="600000">$600,000</option>
                                                                    <option <?php if ($request->price_from == '650000') echo ' selected="selected"'; ?> value="650000">$650,000</option>
                                                                    <option <?php if ($request->price_from == '700000') echo ' selected="selected"'; ?> value="700000">$700,000</option>
                                                                    <option <?php if ($request->price_from == '750000') echo ' selected="selected"'; ?> value="750000">$750,000</option>
                                                                    <option <?php if ($request->price_from == '800000') echo ' selected="selected"'; ?> value="800000">$800,000</option>
                                                                    <option <?php if ($request->price_from == '850000') echo ' selected="selected"'; ?> value="850000">$850,000</option>
                                                                    <option <?php if ($request->price_from == '900000') echo ' selected="selected"'; ?> value="900000">$900,000</option>
                                                                    <option <?php if ($request->price_from == '950000') echo ' selected="selected"'; ?> value="950000">$950,000</option>
                                                                    <option <?php if ($request->price_from == '1000000') echo ' selected="selected"'; ?> value="1000000">$1,000,000</option>
                                                                    <option <?php if ($request->price_from == '1100000') echo ' selected="selected"'; ?> value="1100000">$1,100,000</option>
                                                                    <option <?php if ($request->price_from == '1200000') echo ' selected="selected"'; ?> value="1200000">$1,200,000</option>
                                                                    <option <?php if ($request->price_from == '1300000') echo ' selected="selected"'; ?> value="1300000">$1,300,000</option>
                                                                    <option <?php if ($request->price_from == '1400000') echo ' selected="selected"'; ?> value="1400000">$1,400,000</option>
                                                                    <option <?php if ($request->price_from == '1500000') echo ' selected="selected"'; ?> value="1500000">$1,500,000</option>
                                                                    <option <?php if ($request->price_from == '1600000') echo ' selected="selected"'; ?> value="1600000">$1,600,000</option>
                                                                    <option <?php if ($request->price_from == '1700000') echo ' selected="selected"'; ?> value="1700000">$1,700,000</option>
                                                                    <option <?php if ($request->price_from == '1800000') echo ' selected="selected"'; ?> value="1800000">$1,800,000</option>
                                                                    <option <?php if ($request->price_from == '1900000') echo ' selected="selected"'; ?> value="1900000">$1,900,000</option>
                                                                    <option <?php if ($request->price_from == '2000000') echo ' selected="selected"'; ?> value="2000000">$2,000,000</option>
                                                                    <option <?php if ($request->price_from == '2250000') echo ' selected="selected"'; ?> value="2250000">$2,250,000</option>
                                                                    <option <?php if ($request->price_from == '2500000') echo ' selected="selected"'; ?> value="2500000">$2,500,000</option>
                                                                    <option <?php if ($request->price_from == '2750000') echo ' selected="selected"'; ?> value="2750000">$2,750,000</option>
                                                                    <option <?php if ($request->price_from == '3000000') echo ' selected="selected"'; ?> value="3000000">$3,000,000</option>
                                                                    <option <?php if ($request->price_from == '3500000') echo ' selected="selected"'; ?> value="3500000">$3,500,000</option>
                                                                    <option <?php if ($request->price_from == '4000000') echo ' selected="selected"'; ?> value="4000000">$4,000,000</option>
                                                                    <option <?php if ($request->price_from == '4500000') echo ' selected="selected"'; ?> value="4500000">$4,500,000</option>
                                                                    <option <?php if ($request->price_from == '5000000') echo ' selected="selected"'; ?> value="5000000">$5,000,000</option>
                                                                    <option <?php if ($request->price_from == '6000000') echo ' selected="selected"'; ?> value="6000000">$6,000,000</option>
                                                                    <option <?php if ($request->price_from == '7000000') echo ' selected="selected"'; ?> value="7000000">$7,000,000</option>
                                                                    <option <?php if ($request->price_from == '8000000') echo ' selected="selected"'; ?> value="8000000">$8,000,000</option>
                                                                    <option <?php if ($request->price_from == '9000000') echo ' selected="selected"'; ?> value="9000000">$9,000,000</option>
                                                                    <option <?php if ($request->price_from == '10000000') echo ' selected="selected"'; ?> value="10000000">$10,000,000</option>
                                                                    <option <?php if ($request->price_from == '12000000') echo ' selected="selected"'; ?> value="12000000">$12,000,000</option>
                                                                    <option <?php if ($request->price_from == '15000000') echo ' selected="selected"'; ?> value="15000000">$15,000,000</option>
                                                                </select>
                                                                <select class="form-control" name="price_to">
                                                                    <option value="">Max Price</option>
                                                                    <option <?php if ($request->price_to == '50000') echo ' selected="selected"'; ?> value="50000">$50,000</option>
                                                                    <option <?php if ($request->price_to == '75000') echo ' selected="selected"'; ?> value="75000">$75,000</option>
                                                                    <option <?php if ($request->price_to == '100000') echo ' selected="selected"'; ?> value="100000">$100,000</option>
                                                                    <option <?php if ($request->price_to == '125000') echo ' selected="selected"'; ?> value="125000">$125,000</option>
                                                                    <option <?php if ($request->price_to == '150000') echo ' selected="selected"'; ?> value="150000">$150,000</option>
                                                                    <option <?php if ($request->price_to == '175000') echo ' selected="selected"'; ?> value="175000">$175,000</option>
                                                                    <option <?php if ($request->price_to == '200000') echo ' selected="selected"'; ?> value="200000">$200,000</option>
                                                                    <option <?php if ($request->price_to == '225000') echo ' selected="selected"'; ?> value="225000">$225,000</option>
                                                                    <option <?php if ($request->price_to == '250000') echo ' selected="selected"'; ?> value="250000">$250,000</option>
                                                                    <option <?php if ($request->price_to == '275000') echo ' selected="selected"'; ?> value="275000">$275,000</option>
                                                                    <option <?php if ($request->price_to == '300000') echo ' selected="selected"'; ?> value="300000">$300,000</option>
                                                                    <option <?php if ($request->price_to == '325000') echo ' selected="selected"'; ?> value="325000">$325,000</option>
                                                                    <option <?php if ($request->price_to == '350000') echo ' selected="selected"'; ?> value="350000">$350,000</option>
                                                                    <option <?php if ($request->price_to == '375000') echo ' selected="selected"'; ?> value="375000">$375,000</option>
                                                                    <option  <?php if ($request->price_to == '400000') echo ' selected="selected"'; ?> value="400000">$400,000</option>
                                                                    <option <?php if ($request->price_to == '425000') echo ' selected="selected"'; ?> value="425000">$425,000</option>
                                                                    <option <?php if ($request->price_to == '450000') echo ' selected="selected"'; ?> value="450000">$450,000</option>
                                                                    <option <?php if ($request->price_to == '475000') echo ' selected="selected"'; ?> value="475000">$475,000</option>
                                                                    <option <?php if ($request->price_to == '500000') echo ' selected="selected"'; ?> value="500000">$500,000</option>
                                                                    <option <?php if ($request->price_to == '550000') echo ' selected="selected"'; ?> value="550000">$550,000</option>
                                                                    <option <?php if ($request->price_to == '600000') echo ' selected="selected"'; ?> value="600000">$600,000</option>
                                                                    <option <?php if ($request->price_to == '650000') echo ' selected="selected"'; ?> value="650000">$650,000</option>
                                                                    <option <?php if ($request->price_to == '700000') echo ' selected="selected"'; ?> value="700000">$700,000</option>
                                                                    <option <?php if ($request->price_to == '750000') echo ' selected="selected"'; ?> value="750000">$750,000</option>
                                                                    <option <?php if ($request->price_to == '800000') echo ' selected="selected"'; ?> value="800000">$800,000</option>
                                                                    <option <?php if ($request->price_to == '850000') echo ' selected="selected"'; ?> value="850000">$850,000</option>
                                                                    <option <?php if ($request->price_to == '900000') echo ' selected="selected"'; ?> value="900000">$900,000</option>
                                                                    <option <?php if ($request->price_to == '950000') echo ' selected="selected"'; ?> value="950000">$950,000</option>
                                                                    <option <?php if ($request->price_to == '1000000') echo ' selected="selected"'; ?> value="1000000">$1,000,000</option>
                                                                    <option <?php if ($request->price_to == '1100000') echo ' selected="selected"'; ?> value="1100000">$1,100,000</option>
                                                                    <option <?php if ($request->price_to == '1200000') echo ' selected="selected"'; ?> value="1200000">$1,200,000</option>
                                                                    <option <?php if ($request->price_to == '1300000') echo ' selected="selected"'; ?> value="1300000">$1,300,000</option>
                                                                    <option <?php if ($request->price_to == '1400000') echo ' selected="selected"'; ?> value="1400000">$1,400,000</option>
                                                                    <option <?php if ($request->price_to == '1500000') echo ' selected="selected"'; ?> value="1500000">$1,500,000</option>
                                                                    <option <?php if ($request->price_to == '1600000') echo ' selected="selected"'; ?> value="1600000">$1,600,000</option>
                                                                    <option <?php if ($request->price_to == '1700000') echo ' selected="selected"'; ?> value="1700000">$1,700,000</option>
                                                                    <option <?php if ($request->price_to == '1800000') echo ' selected="selected"'; ?> value="1800000">$1,800,000</option>
                                                                    <option <?php if ($request->price_to == '1900000') echo ' selected="selected"'; ?> value="1900000">$1,900,000</option>
                                                                    <option <?php if ($request->price_to == '2000000') echo ' selected="selected"'; ?> value="2000000">$2,000,000</option>
                                                                    <option <?php if ($request->price_to == '2250000') echo ' selected="selected"'; ?> value="2250000">$2,250,000</option>
                                                                    <option <?php if ($request->price_to == '2500000') echo ' selected="selected"'; ?> value="2500000">$2,500,000</option>
                                                                    <option <?php if ($request->price_to == '2750000') echo ' selected="selected"'; ?> value="2750000">$2,750,000</option>
                                                                    <option <?php if ($request->price_to == '3000000') echo ' selected="selected"'; ?> value="3000000">$3,000,000</option>
                                                                    <option <?php if ($request->price_to == '3500000') echo ' selected="selected"'; ?> value="3500000">$3,500,000</option>
                                                                    <option <?php if ($request->price_to == '4000000') echo ' selected="selected"'; ?> value="4000000">$4,000,000</option>
                                                                    <option <?php if ($request->price_to == '4500000') echo ' selected="selected"'; ?> value="4500000">$4,500,000</option>
                                                                    <option <?php if ($request->price_to == '5000000') echo ' selected="selected"'; ?> value="5000000">$5,000,000</option>
                                                                    <option <?php if ($request->price_to == '6000000') echo ' selected="selected"'; ?> value="6000000">$6,000,000</option>
                                                                    <option <?php if ($request->price_to == '7000000') echo ' selected="selected"'; ?> value="7000000">$7,000,000</option>
                                                                    <option <?php if ($request->price_to == '8000000') echo ' selected="selected"'; ?> value="8000000">$8,000,000</option>
                                                                    <option <?php if ($request->price_to == '9000000') echo ' selected="selected"'; ?> value="9000000">$9,000,000</option>
                                                                    <option <?php if ($request->price_to == '10000000') echo ' selected="selected"'; ?> value="10000000">$10,000,000</option>
                                                                    <option <?php if ($request->price_to == '12000000') echo ' selected="selected"'; ?> value="12000000">$12,000,000</option>
                                                                    <option <?php if ($request->price_to == '15000000') echo ' selected="selected"'; ?> value="15000000">$15,000,000</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="checkboxes">
                                                                    <div class="filter-tags-wrap">
                                                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                            <label for="styled-checkbox-1">Surrounding Suburbs</label>
                                                                    </div>
                                                                </div>
                                                                <!--<div class="checkboxes">-->
                                                                <!--    <div class="filter-tags-wrap">-->
                                                                <!--        <input id="check-b" type="checkbox" name="check">-->
                                                                <!--        <label for="check-b">Surrounding Suburbs</label>-->
                                                                <!--    </div>-->
                                                                <!--</div>-->
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                         
                                        </div>
                                    </div>

                                        </div>
                                    </div>


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

    

        <!-- START Buy Result -->
        <?php if(!empty($result)){ ?>
        <section class="recently portfolio bg-white-1 home18 buy-result">
            
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="sec-title pb1">
                    <h2>Recently Sold Properties</h2>
                </div>
                    </div>
                </div>

                <div class="row" id="append_results">
                    <?php
                        $i=0;
                        foreach ($result as $key => $value){
                    ?>
                    <div class="col-md-4">
                        <div class="agents-grid">
                            <div class="landscapes">
                                <div class="project-single">

                                    <div class="project-inner">
                                        
                                        <div class="homes">
                                            <!-- homes img -->
                                            <a href="individual-result/{{$value['id']}}" class="homes-img img-box hover-effect">
                                                
                                                <?php if(isset($value['assets'][0]['url'])){ ?>
                                                    <img src="{{$value['assets'][0]['url']}}"  alt="home-1" class="img-responsive">
                                                <?php }else{ ?>
                                                    <img src="images/properties1.jpg" alt="home-1" class="img-responsive">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- homes content -->
                                    <div class="homes-content">
                                        <!-- homes address -->
                                        <h3><a href="individual-result/{{$value['id']}}">{{$value['property']['address']['full_address']}}</a></h3>
                                        
                                        <!-- homes List -->
                                        <div class="text-center properties-icon">
                                            <?php foreach ($value['property']['features'] as $keys => $values){
                                                if($values['name'] == 'Bedroom'){
                                                    $img="images/bed.svg";
                                                }
                                                if($values['name'] == 'Bathroom'){
                                                    $img="images/bath.svg";
                                                }

                                                if($values['name'] == 'Parking'){
                                                    $img="images/car.svg";
                                                }

                                                if($values['name'] == 'Bedroom' || $values['name'] == 'Bathroom' || $values['name'] == 'Parking'){
                                             ?>
                                            <a>
                                                <span>{{$values['qty']  ?? '-' }}</span>
                                                <img src="{{$img}}">
                                            </a>
                                            <?php } } ?>
                                            
                                        </div>

                                        <p class="homes-address mb-3">
                                            <a href="#!">
                                                <span style="text-transform:uppercase">Sold On {{date('d F Y', strtotime($value['sale']['settlement']))}}</span>
                                            </a>
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $i++; }  ?>

                </div>

                <?php 
                    if(!empty($result)){ 
                        if($i == 12 && $next != null){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center mt2">
                        <a href="#!" class="see-more" id="see-more">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } }
                ?>
                
            </div>

        </section>
    <?php }  else{ ?>

    		<section class="no-result">
		            <div class="container">
		                <div class="row">
		                    <div class="col-md-12 text-center">
		                        <p>Thanks for searching. We do not currently have any sold properties in your area.</p>
		                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
		                    </div>
		                </div>
		            </div>
		        </section>
		        <?php } ?>
        </div>

@include('layout.footer')