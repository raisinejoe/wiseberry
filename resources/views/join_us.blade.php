@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h3>Career In Real Estate</h3>-->
        <div class="join-us">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">JOIN US</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <section class="notice-repair-section p-80">
            <input type="hidden" name="" id="tags">
            <div class="container">
               
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2>{{$data->heading}}</h2>
                            {!! $data->desc !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail_join" id="capt_form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                          <option value="All">Wiseberry Office *</option>
                          <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                          <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                          <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                          <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                          <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                          <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                          <option value="Wiseberry Dural">Wiseberry Dural</option>
                          <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                          <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                          <option value="Wiseberry Forster">Wiseberry Forster</option>
                          <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                          <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                          <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                          <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                          <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                          <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                          <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                          <option value="Wiseberry Picton ">Wiseberry Picton </option>
                          <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                          <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                          <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                          <option value="Wiseberry Taree">Wiseberry Taree</option>
                          <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                          <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>
                  </div>
                  
                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary see-more">LET&apos;S GO!</button>
                  </div>

                </div>
             
              
            </form>
                    </div>
                </div>

            </div>
            
            

        </section>
        </div>

@include('layout.footer')