Hello <br><br>

<p>First Name: {{$fname}}</p>
<p>Last Name: {{$lname}}</p>
<p>Contact Number: {{$contact}}</p>
<p>Email: {{$email}}</p>
<p>Property Address: {{$address}}</p>
<p>Office: {{$office}}</p>
<p>Authority to Enter: {{$authority}}</p>
<p>Preferred Access: {{$pref_address}}</p>
<p>Comments: {{$comments}}</p>