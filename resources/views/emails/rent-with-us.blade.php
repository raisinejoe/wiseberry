Hello <br><br>

<p>First Name: {{$fname}}</p>
<p>Last Name: {{$lname}}</p>
<p>Contact Number: {{$contact}}</p>
<p>Email: {{$email}}</p>
<p>Property Type: {{$property}}</p>
<p>Desired Region(s) or Suburb(s): {{$region}}</p>
<p>Bed: {{$bed}}</p>
<p>Bath: {{$bath}}</p>
<p>Car: {{$car}}</p>
<p>Wiseberry Office: {{$office}}</p>
<p>Weekly Min Price: {{$min_price}}</p>
<p>Weekly Max Price: {{$max_price}}</p>
<p>Comments: {{$comments}}</p>