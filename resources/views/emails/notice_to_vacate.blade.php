Hello <br><br>

<p>First Name: {{$fname}}</p>
<p>Last Name: {{$lname}}</p>
<p>Contact Number: {{$contact}}</p>
<p>Email: {{$email}}</p>
<p>Property Address: {{$address}}</p>
<p>Office: {{$office}}</p>
<p>Are your breaking your lease?: {{$lease}}</p>
<p>Vacate Date: {{$vac_date}}</p>
<p>Forwarding Address: {{$for_address}}</p>
<p>Comments: {{$comments}}</p>