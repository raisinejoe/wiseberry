Hello <br><br>

<p>First Name: {{$fname}}</p>
<p>Last Name: {{$lname}}</p>
<p>Contact Number: {{$contact}}</p>
<p>Email: {{$email}}</p>
<p>Property Address: {{$address}}</p>
<p>Suburb: {{$suburb}}</p>
<p>Property Type: {{$property_type}}</p>
<p>Preferred Wiseberry Office: {{$office}}</p>
<p>Comments: {{$comments}}</p>