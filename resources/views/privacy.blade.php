@include('layout.header')
@include('layout.menu')


<div class="clearfix"></div>
        <!-- Header Container / End -->
<!--<h2>Wiseberry Privacy Polic</h2>-->
        <!-- STAR HEADER IMAGE -->
        <div class="privacy">
            <section class="header-image home-18 d-flex align-items-center inner-banner" id="slider" style="background: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

       <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WANT TO KNOW HOW MUCH MY PROPERTY IS WORTH.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="appraisal">REQUEST YOUR APPRAISAL</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


  
     <section class="privacy-section p-80">
         
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                     
                     <h1>{{$data->heading}}</h1>
                     {!! $data->desc !!}
<h2>Wiseberry ‘Privacy Policy’ Current as at November 2018.</h2>


                 </div>
             </div>
         </div>

     </section>
        </div>

       <!-- START SUBSCRIBE NOW -->


@include('layout.footer')