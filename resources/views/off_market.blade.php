@include('layout.header')
@include('layout.menu')
    <div class="clearfix"></div>
        <!-- Header Container / End -->
<!--<h2>Properties For Sale</h2>-->
        <div class="off-market">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner" id="slider" style="background: url({{$img_path}}{{$data->banner}});">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">OFF MARKET PROPERTIES</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <!--<h1>{{$data->sub_title}}</h1>-->
                        <h1>I WANT TO KNOW HOW MUCH MONEY I CAN BORROW.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a style="color: #fff;cursor: pointer;" data-toggle="modal"  data-target="#myModal_calculator">HOME LOANS CALCULATOR</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
                <input type="hidden" name="" id="tags">
            </div>
        </section>





        <section class="notice-repair-section p-80">

            <div class="container">

              
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h1>{{$data->sub_title}}</h1>
            <p>Register now for exclusive off market properties from across the Wiseberry network.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail" id="capt_form" method="POST">

                          <div class="row">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" required  name="fname" placeholder="First Name*">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" required name="pin" placeholder="Postcode*">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="oldtags" placeholder="Suburb/Locality">
                                <input type="hidden" id="oldtags_val" name="locality" >
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <select class="form-control" required name="office">
                                    <option value="">Select an Office</option>
                                    <option value="All">All</option>
                                    <option value="Wiseberry Bankstown">Wiseberry Bankstown</option>
                                    <option value="Wiseberry Baulkham Hills">Wiseberry Baulkham Hills</option>
                                    <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                                    <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                                    <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                                    <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                                    <option value="Wiseberry Dural">Wiseberry Dural</option>
                                    <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                                    <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                                    <option value="Wiseberry Forster">Wiseberry Forster</option>
                                    <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                                    <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                                    <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                                    <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                                    <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                                    <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                                    <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                                    <option value="Wiseberry Picton ">Wiseberry Picton </option>
                                    <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                                    <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                                    <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                                    <option value="Wiseberry Taree">Wiseberry Taree</option>
                                    <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                                    <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <input type="text" class="form-control" id="subtags" placeholder="Suburb/Region Looking to Buy">
                                <input type="hidden" id="subtags_val" name="region" >
                              </div>
                            </div>


                            <div class="col-md-6" style="padding: 0 30px;">
                              <div class="row">
                                  <div class="col-md-4 first">
                                  <div class="form-group">
                                <select class="form-control" name="bed">
                                    <option value="any">Any Bed</option>
                                    <option value="1">1 Bed</option>
                                    <option value="2">2 Bed</option>
                                    <option value="3">3 Bed</option>
                                    <option value="4">4 Bed</option>
                                    <option value="5">5 Bed</option>
                                </select>
                              </div>
                              </div>
                              <div class="col-md-4 second">
                                  <div class="form-group">
                                <select class="form-control" name="bath">
                                    <option value="any">Any Bath</option>
                                    <option value="1">1 Bath</option>
                                    <option value="2">2 Bath</option>
                                    <option value="3">3 Bath</option>
                                    <option value="4">4 Bath</option>
                                    <option value="5">5 Bath</option>
                                </select>
                              </div>
                              </div>
                              <div class="col-md-4 third">
                                  <div class="form-group">
                                <select class="form-control" name="parking">
                                    <option value="any">Any Parking</option>
                                    <option value="1">1 Car</option>
                                    <option value="2">2 Car</option>
                                    <option value="3">3 Car</option>
                                    <option value="4">4 Car</option>
                                    <option value="5">5 Car</option>
                                </select>
                              </div>
                              </div>
                              </div>

                              <div class="row">
                                  <div class="col-md-6 first">
                                  <div class="form-group">
                                <select class="form-control" name="price_from">
                                                                    <option value="">Min Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                              </div>
                              </div>
                              <div class="col-md-6 second">
                                  <div class="form-group">
                                <select class="form-control" name="price_to">
                                                                    <option value="">Max Price</option>
                                                                    <option value="50000">$50,000</option>
                                                                    <option value="75000">$75,000</option>
                                                                    <option value="100000">$100,000</option>
                                                                    <option value="125000">$125,000</option>
                                                                    <option value="150000">$150,000</option>
                                                                    <option value="175000">$175,000</option>
                                                                    <option value="200000">$200,000</option>
                                                                    <option value="225000">$225,000</option>
                                                                    <option value="250000">$250,000</option>
                                                                    <option value="275000">$275,000</option>
                                                                    <option value="300000">$300,000</option>
                                                                    <option value="325000">$325,000</option>
                                                                    <option value="350000">$350,000</option>
                                                                    <option value="375000">$375,000</option>
                                                                    <option value="400000">$400,000</option>
                                                                    <option value="425000">$425,000</option>
                                                                    <option value="450000">$450,000</option>
                                                                    <option value="475000">$475,000</option>
                                                                    <option value="500000">$500,000</option>
                                                                    <option value="550000">$550,000</option>
                                                                    <option value="600000">$600,000</option>
                                                                    <option value="650000">$650,000</option>
                                                                    <option value="700000">$700,000</option>
                                                                    <option value="750000">$750,000</option>
                                                                    <option value="800000">$800,000</option>
                                                                    <option value="850000">$850,000</option>
                                                                    <option value="900000">$900,000</option>
                                                                    <option value="950000">$950,000</option>
                                                                    <option value="1000000">$1,000,000</option>
                                                                    <option value="1100000">$1,100,000</option>
                                                                    <option value="1200000">$1,200,000</option>
                                                                    <option value="1300000">$1,300,000</option>
                                                                    <option value="1400000">$1,400,000</option>
                                                                    <option value="1500000">$1,500,000</option>
                                                                    <option value="1600000">$1,600,000</option>
                                                                    <option value="1700000">$1,700,000</option>
                                                                    <option value="1800000">$1,800,000</option>
                                                                    <option value="1900000">$1,900,000</option>
                                                                    <option value="2000000">$2,000,000</option>
                                                                    <option value="2250000">$2,250,000</option>
                                                                    <option value="2500000">$2,500,000</option>
                                                                    <option value="2750000">$2,750,000</option>
                                                                    <option value="3000000">$3,000,000</option>
                                                                    <option value="3500000">$3,500,000</option>
                                                                    <option value="4000000">$4,000,000</option>
                                                                    <option value="4500000">$4,500,000</option>
                                                                    <option value="5000000">$5,000,000</option>
                                                                    <option value="6000000">$6,000,000</option>
                                                                    <option value="7000000">$7,000,000</option>
                                                                    <option value="8000000">$8,000,000</option>
                                                                    <option value="9000000">$9,000,000</option>
                                                                    <option value="10000000">$10,000,000</option>
                                                                    <option value="12000000">$12,000,000</option>
                                                                    <option value="15000000">$15,000,000</option>
                                                                </select>
                              </div>
                              </div>
                              
                              </div>
                              
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                  <textarea class="form-control" name="comments" placeholder="Additional Comments"></textarea>
                              </div>
                            </div>

                            
                            <div class="col-md-12">
                                
                                <label>
                                  <input type="checkbox" name="check">
                                  <span>I have read and agree to the Privacy Statement. *</span>
                                </label>

                            </div>
                            
                            <div class="col-md-12 recapthcha-section">
                                <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                              </div>

                            <div class="col-md-12 text-center">
                              <button type="submit" class="btn btn-primary see-more">LET’S GO!</button>
                            </div>

                          </div>
                        </form>
                    </div>
                </div>

            </div>
            
            

        </section>
        </div>

@include('layout.footer')
<!-- END SECTION WHY WISEBERRY -->

       
       <!-- START SUBSCRIBE NOW -->

       
