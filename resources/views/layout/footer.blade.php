<!-- START  -->
<style type="text/css">
  .ui-datepicker-month{
    color: #fff !important;
  }
  /*.ui-menu .ui-menu-item-wrapper{*/
  /*  background: #ffffff !important;*/
  /*}*/
  .ui-widget.ui-widget-content{
    background: #ffffff !important;
  }
</style>
<script src="https://calcs.widgetworks.com.au/s/daparodo/live.js"></script>
       <section class="subscribe-section">
           <div class="container-fluid">
               <div class="row">
                   
                   <div class="col-lg-6 col-md-5 col-xs-12">
                       
                       <ul class="social">
                        <li><a href="https://www.facebook.com/wiseberryrealestate" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="https://www.instagram.com/wiseberryrealestate/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/wiseberry" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="https://www.youtube.com/user/WiseberryRealEstate" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="https://twitter.com/wiseberry_re" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    </ul>

                   </div>
                   <div class="col-lg-6 col-md-7 col-xs-12">
                       
                        
                          <label for="email">SUBSCRIBE NOW</label>
                          
                          <input type="email" class="form-control" placeholder="Enter an email address" id="subscribe_id">
                           <!-- <div class="show-reg-form modal-open"><a href="#">RENT</a></div> -->
                          <button class="btn btn-primary" data-toggle="modal" id="subscribe" data-target="#myModal_s">Subscribe</button>
                        
                   </div>

               </div>
           </div>
       </section>
       <!-- END SECTION SUBSCRIBE NOW -->

        <!-- START FOOTER -->
        <footer class="first-footer">
            <div class="top-footer">
                <div class="container-fluid responsive">
                    <div class="row">
                        <?php
                            $menu = getmenu();
                            $menus=$menu->menus;
                        ?>
                        <?php foreach($menus as $key => $value){ 
                          if($value->title != 'Blogs' && $value->title != 'Our Offices' && $value->title != 'Our Agents'){
                          ?>
                        <div class="col-lg-2 col-md-2">

                            <h5 class="title">{{$value->title}}</h5>
                            <ul class="links">
                                <li>
                                    <?php 
                                    if(isset($value->submenu)){ 
                                    foreach($value->submenu as $keys => $values){ ?>
                                        <a href="{{$values->link}}">{{$values->title}}</a>
                                    <?php }} ?>
                                </li>
                            </ul>
                        </div>
                        <?php }} ?>


                </div>
            </div>

            <div class="container-fluid responsive1">
              <div class="accordion" id="accordionExample">
                <?php
                    $menu = getmenu();
                    $menus=$menu->menus;
                ?>
                <?php $ss=1; foreach($menus as $key => $value){ 
                  $sh = ""; if($ss == 1){ $cl="One"; $sh="show"; } else if($ss == 2){ $cl="Two"; } else if($ss == 3){ $cl="Three"; } else if($ss == 4){ $cl="four"; } else if($ss == 5){ $cl="five"; }
                  if($value->title != 'Blogs' && $value->title != 'Our Offices' && $value->title != 'Our Agents'){
                ?>
                <div class="card">
                  <div class="card-head" id="heading{{$ss}}">
                    
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapse{{$ss}}" aria-expanded="true" aria-controls="collapse{{$ss}}">
                        {{$value->title}}
                    </h2>
                  </div>

                  <div id="collapse{{$ss}}" class="collapse {{$sh}}" aria-labelledby="heading{{$ss}}" data-parent="#accordionExample">
                    <div class="card-body">
                      <ul class="links">
                            <li>
                                <?php 
                                    if(isset($value->submenu)){ 
                                    foreach($value->submenu as $keys => $values){ 
                                ?>
                                    <a href="{{$values->link}}">{{$values->title}}</a>
                                <?php }} ?>
                            </ul>
                    </div>
                  </div>
                </div>
                <?php } $ss++;} ?>
              
            </div>
            </div>
            <div class="second-footer">
                <div class="container">
                        <div class="col-md-12">
                            <div class="copyright text-center">
                                <li><a href="index.html">© Wiseberry 2021</a></li>
                                <li><a href="privacy-policy"><span></span>Privacy Policy</a></li>
                                <li><a href="terms"><span></span>Terms & Conditions</a></li>
                                
                            </div>
                        </div>
                </div>
            </div>
        </footer>

        

        <a data-scroll href="#wrapper" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
        <!-- END FOOTER -->


    
  <div class="modal fade home-rent subscribe" id="myModal_s">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">SUBSCRIBE</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <?php
          if(Request::segment(1) == 'individual-result' || Request::segment(1) == 'individual-rent-results' || Request::segment(1) == 'individual-agent' || Request::segment(1) == 'office-home' ){

            $dy_html='<input type="hidden" id="ind_org_id" name="office">';
            $pageto="../sendmail_ind_agent";
          }else{
            $pageto="sendmail_ind_agent";
            // $dy_html='<div class="col-md-6"><div class="form-group"><select class="form-control" id="list_offices_sub" required name="office"></select></div></div>';
            $dy_html='<input type="hidden" id="ind_org_id" value="4" name="office">';
          }
        ?>
        <!-- Modal body -->
        <div class="modal-body ui-front">
            <p>So that we can send you relevant information, please fill out the below.</p>

            <form action="{{$pageto}}" id="capt_form_sub" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required  name="fname" placeholder="First Name*">
                      <input type="hidden" class="form-control" value="{{ Request::segment(1) }}" name="pagename" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required  name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required id="subscribe_email"  name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required id="oldtags" placeholder="Suburb/Locality*">
                      <input type="hidden" class="form-control" id="oldtags_val" name="locality" placeholder="Suburb/Locality*">
                    </div>
                  </div>

                  {!! $dy_html !!}

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" id="contact_type" required name="type">
                          <option>I am a Seller*</option>
                          <option>I am a Buyer</option>
                          <option>I am a Tenant</option>
                          <option>I am a Landlord</option>
                          <option>I am an Investor</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                      <!--<input type="text" class="form-control" name="comments" placeholder="Comments">-->
                    </div>
                  </div>

                  

                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GET STARTED</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>



  <div class="modal fade home-rent subscribe" id="myModal_share">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">SHARE</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body ui-front">
            
           
                <div class="row">
                  
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" class="form-control" id="email_add"  name="email_add" placeholder="Email Address">
                    </div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="button" id="share_buttonmail" class="btn btn-primary">SHARE</button>
                  </div>

                </div>
              </div>
        
        
        
      </div>
    </div>
  </div>

  <div class="modal fade home-rent register-intrest manage-with-us" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">ENQUIRE NOW</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body  ui-front">
            <p>Simply fill out the form on this page and we will have someone contact you!</p>

            <form action="sendmail_manage_with_us" id="capt_form" method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required id="buy_options" name="property_type">
                          <option value="">Property Type *</option>
                          <option value="House">House</option>
                          <option value="Townhouse/Duplex">Townhouse/Duplex</option>
                          <option value="Apartment & Unit">Apartment & Unit</option>
                          <option value="Vacant Land">Vacant Land</option>
                          <option value="Acreage/Rural">Acreage/Rural</option>
                          <option value="Commercial">Commercial</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" id="subtags" placeholder="Suburb/Locality *">
                      <input type="hidden" id="subtags_val" name="region" >
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" id="list_offices" required name="office">
                          <option value="">Wiseberry Office *</option>
                          <option value="234">Wiseberry Bankstown</option>
                          <option value="231">Wiseberry Baulkham Hills</option>
                          <option value="Wiseberry Berowra">Wiseberry Berowra</option>
                          <option value="Wiseberry Campbelltown">Wiseberry Campbelltown</option>
                          <option value="Wiseberry Charmhaven ">Wiseberry Charmhaven </option>
                          <option value="Wiseberry Coastal">Wiseberry Coastal</option>
                          <option value="Wiseberry Dural">Wiseberry Dural</option>
                          <option value="Wiseberry Enmore">Wiseberry Enmore</option>
                          <option value="Wiseberry Five Dock">Wiseberry Five Dock</option>
                          <option value="Wiseberry Forster">Wiseberry Forster</option>
                          <option value="Wiseberry Heritage">Wiseberry Heritage</option>
                          <option value="Wiseberry Kariong">Wiseberry Kariong</option>
                          <option value="Wiseberry Killarney Vale">Wiseberry Killarney Vale</option>
                          <option value="Wiseberry Marrickville">Wiseberry Marrickville</option>
                          <option value="Wiseberry Northern Beaches">Wiseberry Northern Beaches</option>
                          <option value="Wiseberry Peninsula">Wiseberry Peninsula</option>
                          <option value="Wiseberry Penrith ">Wiseberry Penrith </option>
                          <option value="Wiseberry Picton ">Wiseberry Picton </option>
                          <option value="Wiseberry Port Macquarie">Wiseberry Port Macquarie</option>
                          <option value="Wiseberry Prestons">Wiseberry Prestons</option>
                          <option value="Wiseberry Rouse Hill">Wiseberry Rouse Hill</option>
                          <option value="Wiseberry Taree">Wiseberry Taree</option>
                          <option value="Wiseberry Thompsons ">Wiseberry Thompsons </option>
                          <option value="Wiseberry Varsity Lakes">Wiseberry Varsity Lakes</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" name="comments" placeholder="Comments"></textarea>
                    </div>
                  </div>

                  

                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  
                  <div class="col-md-12 recapthcha-section recapthcha-section-manage">
                    <div style="margin-left: auto;width: 70%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GO!</button>
                  </div>

                </div>
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>


  <div class="modal fade home-rent register-intrest" id="myModal_calculator">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Calculator</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body  ui-front">
            <iframe id="wiwo-daparodo" src="about:blank" frameborder="0" width="100%" height="200" data-wiwo-init="false"></iframe>

        </div>
        
        
        
      </div>
    </div>
  </div>


  <div class="modal fade home-rent register-intrest" id="myModal_session">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Success</h4>
          <button type="button" class="close close_session">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body  ui-front">
            <div class="alert alert-success alert-block" role="alert">
              <button class="close" data-dismiss="alert"></button>
              @if(Session::has('success'))
                {{ Session::pull("success") }}
              @endif
              {{ Session::forget('success') }}
            </div>
        </div>
        
        
        
      </div>
    </div>
  </div>
  <input type="hidden" name="session_msg" id="session_msg" value='{{ Session::get("data") }}'>
  

        <!-- START PRELOADER -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- END PRELOADER -->

        <!-- ARCHIVES JS -->
        <script src="https://calcs.widgetworks.com.au/widget/widget-scout.min.js"></script>
        <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/tether.min.js') }}"></script>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/transition.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/mmenu.min.js') }}"></script>
        <script src="{{ asset('js/mmenu.js') }}"></script>
        <script src="{{ asset('js/nice-select.js') }}"></script>
        <script src="{{ asset('js/slick.min.js') }}"></script>
        <script src="{{ asset('js/slick.js') }}"></script>
        <script src="{{ asset('js/fitvids.js') }}"></script>
        <script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/typed.min.js') }}"></script>
        <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('js/smooth-scroll.min.js') }}"></script>
        <script src="{{ asset('js/lightcase.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.js') }}"></script>
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ asset('js/ajaxchimp.min.js') }}"></script>
        <script src="{{ asset('js/newsletter.js') }}"></script>
        <script src="{{ asset('js/jquery.form.js') }}"></script>
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('js/forms-2.js') }}"></script>
        <script src="{{ asset('js/color-switcher.js') }}"></script>

        <!-- Slider Revolution scripts -->
        <script src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        

        <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
        <!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
        <!-- MAIN JS -->
        <script src="{{ asset('js/script.js') }}"></script>


        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


        <script type="text/javascript">
          $(function() {

              setTimeout(function(){ $("#ind_org_id").val($("#ind_org_idd").val()); }, 3000);
            
              var html='<option value="">Wiseberry Office *</option>';
              $.ajax({
                  url: 'https://v2.wiseberryonline.com.au/api/v1/public/organisation?search_kw=&limit=30',
                  type: 'GET',
                  success: function (response) 
                    {
                        var data=response.data;
                        data.forEach(function(item) {
                          html+='<option value="'+item.id+'">'+item.name+'</option>';
                        });

                        $("#list_offices").html(html);
                        $("#list_offices_sub").html(html);
                    },
                  error: function () { },

              });



                $("#test_ajax").click(function(){


                  let URLs = [
                    'https://v2.wiseberryonline.com.au/api/v1/public/suburb?search_kw=a',
                    'https://v2.wiseberryonline.com.au/api/v1/public/member?search_kw=a'
                  ];
                  
                  // let requests = urls.map(url => fetch(url));
                  
                  // Promise.all(requests)
                  //   .then(responses => console.log(responses));

                    var mergedata="";
                    function getAllData(URLs){
                      return Promise.all(URLs.map(fetchData));
                    }

                    function fetchData(URL) {
                      return axios
                        .get(URL)
                        .then(function(response) {
                          return {
                            success: true,
                            data: response.data
                          };
                        })
                        .catch(function(error) {
                          return { success: false };
                        });
                    }

                    getAllData(URLs).then(resp=>{
                      console.log(resp[0].data.data);
                      console.log(resp[1].data.data);
                    }).catch(e=>{console.log(e)})

                    
                  
                 });


          
         // $( "#city" ).autocomplete({
         //        source: function( request, response ) {
         //    var resultFromSource1 = 'https://v2.wiseberryonline.com.au/api/v1/public/suburb?search_kw=a';
         //    var resultFromSource2 = 'https://v2.wiseberryonline.com.au/api/v1/public/member?search_kw=a';
         //    var agregateResults = function(){
         //        if( resultFromSource1 && resultFromSource2){
         //            var result = resultFromSource1.concat(resultFromSource2);
         //            response(result);
         //        }
         //    }
         //    requestFromSource1(function( result ){
         //        resultFromSource1 = result.data;
         //        agregateResults();
         //    });
         //    requestFromSource2(function( result ){
         //        resultFromSource2 = result.data;
         //        agregateResults();
         //    });
         //        }
         //    });


            // $("#tags , #open_tags").autocomplete({  
            //   source: function (request, response) {
            //   var val = request.term;
            //   var array = [];
            //     $.ajax({
            //         url: 'https://v2.wiseberryonline.com.au/api/v1/public/suburb?search_kw='+val,
            //         type: 'GET',
            //         dataType: "json",
            //         beforeSend: function (xhr) {
            //             xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
            //         },
            //         success: function (resp){
                      
            //              var data=resp.data;
            //              response($.map( data, function(item){
            //                return {
            //                    id:item.id, 
            //                    post:item.postcode, 
            //                    state:item.state.abbrev, 
            //                    label: item.name+", "+item.state.abbrev+", "+item.postcode,
            //                    value: item.name
            //                };
            //             }));
            //         }

            //     });

            //   },

            //   select: function (event, ui) {
            //       var v = ui.item.id;
            //       var l = ui.item.label;
            //       var val = ui.item.value;
            //       var postal = ui.item.post;
            //       var state = ui.item.state;
            //       $('#tags_val').val(v);
            //       $('#tags_vals').val(postal);
            //       // $('#tags').val(l);
            //       // update what is displayed in the textbox
            //       this.value = val+", "+state+", "+postal; 
            //       return false;
            //   }
            // });
            var s="";
            var p_name=$("#pagename").val();

            if(p_name == 'rent' || p_name == 'rentopenhome'){
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,rental,office,agent&limit=10&type=rent&query=";
              var type="rental";
            }else if(p_name == 'leased'){
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,listing,office,agent&limit=10&type=leased&query=";
              var type="listing";
            }else if(p_name == 'sell_with_us'){
              // if($("#sel_type").val() == 1){
              //   var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=agent&limit=10&type=sell&query=";
              // }else{
              //   var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=office&limit=10&type=sell&query=";
              // }
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,office,agent&limit=10&query=";
              var type="listing";
              // var type="listing";
            }else if(p_name == 'agent'){
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,office,agent&limit=10&query=";
              var type="listing";
            }else if(p_name == 'office'){
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,office,agent&limit=10&query=";
              var type="listing";
            }else if(p_name == 'office_agent'){
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=agent&limit=10&query=";
              var type="listing";
            }else{
              var urls="https://v2.wiseberryonline.com.au/api/v1/public/autocomplete?limit=10&category=suburb,listing,office,agent&limit=10&type=buy&query=";
              var type="listing";
            }


            $("#tags , #open_tags").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var array = [];
                $.ajax({
                    url: urls+val,
                    type: 'GET',
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                    },
                    success: function (resp){
                      
                         var data=resp.data;
                         response($.map( data, function(item){
                           return {
                               value:item.value,  
                               label: item.label
                           };
                        }));
                    }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.value;
                  var l = ui.item.label;
                  
                  $('#tags_vals').val(v);
                  this.value = l;
                  var exp = v.split(':');
                  $('#tags_val').val(exp[1]);
                  $('#search_type').val(exp[0]);
                  $("#submit_form").click();
                  return false;
              }
            }).data('ui-autocomplete')._renderItem = function(ul, item){
                var val=item.value;
                var exps = val.split(':');
                if(exps[0] == 'suburb'){
                  var img="https://szwebprofile.com/PHP/wiseberry/public/img/m_location.jpeg";
                }else if(exps[0] == type){
                  var img="https://szwebprofile.com/PHP/wiseberry/public/img/location.jpg";
                }else if(exps[0] == 'office'){
                  var img="https://szwebprofile.com/PHP/wiseberry/public/img/office.jpeg";
                }else if(exps[0] == 'agent'){
                  var img="https://szwebprofile.com/PHP/wiseberry/public/img/agent.jpeg";
                }
                s='<img style="width: 12%;" src="'+img+'">&nbsp;&nbsp;&nbsp;'+item.label;
                return $("<li class='ui-autocomplete-row'></li>")
                  .data("item.autocomplete", item)
                  .append(s)
                  .appendTo(ul);
            };



            




            $("#subtags").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var currentRequest = null;
              var array = [];
                currentRequest = $.ajax({
                    url: 'https://v2.wiseberryonline.com.au/api/v1/public/suburb?search_kw='+val,
                    type: 'GET',
                    dataType: "json",
                    beforeSend : function()    {          
		                if(currentRequest != null) {
		                    currentRequest.abort();
		                }
		            },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                    },
                    success: function (resp) 
                      {
                        
                           var data=resp.data;
                           response( $.map( data, function( item ){
                             return {
                                 id:item.id, 
                                 post:item.postcode, 
                                 state:item.state.abbrev, 
                                 label: item.name+", "+item.state.abbrev+", "+item.postcode,
                                 value: item.name
                             };
                          }));
                      }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.id;
                  var l = ui.item.label;
                  var val = ui.item.value;
                  var postal = ui.item.post;
                  var state = ui.item.state;
                  $('#subtags_val').val(v);
                  this.value = val+", "+state+", "+postal; 
                  return false;
              }
            });


            $("#oldtags").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var currentRequest = null;
              var array = [];
                currentRequest = $.ajax({
                    url: 'https://v2.wiseberryonline.com.au/api/v1/public/suburb?search_kw='+val,
                    type: 'GET',
                    dataType: "json",
                    beforeSend : function()    {          
                    if(currentRequest != null) {
                        currentRequest.abort();
                    }
                },
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                    },
                    success: function (resp) 
                      {
                        
                           var data=resp.data;
                           response( $.map( data, function( item ){
                             return {
                                 id:item.id, 
                                 post:item.postcode, 
                                 state:item.state.abbrev, 
                                 label: item.name+", "+item.state.abbrev+", "+item.postcode,
                                 value: item.name
                             };
                          }));
                      }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.id;
                  var l = ui.item.label;
                  var val = ui.item.value;
                  var postal = ui.item.post;
                  var state = ui.item.state;
                  $('#oldtags_val').val(v);
                  this.value = val+", "+state+", "+postal; 
                  return false;
              }
            });



          });

          $(function() {

            
            // $('select').each(function() {

            //   if($(this).attr("name") == 'bed'){
            //     var html='<option value="">Any Bed</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>';

            //     $(this).html(html);
            //   }

            //   if($(this).attr("name") == 'bath'){
            //     var html='<option value="">Any Bath</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>';

            //     $(this).html(html);
            //   }
            //   if($(this).attr("name") == 'parking'){
            //     var html='<option value="">Any Parking</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>';

            //     $(this).html(html);
            //   }
            // });

            
              $( "#notice_to_vac" ).datepicker({
                dateFormat: 'dd/mm/yy'
              });
              $( "#pref_access" ).datepicker({
                dateFormat: 'dd/mm/yy'
              });


              var date1 = $('#datepic');
              var date2 = $('#datepic2');
              $('#datepic').daterangepicker({
                opens: 'bottom',
                autoUpdateInput: false,
                locale: {
                    format: 'DD/MM/YYYY'
                }
              }, function(start, end, label) {
                 $('#datepic').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
              });
              setTimeout(function(){ 

                  if($("#datepic").val() != ''){
                    var dateval = $("#datepic").val().split(' - ')
                    var date_string = moment(dateval[0], "DD/MM/YYYY").format("YYYY-MM-DD");
                    var date_string1 = moment(dateval[1], "DD/MM/YYYY").format("YYYY-MM-DD");
                    jQuery( "#datepic" ).data('daterangepicker').startDate = moment( date_string, "YYYY-MM-DD" );
                    jQuery( "#datepic" ).data('daterangepicker').endDate = moment( date_string1, "YYYY-MM-DD" );
                    jQuery( "#datepic" ).data('daterangepicker').updateView();
                    jQuery( "#datepic" ).data('daterangepicker').updateCalendars();
                    // $('#datepic').data('dateRangePicker').setDateRange(date_string,date_string1);
                  }
               }, 100);
              

              $("#subscribe").click(function(){
                var val=$("#subscribe_id").val();
                $("#subscribe_email").val(val);
              });
              $("#subscribe_ind").click(function(){
                var val=$("#subscribe_ind_id").val();
                $("#subscribe_email").val(val);
              });


              $("#page_option").change(function(){
                  if($(this).val()=='1'){
                    window.location.href = "search";
                  }
                  if($(this).val()=='2'){
                    window.location.href = "rent";
                  }
                  if($(this).val()=='5'){
                    window.location.href = "recently-sold";
                  }
                  if($(this).val()=='6'){
                    window.location.href = "recently-leased";
                  }
              });
              $("#page_option_open").change(function(){
                  
                  if($(this).val()=='4' || $(this).val()=='5' || $(this).val()=='6' || $(this).val()=='7'){
                    window.location.href = "rent-open-home?option="+$(this).val();
                  }else{
                    window.location.href = "open-home?option="+$(this).val();
                  }
              });
              $("#sel_type").change(function(){
                  
                  
                    window.location.href = "sell-with-us?option="+$(this).val();
              });
          });

          var html="";
          var prop_hid=$("#prop_hid").val();
          $.ajax({
                url: 'https://v2.wiseberryonline.com.au/api/v1/public/menu?name=property_type',
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                },
                success: function (response) 
                  {
                    var data=response.data.property_type;
                    html+="<option value=''>Property Type</option>";
                    data.forEach(function(item) {
                        if(item.id == prop_hid){
                          html+="<option selected value="+item.id+">"+item.name+"</option>"
                        }else{
                          html+="<option value="+item.id+">"+item.name+"</option>"
                        }
                        
                    });
                    $("#buy_options").html(html);
                    $("#buy_options_dup").html(html);
                  },
                error: function () { },

            });

          var htmls="";
          $.ajax({
                url: 'https://v2.wiseberryonline.com.au/api/v1/public/menu?name=subscribe_contact_type',
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                },
                success: function (response) 
                  {
                    var data=response.data.subscribe_contact_type;
                    htmls+="<option selected value=''>Contact Type</option>";
                    data.forEach(function(item) {
                        
                          htmls+="<option value="+item.id+">"+item.name+"</option>"
                        
                    });
                    $("#contact_type").html(htmls);
                  },
                error: function () { },

            });

          $("#prop_address").blur(function(){
            var val=$(this).val();
            $.ajax({
                url: 'https://v2.wiseberryonline.com.au/api/v1/public/rental/find-by-address?search_address='+val,
                type: 'GET',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                },
                success: function (response) 
                  {
                    var data=response.data.id;
                    $("#prop_address_id").val(data);
                  },
                error: function () { },

            });
          });



          $("#agenttags").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var array = [];
                $.ajax({
                    url: 'https://v2.wiseberryonline.com.au/api/v1/public/member?search_kw='+val,
                    type: 'GET',
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                    },
                    success: function (resp) 
                      {
                        
                           var data=resp.data;
                           response( $.map( data, function( item ){
                             return {
                                 id:item.id, 
                                 fname:item.contact.first_name, 
                                 lname:item.contact.last_name, 
                                 label: item.contact.first_name+" "+item.contact.last_name,
                                 value: item.position
                             };
                          }));
                      }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.id;
                  var fname = ui.item.fname;
                  var lname = ui.item.lname;
                  $('#agenttags_val').val(fname);
                  this.value = fname+" "+lname; 
                  return false;
              }
            });



            $("#office_tags").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var array = [];
                $.ajax({
                    url: 'https://v2.wiseberryonline.com.au/api/v1/public/organisation?search_kw='+val,
                    type: 'GET',
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNjJkOTVkNTc3ZTBjZTVlYTExMjQ5Yzk3MjgzZmEyNTliMmE2YmM3YjhmNGQ5NmQ4YzFjZDIxM2JjYmRmNWQxMWQxOTFkNDQ4ZjMzMmU0YTIiLCJpYXQiOjE2Mjg0OTMxODMsIm5iZiI6MTYyODQ5MzE4MywiZXhwIjoxNjYwMDI5MTgwLCJzdWIiOiIxNTYxNjc0Iiwic2NvcGVzIjpbXX0.Lbot2m5Iy7x6LlNyp_lBMV87ED8KFTVGNeDbTkwa3nRvr3xfn_R2C8UnX3Q0_b7lrCnjDTOf_UBSBT0bcimCAiCTTlzg3FRYuYJkwQDL0nIJry4-jBXL7QMPujAmHE670BX0_1oSyWcYH-E0jElZmX_uCCY3qmXCnjxZix3HKnmpyNsA5IRC5nsncbAPPMA_Q5v7S0CoJZDnsVNsdPMcVNsJUEXyaUQ8zBIiOxmAfqR5WmsrTYBdcvuRKTxqzl3iz1EGiaVV1nXbM1zfHyQnhZLvdaSLWOJCkGqZILPU5RK4YseTyukmLy-tgU9n8u1RVfyyB1sLTtDCyt6xk3tmHmC7N-6DmacnQ_nU0IrVZ_pM0nN9fQ6BQGHdVhNjcSz1zmTD7s36Wtlo-Pp3nG0lIzhWLhXWlSah1kvbF8vr0hc3G1N0DGVnM0xIPmCFFODb5ikefi9Rg9Zlj60ROs3XG06ljnXWuEQZdlS3Su5ElLjrKRJF5chjGUNn5F1r4rfZSQGpk6sW3acDL08CDhgXBVYlmKvB9SE8POcLJ9GN1iO_rnF9nIc7D284BYH3wgyg1wGzSWvd3N7KfRq95vTj5yQhwgSy5bWGFu7rD-FxP259iq21jdAfKmDZRq8Qd0Xs9ED1gj_cReTC8hckniMKbl72Q8Op0ezwNLhsHdstXKY');
                    },
                    success: function (resp) 
                      {
                        
                           var data=resp.data;
                           response( $.map( data, function( item ){
                             return {
                                 id:item.id, 
                                 label: item.name,
                                 value: item.name
                             };
                          }));
                      }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.id;
                  var n = ui.item.label;
                  $('#office_tags').val(n);
                  $('#office_tags_vals').val(v);
                  return false;
              }
            });



            $("#see-more").click(function(){
            var pageno=$("#pageno").val();
            var pagename=$("#pagename").val();
            var api="";
            var redirect="";
            if(pagename == "search"){
              api='https://szwebprofile.com/PHP/wiseberry/public/search-results-api';
              redirect="individual-result";
            }
            if(pagename == "rent"){
              api='https://szwebprofile.com/PHP/wiseberry/public/rent-results-api';
              redirect="individual-rent-results";
            }
            if(pagename == "recently_sold"){
              api='https://szwebprofile.com/PHP/wiseberry/public/recently-sold-results-api';
              redirect="individual-result";
            }
            if(pagename == "recently_leased"){
              api='https://szwebprofile.com/PHP/wiseberry/public/recently-leased-results-api';
              redirect="individual-result";
            }
            var html="";
            var cnt=1;
              $.ajax({
                  url: api,
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, item) {
                        html+='<div class="col-md-4">';
                          html+='<div class="agents-grid">';
                              html+='<div class="landscapes">';
                                  html+='<div class="project-single">';

                                      html+='<div class="project-inner">';
                                          
                                          html+='<div class="homes">';
                                              html+='<a href="'+redirect+'/'+item.id+'" class="homes-img img-box hover-effect">';
                                                var img='images/properties1.jpg';
                                                if(item.assets != ""){
                                                  $.each(item.assets, function(j, items) {
                                                      if(items.extension != 'mp4' && items.floorplan != 1){
                                                        img=items.url;
                                                        return false;
                                                      }
                                                  });
                                                }
                                                
                                                html+='<img src="'+img+'" alt="home-1" class="img-responsive">';
                                              html+='</a>';
                                          html+='</div>';
                                          
                                      html+='</div>';
                                      html+='<div class="homes-content">';
                                          html+='<h3><a href="'+redirect+'/'+item.id+'">'+item.property.address.full_address+'</a></h3>';
                                          html+='<div class="text-center properties-icon">';
                                            $.each(item.property.features, function(k, itemk) {
                                              var image="";
                                              if(itemk.name == "Bedroom"){
                                                image="images/bed.svg";
                                              }
                                              if(itemk.name == "Bathroom"){
                                                image="images/bath.svg";
                                              }
                                              if(itemk.name == "Parking"){
                                                image="images/car.svg";
                                              }
                                              if(itemk.name == "Parking" || itemk.name == "Bedroom" || itemk.name == "Bathroom"){
                                                if(itemk.qty != 0){
                                                  html+='<a><span>'+itemk.qty+'</span>';
                                                  html+='<img src="'+image+'"></a>';
                                                }
                                              }
                                            });
                                          html+='</div>';

                                          html+='<p class="homes-address mb-3">';
                                              html+='<a href="#!">';
                                                  html+='<span style="text-transform: uppercase;">'+item.price_view+'</span>';
                                              html+='</a>';
                                          html+='</p>';
                                      html+='</div>';
                                  html+='</div>';
                              html+='</div>';
                          html+='</div>';
                        html+='</div>';
                        cnt=cnt+1;
                      });
                      if(cnt < 12){
                        $("#see-more").hide();
                      }
                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });


            $("#see-more-open").click(function(){
            var pageno=$("#pageno").val();
            var pagename=$("#pagename").val();
            var api="";
            var redirect="";
            if(pagename == "openhome"){
              api='https://szwebprofile.com/PHP/wiseberry/public/open-homes-result-api';
              redirect="individual-result";
            }
            // if(pagename == "auction"){
            //   api='https://szwebprofile.com/PHP/wiseberry/public/auction-result-api';
            // }
            // if(pagename == "rentopenhome"){
            //   api='https://szwebprofile.com/PHP/wiseberry/public/rent-open-home-results-api';
            //   redirect="individual-rent-results";
            // }
            var html="";
            var cnt=1;
              $.ajax({
                  url: api,
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, itemw) {
                        var da = itemw.starts;
                        var formatt= "";
                        $.ajax({
                          url: "https://szwebprofile.com/PHP/wiseberry/file_upload/format.php?da="+da,
                          type: 'POST',
                          'async': false,
                          data:$("#search_form").serialize(),
                          success: function (response) 
                            {
                              formatt=response;
                            }
                          });
                        var day=formatt.split('_');
                        $.each(itemw.listings, function(w, item) {
                          html+='<tr data-html2canvas-ignore="true">';
                            html+='<td class="text-center">';
                              html+='<div class="form-group">';
                                html+='<input type="checkbox" class="down_check" id="html">';
                                html+='<label for="html"></label>';
                              html+='</div>';
                            html+='</td>';
                            html+='<td>';
                              html+='<div class="Property">';
                                html+='<a href="'+redirect+'/'+item.id+'" class="homes-img img-box hover-effect">';
                                      var img='images/properties1.jpg';
                                      if(item.assets != ""){
                                        $.each(item.assets, function(j, items) {
                                            if(items.floorplan != 1){
                                              img=items.url;
                                              return false;
                                            }
                                        });
                                      }
                                      
                                      html+='<img src="'+img+'" alt="home-1" class="img-responsive">';
                                    html+='</a>';
                            html+='</div>';
                            html+='</td>';
                            html+='<td>';
                               
                                
                              html+='<h3><a href="'+redirect+'/'+item.id+'">'+item.property.address.full_address+'</a></h3>';
                            html+='</td>';
                            html+='<td>';
                                html+='<div class="text-center properties-icon">';
                                    $.each(item.property.features, function(k, itemk) {
                                      var image="";
                                      if(itemk.name == "Bedroom"){
                                        image="images/bed.svg";
                                      }
                                      if(itemk.name == "Bathroom"){
                                        image="images/bath.svg";
                                      }
                                      if(itemk.name == "Parking"){
                                        image="images/car.svg";
                                      }
                                      if(itemk.name == "Parking" || itemk.name == "Bedroom" || itemk.name == "Bathroom"){
                                        html+='<a><span>'+itemk.qty+'</span>';
                                        html+='<img src="'+image+'"></a>';
                                      }
                                    });
                                                              
                                html+='</div>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="rupee">'+item.price_view+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="date">'+day[0]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="time">'+day[1]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<i class="fa fa-calendar"><a href="https://calendar.google.com/calendar/u/0/r/eventedit?text=Wiseberry%20meet&location='+item.property.address.full_address+'"></a></i>';
                            html+='</td>';
                          html+='</tr>';
                        });
                        cnt=cnt+1;
                      });
                      if(cnt < 12){
                        $("#see-more-open").hide();
                      }
                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });



            $("#see-more-rentopen").click(function(){
            var pageno=$("#pageno").val();
            var pagename=$("#pagename").val();
            var api="";
            var redirect="";
            if(pagename == "rentopenhome"){
              api='https://szwebprofile.com/PHP/wiseberry/public/rent-open-home-results-api';
              redirect="individual-rent-results";
            }
            var html="";
            var cnt=1;
              $.ajax({
                  url: api,
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, itemw) {
                        var da = itemw.starts;
                        var formatt= "";
                        $.ajax({
                          url: "https://szwebprofile.com/PHP/wiseberry/file_upload/format.php?da="+da,
                          type: 'POST',
                          'async': false,
                          data:$("#search_form").serialize(),
                          success: function (response) 
                            {
                              formatt=response;
                            }
                          });
                        var day=formatt.split('_');
                        $.each(itemw.rentals, function(w, item) {
                          html+='<tr data-html2canvas-ignore="true">';
                            html+='<td class="text-center">';
                              html+='<div class="form-group">';
                                html+='<input type="checkbox" class="down_check" id="html">';
                                html+='<label for="html"></label>';
                              html+='</div>';
                            html+='</td>';
                            html+='<td>';
                              html+='<div class="Property">';
                                html+='<a href="'+redirect+'/'+item.id+'" class="homes-img img-box hover-effect">';
                                      var img='images/properties1.jpg';
                                      if(item.assets != ""){
                                        $.each(item.assets, function(j, items) {
                                            if(items.floorplan != 1){
                                              img=items.url;
                                              return false;
                                            }
                                        });
                                      }
                                      
                                      html+='<img src="'+img+'" alt="home-1" class="img-responsive">';
                                    html+='</a>';
                            html+='</div>';
                            html+='</td>';
                            html+='<td>';
                               
                                
                              html+='<h3><a href="'+redirect+'/'+item.id+'">'+item.property.address.full_address+'</a></h3>';
                            html+='</td>';
                            html+='<td>';
                                html+='<div class="text-center properties-icon">';
                                    $.each(item.property.features, function(k, itemk) {
                                      var image="";
                                      if(itemk.name == "Bedroom"){
                                        image="images/bed.svg";
                                      }
                                      if(itemk.name == "Bathroom"){
                                        image="images/bath.svg";
                                      }
                                      if(itemk.name == "Parking"){
                                        image="images/car.svg";
                                      }
                                      if(itemk.name == "Parking" || itemk.name == "Bedroom" || itemk.name == "Bathroom"){
                                        html+='<a><span>'+itemk.qty+'</span>';
                                        html+='<img src="'+image+'"></a>';
                                      }
                                    });
                                                              
                                html+='</div>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="rupee">'+item.price_view+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="date">'+day[0]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="time">'+day[1]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<i class="fa fa-calendar"><a href="https://calendar.google.com/calendar/u/0/r/eventedit?text=Wiseberry%20meet&location='+item.property.address.full_address+'"></a></i>';
                            html+='</td>';
                          html+='</tr>';
                        });
                        cnt=cnt+1;
                      });
                      if(cnt < 12){
                        $("#see-more-open").hide();
                      }
                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });


            $("#see-more-auction").click(function(){
            var pageno=$("#pageno").val();
            var pagename=$("#pagename").val();
            var api="";
            var redirect="";
            if(pagename == "auction"){
              api='https://szwebprofile.com/PHP/wiseberry/public/auction-result-api';
            }
            var html="";
            var cnt=1;
              $.ajax({
                  url: api,
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, item) {
                        var da = item.auction_date;
                        var formatt= "";
                        $.ajax({
                          url: "https://szwebprofile.com/PHP/wiseberry/file_upload/format.php?da="+da,
                          type: 'POST',
                          'async': false,
                          data:$("#search_form").serialize(),
                          success: function (response) 
                            {
                              formatt=response;
                            }
                          });
                        var day=formatt.split('_');
                          html+='<tr data-html2canvas-ignore="true">';
                            html+='<td class="text-center">';
                              html+='<div class="form-group">';
                                html+='<input type="checkbox" class="down_check" id="html">';
                                html+='<label for="html"></label>';
                              html+='</div>';
                            html+='</td>';
                            html+='<td>';
                              html+='<div class="Property">';
                                html+='<a href="'+redirect+'/'+item.id+'" class="homes-img img-box hover-effect">';
                                      var img='images/properties1.jpg';
                                      if(item.assets != ""){
                                        $.each(item.assets, function(j, items) {
                                            if(items.floorplan != 1){
                                              img=items.url;
                                              return false;
                                            }
                                        });
                                      }
                                      
                                      html+='<img src="'+img+'" alt="home-1" class="img-responsive">';
                                    html+='</a>';
                            html+='</div>';
                            html+='</td>';
                            html+='<td>';
                               
                                
                              html+='<h3><a href="'+redirect+'/'+item.id+'">'+item.property.address.full_address+'</a></h3>';
                            html+='</td>';
                            html+='<td>';
                                html+='<div class="text-center properties-icon">';
                                    $.each(item.property.features, function(k, itemk) {
                                      var image="";
                                      if(itemk.name == "Bedroom"){
                                        image="images/bed.svg";
                                      }
                                      if(itemk.name == "Bathroom"){
                                        image="images/bath.svg";
                                      }
                                      if(itemk.name == "Parking"){
                                        image="images/car.svg";
                                      }
                                      if(itemk.name == "Parking" || itemk.name == "Bedroom" || itemk.name == "Bathroom"){
                                        html+='<a><span>'+itemk.qty+'</span>';
                                        html+='<img src="'+image+'"></a>';
                                      }
                                    });
                                                              
                                html+='</div>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="rupee">'+item.price_view+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="date">'+day[0]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<p class="time">'+day[1]+'</p>';
                            html+='</td>';
                            html+='<td>';
                                html+='<i class="fa fa-calendar"><a href="https://calendar.google.com/calendar/u/0/r/eventedit?text=Wiseberry%20meet&location='+item.property.address.full_address+'"></a></i>';
                            html+='</td>';
                          html+='</tr>';
                        cnt=cnt+1;
                      });
                      if(cnt < 12){
                        $("#see-more-open").hide();
                      }
                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });



            $("#see-more-agent").click(function(){
            var pageno=$("#pageno").val();
            var html="";
            var cnt=1;
              $.ajax({
                  url: 'https://szwebprofile.com/PHP/wiseberry/public/agent-search-results-api',
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, item) {
                        html+='<div class="col-md-3">';
                          html+='<div class="team-wrap">';
                              html+='<div class="team-img img-box">';
                                html+='<a href="individual-agent/'+item.id+'_'+item.contact.id+'" class="homes-img img-box">';
                                              
                                          html+='<img src="images/member1.jpg" alt="" />';
                                                
                                        html+='</a>';
                                html+='</div>';
                                html+='<div class="team-content-persons">';
                                html+='<div class="team-info">';
                                  html+='<h3>'+item.contact.first_name+' '+item.contact.last_name+'</h3>';
                                  html+='<p>'+item.position+'</p>';
                                  if(!! item.organisation){
                                    html+='<p>'+item.organisation.name+'</p>';
                                  }
                                    
                                html+='<div class="team-socials">';

                                  html+='<ul>';
                                    html+='<li>';
                                      $.each(item.contact.emails, function(j, items) {
                                       
                                            html+='<a href="mailto:'+items.value+'" title="mail"><img src="images/agent-icon1.png"></a>';
                                        
                                      });
                                      
                                      $.each(item.contact.phones, function(k, itemk) {
                                        
                                            html+='<a href="downloadcard/'+item.contact.first_name+'_'+itemk.value+'" title="twitter"><img src="images/agent-icon2.png"></a>';
                                        
                                      });
                                    html+='</li>';
                                  html+='<ul>';
                                      html+='</div>';
                                  html+='</div>';
                              html+='</div>';
                          html+='</div>';
                        html+='</div>';
                        cnt=cnt+1;
                      });

                      if(cnt < 12){
                        $("#see-more-agent").hide();
                      }
                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });



            $("#see-more-office").click(function(){
            var pageno=$("#pageno").val();
            var html="";
            var cnt="";
              $.ajax({
                  url: 'https://szwebprofile.com/PHP/wiseberry/public/office-search-api',
                  type: 'POST',
                  data:$("#search_form").serialize(),
                  success: function (response) 
                    {
                      
                      $.each(JSON.parse(response), function(i, item) {
                        html+='<div class="col-md-4">';
                          html+='<div class="agents-grid">';
                          html+='<div class="landscapes">';
                          html+='<div class="project-single">';
                          html+='<div class="project-inner">';
                              html+='<div class="homes">';
                                html+='<a href="office-home/'+item.id+'" class="homes-img img-box">';
                                          
                                              html+='<img src="images/offices1.jpg" alt="home-1" class="img-responsive">';
                                html+='</a>';
                              html+='</div>';
                              html+='</div>';
                              html+='<div class="homes-content">';
                                html+='<h3><a href="office-home/'+item.id+'">'+item.name+'</a></h3>';
                                if(item.address != null){
                                  html+='<h1 class="address">'+item.address.full_address+'</h1>';
                                }
                                
                                html+='<div class="text-center properties-icon">';
                                      $.each(item.phones, function(k, itemk) {
                                        html+='<a href="tel:'+itemk.value+'"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve"><style type="text/css">.st0{fill:#830B2C;}</style><path class="st0" d="M540,95.2c245.2,0,444.8,199.5,444.8,444.8S785.2,984.8,540,984.8S95.2,785.2,95.2,540S294.8,95.2,540,95.2zM540,6.3C245.3,6.3,6.3,245.3,6.3,540s239,533.7,533.7,533.7s533.7-239,533.7-533.7S834.7,6.3,540,6.3z M740.1,776.2l-78.3-151.1L616,647.6c-49.8,24.2-151.2-173.9-102.5-200l46.3-22.8l-77.7-151.6l-46.8,23.1c-160.2,83.5,94.2,577.8,258,502.9L740.1,776.2z"/></svg>';
                                        html+=''+itemk.value+'</a>';
                                      });

                                      $.each(item.emails, function(l, iteml) {
                                        html+='<a href="mailto:'+iteml.value+'"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve"><style type="text/css">.st0{fill:#830B2C;}</style><path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4zM540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/></svg>';
                                        html+=''+iteml.value+'</a>';
                                      });
                                html+='</div>';
                            html+='</div>';
                        html+='</div>';
                    html+='</div>';
                  html+='</div>';
                  html+='</div>';
                  cnt=cnt+1;
                      });

                      if(cnt < 12){
                        $("#see-more-office").hide();
                      }

                      $("#append_results").append(html);
                      $("#pageno").val(parseInt(pageno)+ 1);
                    },
                  error: function () { },

              });
            });



            $("#blogsearch").autocomplete({  
              source: function (request, response) {
              var val = request.term;
              var array = [];
                $.ajax({
                    url: 'https://szwebprofile.com/PHP/wiseberry/public/blog-api?search_kw='+val,
                    type: 'GET',
                    dataType: "json",
                    success: function (resp) 
                      {
                           // var data=resp.data;
                           response( $.map( resp, function( item ){
                             return {
                                 id:item.id, 
                                 label: item.name,
                                 value: item.name
                             };
                          }));
                      }

                });

              },

              select: function (event, ui) {
                  var v = ui.item.id;
                  var n = ui.item.label;
                  $('#blogsearch').val(n);
                  $('#blogid').val(v);
                  return false;
              }
            });


            $("#share_buttonmail").click(function(){
                var email = $("#email_add").val();
                $.ajax({
                    url: 'https://szwebprofile.com/PHP/wiseberry/file_upload/sendmail.php?email='+email,
                    type: 'GET',
                    success: function (resp) 
                      {
                           $("#myModal_share").modal('hide');
                      }

                });
            });




        </script>

    </div>
    <!-- Wrapper / End -->
</body>

<script type="text/javascript">
            $('#collapseDiv').on('shown.bs.collapse', function () {
       $(".glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
    });

    $('#collapseDiv').on('hidden.bs.collapse', function () {
       $(".glyphicon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    });
</script>

<script type="text/javascript">
    $("input[type=checkbox]").change(function () {
        if ($(this).prop("checked")) {
            $(this).val(true);
        } else {
            $(this).val(false);
        }
    });

    $("input[type=radio]").change(function () {
        if ($(this).prop("checked")) {
            $(this).addClass("checked");
        } else {
            $(this).removeClass("checked");
        }
    });
  $('#capt_form').on('submit', function(e) {
    if(grecaptcha.getResponse() == "") {
      e.preventDefault();
      alert("Recaptcha Validation Failed!!!!");
    } else if($('input[type="checkbox"]').val() == 'true'){
      return;
    }else{
      alert("Privacy Statement Validation Failed!!!!");
      e.preventDefault();
    }
  });

  $('#capt_form_sub').on('submit', function(e) {
    if(grecaptcha.getResponse() == "") {
      e.preventDefault();
      alert("Recaptcha Validation Failed!!!!");
    }else{
      
    }
  });

  $('#capt_form_fin').on('submit', function(e) {
    var atLeastOneChecked = false;
    if(grecaptcha.getResponse() == "") {
      e.preventDefault();
      alert("Recaptcha Validation Failed!!!!");
    }else{
      atLeastOneChecked = true;
    }

    atLeastOneChecked = false;
    $("input[type=radio]").each(function() {
        if ($(this).hasClass("checked")) {
            atLeastOneChecked = true;
        }

    });

    if (!atLeastOneChecked) {
      alert("Describe Field is required!!!!");
      e.preventDefault();
    }else{

    }
  });

  $(window).on('load', function() {
    if($("#session_msg").val() == "success"){

      $('#myModal_session').modal('show');
    }
  });

  $("#tags").keyup(function(){
    if($(this).val() == ''){
      $("#tags_val").val("");
    }
  });

  $(".close_session").click(function(){
    location.reload();
  });
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>

</html>