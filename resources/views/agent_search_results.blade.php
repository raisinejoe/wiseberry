@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h2>Australian Premium Real Estate Agents</h2>-->
<!--<h3>Real Estate Agents In “Suburb” (Example: Real Estate Agents In Castle Hill NSW 2154)</h3>
<h1>Your Local Real Estate Agent</h1>
<h2>Australian Premium Real Estate Agents</h2>
<h3>Real Estate Agents In “Suburb” (Example: Real Estate Agents In Castle Hill NSW 2154)</h3>
-->

        <div class="agent-search-results">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center  inner-banner  inner-banner-one" id="slider" style="background-image: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            

                            <div class="banner-search-wrap home buy-home buy-result">
                                 
                                 <h1 class="text-center">FIND A LOCAL AGENT</h1>
                                 <div class="inner">
                                    <form method="POST" id="search_form" action="agent-search-results">
                                         
                                    <div class="row">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="pageno" id="pageno" value="2">
                                        <input type="hidden" name="search_type" id="search_type" value="">
                                            <input type="hidden" name="pagename" id="pagename" value="agent">
                                        <div class="col-md-9 pl-0">
                                            <input type="txt" name="suburb_name" value="{{$request->agent_name}}" id="tags" placeholder="Search by Office, Suburb, Postcode or Agent Name" class="form-control" style="border-right: 1px solid;">
                                            <input type="hidden" name="suburb" value="{{$request->agent}}" id="tags_val" placeholder="Search by Office, Suburb, Postcode or Agent Name" class="form-control" style="border-right: 1px solid;">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" id="submit_form" name="submit">Search</button>
                                        </div>
                                    </div>

                                   


                                     </form>
          
                                 </div>     
                                        
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

    

        <!-- START Buy Result -->
        <?php if(!empty($result)){ ?>
        <section class="recently portfolio bg-white-1 home18 buy-result agent-result">
            
            <div class="container-fluid">
                <div class="row" id="append_results">
                    <?php
                        $i=0;
                        foreach ($result as $key => $value){
                            
                    ?>
                    <div class="col-md-3 {{$i}}">
                        <div class="team-wrap">
                            <div class="team-img img-box">
                            	<a href="individual-agent/{{$value['id']}}" class="homes-img img-box">
                                    <?php if(isset($value['contact']['profile_picture']) && $value['contact']['profile_picture'] != null){ ?>
                                        <img src="{{$value['contact']['profile_picture']}}" alt="home-1">
                                    <?php }else{ ?>
                                        <img src="images/member1.jpg" alt="" />
                                    <?php } ?>
                            	</a>
                            </div>
                            <div class="team-content-persons">
                                <div class="team-info">
                                    <h3>{{$value['contact']['first_name']  ?? '' }} {{$value['contact']['last_name']  ?? '' }}</h3>
                                    <p>{{$value['position']  ?? '' }}</p>
                                    <p>{{$value['organisation']['name']  ?? '' }}</p>
                                    <p><a href="tel:{{$value['contact']['phones'][0]['value']  ?? '' }}">{{$value['contact']['phones'][0]['value']  ?? '' }}</a></p>
                                    <div class="team-socials">
                                        <ul>
                                            <li>
                                                <a href="mailto:{{$value['contact']['emails'][0]['value']  ?? '' }}" title="mail"><!--<img src="images/agent-icon1.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#830B2C;}
</style>
<path class="st0" d="M540,95.4c245.1,0,444.6,199.4,444.6,444.6S785.1,984.6,540,984.6S95.4,785.1,95.4,540S294.9,95.4,540,95.4z
     M540,6.5C245.4,6.5,6.5,245.4,6.5,540s238.9,533.5,533.5,533.5s533.5-238.9,533.5-533.5S834.6,6.5,540,6.5z M540,564.5L273.6,361.3
    h532.7L540,564.5z M540,621.7L273.2,415.9v301h533.5v-301L540,621.7z"/>
</svg></a>
                                                <a href="downloadcard/{{$value['contact']['first_name']}}_{{$value['contact']['phones'][0]['value']  ?? '' }}_{{$value['contact']['emails'][0]['value']  ?? '' }}" title="twitter"><!--<img src="images/agent-icon2.png">--><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1080 1080" style="enable-background:new 0 0 1080 1080;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#830B2C;}
	.st1{font-family:'Arial-BoldMT';}
	.st2{font-size:655.684px;}
</style>
<g>
	<path class="st0" d="M540,101c242,0,439,196.9,439,439S782,979,540,979S101,782,101,540S298,101,540,101z M540,13.3
		C249.1,13.3,13.3,249.1,13.3,540s235.9,526.7,526.7,526.7s526.7-235.9,526.7-526.7S830.9,13.3,540,13.3z"/>
	<text transform="matrix(1 0 0 1 335.5405 807.1714)" class="st0 st1 st2">V</text>
</g>
</svg></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  } ?>
                </div>

                <?php 
                    if(!empty($result)){ 
                        if($i == 12 && $next != null){
                ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="#!" class="see-more" id="see-more-agent">SEE MORE</a>
                    </div>
                </div>
                <?php
                  } } 
                ?>

            </div>

        </section>
        <?php } else{ ?>
        <section class="no-result">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p>We do not currently have an agent under this search</p>
                        <p style="margin-bottom: 0;">Please search again or contact us.</p>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <?php } ?>

@include('layout.footer')