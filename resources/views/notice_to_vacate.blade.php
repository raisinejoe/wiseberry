@include('layout.header')
@include('layout.menu')

<div class="clearfix"></div>
        <!-- Header Container / End -->

        <div class="notice-to-vacate">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">NOTICE TO VACATE</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->

        <section class="banner-bottom">
            <input type="hidden" name="" id="tags">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>LET US HELP YOU FIND THE RIGHT RENTAL PROPERTY.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a href="rent">LET&apos;S GO</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>





        <section class="notice-repair-section p-80">

            <div class="container">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <h2>{{$data->heading}}</h2>
                            {!! $data->desc !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        
                        <form action="sendmail_notice_to_vacate" id="capt_form" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" required class="form-control" name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" required class="form-control" name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      
                      <input type="text" required class="form-control" id="prop_address"  placeholder="Property Address*">
                      <input type="hidden" class="form-control" id="prop_address_id" name="address" placeholder="Property Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="office">
                          <option value="">Wiseberry Office*</option>
                          <option>Wiseberry Bankstown</option>
                          <option>Wiseberry Baulkham Hills</option>
                          <option>Wiseberry Berowra</option>
                          <option>Wiseberry Campbelltown</option>
                          <option>Wiseberry Charmhaven</option>
                          <option>Wiseberry Gosford</option>
                          <option>Wiseberry Dural</option>
                          <option>Wiseberry Enmore</option>
                          <option>Wiseberry Five Dock</option>
                          <option>Wiseberry Forster</option>
                          <option>Wiseberry Gorokan</option>
                          <option>Wiseberry Kariong</option>
                          <option>Wiseberry Killarney Vale</option>
                          <option>Wiseberry Marrickville</option>
                          <option>Wiseberry Mona Vale</option>
                          <option>Wiseberry Umina Beach</option>
                          <option>Wiseberry Penrith</option>
                          <option>Wiseberry Picton</option>
                          <option>Wiseberry Port Macquarie</option>
                          <option>Wiseberry Prestons</option>
                          <option>Wiseberry Rouse Hill</option>
                          <option>Wiseberry Taree</option>
                          <option>Wiseberry Richmond</option>
                          <option>Wiseberry Varsity Lakes </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <select class="form-control" required name="lease">
                          <option value="">Are your breaking your lease?</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" id="notice_to_vac" required name="vac_date" class="form-control date" placeholder="Vacate Date*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" name="for_address" required class="form-control" placeholder="Forwarding Address*">
                    </div>
                  </div>


                  <div class="col-md-6">
                    <div class="form-group">
                        <textarea class="form-control" required name="comments" placeholder="Reason*"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                      
                      <label>
                        <input type="checkbox" name="check">
                        <span>I have read and agree to the Privacy Statement. *</span>
                      </label>

                  </div>
                  <div class="col-md-12">
                    <div style="margin-left: auto;width: 68%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary see-more">LET&apos;S GO!</button>
                  </div>

                </div>
             
              
            </form>
                    </div>
                </div>

            </div>
            
            

        </section>
        </div>

@include('layout.footer')