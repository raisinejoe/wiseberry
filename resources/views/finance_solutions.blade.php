@include('layout.header')
@include('layout.menu')

    <div class="clearfix"></div>
        <!-- Header Container / End -->

<!--<h3>How Much Money Can I Borrow?</h3>-->
        <div class="finance-solution">
            <!-- STAR HEADER IMAGE -->
        <section class="header-image home-18 d-flex align-items-center inner-banner back-100" id="slider" style="background: url('{{$img_path}}{{$data->banner}}');">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="left wow fadeInLeft">
                            <h1 class="banner-title">FINANCE SOLUTIONS</h1>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!-- END HEADER IMAGE -->


        <section class="banner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-6 pr-0 pl-0">
                        <h1>I WOULD LIKE TO KNOW MORE ABOUT WISEBERRY FINANCE.</h1>
                    </div>
                    <div class="col-md-4 pr-0 pl-0">
                        <a data-toggle="modal" data-target="#myModalone" href="#!">CONTACT US</a>
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>
        </section>


<section class="buy-landing-page">


    

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section owner buy pt6 mb1 finance">
         <div class="container">
                
             <div class="row">
                 

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$p1->heading}}</h2>
                            {!! $p1->desc !!}
                            <!-- <a href="#!">FIND OUT MORE</a> -->
                        </div>
                    </div>


                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" src="{{$img_path}}{{$p1->image}}">
                        </div>
                    </div>
                    <input type="hidden" name="" id="tags">
                 
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->


 <!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy">
         <div class="container">
             <div class="row">
                 
                  <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="img-box">
                            <img alt="image" alt="image" src="{{$img_path}}{{$p2->image}}">
                        </div>
                    </div>

                 <div class="col-lg-6 col-md-6 who-1">
                        <div class="text-center">
                            <h2>{{$p2->heading}}</h2>
                            {!! $p2->desc !!}
 <!--<div class="show-reg-form modal-open" data-toggle="modal" data-target="#myModalone"><a href="#">USE OUR CALCULATOR</a></div>-->
 <a style="cursor: pointer;" data-toggle="modal"  data-target="#myModal_calculator">HOME LOANS CALCULATOR</a>
<!--<button type="button" class="show-reg-form why" ><a href="#">USE OUR CALCULATOR</a></button>-->

<!-- <a href="#!">USE OUR CALCULATOR</a> -->
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->





<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process finance-solution">
         <div class="container">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center">
                            <h2>{{$p3->heading}}</h2>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>1</span>
                            <h6>{{$p3->s1}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>2</span>
                            <h6>{{$p3->s2}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>3</span>
                            <h6>{{$p3->s3}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>4</span>
                            <h6>{{$p3->s4}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>5</span>
                            <h6>{{$p3->s5}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>6</span>
                           <h6>{{$p3->s6}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>7</span>
                            <h6>{{$p3->s7}}</h6>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="count text-center">
                            <span>8</span>
                            <h6>{{$p3->s8}}</h6>
                        </div>
                    </div>
                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

<!-- START SECTION WHY WISEBERRY -->


     <section class="why-section buy process finance-solution client">
         <div class="container-fluid">
             <div class="row">
                 
                 <div class="col-lg-12 col-md-12 who-1">
                        <div class="text-center">
                            <h2>ACCESS TO 50+ LENDERS</h2>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="text-center">
                            
                            <ul class="client-list">
                                <li><img src="images/client1.png" alt=""></li>
                                <li><img src="images/client2.png" alt=""></li>
                                <li><img src="images/client3.png" alt=""></li>
                                <li><img src="images/client4.png" alt=""></li>
                                <li><img src="images/client5.png" alt=""></li>
                                <li><img src="images/client6.png" alt=""></li>
                                <li><img src="images/client7.png" alt=""></li>
                                <li><img src="images/client8.png" alt=""></li>
                                <li><img src="images/client9.png" alt=""></li>
                                <li><img src="images/client10.png" alt=""></li>
                            </ul>

                            <ul class="client-list">
                                <li><img src="images/client11.png" alt=""></li>
                                <li><img src="images/client12.png" alt=""></li>
                                <li><img src="images/client13.png" alt=""></li>
                                <li><img src="images/client14.png" alt=""></li>
                                <li><img src="images/client15.png" alt=""></li>
                                <li><img src="images/client16.png" alt=""></li>
                                <li><img src="images/client17.png" alt=""></li>
                                <li><img src="images/client18.png" alt=""></li>
                            </ul>

                            <ul class="client-list">
                                <li><img src="images/client19.png" alt=""></li>
                                <li><img src="images/client20.png" alt=""></li>
                                <li><img src="images/client21.png" alt=""></li>
                                <li><img src="images/client22.png" alt=""></li>
                                <li><img src="images/client23.png" alt=""></li>
                                <li><img src="images/client24.png" alt=""></li>
                                <li><img src="images/client25.png" alt=""></li>
                                <li><img src="images/client26.png" alt=""></li>
                                <li><img src="images/client27.png" alt=""></li>
                                <li><img src="images/client28.png" alt=""></li>
                            </ul>

                            <ul class="client-list">
                                <li><img src="images/client29.png" alt=""></li>
                                <li><img src="images/client30.png" alt=""></li>
                                <li><img src="images/client31.png" alt=""></li>
                                <li><img src="images/client31.png" alt=""></li>
                                <li><img src="images/client33.png" alt=""></li>
                                <li><img src="images/client34.png" alt=""></li>
                                <li><img src="images/client35.png" alt=""></li>
                            </ul>

                            <p class="text-center">*Wiseberry Financial Services is powered by My Local Broker Pty Ltd ATF My Local Broker Unit Trust ACN
605 003 174, Australian Credit License 481374.</p>

                        </div>
                    </div>

                   
             </div>
         </div>
     </section>


<!-- END SECTION WHY WISEBERRY -->

    
    <!-- START SECTION BLOG -->
        <section class="blog-section bg-white-2 home18" style="padding: 8rem 0 8rem;">
            <div class="container-fluid">
                <div class="sec-title">
                    <h2>TIPS AND ADVICE</h2>
                </div>
                <div class="news-wrap">
                    <div class="row">
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice1.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>INVESTORS</h6>
                                    <a href="#!"><h3>Current Areas That Are Booming</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>The kitchen is the heart of the home. When you decorate your kitchen, you want it to be something special, a place that will make you delighted to come home.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice2.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>BUYERS</h6>
                                    <a href="#!"><h3>What to Look Out For When Buying</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Bathroom renovation is a big task, and can be especially daunting when working with a space that’s awkwardly shaped, has quirky pipelines or is lacking in natural lighting.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 col-xs-12">
                            <div class="news-item text-center">
                                <a href="#!" class="news-img-link">
                                    <div class="news-item-img img-box hover-effect">
                                        <img class="img-responsive" src="images/tipadnadvice3.jpg" alt="blog image">
                                    </div>
                                </a>
                                <div class="news-item-text">
                                    <h6>FINANCE</h6>
                                    <a href="#!"><h3>Borrowing Money Tips</h3></a>
                                    <div class="news-item-descr big-news">
                                        <p>Data from realestate.com.au shows swimming pools as the number one feature Australian buyers are searching for when on the property hunt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="blogs" class="read-more">SEE MORE</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- END SECTION BLOG -->

       



 


</section>

<!-- The Modal -->
  <div class="modal fade home-rent register-intrest finance-solution" id="myModalone">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        
        
        <!-- Modal body -->
        <div class="modal-body  ui-front">
            
            <div class="one">
                <div class="modal-header">
          <h4 class="modal-title">WISEBERRY FINANCE SOLUTIONS</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
            <p>Simply fill out the form on this page and we will have someone give you a call!</p>

             <form action="sendmail_finance_solutions" id="capt_form_fin" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="fname" placeholder="First Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="lname" placeholder="Last Name*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="tel" class="form-control" required name="contact" placeholder="Contact Number*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" required name="email" placeholder="Email Address*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required name="pin" placeholder="Postcode*">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" required id="oldtags"  placeholder="Suburb/Locality">
                      <input type="hidden" class="form-control" id="oldtags_val" name="locality" placeholder="Desired Region(s) or Suburb(s)*">
                    </div>
                  </div>
            </div>    
</div>
                  

                  <div class="col-md-12 stripe-section">
                      
                      <h1 class="stripe">Which best describes your request?</h1>

                  </div>

                  <div class="two">
                      <div class="col-md-12">

                        <!-- partial:index.partial.html -->
                        <div class="form__item">
                            <?php 
                                foreach ($result['enquiry_finance_type'] as $key => $value) { 
                                    if($value['id'] == "1"){
                                        $id="custom-radio--male";
                                    }else if($value['id'] == "2"){
                                        $id="custom-radio--female";
                                    }else if($value['id'] == "3"){
                                        $id="custom-radio--nonDiscolse1";
                                    }else if($value['id'] == "4"){
                                        $id="custom-radio--nonDiscolse2";
                                    }else if($value['id'] == "5"){
                                        $id="custom-radio--nonDiscolse3";
                                    }

                            ?>
                            <div class="custom-radio">
                                <input class="custom-radio__control" id="{{$id}}" name="request_best" type="radio" value="{{$value['id']}}">
                                <label class="custom-radio__label" for="{{$id}}">{{$value['name']}}</label>
                            </div>
                            <?php } ?>
                        </div>
                        <!-- partial -->


                  </div>

                  <div class="col-md-12 recapthcha-section">
                    <div style="margin-left: auto;width: 70%;" class="g-recaptcha brochure__form__captcha" id="rcaptcha" data-sitekey="6LdaxhMcAAAAAAEBXfjYTtwmuspMCkprcFzORgSU"></div>
                  </div>

                  <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">LET’S GET STARTED</button>
                  </div>
                  </div>

                
             
              
            </form>

        </div>
        
        
        
      </div>
    </div>
  </div>
        </div>



      

@include('layout.footer')
<!-- END SECTION WHY WISEBERRY -->

       
       <!-- START SUBSCRIBE NOW -->

       
