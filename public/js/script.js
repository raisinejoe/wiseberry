
"use strict";

jQuery(document).on('ready', function ($) {

    /*---------------------------------
     //------ PRELOADER ------//
     ----------------------------------*/
    $('#status').fadeOut();
    $('#preloader').delay(200).fadeOut('slow');

    /*---------------------------------
     //------ ANIMATE HEADER ------//
     ----------------------------------*/
    $(window).on('scroll', function () {
        var sticky = $(".sticky-header");
        var scroll = $(window).scrollTop();
        if (scroll < 265) {
            sticky.removeClass("sticky");
        } else {
            sticky.addClass("sticky");
        }
    });

    /*---------------------------------
     //------ Rev Slider ------//
     ----------------------------------*/
    var tpj = jQuery;
    var revapi18;
    if (tpj("#rev_slider_18_1").revolution === undefined) {
        revslider_showDoubleJqueryError("#rev_slider_18_1");
    } else {
        revapi18 = tpj("#rev_slider_18_1").show().revolution({
            sliderType: "carousel",
            jsFileLocation: "revolution/js/",
            sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                mouseScrollReverse: "default",
                onHoverStop: "on",
                thumbnails: {
                    style: "gyges",
                    enable: true,
                    width: 50,
                    height: 50,
                    min_width: 50,
                    wrapper_padding: 5,
                    wrapper_color: "transparent",
                    tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                    visibleAmount: 5,
                    hide_onmobile: false,
                    hide_over: 1240,
                    hide_onleave: false,
                    direction: "horizontal",
                    span: false,
                    position: "inner",
                    space: 5,
                    h_align: "center",
                    v_align: "top",
                    h_offset: 0,
                    v_offset: 20
                },
                tabs: {
                    style: "gyges",
                    enable: true,
                    width: 220,
                    height: 80,
                    min_width: 220,
                    wrapper_padding: 0,
                    wrapper_color: "transparent",
                    tmp: '<div class="tp-tab-content">  <span class="tp-tab-date">{{param1}}</span>  <span class="tp-tab-title">{{title}}</span></div><div class="tp-tab-image"></div>',
                    visibleAmount: 6,
                    hide_onmobile: true,
                    hide_under: 1240,
                    hide_onleave: false,
                    hide_delay: 200,
                    direction: "vertical",
                    span: true,
                    position: "inner",
                    space: 0,
                    h_align: "left",
                    v_align: "center",
                    h_offset: 0,
                    v_offset: 0
                }
            },
            carousel: {
                horizontal_align: "center",
                vertical_align: "center",
                fadeout: "off",
                maxVisibleItems: 5,
                infinity: "on",
                space: 0,
                stretch: "off",
                showLayersAllTime: "off",
                easing: "Power3.easeInOut",
                speed: "800"
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [800, 700, 400, 300],
            gridheight: [600, 600, 500, 400],
            lazyType: "single",
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }

    /*----------------------------------
    //------ SMOOTHSCROLL ------//
    -----------------------------------*/
    smoothScroll.init({
        speed: 1000, // Integer. How fast to complete the scroll in milliseconds
        offset: 200, // Integer. How far to offset the scrolling anchor location in pixels

    });

    /*----------------------------------
    //------ LIGHTCASE ------//
    -----------------------------------*/
    $('a[data-rel^=lightcase]').lightcase();


    /*----------------------------------
    //------ ISOTOPE GALLERY ------//
    -----------------------------------*/
    /* activate jquery isotope */
    $(window).on('load', function () {
        var $container = $('.portfolio-items').isotope({
            itemSelector: '.item',
            masonry: {
                columnWidth: '.col-xs-12'
            }
        });
    });
    // init Isotope
    var $grid = $('.portfolio-items').isotope({
        // options...
    });
    // layout Isotope after each image loads
    $grid.imagesLoaded().progress(function () {
        $grid.isotope('layout');
    });
    // bind filter button click
    var filters = $('.filters-group ul li');
    filters.on('click', function () {
        filters.removeClass('active');
        $(this).addClass('active');
        var filterValue = $(this).attr('data-filter');
        // use filterFn if matches value
        $('.portfolio-items').isotope({
            filter: filterValue
        });
    });

    /*----------------------------------
    //------ OWL CAROUSEL ------//
    -----------------------------------*/
    $('.style1').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1,
                margin: 20
            },
            500: {
                items: 1,
                margin: 20
            },
            768: {
                items: 2,
                margin: 20
            },
            991: {
                items: 2,
                margin: 20
            },
            1025: {
                items: 3,
                margin: 20
            }
        }
    });

    $('.style2').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        autoWidth: false,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive: {
            0: {
                items: 2,
                margin: 20
            },
            400: {
                items: 2,
                margin: 20
            },
            500: {
                items: 3,
                margin: 20
            },
            768: {
                items: 4,
                margin: 20
            },
            992: {
                items: 5,
                margin: 20
            },
            1000: {
                items: 6,
                margin: 20
            }
        }
    });

    $('.style3').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1,
                margin: 20
            },
            500: {
                items: 1,
                margin: 20
            },
            768: {
                items: 2,
                margin: 20
            },
            991: {
                items: 2,
                margin: 20
            },
            1000: {
                items: 5,
                margin: 20
            }
        }
    });

    $('.carousel4').owlCarousel({
        autoPlay: false,
        navigation: true,
        slideSpeed: 600,
        items: 3,
        itemsDesktop: [1239, 3],
        itemsTablet: [991, 2],
        itemsMobile: [767, 1]
    });

    /*----------------------------------
    //------ TOP LOCATION ------//
    -----------------------------------*/
    if ($('#tp-carousel').length) {
        $('#tp-carousel').owlCarousel({
            loop: true,
            margin: 2,
            dots: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 2,
                    nav: true
                },
                1024: {
                    items: 3,
                    nav: true
                },
                1025: {
                    items: 5,
                    nav: true,
                    loop: false
                }
            }
        })
    }

    /*----------------------------------
    //------ JQUERY SCROOLTOP ------//
    -----------------------------------*/
    var go = $(".go-up");
    $(window).on('scroll', function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop >= 50) {
            go.fadeIn();
        } else {
            go.fadeOut();
        }
    });

    /*----------------------------------
    //----- JQUERY COUNTER UP -----//
    -----------------------------------*/
    $('.counter').counterUp({
        delay: 10,
        time: 5000,
        offset: 100,
        beginAt: 0,
        formatter: function (n) {
            return n.replace(/,/g, '.');
        }
    });

    /*----------------------------------
    //------ MAGNIFIC POPUP ------//
    -----------------------------------*/
    $(document).ready(function () {
        $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    });

    /*----------------------------------------------
    //------ FILTER TOGGLE (ON GOOGLE MAPS) ------//
    ----------------------------------------------*/
    $('.filter-toggle').on('click', function () {
        $(this).parent().find('form').stop(true, true).slideToggle();
    });

    /*----------------------------------
    //------ RANGE SLIDER ------//
    -----------------------------------*/
    $(".slider-range").slider({
        range: true,
        min: 5000,
        max: 200000,
        step: 1000,
        values: [60000, 130000],
        slide: function (event, ui) {
            $(".slider_amount").val("$" + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " - $" + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        }
    });
    $(".slider_amount").val("Price Range: $" + $(".slider-range").slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + " - $" + $(".slider-range").slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    
    /*----------------------------------
    //------ MODAL ------//
    -----------------------------------*/
    var modal = {};
    modal.hide = function () {
        $('.modal').fadeOut();
        $("html, body").removeClass("hid-body");
    };
    $('.modal-open').on("click", function (e) {
        e.preventDefault();
        $('.modal').fadeIn();
        $("html, body").addClass("hid-body");
    });
    $('.close-reg').on("click", function () {
        modal.hide();
    });
    
    /*----------------------------------
    //------ TABS ------//
    -----------------------------------*/
    $(".tabs-menu a").on("click", function (a) {
        a.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var b = $(this).attr("href");
        $(".tab-contents").not(b).css("display", "none");
        $(b).fadeIn();
    });

}(jQuery));


    /*----------------------------------
    //------ INDIVIDUAL RENTAL ------//
    -----------------------------------*/

    $("[id^=carousel-thumbs]").carousel({
    interval: false
});

/** Pause/Play Button **/
$(".carousel-pause").click(function () {
    var id = $(this).attr("href");
    if ($(this).hasClass("pause")) {
        $(this).removeClass("pause").toggleClass("play");
        $(this).children(".sr-only").text("Play");
        $(id).carousel("pause");
    } else {
        $(this).removeClass("play").toggleClass("pause");
        $(this).children(".sr-only").text("Pause");
        $(id).carousel("cycle");
    }
    $(id).carousel;
});

/** Fullscreen Buttun **/
$(".carousel-fullscreen").click(function () {
    var id = $(this).attr("href");
    $(id).find(".active").ekkoLightbox({
        type: "image"
    });
});

if ($("[id^=carousel-thumbs] .carousel-item").length < 2) {
    $("#carousel-thumbs [class^=carousel-control-]").remove();
    $("#carousel-thumbs").css("padding", "0 5px");
}

$("#carousel").on("slide.bs.carousel", function (e) {
    var id = parseInt($(e.relatedTarget).attr("data-slide-number"));
    var thumbNum = parseInt(
        $("[id=carousel-selector-" + id + "]")
            .parent()
            .parent()
            .attr("data-slide-number")
    );
    $("[id^=carousel-selector-]").removeClass("selected");
    $("[id=carousel-selector-" + id + "]").addClass("selected");
    $("#carousel-thumbs").carousel(thumbNum);
});



















/*!
 * Lightbox for Bootstrap by @ashleydw
 * https://github.com/ashleydw/lightbox
 *
 * License: https://github.com/ashleydw/lightbox/blob/master/LICENSE
 */
+function ($) {

'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var Lightbox = (function ($) {

    var NAME = 'ekkoLightbox';
    var JQUERY_NO_CONFLICT = $.fn[NAME];

    var Default = {
        title: '',
        footer: '',
        maxWidth: 9999,
        maxHeight: 9999,
        showArrows: true, //display the left / right arrows or not
        wrapping: true, //if true, gallery loops infinitely
        type: null, //force the lightbox into image / youtube mode. if null, or not image|youtube|vimeo; detect it
        alwaysShowClose: false, //always show the close button, even if there is no title
        loadingMessage: '<div class="ekko-lightbox-loader"><div><div></div><div></div></div></div>', // http://tobiasahlin.com/spinkit/
        leftArrow: '<span>&#10094;</span>',
        rightArrow: '<span>&#10095;</span>',
        strings: {
            close: 'Close',
            fail: 'Failed to load image:',
            type: 'Could not detect remote target type. Force the type using data-type'
        },
        doc: document, // if in an iframe can specify top.document
        onShow: function onShow() {},
        onShown: function onShown() {},
        onHide: function onHide() {},
        onHidden: function onHidden() {},
        onNavigate: function onNavigate() {},
        onContentLoaded: function onContentLoaded() {}
    };

    var Lightbox = (function () {
        _createClass(Lightbox, null, [{
            key: 'Default',

            /**
       Class properties:
     _$element: null -> the <a> element currently being displayed
    _$modal: The bootstrap modal generated
       _$modalDialog: The .modal-dialog
       _$modalContent: The .modal-content
       _$modalBody: The .modal-body
       _$modalHeader: The .modal-header
       _$modalFooter: The .modal-footer
    _$lightboxContainerOne: Container of the first lightbox element
    _$lightboxContainerTwo: Container of the second lightbox element
    _$lightboxBody: First element in the container
    _$modalArrows: The overlayed arrows container
     _$galleryItems: Other <a>'s available for this gallery
    _galleryName: Name of the current data('gallery') showing
    _galleryIndex: The current index of the _$galleryItems being shown
     _config: {} the options for the modal
    _modalId: unique id for the current lightbox
    _padding / _border: CSS properties for the modal container; these are used to calculate the available space for the content
     */

            get: function get() {
                return Default;
            }
        }]);

        function Lightbox($element, config) {
            var _this = this;

            _classCallCheck(this, Lightbox);

            this._config = $.extend({}, Default, config);
            this._$modalArrows = null;
            this._galleryIndex = 0;
            this._galleryName = null;
            this._padding = null;
            this._border = null;
            this._titleIsShown = false;
            this._footerIsShown = false;
            this._wantedWidth = 0;
            this._wantedHeight = 0;
            this._touchstartX = 0;
            this._touchendX = 0;

            this._modalId = 'ekkoLightbox-' + Math.floor(Math.random() * 1000 + 1);
            this._$element = $element instanceof jQuery ? $element : $($element);

            this._isBootstrap3 = $.fn.modal.Constructor.VERSION[0] == 3;

            var h4 = '<h4 class="modal-title">' + (this._config.title || "&nbsp;") + '</h4>';
            var btn = '<button type="button" class="close" data-dismiss="modal" aria-label="' + this._config.strings.close + '"><span aria-hidden="true">&times;</span></button>';

            var header = '<div class="modal-header' + (this._config.title || this._config.alwaysShowClose ? '' : ' hide') + '">' + '<i class="fa fa-print" aria-hidden="true"></i>' + '<i class="fa fa-share" aria-hidden="true"></i>' + (this._isBootstrap3 ? btn + h4 : h4 + btn) + '</div>';
            var footer = '<div class="modal-footer' + (this._config.footer ? '' : ' hide') + '">' + (this._config.footer || "&nbsp;") + '</div>';
            var body = '<div class="modal-body"><div class="ekko-lightbox-container"><div class="ekko-lightbox-item fade in show"></div><div class="ekko-lightbox-item fade"></div></div></div>';
            var dialog = '<div class="modal-dialog" role="document"><div class="modal-content">' + header + body + footer + '</div></div>';
            $(this._config.doc.body).append('<div id="' + this._modalId + '" class="ekko-lightbox modal fade" tabindex="-1" tabindex="-1" role="dialog" aria-hidden="true">' + dialog + '</div>');

            this._$modal = $('#' + this._modalId, this._config.doc);
            this._$modalDialog = this._$modal.find('.modal-dialog').first();
            this._$modalContent = this._$modal.find('.modal-content').first();
            this._$modalBody = this._$modal.find('.modal-body').first();
            this._$modalHeader = this._$modal.find('.modal-header').first();
            this._$modalFooter = this._$modal.find('.modal-footer').first();

            this._$lightboxContainer = this._$modalBody.find('.ekko-lightbox-container').first();
            this._$lightboxBodyOne = this._$lightboxContainer.find('> div:first-child').first();
            this._$lightboxBodyTwo = this._$lightboxContainer.find('> div:last-child').first();

            this._border = this._calculateBorders();
            this._padding = this._calculatePadding();

            this._galleryName = this._$element.data('gallery');
            if (this._galleryName) {
                this._$galleryItems = $(document.body).find('*[data-gallery="' + this._galleryName + '"]');
                this._galleryIndex = this._$galleryItems.index(this._$element);
                $(document).on('keydown.ekkoLightbox', this._navigationalBinder.bind(this));

                // add the directional arrows to the modal
                if (this._config.showArrows && this._$galleryItems.length > 1) {
                    this._$lightboxContainer.append('<div class="ekko-lightbox-nav-overlay"><a href="#">' + this._config.leftArrow + '</a><a href="#">' + this._config.rightArrow + '</a></div>');
                    this._$modalArrows = this._$lightboxContainer.find('div.ekko-lightbox-nav-overlay').first();
                    this._$lightboxContainer.on('click', 'a:first-child', function (event) {
                        event.preventDefault();
                        return _this.navigateLeft();
                    });
                    this._$lightboxContainer.on('click', 'a:last-child', function (event) {
                        event.preventDefault();
                        return _this.navigateRight();
                    });
                    this.updateNavigation();
                }
            }

            this._$modal.on('show.bs.modal', this._config.onShow.bind(this)).on('shown.bs.modal', function () {
                _this._toggleLoading(true);
                _this._handle();
                return _this._config.onShown.call(_this);
            }).on('hide.bs.modal', this._config.onHide.bind(this)).on('hidden.bs.modal', function () {
                if (_this._galleryName) {
                    $(document).off('keydown.ekkoLightbox');
                    $(window).off('resize.ekkoLightbox');
                }
                _this._$modal.remove();
                return _this._config.onHidden.call(_this);
            }).modal(this._config);

            $(window).on('resize.ekkoLightbox', function () {
                _this._resize(_this._wantedWidth, _this._wantedHeight);
            });
            this._$lightboxContainer.on('touchstart', function () {
                _this._touchstartX = event.changedTouches[0].screenX;
            }).on('touchend', function () {
                _this._touchendX = event.changedTouches[0].screenX;
                _this._swipeGesure();
            });
        }

        _createClass(Lightbox, [{
            key: 'element',
            value: function element() {
                return this._$element;
            }
        }, {
            key: 'modal',
            value: function modal() {
                return this._$modal;
            }
        }, {
            key: 'navigateTo',
            value: function navigateTo(index) {

                if (index < 0 || index > this._$galleryItems.length - 1) return this;

                this._galleryIndex = index;

                this.updateNavigation();

                this._$element = $(this._$galleryItems.get(this._galleryIndex));
                this._handle();
            }
        }, {
            key: 'navigateLeft',
            value: function navigateLeft() {

                if (!this._$galleryItems) return;

                if (this._$galleryItems.length === 1) return;

                if (this._galleryIndex === 0) {
                    if (this._config.wrapping) this._galleryIndex = this._$galleryItems.length - 1;else return;
                } else //circular
                    this._galleryIndex--;

                this._config.onNavigate.call(this, 'left', this._galleryIndex);
                return this.navigateTo(this._galleryIndex);
            }
        }, {
            key: 'navigateRight',
            value: function navigateRight() {

                if (!this._$galleryItems) return;

                if (this._$galleryItems.length === 1) return;

                if (this._galleryIndex === this._$galleryItems.length - 1) {
                    if (this._config.wrapping) this._galleryIndex = 0;else return;
                } else //circular
                    this._galleryIndex++;

                this._config.onNavigate.call(this, 'right', this._galleryIndex);
                return this.navigateTo(this._galleryIndex);
            }
        }, {
            key: 'updateNavigation',
            value: function updateNavigation() {
                if (!this._config.wrapping) {
                    var $nav = this._$lightboxContainer.find('div.ekko-lightbox-nav-overlay');
                    if (this._galleryIndex === 0) $nav.find('a:first-child').addClass('disabled');else $nav.find('a:first-child').removeClass('disabled');

                    if (this._galleryIndex === this._$galleryItems.length - 1) $nav.find('a:last-child').addClass('disabled');else $nav.find('a:last-child').removeClass('disabled');
                }
            }
        }, {
            key: 'close',
            value: function close() {
                return this._$modal.modal('hide');
            }

            // helper private methods
        }, {
            key: '_navigationalBinder',
            value: function _navigationalBinder(event) {
                event = event || window.event;
                if (event.keyCode === 39) return this.navigateRight();
                if (event.keyCode === 37) return this.navigateLeft();
            }

            // type detection private methods
        }, {
            key: '_detectRemoteType',
            value: function _detectRemoteType(src, type) {

                type = type || false;

                if (!type && this._isImage(src)) type = 'image';
                if (!type && this._getYoutubeId(src)) type = 'youtube';
                if (!type && this._getVimeoId(src)) type = 'vimeo';
                if (!type && this._getInstagramId(src)) type = 'instagram';

                if (!type || ['image', 'youtube', 'vimeo', 'instagram', 'video', 'url'].indexOf(type) < 0) type = 'url';

                return type;
            }
        }, {
            key: '_isImage',
            value: function _isImage(string) {
                return string && string.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
            }
        }, {
            key: '_containerToUse',
            value: function _containerToUse() {
                var _this2 = this;

                // if currently showing an image, fade it out and remove
                var $toUse = this._$lightboxBodyTwo;
                var $current = this._$lightboxBodyOne;

                if (this._$lightboxBodyTwo.hasClass('in')) {
                    $toUse = this._$lightboxBodyOne;
                    $current = this._$lightboxBodyTwo;
                }

                $current.removeClass('in show');
                setTimeout(function () {
                    if (!_this2._$lightboxBodyTwo.hasClass('in')) _this2._$lightboxBodyTwo.empty();
                    if (!_this2._$lightboxBodyOne.hasClass('in')) _this2._$lightboxBodyOne.empty();
                }, 500);

                $toUse.addClass('in show');
                return $toUse;
            }
        }, {
            key: '_handle',
            value: function _handle() {

                var $toUse = this._containerToUse();
                this._updateTitleAndFooter();

                var currentRemote = this._$element.attr('data-remote') || this._$element.attr('href');
                var currentType = this._detectRemoteType(currentRemote, this._$element.attr('data-type') || false);

                if (['image', 'youtube', 'vimeo', 'instagram', 'video', 'url'].indexOf(currentType) < 0) return this._error(this._config.strings.type);

                switch (currentType) {
                    case 'image':
                        this._preloadImage(currentRemote, $toUse);
                        this._preloadImageByIndex(this._galleryIndex, 3);
                        break;
                    case 'youtube':
                        this._showYoutubeVideo(currentRemote, $toUse);
                        break;
                    case 'vimeo':
                        this._showVimeoVideo(this._getVimeoId(currentRemote), $toUse);
                        break;
                    case 'instagram':
                        this._showInstagramVideo(this._getInstagramId(currentRemote), $toUse);
                        break;
                    case 'video':
                        this._showHtml5Video(currentRemote, $toUse);
                        break;
                    default:
                        // url
                        this._loadRemoteContent(currentRemote, $toUse);
                        break;
                }

                return this;
            }
        }, {
            key: '_getYoutubeId',
            value: function _getYoutubeId(string) {
                if (!string) return false;
                var matches = string.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
                return matches && matches[2].length === 11 ? matches[2] : false;
            }
        }, {
            key: '_getVimeoId',
            value: function _getVimeoId(string) {
                return string && string.indexOf('vimeo') > 0 ? string : false;
            }
        }, {
            key: '_getInstagramId',
            value: function _getInstagramId(string) {
                return string && string.indexOf('instagram') > 0 ? string : false;
            }

            // layout private methods
        }, {
            key: '_toggleLoading',
            value: function _toggleLoading(show) {
                show = show || false;
                if (show) {
                    this._$modalDialog.css('display', 'none');
                    this._$modal.removeClass('in show');
                    $('.modal-backdrop').append(this._config.loadingMessage);
                } else {
                    this._$modalDialog.css('display', 'block');
                    this._$modal.addClass('in show');
                    $('.modal-backdrop').find('.ekko-lightbox-loader').remove();
                }
                return this;
            }
        }, {
            key: '_calculateBorders',
            value: function _calculateBorders() {
                return {
                    top: this._totalCssByAttribute('border-top-width'),
                    right: this._totalCssByAttribute('border-right-width'),
                    bottom: this._totalCssByAttribute('border-bottom-width'),
                    left: this._totalCssByAttribute('border-left-width')
                };
            }
        }, {
            key: '_calculatePadding',
            value: function _calculatePadding() {
                return {
                    top: this._totalCssByAttribute('padding-top'),
                    right: this._totalCssByAttribute('padding-right'),
                    bottom: this._totalCssByAttribute('padding-bottom'),
                    left: this._totalCssByAttribute('padding-left')
                };
            }
        }, {
            key: '_totalCssByAttribute',
            value: function _totalCssByAttribute(attribute) {
                return parseInt(this._$modalDialog.css(attribute), 10) + parseInt(this._$modalContent.css(attribute), 10) + parseInt(this._$modalBody.css(attribute), 10);
            }
        }, {
            key: '_updateTitleAndFooter',
            value: function _updateTitleAndFooter() {
                var title = this._$element.data('title') || "";
                var caption = this._$element.data('footer') || "";

                this._titleIsShown = false;
                if (title || this._config.alwaysShowClose) {
                    this._titleIsShown = true;
                    this._$modalHeader.css('display', '').find('.modal-title').html(title || "&nbsp;");
                } else this._$modalHeader.css('display', 'none');

                this._footerIsShown = false;
                if (caption) {
                    this._footerIsShown = true;
                    this._$modalFooter.css('display', '').html(caption);
                } else this._$modalFooter.css('display', 'none');

                return this;
            }
        }, {
            key: '_showYoutubeVideo',
            value: function _showYoutubeVideo(remote, $containerForElement) {
                var id = this._getYoutubeId(remote);
                var query = remote.indexOf('&') > 0 ? remote.substr(remote.indexOf('&')) : '';
                var width = this._$element.data('width') || 560;
                var height = this._$element.data('height') || width / (560 / 315);
                return this._showVideoIframe('//www.youtube.com/embed/' + id + '?badge=0&autoplay=1&html5=1' + query, width, height, $containerForElement);
            }
        }, {
            key: '_showVimeoVideo',
            value: function _showVimeoVideo(id, $containerForElement) {
                var width = this._$element.data('width') || 500;
                var height = this._$element.data('height') || width / (560 / 315);
                return this._showVideoIframe(id + '?autoplay=1', width, height, $containerForElement);
            }
        }, {
            key: '_showInstagramVideo',
            value: function _showInstagramVideo(id, $containerForElement) {
                // instagram load their content into iframe's so this can be put straight into the element
                var width = this._$element.data('width') || 612;
                var height = width + 80;
                id = id.substr(-1) !== '/' ? id + '/' : id; // ensure id has trailing slash
                $containerForElement.html('<iframe width="' + width + '" height="' + height + '" src="' + id + 'embed/" frameborder="0" allowfullscreen></iframe>');
                this._resize(width, height);
                this._config.onContentLoaded.call(this);
                if (this._$modalArrows) //hide the arrows when showing video
                    this._$modalArrows.css('display', 'none');
                this._toggleLoading(false);
                return this;
            }
        }, {
            key: '_showVideoIframe',
            value: function _showVideoIframe(url, width, height, $containerForElement) {
                // should be used for videos only. for remote content use loadRemoteContent (data-type=url)
                height = height || width; // default to square
                $containerForElement.html('<div class="embed-responsive embed-responsive-16by9"><iframe width="' + width + '" height="' + height + '" src="' + url + '" frameborder="0" allowfullscreen class="embed-responsive-item"></iframe></div>');
                this._resize(width, height);
                this._config.onContentLoaded.call(this);
                if (this._$modalArrows) this._$modalArrows.css('display', 'none'); //hide the arrows when showing video
                this._toggleLoading(false);
                return this;
            }
        }, {
            key: '_showHtml5Video',
            value: function _showHtml5Video(url, $containerForElement) {
                // should be used for videos only. for remote content use loadRemoteContent (data-type=url)
                var width = this._$element.data('width') || 560;
                var height = this._$element.data('height') || width / (560 / 315);
                $containerForElement.html('<div class="embed-responsive embed-responsive-16by9"><video width="' + width + '" height="' + height + '" src="' + url + '" preload="auto" autoplay controls class="embed-responsive-item"></video></div>');
                this._resize(width, height);
                this._config.onContentLoaded.call(this);
                if (this._$modalArrows) this._$modalArrows.css('display', 'none'); //hide the arrows when showing video
                this._toggleLoading(false);
                return this;
            }
        }, {
            key: '_loadRemoteContent',
            value: function _loadRemoteContent(url, $containerForElement) {
                var _this3 = this;

                var width = this._$element.data('width') || 560;
                var height = this._$element.data('height') || 560;

                var disableExternalCheck = this._$element.data('disableExternalCheck') || false;
                this._toggleLoading(false);

                // external urls are loading into an iframe
                // local ajax can be loaded into the container itself
                if (!disableExternalCheck && !this._isExternal(url)) {
                    $containerForElement.load(url, $.proxy(function () {
                        return _this3._$element.trigger('loaded.bs.modal');l;
                    }));
                } else {
                    $containerForElement.html('<iframe src="' + url + '" frameborder="0" allowfullscreen></iframe>');
                    this._config.onContentLoaded.call(this);
                }

                if (this._$modalArrows) //hide the arrows when remote content
                    this._$modalArrows.css('display', 'none');

                this._resize(width, height);
                return this;
            }
        }, {
            key: '_isExternal',
            value: function _isExternal(url) {
                var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
                if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) return true;

                if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(':(' + ({
                    "http:": 80,
                    "https:": 443
                })[location.protocol] + ')?$'), "") !== location.host) return true;

                return false;
            }
        }, {
            key: '_error',
            value: function _error(message) {
                console.error(message);
                this._containerToUse().html(message);
                this._resize(300, 300);
                return this;
            }
        }, {
            key: '_preloadImageByIndex',
            value: function _preloadImageByIndex(startIndex, numberOfTimes) {

                if (!this._$galleryItems) return;

                var next = $(this._$galleryItems.get(startIndex), false);
                if (typeof next == 'undefined') return;

                var src = next.attr('data-remote') || next.attr('href');
                if (next.attr('data-type') === 'image' || this._isImage(src)) this._preloadImage(src, false);

                if (numberOfTimes > 0) return this._preloadImageByIndex(startIndex + 1, numberOfTimes - 1);
            }
        }, {
            key: '_preloadImage',
            value: function _preloadImage(src, $containerForImage) {
                var _this4 = this;

                $containerForImage = $containerForImage || false;

                var img = new Image();
                if ($containerForImage) {
                    (function () {

                        // if loading takes > 200ms show a loader
                        var loadingTimeout = setTimeout(function () {
                            $containerForImage.append(_this4._config.loadingMessage);
                        }, 200);

                        img.onload = function () {
                            if (loadingTimeout) clearTimeout(loadingTimeout);
                            loadingTimeout = null;
                            var image = $('<img />');
                            image.attr('src', img.src);
                            image.addClass('img-fluid');

                            // backward compatibility for bootstrap v3
                            image.css('width', '100%');

                            $containerForImage.html(image);
                            if (_this4._$modalArrows) _this4._$modalArrows.css('display', ''); // remove display to default to css property

                            _this4._resize(img.width, img.height);
                            _this4._toggleLoading(false);
                            return _this4._config.onContentLoaded.call(_this4);
                        };
                        img.onerror = function () {
                            _this4._toggleLoading(false);
                            return _this4._error(_this4._config.strings.fail + ('  ' + src));
                        };
                    })();
                }

                img.src = src;
                return img;
            }
        }, {
            key: '_swipeGesure',
            value: function _swipeGesure() {
                if (this._touchendX < this._touchstartX) {
                    return this.navigateRight();
                }
                if (this._touchendX > this._touchstartX) {
                    return this.navigateLeft();
                }
            }
        }, {
            key: '_resize',
            value: function _resize(width, height) {

                height = height || width;
                this._wantedWidth = width;
                this._wantedHeight = height;

                var imageAspecRatio = width / height;

                // if width > the available space, scale down the expected width and height
                var widthBorderAndPadding = this._padding.left + this._padding.right + this._border.left + this._border.right;

                // force 10px margin if window size > 575px
                var addMargin = this._config.doc.body.clientWidth > 575 ? 20 : 0;
                var discountMargin = this._config.doc.body.clientWidth > 575 ? 0 : 20;

                var maxWidth = Math.min(width + widthBorderAndPadding, this._config.doc.body.clientWidth - addMargin, this._config.maxWidth);

                if (width + widthBorderAndPadding > maxWidth) {
                    height = (maxWidth - widthBorderAndPadding - discountMargin) / imageAspecRatio;
                    width = maxWidth;
                } else width = width + widthBorderAndPadding;

                var headerHeight = 0,
                    footerHeight = 0;

                // as the resize is performed the modal is show, the calculate might fail
                // if so, default to the default sizes
                if (this._footerIsShown) footerHeight = this._$modalFooter.outerHeight(true) || 55;

                if (this._titleIsShown) headerHeight = this._$modalHeader.outerHeight(true) || 67;

                var borderPadding = this._padding.top + this._padding.bottom + this._border.bottom + this._border.top;

                //calculated each time as resizing the window can cause them to change due to Bootstraps fluid margins
                var margins = parseFloat(this._$modalDialog.css('margin-top')) + parseFloat(this._$modalDialog.css('margin-bottom'));

                var maxHeight = Math.min(height, $(window).height() - borderPadding - margins - headerHeight - footerHeight, this._config.maxHeight - borderPadding - headerHeight - footerHeight);

                if (height > maxHeight) {
                    // if height > the available height, scale down the width
                    width = Math.ceil(maxHeight * imageAspecRatio) + widthBorderAndPadding;
                }

                this._$lightboxContainer.css('height', maxHeight);
                this._$modalDialog.css('flex', 1).css('maxWidth', width);

                var modal = this._$modal.data('bs.modal');
                if (modal) {
                    // v4 method is mistakenly protected
                    try {
                        modal._handleUpdate();
                    } catch (Exception) {
                        modal.handleUpdate();
                    }
                }
                return this;
            }
        }], [{
            key: '_jQueryInterface',
            value: function _jQueryInterface(config) {
                var _this5 = this;

                config = config || {};
                return this.each(function () {
                    var $this = $(_this5);
                    var _config = $.extend({}, Lightbox.Default, $this.data(), typeof config === 'object' && config);

                    new Lightbox(_this5, _config);
                });
            }
        }]);

        return Lightbox;
    })();

    $.fn[NAME] = Lightbox._jQueryInterface;
    $.fn[NAME].Constructor = Lightbox;
    $.fn[NAME].noConflict = function () {
        $.fn[NAME] = JQUERY_NO_CONFLICT;
        return Lightbox._jQueryInterface;
    };

    return Lightbox;
})(jQuery);
//# sourceMappingURL=ekko-lightbox.js.map

}(jQuery);







/*
This is a super simple slider using Ken Wheeler's "Slick Slider." 
It's responsive, swipable and light weight.

Visit Ken's site for the source code and docs. 

Source: https://kenwheeler.github.io/slick/

*/


/*
The following executes Ken's Slick Slider with several options.
*/

$('.quotes').slick({
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 6000,
  speed: 800,
  slidesToShow: 1,
  adaptiveHeight: true
});


/*
To prevent the flashing of unstyled content (FOUC) I created a class ".no-fouc"
which hides the slider.  When everything is ready to roll, I simply remove the
.no-fouc class from the slider section using the script below.  I placed the CSS snippet in the head of the HTML
page so that it's loaded before other things for obvious reasons.  What about folks with JS turned off?  
Well, I don't worry about them too much anymore.  Oh my.  I feel the heat from the flames already.  :) 
*/

$( document ).ready(function() {
$('.no-fouc').removeClass('no-fouc');
});




  
$('#collapseDiv').on('shown.bs.collapse', function () {
       $(".fa1").removeClass("fa fa-angle-down").addClass("fa fa-angle-up");
    });

    $('#collapseDiv').on('hidden.bs.collapse', function () {
       $(".fa1").removeClass("fa fa-angle-up").addClass("fa fa-angle-down");
    });
    
    
   
